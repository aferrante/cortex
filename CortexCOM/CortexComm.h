// CortexComm.h : Declaration of the CCortexComm

#ifndef __CORTEXCOMM_H_
#define __CORTEXCOMM_H_

#include "resource.h"       // main symbols
#include <asptlb.h>         // Active Server Pages Definitions

/////////////////////////////////////////////////////////////////////////////
// CCortexComm
class ATL_NO_VTABLE CCortexComm : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CCortexComm, &CLSID_CortexComm>,
	public ISupportErrorInfo,
	public IDispatchImpl<ICortexComm, &IID_ICortexComm, &LIBID_CORTEXCOMLib>
{
public:
	CCortexComm()
	{ 
		m_bOnStartPageCalled = FALSE;
	}

public:

DECLARE_REGISTRY_RESOURCEID(IDR_CORTEXCOMM)
DECLARE_NOT_AGGREGATABLE(CCortexComm)

BEGIN_COM_MAP(CCortexComm)
	COM_INTERFACE_ENTRY(ICortexComm)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ICortexComm
public:
	STDMETHOD(Exchange)(/*[in]*/ BSTR bstrInParams, /*[out,retval]*/ BSTR * bOutParams);
	//Active Server Pages Methods
	STDMETHOD(OnStartPage)(IUnknown* IUnk);
	STDMETHOD(OnEndPage)();

private:
	CComPtr<IRequest> m_piRequest;					//Request Object
	CComPtr<IResponse> m_piResponse;				//Response Object
	CComPtr<ISessionObject> m_piSession;			//Session Object
	CComPtr<IServer> m_piServer;					//Server Object
	CComPtr<IApplicationObject> m_piApplication;	//Application Object
	BOOL m_bOnStartPageCalled;						//OnStartPage successful?
};

#endif //__CORTEXCOMM_H_
