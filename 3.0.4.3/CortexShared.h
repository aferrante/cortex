// CortexShared.h: interface for the CCortexMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_)
#define AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_

#include "CortexDefines.h"


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCortexMessage  
{
public:
	CCortexMessage();
	virtual ~CCortexMessage();

	char* m_pchResponse[CX_XML_BUFFER_COUNT];  // various XML element buffers
	int m_nTxStep;  // Transmission step (so we can detect where in the process a failure might occur)
	int m_nFailure; // failure code
	int m_nType;  // command type
	char* m_pchType;

	int FormatContent(int nBufferIndex, int nBufferMax, char* pchContent, ...);
	int SetContent(int nBufferIndex, char* pchContent);
	int ClearContent(int nBufferIndex);
	char* XMLTextNodeValue(IXMLDOMNodePtr pNode);
	int SetType(char* pchContent);
};


class CCortexUtil  
{
public:
	CCortexUtil();
	virtual ~CCortexUtil();

	char* ConvertWideToT(void* pbstrIn);
	int ConvertWideToT(char* pchBuffer, unsigned long ulMaxBufLen, void* pbstrIn);
};

#endif // !defined(AFX_CORTEXSHARED_H__BF3CE24A_470B_4F0F_8D0E_A7B654EECA03__INCLUDED_)
