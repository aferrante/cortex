// CortexCOM.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//		To build a separate proxy/stub DLL, 
//		run nmake -f CortexCOMps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include "initguid.h"
#include "CortexCOM.h"

#include "CortexCOM_i.c"
#include <initguid.h>
#include "CortexComm.h"

extern int g_nCortexPort;  // listen port for cortex setup.
extern CString g_szCortexHost;

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
	OBJECT_ENTRY(CLSID_CortexComm, CCortexComm)
END_OBJECT_MAP()

class CCortexCOMApp : public CWinApp
{
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
};

CCortexCOMApp theApp;

BOOL CCortexCOMApp::InitInstance()
{
	_Module.Init(ObjectMap, m_hInstance);
  SetRegistryKey(_T("Video Design Software"));

	// move to setcontext, we don't need a listener yet.
//	g_nCortexPort = GetProfileInt( _T("Comm"), _T("port"), 9973 );
//	g_szCortexHost = GetProfileString( _T("Comm"), _T("listen"), _T("localhost") );

//	WriteProfileInt( _T("Comm"), _T("port"), g_nCortexPort );
//	WriteProfileString( _T("Comm"), _T("listen"), g_szCortexHost );
		CString szTemp;
		_timeb m_timebTick; // the last time check inside the thread
		_ftime( &m_timebTick );

		szTemp.Format(_T("%d"), m_timebTick.time);
		WriteProfileString( _T("Debug"), _T("InitInstance"), szTemp );

	return CWinApp::InitInstance();
}

int CCortexCOMApp::ExitInstance() // we are exiting, can just write stuff out at the end.
{
//		WriteProfileInt( _T("Modules"), _T("NumModules"), m_nNumModules );  // type this one in manually
		WriteProfileString( _T("Modules"), _T("Module_999_Name"), _T("SampleModule") );
		WriteProfileString( _T("Modules"), _T("Module_999_Host"), _T("SampleHost") );
		CString szTemp;
		_timeb m_timebTick; // the last time check inside the thread
		_ftime( &m_timebTick );

		szTemp.Format(_T("%d"), m_timebTick.time);
		WriteProfileString( _T("Debug"), _T("ExitInstance"), szTemp );
		WriteProfileInt( _T("Modules"), _T("Module_999_Port"), 99999 );
	if((g_nCortexPort>0)&&(g_szCortexHost.GetLength()>0))
	{
		WriteProfileInt( _T("Comm"), _T("port"), g_nCortexPort );
		WriteProfileString( _T("Comm"), _T("listen"), g_szCortexHost );
	}
	_Module.Term();
	return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return (AfxDllCanUnloadNow()==S_OK && _Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	// registers object, typelib and all interfaces in typelib
	return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	_Module.UnregisterServer();
	return S_OK;
}


