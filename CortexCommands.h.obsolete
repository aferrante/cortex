#ifndef CORTEXCOMMANDS_INCLUDED
#define CORTEXCOMMANDS_INCLUDED

// command width is 0x3f. 
// top two bits are used as flags, do not define any commands using the 0xc0 space
// if more than 64 commands are necessary, set the CORTEX_CMD_HASSUBC flag and use 
// a subcommand byte (full width 0xff - allows 16384 unique commands)

#define CORTEX_CMD_HASDATA 0x80 // command has data to follow
#define CORTEX_CMD_HASSUBC 0x40 // command has a subcommand

#define CORTEX_CMD_MIN 0x00
#define CORTEX_CMD_MAX 0x3f

// commands used by cortex to command cortex listeners
#define CORTEX_CMD_SPARK			0x31  // run the spark list. main cortex hostname:commandport in data.
#define CORTEX_CMD_KILL				0x32  // send kill commands to the exes on the spark list.
#define CORTEX_CMD_KILLSELF		0x33  // end process, possibly for upgrade. (upgrading process should shell execute after upgrade)

// commands used by cortex to command resources and processes
#define CORTEX_CMD_CHGCOMM		0x01  //must use a subcmd.  changes the way other modules communicate with cortex
#define CORTEX_SCMD_HOST			0x01    //change the host (can be used for switching to backup)
#define CORTEX_SCMD_PORT			0x02    //change the ports // has data "commandport:statusport"

#define CORTEX_CMD_GETNAME		0x02  // get the familiar name for a process or resource. (no sub, or sub = 0x00)
#define CORTEX_SCMD_DEST				0x01  // get the familiar name for all destinations in a resource.

#define CORTEX_CMD_CHGNAME		0x03  // change the familiar name for a process or resource (if we have one thats identical, must append a suffix to make unique)  new name given in data
#define CORTEX_SCMD_DEST				0x01  // change the familiar name for a destination in a resource.  Ordinal and newname in data.

#define CORTEX_CMD_GETTYPE		0x04  // get the type of a resource 
#define CORTEX_SCMD_DEST				0x01  // get the subtype of a resource destination (driver set).

#define CORTEX_CMD_GETFILES		0x05  //get files associated with module. //must use subcmd
#define CORTEX_SCMD_ADMINLIST   0x01 // retrieves a list of files necessary for admin
#define CORTEX_SCMD_AUTHLIST    0x02 // retrieves a list of files necessary for auth
#define CORTEX_SCMD_STATUSLIST  0x03 // retrieves a list of files necessary for status
#define CORTEX_SCMD_HELPLIST    0x04 // retrieves a list of files necessary for help
#define CORTEX_SCMD_GETFILE     0x10 // retrieves a single file.

#define CORTEX_CMD_GETSTATUS	0x06  //get status of module. machine codes, not HTML

#define CORTEX_CMD_ASSIGN			0x07  //set ownership of module // has data familiar name of owning module

#define CORTEX_CMD_BYE        0x0f  //command module to shut down.


// subcommands used by resources and processes to answer cortex  (reply cmd should be ACK or NAK plus flags)
#define CORTEX_SRREPLY_OK					0x00   
#define CORTEX_SRREPLY_CANNOT			0x01  // not able to comply with cmd

// commands used by resources and processes to request things from cortex
#define CORTEX_REQ_HELLO				0x01  // has no sub.  has data "host:commandport:statusport"
#define CORTEX_REQ_GIVESTATUS		0x02  // gives formatted status of module and destinations, etc
#define CORTEX_REQ_REQOWN		    0x03  // requests ownership of a destination
#define CORTEX_REQ_RELOWN		    0x04  // releases ownership of a destination

// subcommands used by cortex to answer resources and processes. (reply cmd should be ACK or NAK plus flags)
#define CORTEX_SCREPLY_OK  0x00  // has data "commandport:statusport"
#define CORTEX_SCREPLY_CHANGEPORTS  0x01  // has data "commandport:statusport"

// commands used by authoring and status modules to request things from cortex via activeX
#define CORTEX_REQ_GETPORTS 0x10  // has no sub.  has data "host:commandport" for authoring or "host:statusport" for status - these are the ports of the modules direct, not of cortex


#endif //CORTEXCOMMANDS_INCLUDED