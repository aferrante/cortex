/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 3.01.75 */
/* at Fri May 26 16:34:19 2006
 */
/* Compiler settings for CortexUtilCOM.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: none
*/
//@@MIDL_FILE_HEADING(  )
#include "rpc.h"
#include "rpcndr.h"
#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CortexUtilCOM_h__
#define __CortexUtilCOM_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __ICortexUtil_FWD_DEFINED__
#define __ICortexUtil_FWD_DEFINED__
typedef interface ICortexUtil ICortexUtil;
#endif 	/* __ICortexUtil_FWD_DEFINED__ */


#ifndef __CortexUtil_FWD_DEFINED__
#define __CortexUtil_FWD_DEFINED__

#ifdef __cplusplus
typedef class CortexUtil CortexUtil;
#else
typedef struct CortexUtil CortexUtil;
#endif /* __cplusplus */

#endif 	/* __CortexUtil_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __ICortexUtil_INTERFACE_DEFINED__
#define __ICortexUtil_INTERFACE_DEFINED__

/****************************************
 * Generated header for interface: ICortexUtil
 * at Fri May 26 16:34:19 2006
 * using MIDL 3.01.75
 ****************************************/
/* [unique][helpstring][dual][uuid][object] */ 



EXTERN_C const IID IID_ICortexUtil;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    interface DECLSPEC_UUID("551EAD71-F394-4725-BCDC-796162F3E098")
    ICortexUtil : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE OnStartPage( 
            /* [in] */ IUnknown __RPC_FAR *piUnk) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnEndPage( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE cxUtil( 
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutParams) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICortexUtilVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            ICortexUtil __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            ICortexUtil __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            ICortexUtil __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OnStartPage )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ IUnknown __RPC_FAR *piUnk);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OnEndPage )( 
            ICortexUtil __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *cxUtil )( 
            ICortexUtil __RPC_FAR * This,
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutParams);
        
        END_INTERFACE
    } ICortexUtilVtbl;

    interface ICortexUtil
    {
        CONST_VTBL struct ICortexUtilVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICortexUtil_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICortexUtil_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICortexUtil_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICortexUtil_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICortexUtil_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICortexUtil_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICortexUtil_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICortexUtil_OnStartPage(This,piUnk)	\
    (This)->lpVtbl -> OnStartPage(This,piUnk)

#define ICortexUtil_OnEndPage(This)	\
    (This)->lpVtbl -> OnEndPage(This)

#define ICortexUtil_cxUtil(This,bstrInParams,pbOutParams)	\
    (This)->lpVtbl -> cxUtil(This,bstrInParams,pbOutParams)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE ICortexUtil_OnStartPage_Proxy( 
    ICortexUtil __RPC_FAR * This,
    /* [in] */ IUnknown __RPC_FAR *piUnk);


void __RPC_STUB ICortexUtil_OnStartPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ICortexUtil_OnEndPage_Proxy( 
    ICortexUtil __RPC_FAR * This);


void __RPC_STUB ICortexUtil_OnEndPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexUtil_cxUtil_Proxy( 
    ICortexUtil __RPC_FAR * This,
    /* [in] */ BSTR bstrInParams,
    /* [retval][out] */ BSTR __RPC_FAR *pbOutParams);


void __RPC_STUB ICortexUtil_cxUtil_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICortexUtil_INTERFACE_DEFINED__ */



#ifndef __CORTEXUTILCOMLib_LIBRARY_DEFINED__
#define __CORTEXUTILCOMLib_LIBRARY_DEFINED__

/****************************************
 * Generated header for library: CORTEXUTILCOMLib
 * at Fri May 26 16:34:19 2006
 * using MIDL 3.01.75
 ****************************************/
/* [helpstring][version][uuid] */ 



EXTERN_C const IID LIBID_CORTEXUTILCOMLib;

#ifdef __cplusplus
EXTERN_C const CLSID CLSID_CortexUtil;

class DECLSPEC_UUID("90FF2AAC-3FA6-4ACA-AB21-7261E20D72D8")
CortexUtil;
#endif
#endif /* __CORTEXUTILCOMLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
