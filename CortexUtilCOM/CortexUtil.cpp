// CortexUtil.cpp : Implementation of CCortexUtil
#include "stdafx.h"
#include "CortexUtilCOM.h"
#include "CortexUtil.h"
#include "..\..\Common\HTTP\HTTP10.h"
#include "..\..\Common\TXT\FileUtil.h"
#include "..\3.0.3.1\CortexDefines.h"

/////////////////////////////////////////////////////////////////////////////
// CCortexUtil

STDMETHODIMP CCortexUtil::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ICortexUtil,
	};
	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CCortexUtil::OnStartPage (IUnknown* pUnk)  
{
	if(!pUnk)
		return E_POINTER;

	CComPtr<IScriptingContext> spContext;
	HRESULT hr;

	// Get the IScriptingContext Interface
	hr = pUnk->QueryInterface(IID_IScriptingContext, (void **)&spContext);
	if(FAILED(hr))
		return hr;

	// Get Request Object Pointer
	hr = spContext->get_Request(&m_piRequest);
	if(FAILED(hr))
	{
		spContext.Release();
		return hr;
	}

	// Get Response Object Pointer
	hr = spContext->get_Response(&m_piResponse);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		return hr;
	}
	
	// Get Server Object Pointer
	hr = spContext->get_Server(&m_piServer);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		return hr;
	}
	
	// Get Session Object Pointer
	hr = spContext->get_Session(&m_piSession);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		m_piServer.Release();
		return hr;
	}

	// Get Application Object Pointer
	hr = spContext->get_Application(&m_piApplication);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		m_piServer.Release();
		m_piSession.Release();
		return hr;
	}
	m_bOnStartPageCalled = TRUE;
	return S_OK;
}

STDMETHODIMP CCortexUtil::OnEndPage ()  
{
	m_bOnStartPageCalled = FALSE;
	// Release all interfaces
	m_piRequest.Release();
	m_piResponse.Release();
	m_piServer.Release();
	m_piSession.Release();
	m_piApplication.Release();

	return S_OK;
}


STDMETHODIMP CCortexUtil::cxUtil( BSTR bstrInParams, BSTR* pbOutParams)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if(pbOutParams==NULL) return E_POINTER;

	CString szInput, szOutput;

	szInput = (LPCWSTR)bstrInParams;


  if (bstrInParams != NULL)  
  {
		LPSTR   p = (char*)szInput.GetBuffer(::SysStringLen(bstrInParams) + 1);
		BOOL    bUsedDefaultChar;

		::WideCharToMultiByte(CP_ACP, 0, bstrInParams, -1, 
						p, ::SysStringLen(bstrInParams)+1, 
						NULL, &bUsedDefaultChar);
		szInput.ReleaseBuffer();

/*
	FILE* fp = fopen("CXUtil.txt", "wb");
	if(fp)
	{
		fprintf(fp, "%s\r\n\r\n", szInput);
		fclose(fp);
	}
*/

		CHTTP10 http;

		char* pszContext = NULL;
		char** ppszNames = NULL;
		char** ppszValues = NULL;
		unsigned long ulNumPairs=0;

		pszContext = (char*) szInput.GetBuffer(0);

		if(http.URLDecode(&pszContext, &ppszNames, &ppszValues, &ulNumPairs)>=HTTP_SUCCESS)
		{
			// check context....

			szOutput=((pszContext)&&(strlen(pszContext)))?pszContext:"";
			unsigned long i=0;

			bool bURLencode = true;
			// pszContext is the utility function to call
			// input arguments are specific to call
			// returnvalue= will be a standard returned param.
			// error= will be a standard returned param if there is an error to report.

			// everything is URL encoded when returned, unless urlencode=false is passed in (may corrupt the thing)

			i=0;
			while(i<ulNumPairs)
			{
				if((ppszNames)&&(ppszNames[i])&&(ppszValues)&&(ppszValues[i]))
				{
					if(stricmp(ppszNames[i], "urlencode")==0)
					{
						if(stricmp(ppszValues[i], "false")==0)
						{
							bURLencode = false; break;

						}
					}
				}
				i++;
			}



			if((pszContext)&&(strlen(pszContext)))
			{
				if(stricmp(pszContext, "base64encode")==0)
				{
				}
				else
				if(stricmp(pszContext, "")==0)
				{
				}

			}
			else
			{
				szOutput="error=invalid%20function";
			}


			i=0;
			while(i<ulNumPairs)
			{
				if((ppszNames)&&(ppszNames[i]))
				{
					free(ppszNames[i]);
				}
				if((ppszValues)&&(ppszValues[i]))
				{
					free(ppszValues[i]);
				}
				i++;
			}

			if(ppszNames) free(ppszNames);
			if(ppszValues) free(ppszValues);
			if(pszContext) free(pszContext);
		}
	}
	CComBSTR bstrRtnVal = szOutput.AllocSysString();

	*pbOutParams = bstrRtnVal.Detach();

	return S_OK;
}
