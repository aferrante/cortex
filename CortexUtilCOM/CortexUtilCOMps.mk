
CortexUtilCOMps.dll: dlldata.obj CortexUtilCOM_p.obj CortexUtilCOM_i.obj
	link /dll /out:CortexUtilCOMps.dll /def:CortexUtilCOMps.def /entry:DllMain dlldata.obj CortexUtilCOM_p.obj CortexUtilCOM_i.obj kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib 

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL $<

clean:
	@del CortexUtilCOMps.dll
	@del CortexUtilCOMps.lib
	@del CortexUtilCOMps.exp
	@del dlldata.obj
	@del CortexUtilCOM_p.obj
	@del CortexUtilCOM_i.obj
