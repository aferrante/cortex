// CortexUtil.h : Declaration of the CCortexUtil

#ifndef __CORTEXUTIL_H_
#define __CORTEXUTIL_H_

#include "resource.h"       // main symbols
#include <asptlb.h>         // Active Server Pages Definitions

/////////////////////////////////////////////////////////////////////////////
// CCortexUtil
class ATL_NO_VTABLE CCortexUtil : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CCortexUtil, &CLSID_CortexUtil>,
	public ISupportErrorInfo,
	public IDispatchImpl<ICortexUtil, &IID_ICortexUtil, &LIBID_CORTEXUTILCOMLib>
{
public:
	CCortexUtil()
	{ 
		m_bOnStartPageCalled = FALSE;
	}

public:

DECLARE_REGISTRY_RESOURCEID(IDR_CORTEXUTIL)
DECLARE_NOT_AGGREGATABLE(CCortexUtil)

BEGIN_COM_MAP(CCortexUtil)
	COM_INTERFACE_ENTRY(ICortexUtil)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ICortexUtil
public:
	STDMETHOD(cxUtil)(/*[in]*/ BSTR bstrInParams, /*[out,retval]*/ BSTR* pbOutParams);
	//Active Server Pages Methods
	STDMETHOD(OnStartPage)(IUnknown* IUnk);
	STDMETHOD(OnEndPage)();

private:
	CComPtr<IRequest> m_piRequest;					//Request Object
	CComPtr<IResponse> m_piResponse;				//Response Object
	CComPtr<ISessionObject> m_piSession;			//Session Object
	CComPtr<IServer> m_piServer;					//Server Object
	CComPtr<IApplicationObject> m_piApplication;	//Application Object
	BOOL m_bOnStartPageCalled;						//OnStartPage successful?
};

#endif //__CORTEXUTIL_H_
