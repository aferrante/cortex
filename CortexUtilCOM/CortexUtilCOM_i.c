/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 3.01.75 */
/* at Fri May 26 16:34:19 2006
 */
/* Compiler settings for CortexUtilCOM.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: none
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_ICortexUtil = {0x551EAD71,0xF394,0x4725,{0xBC,0xDC,0x79,0x61,0x62,0xF3,0xE0,0x98}};


const IID LIBID_CORTEXUTILCOMLib = {0x339951B9,0xB1A0,0x423D,{0x93,0xBD,0xEB,0x1C,0xA6,0x23,0xD5,0xA5}};


const CLSID CLSID_CortexUtil = {0x90FF2AAC,0x3FA6,0x4ACA,{0xAB,0x21,0x72,0x61,0xE2,0x0D,0x72,0xD8}};


#ifdef __cplusplus
}
#endif

