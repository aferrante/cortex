// CortexData.cpp: implementation of the CCortexData and related support classes.
//
//////////////////////////////////////////////////////////////////////

//#include <stdio.h>
//#include "CortexData.h"

#include <stdafx.h>
#include <process.h>
#include "Cortex.h"
#include "CortexHandler.h" 
#include "CortexMain.h" 
#include "CortexData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CCortexMain* g_pcortex;
extern CCortexApp theApp;

//////////////////////////////////////////////////////////////////////
// CPeriodicQuery Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPeriodicQuery::CPeriodicQuery()
{
	m_pszQuery = NULL;
	m_ulInterval = 0; // number of seconds between calls
	m_ulOffset=0;   // number of seconds to offset
	m_ulLastSent=0; // unixtime, last sent.
}

CPeriodicQuery::~CPeriodicQuery()
{
	if(m_pszQuery) free(m_pszQuery);
}



//////////////////////////////////////////////////////////////////////
// CCortexObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexObject::CCortexObject()
{
	InitializeCriticalSection(&m_critText);

	m_pszName		= NULL;
	m_usType		= CX_TYPE_UNDEF;

	// destinations within object
	m_ppszDestName		= NULL;
	m_ppszDestParams	= NULL;  // has to be generic for all types
	m_ulNumDest				= 0; // number of destinations.

	// dependencies within object
	m_ppszResourceName		= NULL;
	m_ppszResourceParams	= NULL;  // has to be generic for all types
	m_ulNumResources			= NULL; // number of dependency resources.

	// IP and ports
	m_pszHost				= NULL;
	m_usFilePort		= CX_PORT_INVALID;
	m_usCommandPort	= CX_PORT_INVALID;
	m_usStatusPort	= CX_PORT_INVALID;

	// cortex assigned status
	m_ulStatus	= CX_STATUS_UNINIT;
	m_ulOwner		= CX_OWNER_INVALID;

	// there is a negotiation to set the following from default values.
	m_ulStatusIntervalMS	= CX_TIME_PING;		// the interval, in milliseconds, at which we expect to hear status.
	m_ulFailureIntervalMS	= CX_TIME_FAIL;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	//m_timebLastStatus; // the time of the last status received (parseable)
	//m_timebLastTime;		// the last time received (object's local time)
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for object status
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
}

CCortexObject::~CCortexObject()
{
	Free();
	DeleteCriticalSection(&m_critText);
}

void CCortexObject::Free()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_ppszDestName)
	{
		unsigned long i=0;
		while(i<m_ulNumDest)
		{
			if(m_ppszDestName[i]) free(m_ppszDestName[i]); // must use malloc to allocate
			if(m_ppszDestParams[i]) free(m_ppszDestParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszDestName);
	}
	if(m_ppszResourceName)
	{
		unsigned long i=0;
		while(i<m_ulNumResources)
		{
			if(m_ppszResourceName[i]) free(m_ppszResourceName[i]); // must use malloc to allocate
			if(m_ppszResourceParams[i]) free(m_ppszResourceParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszResourceName);
	}
	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
}

char* CCortexObject::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CCortexObject::SetStatusText(char* pszText, unsigned long ulStatus, unsigned long ulStatusCounter)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter=ulStatusCounter;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter=ulStatusCounter;
		}
		LeaveCriticalSection(&m_critText);
	}
	return nRV;
}



//////////////////////////////////////////////////////////////////////
// CCortexData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexData::CCortexData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critObj);
	
	m_usResourcePortLast	= CX_PORT_INVALID;
	m_usProcessPortLast		= CX_PORT_INVALID;

	m_ppObj = NULL;
	m_nNumObjects = 0;
	m_nNumResources = 0;  // this is for convenience only
	m_nNumProcesses = 0;  // this is for convenience only

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host, needed for redirects
	m_nLastSettingsMod = -1;
	m_nSettingsMod = -1;

	m_bProcessSuspended = false;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_ppPeriodicQuery = NULL;	
	m_nNumQueries = 0;
	m_ulQueriesStart = 0;

}

CCortexData::~CCortexData()
{
	EnterCriticalSection(&m_critObj);
	if(m_ppObj)
	{
		int i=0;
		while(i<m_nNumObjects)
		{
			if(m_ppObj[i]) delete m_ppObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppObj; // delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critObj);
	if(m_ppPeriodicQuery)
	{
		int i=0;
		while(i<m_nNumQueries)
		{
			if(m_ppPeriodicQuery[i]) delete m_ppPeriodicQuery[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate
	}

	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critObj);
}

char* CCortexData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CCortexData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pcortex->m_settings.m_pszIconPath)&&(strlen(g_pcortex->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pcortex->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pcortex->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pcortex->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pcortex->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pcortex->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}

// utility
int	CCortexData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszHost) free(m_pszHost);
						m_pszHost=pch;
						return CX_SUCCESS;
					}
				}
			}
		}
	}
	return CX_ERROR;
}

int	CCortexData::AddObject(CCortexObject* pObj)
{
	if(pObj)
	{
		CCortexObject** ppObj = new CCortexObject*[m_nNumObjects+1];
		if(ppObj)
		{
			EnterCriticalSection(&m_critObj);
			int n=0;
			if(m_ppObj)
			{
				while(n<m_nNumObjects)
				{
					ppObj[n] = m_ppObj[n];
					n++;
				}
				delete [] m_ppObj;
			}
			ppObj[n] = pObj;
			m_ppObj = ppObj;
			m_nNumObjects++;
			if(pObj->m_usType&CX_TYPE_PROCESS) 
				m_nNumProcesses++;
			else
				m_nNumResources++;
 			LeaveCriticalSection(&m_critObj);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int	CCortexData::DeleteObject(CCortexObject* pObj)
{
	if(pObj)
	{
		if((m_ppObj)&&(m_nNumObjects>0))
		{
			EnterCriticalSection(&m_critObj);
			int n=0;
			while(n<m_nNumObjects)
			{
				if(m_ppObj[n] == pObj)
				{
					delete m_ppObj[n];
					CCortexObject** ppObj = new CCortexObject*[m_nNumObjects-1];
					if(ppObj)
					{
						int i=0;
						while(i<n)
						{
							ppObj[i] = m_ppObj[i];
							i++;
						}
						while(i<m_nNumObjects-1)
						{
							ppObj[i] = m_ppObj[i+1];
							i++;
						}
						m_nNumObjects--;
						delete [] m_ppObj;
						m_ppObj = ppObj;
					}
					else // couldnt allocate array.
					{
						int i=n;
						while(i<m_nNumObjects-1)
						{
							m_ppObj[i] = m_ppObj[i+1];
							i++;
						}
						m_nNumObjects--;
						m_ppObj[i] = NULL;
					}
					LeaveCriticalSection(&m_critObj);
					return CX_SUCCESS;
				}
				n++;
			}
			LeaveCriticalSection(&m_critObj);
		}
	}
	return CX_ERROR;
}


int CCortexData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pcortex)&&(g_pcortex->m_settings.m_pszExchange)&&(strlen(g_pcortex->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		if(strcmp(pszTableName, "Keep_Alive")==0)  // special for cortex
		{
			_timeb timebMod;
			_ftime( &timebMod );

			szTemp.Format("%s", pszTableName );
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET mod = %d WHERE criterion = '%s'",
				g_pcortex->m_settings.m_pszExchange,
				(unsigned long)(timebMod.time - (timebMod.timezone*60) +(timebMod.dstflag?3600:0)), // local time....
				szTemp		
				);
		}
		else
		{
			szTemp.Format("DBT_%s", pszTableName );
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
				g_pcortex->m_settings.m_pszExchange,
				CX_DB_MOD_MAX,
				g_pcortex->m_settings.m_pszExchange,
				szTemp, szTemp		
				);
		}

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int CCortexData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pcortex)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pcortex->m_settings.m_pszExchange)&&(strlen(g_pcortex->m_settings.m_pszExchange)))?g_pcortex->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = CX_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pcortex->m_settings.m_pszSettings)&&(strlen(g_pcortex->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pcortex->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0) m_bProcessSuspended = true;
					else m_bProcessSuspended = false;
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			return nReturn;
		}
	}
	return CX_ERROR;
}
