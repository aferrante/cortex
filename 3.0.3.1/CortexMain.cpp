// CortexMain.cpp: implementation of the CCortexMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Cortex.h"  // just included to have access to windowing environment
#include "CortexDlg.h"  // just included to have access to windowing environment
#include "CortexHandler.h"  // just included to have access to windowing environment

#include "CortexMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
CCortexMain* g_pcortex=NULL;

extern CMessager* g_pmsgr;  // from Messager.cpp

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMain::CCortexMain()
{
}

CCortexMain::~CCortexMain()
{
}

/*

char*	CCortexMain::CortexTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply cortex scripting language
{
	return pszBuffer;
}

int		CCortexMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return CX_SUCCESS;
}
*/

SOCKET*	CCortexMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // cortex initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
//CString foo; foo.Format("sending to %s:%d",pchHost, usPort);AfxMessageBox(foo);

	if(m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps)>=NET_SUCCESS)
	{
		m_net.SendData(NULL, *ps, 5000, 0, NET_SND_CLNTACK);
	}

	return ps;
}

int		CCortexMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex replies to an object server after receiving data. (usually ack or nak)
{
	return CX_SUCCESS;
}

int		CCortexMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex answers a request from an object client
{
	return CX_SUCCESS;
}

int CCortexMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			m_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			m_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

void CortexMainThread(void* pvArgs)
{
	CCortexApp* pApp = (CCortexApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.
	CCortexMain cortex;

	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_START;
	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNINIT;

	cortex.m_data.GetHost();

	g_pcortex = &cortex;

	CDBUtil* pdb = NULL;

	// windows stuff  *** removed!
//	CCortexDlg* pdlg = ((CCortexDlg*)(((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg));
//	CListCtrlEx* plist = (CListCtrlEx*)(&(pdlg->m_lce));
//	CWnd* pwndStatus = ((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT);

	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Cortex\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// modes.
			if(stricmp(pch, "/c")==0) // clone mode  (check for this first, it invalidates some of the rest)
			{
				//TODO create clone mode!
				cortex.m_settings.m_ulMainMode |= CX_MODE_CLONE;  // trumps listener
			}
			else
			if(stricmp(pch, "/l")==0) // listener mode
			{
				//TODO create listener mode
				cortex.m_settings.m_ulMainMode |= CX_MODE_LISTENER;  // trumped by clone
			}
			else
			if(stricmp(pch, "/q")==0) // quiet mode (works in listener or default, clone is by def quiet)
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_QUIET;	
			}
			else
			if(stricmp(pch, "/v")==0) // volatile mode settings used this session are not retained by default - does not override explicit saves via http interface
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_VOLATILE;	
			}
			else

			// params
			if(strnicmp(pch, "/s:", 3)==0) // server listen port(s) override
			{
			}
			else
			if(strnicmp(pch, "/r:", 3)==0) // resource port range override
			{
			}
			else
			if(strnicmp(pch, "/p:", 3)==0) // process port range override
			{
			}

			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

	if(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE)
		strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
	else
	if(cortex.m_settings.m_ulMainMode&CX_MODE_LISTENER)
		strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
	else  // default
		strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// get settings
		cortex.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		cortex.m_settings.m_pszName = file.GetIniString("Main", "Name", "Cortex");
//		cortex.m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);

		cortex.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");
		cortex.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_pcortex->m_data.m_key.m_pszLicenseString) free(g_pcortex->m_data.m_key.m_pszLicenseString);
		g_pcortex->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(cortex.m_settings.m_pszLicense)+1);
		if(g_pcortex->m_data.m_key.m_pszLicenseString)
		sprintf(g_pcortex->m_data.m_key.m_pszLicenseString, "%s", cortex.m_settings.m_pszLicense);

		g_pcortex->m_data.m_key.InterpretKey();

		if(g_pcortex->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pcortex->m_data.m_key.m_ulNumParams)
			{
				if((g_pcortex->m_data.m_key.m_ppszParams)
					&&(g_pcortex->m_data.m_key.m_ppszValues)
					&&(g_pcortex->m_data.m_key.m_ppszParams[i])
					&&(g_pcortex->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pcortex->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_pcortex->m_data.m_nMaxLicensedDevices = atoi(g_pcortex->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!cortex.m_data.m_key.m_bExpires)
					||((cortex.m_data.m_key.m_bExpires)&&(!cortex.m_data.m_key.m_bExpired))
					||((cortex.m_data.m_key.m_bExpires)&&(cortex.m_data.m_key.m_bExpireForgiveness)&&(cortex.m_data.m_key.m_ulExpiryDate+cortex.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!cortex.m_data.m_key.m_bMachineSpecific)
					||((cortex.m_data.m_key.m_bMachineSpecific)&&(cortex.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
				cortex.m_data.m_ulFlags |= CX_STATUS_OK;
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
				cortex.m_data.m_ulFlags |= CX_STATUS_ERROR;
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_ERROR;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
		}
		
		
		cortex.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
		cortex.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
		cortex.m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

		cortex.m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
		cortex.m_settings.m_usProcessPortMax = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

		cortex.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		cortex.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
//		cortex.m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
		cortex.m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 0)?true:false;
		cortex.m_settings.m_bUseDB = file.GetIniInt("Mode", "UseDB", 0)?true:false;

		cortex.m_settings.m_nSparkStaggerIntervalMS = file.GetIniInt("Timing", "SparkInterval", 1000);
		
		cortex.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", cortex.m_settings.m_pszName?cortex.m_settings.m_pszName:"Cortex");
		cortex.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		cortex.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
//		cortex.m_settings.m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", cortex.m_settings.m_pszName?cortex.m_settings.m_pszName:"CortexBackup");
//		cortex.m_settings.m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
//		cortex.m_settings.m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
		cortex.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		cortex.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		cortex.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
		cortex.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
		cortex.m_settings.m_bPeriodicQueries = file.GetIniInt("Database", "UsePeriodicQueries", 0)?true:false; 
		if(cortex.m_settings.m_bPeriodicQueries)
		{
			cortex.m_settings.m_pszPeriodicQueries = file.GetIniString("Database", "PeriodicQueries", "600|sp_PrimaryToBackup");    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
			CSafeBufferUtil sbu;
			char* pch = sbu.Token(cortex.m_settings.m_pszPeriodicQueries, strlen(cortex.m_settings.m_pszPeriodicQueries), "|");
			while(pch)
			{
				unsigned long ulInterval = atol(pch);
				unsigned long ulOffset=0;   // number of seconds to offset
				pch = strchr(pch, '+');  //search for offset
				if(pch)
				{
					pch++;
					ulOffset=atol(pch);
				}

				pch = sbu.Token(NULL, NULL, "|");

				if((pch)&&(strlen(pch)>0))
				{
					CPeriodicQuery* pQuery = new CPeriodicQuery;
					if(pQuery)
					{
						pQuery->m_pszQuery = (char*)malloc(strlen(pch)+1);
						if(pQuery->m_pszQuery)
						{
							strcpy(pQuery->m_pszQuery, pch);
							pQuery->m_ulInterval = ulInterval; // number of seconds between calls
							pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

							CPeriodicQuery** ppQueries = new CPeriodicQuery*[cortex.m_data.m_nNumQueries+1];
							if(ppQueries)
							{
								int i=0;
								if((cortex.m_data.m_ppPeriodicQuery)&&(cortex.m_data.m_nNumQueries))
								{
									while(i<cortex.m_data.m_nNumQueries)
									{
										ppQueries[i] = cortex.m_data.m_ppPeriodicQuery[i];
										i++;
									}
									delete [] cortex.m_data.m_ppPeriodicQuery;
								}
								ppQueries[i] = pQuery;
								cortex.m_data.m_nNumQueries = i+1;
								cortex.m_data.m_ppPeriodicQuery = ppQueries;

							}
							else delete pQuery;
						}
						else delete pQuery;
					}
				}
				pch = sbu.Token(NULL, NULL, "|");
			}
		}

		pszParams = file.GetIniString("FileServer", "SettingsURL", "default.asp");

		if(pszParams)
		{
			if(cortex.m_data.m_pszHost)
			{
				if(pApp->m_pszSettingsURL) free(pApp->m_pszSettingsURL);
				_snprintf(pszPath, MAX_PATH, "http://%s/%s", cortex.m_data.m_pszHost, pszParams);  
				pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
				if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);
			}

			free(pszParams);
		}
// get the security settings from a different file (encrypted) default to yes.
/*
		_snprintf(pszPath, MAX_PATH, "%s%s", pszCurrentDir, CX_SETTINGS_FILE_ENCRYPT);  // esf = encrypted settings file
		pszParams = file.GetIniString("Mode", "Encrypted", pszPath);  //reasonable default. - could search the file system, though.
		if(pszParams)
		{
			CFileUtil encfile;
			encfile.GetSettings(pszParams, true); 
			if(encfile.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				cortex.m_settings.m_bUseAuthentication = encfile.GetIniInt("Authentication", "Use", 1)?true:false;
				cortex.m_settings.m_pszUserName = encfile.GetIniString("Authentication", "MasterID", CX_AUTH_USER);
				cortex.m_settings.m_pszPassword = encfile.GetIniString("Authentication", "MasterPW", CX_AUTH_PWD);
				cortex.m_settings.m_pszSecurityFile1 = encfile.GetIniString("Authentication", "File", CX_SETTINGS_FILE_SECURE1);
				cortex.m_settings.m_pszSecurityFile2 = encfile.GetIniString("Authentication", "Backup", CX_SETTINGS_FILE_SECURE2);
			}
			free(pszParams); pszParams=NULL;
		}
*/
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	if(cortex.m_settings.m_bUseLog)
	{
		bUseLog = cortex.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
		pszParams = file.GetIniString("Messager", "LogFileIni", "Cortex|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			cortex.m_data.m_ulFlags |= (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR);
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "--------------------------------------------------\n\
-------------- Cortex %s start --------------", CX_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	cortex.m_http.InitializeMessaging(&cortex.m_msgr);
	cortex.m_net.InitializeMessaging(&cortex.m_msgr);

	if(cortex.m_settings.m_bUseDB)
	{
		pdb = new CDBUtil;
		if(pdb == NULL)
		{
			// report error
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not create database object.");
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:database_init", errorstring);  //(Dispatch message)
		}
	}

	CDBconn* pdbConn = NULL;
	if(pdb)
	{

		pdbConn = pdb->CreateNewConnection(cortex.m_settings.m_pszDSN, cortex.m_settings.m_pszUser, cortex.m_settings.m_pszPW);
		if(pdbConn)
		{
			if(pdb->ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				cortex.m_data.m_ulFlags |= (CX_STATUS_FAIL_DB|CX_STATUS_ERROR);
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_connect", errorstring);  //(Dispatch message)
			}
			else
			{
				cortex.m_settings.m_pdbConn = pdbConn;
				cortex.m_settings.m_pdb = pdb;
				cortex.m_data.m_pdbConn = pdbConn;
				cortex.m_data.m_pdb = pdb;
				if(cortex.m_settings.GetFromDatabase(errorstring)<CX_SUCCESS)
				{
					cortex.m_data.m_ulFlags |= (CX_STATUS_FAIL_DB|CX_STATUS_ERROR);
					cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
					cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:database_get", errorstring);  //(Dispatch message)
				}
				else
				{
					if((!cortex.m_settings.m_bUseLog)&&(bUseLog))
					{
						// reset it
						cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

						Sleep(250); // let the message get there.
						cortex.m_msgr.RemoveDestination("log");

					}
				}
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", cortex.m_settings.m_pszDSN, cortex.m_settings.m_pszUser, cortex.m_settings.m_pszPW); 
			cortex.m_data.m_ulFlags |= (CX_STATUS_FAIL_DB|CX_STATUS_ERROR);
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_init", errorstring);  //(Dispatch message)

			//**MSG
		}

	}


//init command and status listeners.
	cortex.m_data.m_ulFlags &= ~CX_STATUS_CMDSVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_CMDSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", cortex.m_settings.m_usCommandPort); 
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)

	if(cortex.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "CortexCommandServer");

		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = CortexCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &cortex;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &cortex.m_net;					// pointer to the object with the Message function.


		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			cortex.m_data.m_ulFlags &= ~CX_STATUS_CMDSVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_CMDSVR_ERROR;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)
			cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:command_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", cortex.m_settings.m_usCommandPort);
			cortex.m_data.m_ulFlags &= ~CX_STATUS_CMDSVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_CMDSVR_RUN;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)
		}
	}

	cortex.m_data.m_ulFlags &= ~CX_STATUS_STATUSSVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_STATUSSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing status server on %d", cortex.m_settings.m_usStatusPort); 
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:status_server_init", errorstring);  //(Dispatch message)

	if(cortex.m_settings.m_usStatusPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName)strcpy(pServer->m_pszName, "CortexStatusServer");

		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = CortexStatusHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &cortex;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &cortex.m_net;					// pointer to the object with the Message function.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			cortex.m_data.m_ulFlags &= ~CX_STATUS_STATUSSVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_STATUSSVR_ERROR;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:status_server_init", errorstring);  //(Dispatch message)
			cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:status_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Status server listening on %d", cortex.m_settings.m_usStatusPort);
			cortex.m_data.m_ulFlags &= ~CX_STATUS_STATUSSVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_STATUSSVR_RUN;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:status_server_init", errorstring);  //(Dispatch message)
		}
	}



// init file server....
/*
	cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing file server on %d", cortex.m_settings.m_usFilePort); 
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
*/
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// got settings already
		if(pszParams) free(pszParams); 
		_snprintf(pszPath, MAX_PATH, "%sroot", pszCurrentDir);  

		pszParams = file.GetIniString("FileServer", "Root", pszPath);  //reasonable default. - could search the file system, though.
//		pszParams = file.GetIniString("FileServer", "Root", "C:\\temp\\Temp\\VDI temp\\Harris\\IBC demo\\root");  // for testing only
		
/*
	// if security (from settings) then create secure obj and attach to http obj
		if(cortex.m_settings.m_bUseAuthentication)
		{
			cortex.m_http.m_pSecure = new CSecurity;
			cortex.m_http.m_pSecure->InitSecurity(
				cortex.m_settings.m_pszSecurityFile1,
				cortex.m_settings.m_pszSecurityFile2,
				cortex.m_settings.m_pszUserName,
				cortex.m_settings.m_pszPassword
				);
		}


		// start the webserver

		if(pszParams)
		{
			if(cortex.m_http.SetRoot(pszParams)!=HTTP_SUCCESS)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set root settings.");
				cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
				cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
			}
			else
			{
				if(cortex.m_http.GetHost()!=HTTP_SUCCESS)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set host name.");
					cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
					cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
					cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				}
				else
				{

					_snprintf(pszPath, MAX_PATH, "http://%s/index.htm", cortex.m_http.m_pszHost);  
					pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);

//					AfxMessageBox(pApp->m_pszSettingsURL);

					// get port from settings
					if(cortex.m_http.InitServer(cortex.m_settings.m_usFilePort, CortexHTTPThread, &cortex, &cortex.m_http, errorstring) <HTTP_SUCCESS)
					{
						cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
						cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
					}
					else
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "File server listening on %s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usFilePort);
						cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
						cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_RUN;
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
					}
				}
			}
			free(pszParams); pszParams = NULL;
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set root settings.");
			cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
		}
*/
	}
	else
	{
		// report failure!
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set file server settings.");
//		cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
//		cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
		cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	}

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is starting registered objects...");
	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_SPARK;
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

	// now run the spark list.
	// this is the list of executables that get sparked

	if(!(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE))
	{
		strcpy(pszFilename, CX_SETTINGS_FILE_SPARK);  // cortex spark list
		CFileUtil fileSpark;
		fileSpark.GetSettings(pszFilename, false); // not encrypted
		if(fileSpark.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			// got settings already
			if(pszParams) free(pszParams); 
			int nNumExe = 0, nCount =0;

			// local objects first
			nNumExe = fileSpark.GetIniInt("Local", "ObjectCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Object %03d Path", nCount);
				pszParams = fileSpark.GetIniString("Local", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_data.m_pszHost))
//					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Starting %s...", pszParams);  
						cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:objects_init", errorstring);  //(Dispatch message)
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.
						sprintf(pszFilename, "%s:%d", cortex.m_data.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
//						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
//						if(_execl( pszParams, pszFilename, NULL )<0) // crashes
						if(_spawnl( _P_NOWAIT , pszParams, pszParams, pszFilename, NULL )<0)
						{
							//report failure
//					AfxMessageBox("failure to spawn exe");
							// need to add an icon handler here too... 
							cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
							cortex.m_data.m_ulFlags |= CX_STATUS_ERROR;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s...", pszParams);  
							cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
							cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:objects_init", errorstring);  //(Dispatch message)
							cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:objects_init", errorstring);

						}
						else
						{
//					AfxMessageBox(pszFilename);
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS); // how much is too much?
						}
//					AfxMessageBox(pszParams);

					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}

			// remote objects
			nNumExe = 0, nCount =0;

// remotes - do later.
			nNumExe = fileSpark.GetIniInt("Remote", "HostCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Remote Host %03d", nCount);
				pszParams = fileSpark.GetIniString("Remote", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_data.m_pszHost))
//					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Starting remote %s...", pszParams);  
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
						cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:remote_objects_init", errorstring);  //(Dispatch message)
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.

						// have to send a spark command exchange to each remote host.
//						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
						sprintf(pszFilename, "%s:%d", cortex.m_data.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already

//						x send a net command

						if(0)
						{
							//report failure
							cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
							cortex.m_data.m_ulFlags |= CX_STATUS_ERROR;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s...", pszParams);  
							cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
							cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:remote_objects_init", errorstring);  //(Dispatch message)
							cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:remote_objects_init", errorstring);
						}
						else
						{
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS); // how much is too much?
						}
					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}
		}
		//else report error.
	}
	
	if((cortex.m_data.m_ulFlags&CX_ICON_MASK) != CX_STATUS_ERROR)
	{
		cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
		cortex.m_data.m_ulFlags |= CX_STATUS_OK;  // green - we want run to be blue when something in progress
	}

	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_RUN;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex main thread running.");  
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:init", "Cortex %s is started. The main thread is running.", CX_CURRENT_VERSION);
	// after this point, we want to update the UI with the "status display", something like:

// [Global Status]
//	listening on host:port.port
// [Resource Status]
//  resource 1: status whatever
//  resource 2: status whatever
// [Process Status]
//  process 1: status whatever
//  process 2: status whatever

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );

	cortex.m_data.m_ulQueriesStart = timebCheckMods.time;
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &cortex.m_data.m_timebTick );


		if(
			  (cortex.m_settings.m_bUseDB)
			&&(cortex.m_settings.m_bPeriodicQueries)
			&&(cortex.m_data.m_ppPeriodicQuery)
			&&(cortex.m_data.m_nNumQueries>0)
			&&(!g_bKillThread)
			&&(!cortex.m_data.m_bProcessSuspended)
			&&(pdbConn)
			&&(pdbConn->m_bConnected)
			&&(cortex.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!cortex.m_data.m_key.m_bExpires)
				||((cortex.m_data.m_key.m_bExpires)&&(!cortex.m_data.m_key.m_bExpired))
				||((cortex.m_data.m_key.m_bExpires)&&(cortex.m_data.m_key.m_bExpireForgiveness)&&(cortex.m_data.m_key.m_ulExpiryDate+cortex.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!cortex.m_data.m_key.m_bMachineSpecific)
				||((cortex.m_data.m_key.m_bMachineSpecific)&&(cortex.m_data.m_key.m_bValidMAC))
				)
			)
		{
			int i=0;
			while(i<cortex.m_data.m_nNumQueries)
			{
				if( (cortex.m_data.m_ppPeriodicQuery[i])
					&&(cortex.m_data.m_ppPeriodicQuery[i]->m_pszQuery)
					&&(strlen(cortex.m_data.m_ppPeriodicQuery[i]->m_pszQuery))
					)
				{
					if(
							(
								(cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent==0)
							&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ulQueriesStart+cortex.m_data.m_ppPeriodicQuery[i]->m_ulOffset)
							)

						||
							(
								(cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent>0)
							&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent+cortex.m_data.m_ppPeriodicQuery[i]->m_ulInterval)
							)
						)
					{
						if(pdb->ExecuteSQL(pdbConn, cortex.m_data.m_ppPeriodicQuery[i]->m_pszQuery)<DB_SUCCESS)
						{
							// error?
						}
						
						cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent = cortex.m_data.m_timebTick.time;
					}
				}
				i++;
			}
		}


		if(
					(cortex.m_data.m_timebTick.time > timebCheckMods.time )
				||((cortex.m_data.m_timebTick.time == timebCheckMods.time)&&(cortex.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
//				&&(!cortex.m_data.m_bProcessSuspended)
			)
		{
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "time to check");  Sleep(5);//(Dispatch message)
			timebCheckMods.time = cortex.m_data.m_timebTick.time + cortex.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = cortex.m_data.m_timebTick.millitm + (unsigned short)(cortex.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if((pdbConn)&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "checkmods %d, %d", timebCheckMods.time, cortex.m_settings.m_ulModsIntervalMS);  //(Dispatch message)
				cortex.m_data.CheckDatabaseMods();
				if(cortex.m_data.m_nSettingsMod != cortex.m_data.m_nLastSettingsMod)
				{
					if(cortex.m_settings.GetFromDatabase()>=CX_SUCCESS)
					{
						cortex.m_data.m_nLastSettingsMod = cortex.m_data.m_nSettingsMod;
					}
				}

				// increment the Keep alive.
				cortex.m_data.IncrementDatabaseMods("Keep_Alive");
			}
		}

		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1);

	}

	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_END;

	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:uninit", "Cortex is shutting down.");  //(Dispatch message)
	cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:uninit", "Cortex %s is shutting down.", CX_CURRENT_VERSION);

	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is shutting down.");  
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

// shut down all the running objects;
	if(cortex.m_data.m_ppObj)
	{
		int i=0;
		// have to shut down all the processes first
		while(i<cortex.m_data.m_nNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_PROCESS))
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:process_object_uninit", "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  //(Dispatch message)
			
				CNetData* pdata = new CNetData;
				if(pdata)
				{

					pdata->m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
					pdata->m_ucCmd = CX_CMD_BYE;       // the command byte
					pdata->m_ucSubCmd = 0;       // the subcommand byte
					pdata->m_pucData =  NULL;
					pdata->m_ulDataLen = 0;

					SOCKET s = NULL; 
			//AfxMessageBox("sending");
					if(cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_pszHost, cortex.m_data.m_ppObj[i]->m_usCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
					{
						//send ack
			//			AfxMessageBox("ack");
						cortex.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
					}
					else
					{
			//			AfxMessageBox("could not send");
					}
					cortex.m_net.CloseConnection(s);

					delete pdata;
				}
				cortex.m_data.m_nNumProcesses--; // do this here for now. - later, let it be a request on the command server, letting cortex know shutdown has been done successfully
/*
				CNetData* pReturnData = NULL;

				SOCKET* ps = cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					pReturnData,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
					CX_AUTH_USER, CX_AUTH_PWD);
					if(pReturnData) delete pReturnData;
					cortex.m_net.CloseConnection(*ps);
*/
			}
			Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS);
			i++;
		}

		while(cortex.m_data.m_nNumProcesses>0)  // wait for the processes to shutdown
		{
			_ftime( &cortex.m_data.m_timebTick );

			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
				AfxGetApp()->PumpMessage();
			Sleep(1);
		}

		// then shut down all the resources.
		i=0;
		while(i<cortex.m_data.m_nNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_RESOURCE))
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:resource_object_uninit", "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  //(Dispatch message)

				CNetData* pdata = new CNetData;
				if(pdata)
				{

					pdata->m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
					pdata->m_ucCmd = CX_CMD_BYE;       // the command byte
					pdata->m_ucSubCmd = 0;       // the subcommand byte
					pdata->m_pucData =  NULL;
					pdata->m_ulDataLen = 0;

					SOCKET s = NULL; 
			//AfxMessageBox("sending");
					if(cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_pszHost, cortex.m_data.m_ppObj[i]->m_usCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
					{
						//send ack
			//			AfxMessageBox("ack");
						cortex.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
					}
					else
					{
			//			AfxMessageBox("could not send");
					}
					cortex.m_net.CloseConnection(s);

					delete pdata;
				}
				cortex.m_data.m_nNumResources--;// do this here for now. - later, let it be a request on the command server, letting cortex know shutdown has been done successfully
/*
				CNetData* pReturnData = NULL;
				SOCKET* ps = cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					pReturnData,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
					CX_AUTH_USER, CX_AUTH_PWD);
				if(pReturnData) delete pReturnData;
				cortex.m_net.CloseConnection(*ps);
*/
			}
			Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS);
			i++;
		}
//CString foo; foo.Format("resources: %d",cortex.m_data.m_nNumResources);AfxMessageBox(foo);
		while(cortex.m_data.m_nNumResources>0)  // wait for the resources to shutdown
		{
			_ftime( &cortex.m_data.m_timebTick );

			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
				AfxGetApp()->PumpMessage();
			Sleep(1);
		}
//		AfxMessageBox("X");
	}
/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no nede to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = cortex.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		cortex.m_net.OpenConnection(cortex.m_http.m_pszHost, 10888, &s); // branding hardcode
		cortex.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		cortex.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	pdlg->SetProgress(CXDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
//	cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
//	_ftime( &cortex.m_data.m_timebTick );
//	cortex.m_http.EndServer();
	_ftime( &cortex.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	cortex.m_data.m_ulFlags &= ~CX_STATUS_CMDSVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_CMDSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_net.StopServer(cortex.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &cortex.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	cortex.m_data.m_ulFlags &= ~CX_STATUS_STATUSSVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_STATUSSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down status server....");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:status_server_uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_net.StopServer(cortex.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &cortex.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is exiting.");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
//		file.SetIniInt("FileServer", "ListenPort", cortex.m_settings.m_usFilePort);
		file.SetIniString("License", "Key", cortex.m_settings.m_pszLicense);
		file.SetIniInt("CommandServer", "ListenPort", cortex.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", cortex.m_settings.m_usStatusPort);
		file.SetIniString("FileServer", "IconPath", cortex.m_settings.m_pszIconPath);

		file.SetIniInt("Resources", "MinPort", cortex.m_settings.m_usResourcePortMin);
		file.SetIniInt("Resources", "MaxPort", cortex.m_settings.m_usResourcePortMax);

		file.SetIniInt("Processes", "MinPort", cortex.m_settings.m_usProcessPortMin);
		file.SetIniInt("Processes", "MaxPort", cortex.m_settings.m_usProcessPortMax);

		file.SetIniInt("Messager", "UseEmail", cortex.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", cortex.m_settings.m_bUseNetwork?1:0);
//		file.SetIniInt("Messager", "LogRemote", cortex.m_settings.m_bUseLogRemote?1:0);
		file.SetIniInt("Mode", "UseClone", cortex.m_settings.m_bUseClone?1:0);
		file.SetIniInt("Mode", "UseDB", cortex.m_settings.m_bUseDB?1:0);

		file.SetIniInt("Timing", "SparkInterval", cortex.m_settings.m_nSparkStaggerIntervalMS);

		file.SetIniString("Database", "DSN", cortex.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", cortex.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", cortex.m_settings.m_pszPW);
//		file.SetIniString("Database", "BackupDSN", cortex.m_settings.m_pszBackupDSN);
//		file.SetIniString("Database", "BackupDBUser", cortex.m_settings.m_pszBackupUser);
//		file.SetIniString("Database", "BackupDBPassword", cortex.m_settings.m_pszBackupPW);
		file.SetIniString("Database", "SettingsTableName", cortex.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", cortex.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", cortex.m_settings.m_pszMessages);  // the Messages table name
		file.SetIniInt("Database", "ModificationCheckInterval", cortex.m_settings.m_ulModsIntervalMS);  // in milliseconds
		file.SetIniInt("Database", "UsePeriodicQueries", cortex.m_settings.m_bPeriodicQueries?1:0);  
		file.SetIniString("Database", "PeriodicQueries", cortex.m_settings.m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.

		if(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE)
			strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
		else
		if(cortex.m_settings.m_ulMainMode&CX_MODE_LISTENER)
			strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
		else  // default
			strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file
		file.SetSettings(pszFilename, false);  

	}


	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is exiting");
	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

	//exiting
	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "-------------- Cortex %s exit ---------------\n\
--------------------------------------------------\n", CX_CURRENT_VERSION);  //(Dispatch message)
///	pdlg->SetProgress(CXDLG_CLEAR); // no point

	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_ENDED;

	g_pcortex = NULL;
	if(pdb)
	{
		pdb->RemoveConnection(pdbConn);
		delete pdb;
	}

	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(300); // another small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void CortexHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CCortexMain* pCortex = (CCortexMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pCortex->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: CortexServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: CortexServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(CX_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: CortexServer/%s", CX_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: CortexServer/%s", CX_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void CortexCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_REQ_HELLO:
						{
//AfxMessageBox("receiving");

							CSafeBufferUtil sbu;

							data.m_ucCmd = NET_CMD_ACK;

							if(data.m_pucData!=NULL)
							{
//AfxMessageBox( (char*)data.m_pucData );
								CCortexObject* pObj = new CCortexObject;
								if(pObj)
								{
									char* pch = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, ":", MODE_SINGLEDELIM);
									if(pch)
									{
										pObj->m_pszHost = (char*)malloc(strlen(pch)+1);
										if(pObj->m_pszHost) strcpy(pObj->m_pszHost, pch);
									}
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usCommandPort = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usStatusPort = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usType = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if(pch)
									{
										pObj->m_pszName = (char*)malloc(strlen(pch)+1);
										if(pObj->m_pszName) strcpy(pObj->m_pszName, pch);
									}
									pObj->SetStatusText("ready", CX_STATUS_OK, 1);  // false = hardcode for now
									EnterCriticalSection(&g_pcortex->m_data.m_critObj);// false = hardcode for now
//AfxMessageBox( "adding" );
									if(g_pcortex->m_data.AddObject(pObj)<CX_SUCCESS)
									{
										//**MSG
									}
									else
									{
										//**MSG success
									}
									LeaveCriticalSection(&g_pcortex->m_data.m_critObj);
								}

								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.


						} break;
/*
					case CX_CMD_BYE:  // not supported by main cortex
						{
	g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
*/
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going

						// NO, dont do this.
						// this is fine for a non-persistent server, but it will report all sorts of errors while just 
						// waiting and timing out on a persistent one.
/*
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
*/
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

void CortexStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

