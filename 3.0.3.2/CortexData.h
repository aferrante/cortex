// CortexData.h: interface for the CCortexData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "CortexDefines.h"
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
#include "../../Common/KEY/LicenseKey.h"


class CPeriodicObject  
{
public:
	CPeriodicObject();
	virtual ~CPeriodicObject();

	char* m_pszObject;
	char* m_pszReturnObject;
	unsigned long m_ulInterval; // number of seconds between calls
	unsigned long m_ulOffset;   // number of seconds to offset
	unsigned long m_ulLastSent; // unixtime, last sent.
};


class CCortexObject  
{
public:
	CCortexObject();
	virtual ~CCortexObject();
	void Free();

	// util objects
	CBufferUtil m_bu;
	CNetUtil m_net;  // network client only.  to send commands and keep connections open (uses m_socketCmd and m_socketStatus below)

	char* m_pszName;
	unsigned short m_usType;

	// destinations within object
	char** m_ppszDestName;
	char** m_ppszDestParams;  // has to be generic for all types
	unsigned long m_ulNumDest; // number of destinations.

	// dependencies within object
	char** m_ppszResourceName;
	char** m_ppszResourceParams;  // has to be generic for all types
	unsigned long m_ulNumResources; // number of dependency resources.

	// IP and ports
	char* m_pszHost;
	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;
//	SOCKET m_socketFile; // current comm socket for retrieving files  // dont store, not persistent conn.
	SOCKET m_socketCmd; // current comm socket for commands
	SOCKET m_socketStatus; // current comm socket for status

	// cortex assigned status
	unsigned long m_ulStatus;
	unsigned long m_ulOwner;

	// there is a negotiation to set the following from default values.
	unsigned long m_ulStatusIntervalMS;		// the interval, in milliseconds, at which we expect to hear status.
	unsigned long m_ulFailureIntervalMS;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	_timeb m_timebLastStatus; // the time of the last status received (parseable)
	_timeb m_timebLastTime;		// the last time received (object's local time)
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for object status

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, unsigned long ulStatusCounter);  // the counter is assigned with a remote get over network

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string
};



class CCortexData  
{
public:
	CCortexData();
	virtual ~CCortexData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host, needed for redirects
	char*	m_pszCompleteHost;	// the complete name of the host

	// cortex object brokering
	unsigned short m_usResourcePortLast;
	unsigned short m_usProcessPortLast;

	CCortexObject** m_ppObj;
	int m_nNumObjects;
	int m_nNumResources;  // this is for convenience only
	int m_nNumProcesses;  // this is for convenience only

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to another cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nLastSettingsMod;
	int m_nSettingsMod;

	bool m_bProcessSuspended;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	int		AddObject(CCortexObject* pObj);
	int		DeleteObject(CCortexObject* pObj);

	CRITICAL_SECTION m_critObj;
	CRITICAL_SECTION m_critText;
	CRITICAL_SECTION m_critQueries;
	CRITICAL_SECTION m_critShell;
	CRITICAL_SECTION m_critHttp;

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);

	CPeriodicObject** m_ppPeriodicQuery;	
	int m_nNumQueries;
	unsigned long m_ulQueriesStart; 

	CPeriodicObject** m_ppPeriodicShell;	
	int m_nNumShellCmds;
	unsigned long m_ulShellStart; 

	CPeriodicObject** m_ppPeriodicHttp;	
	int m_nNumHttpCalls;
	unsigned long m_ulHttpStart; 

	CLicenseKey m_key;


private:
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
