// CortexSettings.cpp: implementation of the CCortexSettings.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Cortex.h"
#include "CortexDefines.h"
#include "CortexSettings.h"
//#include "CortexData.h"
#include "CortexMain.h" 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CCortexMain* g_pcortex;
extern CCortexApp theApp;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexSettings::CCortexSettings()
{
	m_pszName = NULL;
//	m_ulMainMode = CX_MODE_DEFAULT;
	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	// ports
//	m_usFilePort		= CX_PORT_FILE;
	m_usCommandPort	= CX_PORT_CMD;
	m_usStatusPort	= CX_PORT_STATUS;

	// port ranges
	m_usResourcePortMin		= CX_PORT_RESMIN;
	m_usResourcePortMax		= CX_PORT_RESMAX;

	m_usProcessPortMin		= CX_PORT_PRCMIN;
	m_usProcessPortMax		= CX_PORT_PRCMAX;

	// messaging for Cortex
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;

	// database support, interactive menus, settings in CSF file overridden by DB
	m_bUseDB = false; 
	m_nSparkStaggerIntervalMS = 1000;

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;
	m_pszMessages = NULL;
	m_pszExchange = NULL;
	m_pszSettings = NULL;
	// messaging for remote objects
//	m_bUseLogRemote = false;	// write a separate log file for each object
	m_pdb = NULL;
	m_pdbConn = NULL;

	//admin
//	m_bUseAuthentication = true;	// user/password verification, otherwise totally open to all clients
//	m_pszUserName = NULL;  // cortex master username
//	m_pszPassword = NULL;		// cortex master password
//	m_pszSecurityFile1 = NULL;
//	m_pszSecurityFile2 = NULL;

	// backup
	m_bUseClone = false;			// spark a clone (unless a clone itself)
	m_ulModsIntervalMS = 1000;
	m_pszPeriodicQueries = NULL;
	m_bPeriodicQueries = false;
	m_bPeriodicShell=false;
	m_pszPeriodicShellCmds = NULL;  // a pipe-delimited string indicating intervals and shell commands (ShellExecute)
	m_bPeriodicHttp = false;
	m_pszPeriodicHttp = NULL;  // a pipe-delimited string indicating intervals and HTTP gets to run...  results stored in m_pszPeriodicHttpResult
	m_pszPeriodicHttpResult = NULL;

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

}

CCortexSettings::~CCortexSettings()
{
	// DSN params
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
//	if(m_pszUserName) free(m_pszUserName); // must use malloc to allocate
//	if(m_pszPassword) free(m_pszPassword); // must use malloc to allocate
//	if(m_pszSecurityFile1) free(m_pszSecurityFile1); // must use malloc to allocate
//	if(m_pszSecurityFile2) free(m_pszSecurityFile2); // must use malloc to allocate
	if(m_pszPeriodicQueries) free(m_pszPeriodicQueries); // must use malloc to allocate
	if(m_pszPeriodicShellCmds) free(m_pszPeriodicShellCmds); // must use malloc to allocate
	if(m_pszPeriodicHttp) free(m_pszPeriodicHttp); // must use malloc to allocate
	if(m_pszPeriodicHttpResult) free(m_pszPeriodicHttpResult); // must use malloc to allocate

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate	
}

int CCortexSettings::Settings(bool bRead)
{
	// get settings.
	char pszFilename[MAX_PATH];
	char* pszParams = NULL;

	strcpy(pszFilename, "");
//AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }

	if(strlen(pszFilename)<=0)  strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file

//AfxMessageBox(pszFilename);
	CFileUtil file;
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Cortex", m_pszName);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			// recompile license key params
			if(g_pcortex->m_data.m_key.m_pszLicenseString) free(g_pcortex->m_data.m_key.m_pszLicenseString);
			g_pcortex->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pcortex->m_data.m_key.m_pszLicenseString)
			sprintf(g_pcortex->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pcortex->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pcortex->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pcortex->m_data.m_key.m_ulNumParams)
				{
					if((g_pcortex->m_data.m_key.m_ppszParams)
						&&(g_pcortex->m_data.m_key.m_ppszValues)
						&&(g_pcortex->m_data.m_key.m_ppszParams[i])
						&&(g_pcortex->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pcortex->m_data.m_key.m_ppszParams[i], "max")==0)
						{
//							g_pcortex->m_data.m_nMaxLicensedChannels = atoi(g_pcortex->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_pcortex->m_data.m_key.m_bExpires)
						||((g_pcortex->m_data.m_key.m_bExpires)&&(!g_pcortex->m_data.m_key.m_bExpired))
						||((g_pcortex->m_data.m_key.m_bExpires)&&(g_pcortex->m_data.m_key.m_bExpireForgiveness)&&(g_pcortex->m_data.m_key.m_ulExpiryDate+g_pcortex->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pcortex->m_data.m_key.m_bMachineSpecific)
						||((g_pcortex->m_data.m_key.m_bMachineSpecific)&&(g_pcortex->m_data.m_key.m_bValidMAC))
						)
					)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
					g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
			}


			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
//			m_pszName = file.GetIniString("Main", "Name", "Cortex", m_pszName);
	//		m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "", m_pszIconPath);
			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Cortex|YD||1|", m_pszFileSpec);

//			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);			
			// dont decompile license here, it may be getting reset below.  if it's the same, then no need to re-do it anyway.
			
			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

			m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
			m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

			m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
			m_usProcessPortMax = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
	//		m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
			m_bUseClone = file.GetIniInt("Mode", "UseClone", 0)?true:false;
			m_bUseDB = file.GetIniInt("Mode", "UseDB", 0)?true:false;

			m_nSparkStaggerIntervalMS = file.GetIniInt("Timing", "SparkInterval", 1000);
			
			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Cortex"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
	//		m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", m_pszName?m_pszName:"CortexBackup");
	//		m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
	//		m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			m_bPeriodicQueries = file.GetIniInt("Database", "UsePeriodicQueries", 0)?true:false; 
			m_bPeriodicShell = file.GetIniInt("Database", "UsePeriodicShellCmds", 0)?true:false; 
			m_bPeriodicHttp = file.GetIniInt("Database", "UsePeriodicHttp", 0)?true:false; 

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

//			AfxMessageBox("got m_bPeriodicQueries");

			if(m_bPeriodicQueries)
			{
//			AfxMessageBox("m_bPeriodicQueries was true");
	EnterCriticalSection(&g_pcortex->m_data.m_critQueries);
				if(m_pszPeriodicQueries) free(m_pszPeriodicQueries); m_pszPeriodicQueries=NULL;

				if(g_pcortex->m_data.m_ppPeriodicQuery)
				{
					int q=0;
					while(q<g_pcortex->m_data.m_nNumQueries)
					{
						if(g_pcortex->m_data.m_ppPeriodicQuery[q]) delete g_pcortex->m_data.m_ppPeriodicQuery[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pcortex->m_data.m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate

				}
				g_pcortex->m_data.m_ppPeriodicQuery = NULL;
				g_pcortex->m_data.m_nNumQueries=0;

				m_pszPeriodicQueries = file.GetIniString("Database", "PeriodicQueries", "", m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
//				AfxMessageBox("got m_pszPeriodicQueries"); AfxMessageBox(m_pszPeriodicQueries);

				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicQueries)&&(strlen(m_pszPeriodicQueries)))	pszParams = (char*)malloc(strlen(m_pszPeriodicQueries)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicQueries);

//				AfxMessageBox("got pszParams"); AfxMessageBox(pszParams);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
//									AfxMessageBox(pQuery->m_pszObject);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pcortex->m_data.m_nNumQueries+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pcortex->m_data.m_ppPeriodicQuery)&&(g_pcortex->m_data.m_nNumQueries))
										{
											while(npq<g_pcortex->m_data.m_nNumQueries)
											{
												ppQueries[npq] = g_pcortex->m_data.m_ppPeriodicQuery[npq];
												npq++;
											}
											delete [] g_pcortex->m_data.m_ppPeriodicQuery;
										}
										ppQueries[npq] = pQuery;
										g_pcortex->m_data.m_nNumQueries = npq+1;
										g_pcortex->m_data.m_ppPeriodicQuery = ppQueries;
									}
									else
										delete pQuery;
								}
								else 
									delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pcortex->m_data.m_critQueries);

			}
//			else	AfxMessageBox("m_bPeriodicQueries was false");

			if(m_bPeriodicShell)
			{
	EnterCriticalSection(&g_pcortex->m_data.m_critShell);
				if(m_pszPeriodicShellCmds) free(m_pszPeriodicShellCmds); m_pszPeriodicShellCmds=NULL;

				if(g_pcortex->m_data.m_ppPeriodicShell)
				{
					int q=0;
					while(q<g_pcortex->m_data.m_nNumShellCmds)
					{
						if(g_pcortex->m_data.m_ppPeriodicShell[q]) delete g_pcortex->m_data.m_ppPeriodicShell[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pcortex->m_data.m_ppPeriodicShell; // delete array of pointers to objects, must use new to allocate
				}
				g_pcortex->m_data.m_ppPeriodicShell = NULL;
				g_pcortex->m_data.m_nNumShellCmds = 0;

				m_pszPeriodicShellCmds = file.GetIniString("Database", "PeriodicShellCmds", "", m_pszPeriodicShellCmds);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.

//				AfxMessageBox(m_pszPeriodicShellCmds);
				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicShellCmds)&&(strlen(m_pszPeriodicShellCmds)))	pszParams = (char*)malloc(strlen(m_pszPeriodicShellCmds)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicShellCmds);

//				AfxMessageBox("got m_pszPeriodicShellCmds"); AfxMessageBox(m_pszPeriodicShellCmds);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pcortex->m_data.m_nNumShellCmds+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pcortex->m_data.m_ppPeriodicShell)&&(g_pcortex->m_data.m_nNumShellCmds))
										{
											while(npq<g_pcortex->m_data.m_nNumShellCmds)
											{
												ppQueries[npq] = g_pcortex->m_data.m_ppPeriodicShell[npq];
												npq++;
											}
											delete [] g_pcortex->m_data.m_ppPeriodicShell;
										}
										ppQueries[npq] = pQuery;
										g_pcortex->m_data.m_nNumShellCmds = npq+1;
										g_pcortex->m_data.m_ppPeriodicShell = ppQueries;
									}
									else delete pQuery;
								}
								else delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pcortex->m_data.m_critShell);

			}
			if(m_bPeriodicHttp)
			{
	EnterCriticalSection(&g_pcortex->m_data.m_critHttp);
				if(m_pszPeriodicHttp) free(m_pszPeriodicHttp); m_pszPeriodicHttp=NULL;

				if(g_pcortex->m_data.m_ppPeriodicHttp)
				{
					int q=0;
					while(q<g_pcortex->m_data.m_nNumHttpCalls)
					{
						if(g_pcortex->m_data.m_ppPeriodicHttp[q]) delete g_pcortex->m_data.m_ppPeriodicHttp[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pcortex->m_data.m_ppPeriodicHttp; // delete array of pointers to objects, must use new to allocate
				}
				g_pcortex->m_data.m_ppPeriodicHttp = NULL;
				g_pcortex->m_data.m_nNumHttpCalls = 0;

				m_pszPeriodicHttp = file.GetIniString("Database", "PeriodicHttp", "", m_pszPeriodicHttp);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicHttp)&&(strlen(m_pszPeriodicHttp)))	pszParams = (char*)malloc(strlen(m_pszPeriodicHttp)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicHttp);

//				AfxMessageBox("got m_pszPeriodicHttp"); AfxMessageBox(m_pszPeriodicHttp);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pcortex->m_data.m_nNumHttpCalls+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pcortex->m_data.m_ppPeriodicHttp)&&(g_pcortex->m_data.m_nNumHttpCalls))
										{
											while(npq<g_pcortex->m_data.m_nNumHttpCalls)
											{
												ppQueries[npq] = g_pcortex->m_data.m_ppPeriodicHttp[npq];
												npq++;
											}
											delete [] g_pcortex->m_data.m_ppPeriodicHttp;
										}
										ppQueries[npq] = pQuery;
										g_pcortex->m_data.m_nNumHttpCalls = npq+1;
										g_pcortex->m_data.m_ppPeriodicHttp = ppQueries;
									}
									else delete pQuery;
								}
								else delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pcortex->m_data.m_critHttp);

			}

			pszParams = file.GetIniString("FileServer", "SettingsURL", "default.asp", pszParams);
			if(pszParams)
			{
				if(g_pcortex->m_data.m_pszHost)
				{
					char pszPath[MAX_PATH];
					if(theApp.m_pszSettingsURL) free(theApp.m_pszSettingsURL);
					_snprintf(pszPath, MAX_PATH, "http://%s/%s", g_pcortex->m_data.m_pszHost, pszParams);  
					theApp.m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(theApp.m_pszSettingsURL) strcpy(theApp.m_pszSettingsURL, pszPath);
				}

				free(pszParams);
			}


		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("License", "Key", m_pszLicense);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Resources", "MinPort", m_usResourcePortMin);
			file.SetIniInt("Resources", "MaxPort", m_usResourcePortMax);

			file.SetIniInt("Processes", "MinPort", m_usProcessPortMin);
			file.SetIniInt("Processes", "MaxPort", m_usProcessPortMax);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
	//		file.SetIniInt("Messager", "LogRemote", m_bUseLogRemote?1:0);
			file.SetIniInt("Mode", "UseClone", m_bUseClone?1:0);
			file.SetIniInt("Mode", "UseDB", m_bUseDB?1:0);

			file.SetIniInt("Timing", "SparkInterval", m_nSparkStaggerIntervalMS);

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
	//		file.SetIniString("Database", "BackupDSN", m_pszBackupDSN);
	//		file.SetIniString("Database", "BackupDBUser", m_pszBackupUser);
	//		file.SetIniString("Database", "BackupDBPassword", m_pszBackupPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "UsePeriodicQueries", m_bPeriodicQueries?1:0);  
			file.SetIniInt("Database", "UsePeriodicShellCmds", m_bPeriodicShell?1:0);  
			file.SetIniInt("Database", "UsePeriodicHttp", m_bPeriodicHttp?1:0);  

			if((m_bPeriodicQueries)&&(m_pszPeriodicQueries))
			{
//				AfxMessageBox("setting m_pszPeriodicQueries"); AfxMessageBox(m_pszPeriodicQueries);
				file.SetIniString("Database", "PeriodicQueries", m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
			}
			if((m_bPeriodicShell)&&(m_pszPeriodicShellCmds))
			{
//				AfxMessageBox("setting m_pszPeriodicShellCmds"); AfxMessageBox(m_pszPeriodicShellCmds);
				file.SetIniString("Database", "PeriodicShellCmds", m_pszPeriodicShellCmds);    // a pipe-delimited string indicating intervals and cmds
			}
			if((m_bPeriodicHttp)&&(m_pszPeriodicHttp))
			{
//				AfxMessageBox("setting m_pszPeriodicHttp"); AfxMessageBox(m_pszPeriodicHttp);
				file.SetIniString("Database", "PeriodicHttp", m_pszPeriodicHttp);    // a pipe-delimited string indicating intervals and http calls
			}

			file.SetSettings(pszFilename, false);  

		}
		return CX_SUCCESS;
	}
	return CX_ERROR;
}

int CCortexSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==CX_SUCCESS))  //read has to succeed
	{

/*
		// get settings.
		char pszFilename[MAX_PATH];
		char* pszParams = NULL;

		if(g_pcortex->m_settings.m_ulMainMode&CX_MODE_CLONE)
			strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
		else
		if(g_pcortex->m_settings.m_ulMainMode&CX_MODE_LISTENER)
			strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
		else  // default
			strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			// get settings
			g_pcortex->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_pcortex->m_settings.m_pszName = file.GetIniString("Main", "Name", "Cortex");
	//		g_pcortex->m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);

			g_pcortex->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");
			g_pcortex->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");			
			// dont decompile license here, it may be getting reset below.  if it's the same, then no need to re-do it anyway.
			
			g_pcortex->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
			g_pcortex->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

			g_pcortex->m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
			g_pcortex->m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

			g_pcortex->m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
			g_pcortex->m_settings.m_usProcessPortMax = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

			g_pcortex->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_pcortex->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
	//		g_pcortex->m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
			g_pcortex->m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 0)?true:false;
			g_pcortex->m_settings.m_bUseDB = file.GetIniInt("Mode", "UseDB", 0)?true:false;

			g_pcortex->m_settings.m_nSparkStaggerIntervalMS = file.GetIniInt("Timing", "SparkInterval", 1000);
			
			g_pcortex->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex");
			g_pcortex->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_pcortex->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
	//		g_pcortex->m_settings.m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"CortexBackup");
	//		g_pcortex->m_settings.m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
	//		g_pcortex->m_settings.m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
			g_pcortex->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_pcortex->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_pcortex->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
			g_pcortex->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			g_pcortex->m_settings.m_bPeriodicQueries = file.GetIniInt("Database", "UsePeriodicQueries", 0)?true:false; 
			if(g_pcortex->m_settings.m_bPeriodicQueries)
			{
				if(g_pcortex->m_settings.m_pszPeriodicQueries) free(g_pcortex->m_settings.m_pszPeriodicQueries);

				if(g_pcortex->m_data.m_ppPeriodicQuery)
				{
					int i=0;
					while(i<g_pcortex->m_data.m_nNumQueries)
					{
						if(g_pcortex->m_data.m_ppPeriodicQuery[i]) delete g_pcortex->m_data.m_ppPeriodicQuery[i]; // delete objects, must use new to allocate
						i++;
					}
					delete [] g_pcortex->m_data.m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate
				}
				g_pcortex->m_data.m_ppPeriodicQuery = NULL;

				g_pcortex->m_settings.m_pszPeriodicQueries = file.GetIniString("Database", "PeriodicQueries", "600|sp_PrimaryToBackup");    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
				CSafeBufferUtil sbu;
				char* pch = sbu.Token(g_pcortex->m_settings.m_pszPeriodicQueries, strlen(g_pcortex->m_settings.m_pszPeriodicQueries), "|");
				while(pch)
				{
					unsigned long ulInterval = atol(pch);
					unsigned long ulOffset=0;   // number of seconds to offset
					pch = strchr(pch, '+');  //search for offset
					if(pch)
					{
						pch++;
						ulOffset=atol(pch);
					}

					pch = sbu.Token(NULL, NULL, "|");

					if((pch)&&(strlen(pch)>0))
					{
						CPeriodicQuery* pQuery = new CPeriodicQuery;
						if(pQuery)
						{
							pQuery->m_pszQuery = (char*)malloc(strlen(pch)+1);
							if(pQuery->m_pszQuery)
							{
								strcpy(pQuery->m_pszQuery, pch);
								pQuery->m_ulInterval = ulInterval; // number of seconds between calls
								pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

								CPeriodicQuery** ppQueries = new CPeriodicQuery*[g_pcortex->m_data.m_nNumQueries+1];
								if(ppQueries)
								{
									int i=0;
									if((g_pcortex->m_data.m_ppPeriodicQuery)&&(g_pcortex->m_data.m_nNumQueries))
									{
										while(i<g_pcortex->m_data.m_nNumQueries)
										{
											ppQueries[i] = g_pcortex->m_data.m_ppPeriodicQuery[i];
											i++;
										}
										delete [] g_pcortex->m_data.m_ppPeriodicQuery;
									}
									ppQueries[i] = pQuery;
									g_pcortex->m_data.m_nNumQueries = i+1;
									g_pcortex->m_data.m_ppPeriodicQuery = ppQueries;
								}
								else delete pQuery;
							}
							else delete pQuery;
						}
					}
					pch = sbu.Token(NULL, NULL, "|");
				}
			}

			pszParams = file.GetIniString("FileServer", "SettingsURL", "default.asp");
			if(pszParams)
			{
				if(g_pcortex->m_data.m_pszHost)
				{
					char pszPath[MAX_PATH];
					if(theApp.m_pszSettingsURL) free(theApp.m_pszSettingsURL);
					_snprintf(pszPath, MAX_PATH, "http://%s/%s", g_pcortex->m_data.m_pszHost, pszParams);  
					theApp.m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(theApp.m_pszSettingsURL) strcpy(theApp.m_pszSettingsURL, pszPath);
				}

				free(pszParams);
			}

		}
*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = CX_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pcortex->m_data.m_key.m_pszLicenseString) free(g_pcortex->m_data.m_key.m_pszLicenseString);
								g_pcortex->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pcortex->m_data.m_key.m_pszLicenseString)
								sprintf(g_pcortex->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pcortex->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pcortex->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pcortex->m_data.m_key.m_ulNumParams)
									{
										if((g_pcortex->m_data.m_key.m_ppszParams)
											&&(g_pcortex->m_data.m_key.m_ppszValues)
											&&(g_pcortex->m_data.m_key.m_ppszParams[i])
											&&(g_pcortex->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pcortex->m_data.m_key.m_ppszParams[i], "max")==0)
											{
								//				g_pcortex->m_data.m_nMaxLicensedDevices = atoi(g_pcortex->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pcortex->m_data.m_key.m_bExpires)
											||((g_pcortex->m_data.m_key.m_bExpires)&&(!g_pcortex->m_data.m_key.m_bExpired))
											||((g_pcortex->m_data.m_key.m_bExpires)&&(g_pcortex->m_data.m_key.m_bExpireForgiveness)&&(g_pcortex->m_data.m_key.m_ulExpiryDate+g_pcortex->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pcortex->m_data.m_key.m_bMachineSpecific)
											||((g_pcortex->m_data.m_key.m_bMachineSpecific)&&(g_pcortex->m_data.m_key.m_bValidMAC))
											)
										)
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
										g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pcortex->m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("UsePeriodicQueries")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bPeriodicQueries = true;
							else m_bPeriodicQueries = false;
						}
					}
/*
					else
					if(szParameter.CompareNoCase("PeriodicQueries")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszPeriodicQueries)
								{
									free(m_pszPeriodicQueries);
									if(g_pcortex->m_data.m_ppPeriodicQuery)
									{
										int i=0;
										while(i<g_pcortex->m_data.m_nNumQueries)
										{
											if(g_pcortex->m_data.m_ppPeriodicQuery[i]) delete g_pcortex->m_data.m_ppPeriodicQuery[i]; // delete objects, must use new to allocate
											i++;
										}
										delete [] g_pcortex->m_data.m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate
										g_pcortex->m_data.m_ppPeriodicQuery=NULL;
									}
									g_pcortex->m_data.m_nNumQueries=0;
								}
								m_pszPeriodicQueries = pch;
								CSafeBufferUtil sbu;
								pch = sbu.Token(m_pszPeriodicQueries, strlen(m_pszPeriodicQueries), "|");
								while(pch)
								{
									unsigned long ulInterval = atol(pch);
									unsigned long ulOffset=0;   // number of seconds to offset
									pch = strchr(pch, '+');  //search for offset
									if(pch)
									{
										pch++;
										ulOffset=atol(pch);
									}

									pch = sbu.Token(NULL, NULL, "|");

									if((pch)&&(strlen(pch)>0))
									{
										CPeriodicObject* pQuery = new CPeriodicObject;
										if(pQuery)
										{
											pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
											if(pQuery->m_pszObject)
											{
												strcpy(pQuery->m_pszObject, pch);
												pQuery->m_ulInterval = ulInterval; // number of seconds between calls
												pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

												CPeriodicObject** ppQueries = new CPeriodicObject*[g_pcortex->m_data.m_nNumQueries+1];
												if(ppQueries)
												{
													int i=0;
													if((g_pcortex->m_data.m_ppPeriodicQuery)&&(g_pcortex->m_data.m_nNumQueries))
													{
														while(i<g_pcortex->m_data.m_nNumQueries)
														{
															ppQueries[i] = g_pcortex->m_data.m_ppPeriodicQuery[i];
															i++;
														}
														delete [] g_pcortex->m_data.m_ppPeriodicQuery;
													}
													ppQueries[i] = pQuery;
													g_pcortex->m_data.m_nNumQueries = i+1;

												}
												else delete pQuery;
											}
											else delete pQuery;
										}
									}
									pch = sbu.Token(NULL, NULL, "|");
								}
							}
						}
					}
					*/
				}


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;

			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
		//		file.SetIniInt("FileServer", "ListenPort", g_pcortex->m_settings.m_usFilePort);
				file.SetIniString("License", "Key", g_pcortex->m_settings.m_pszLicense);
				file.SetIniInt("CommandServer", "ListenPort", g_pcortex->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_pcortex->m_settings.m_usStatusPort);
				file.SetIniString("FileServer", "IconPath", g_pcortex->m_settings.m_pszIconPath);

				file.SetIniInt("Resources", "MinPort", g_pcortex->m_settings.m_usResourcePortMin);
				file.SetIniInt("Resources", "MaxPort", g_pcortex->m_settings.m_usResourcePortMax);

				file.SetIniInt("Processes", "MinPort", g_pcortex->m_settings.m_usProcessPortMin);
				file.SetIniInt("Processes", "MaxPort", g_pcortex->m_settings.m_usProcessPortMax);

				file.SetIniInt("Messager", "UseEmail", g_pcortex->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_pcortex->m_settings.m_bUseNetwork?1:0);
		//		file.SetIniInt("Messager", "LogRemote", g_pcortex->m_settings.m_bUseLogRemote?1:0);
				file.SetIniInt("Mode", "UseClone", g_pcortex->m_settings.m_bUseClone?1:0);
				file.SetIniInt("Mode", "UseDB", g_pcortex->m_settings.m_bUseDB?1:0);

				file.SetIniInt("Timing", "SparkInterval", g_pcortex->m_settings.m_nSparkStaggerIntervalMS);

				file.SetIniString("Database", "DSN", g_pcortex->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_pcortex->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_pcortex->m_settings.m_pszPW);
		//		file.SetIniString("Database", "BackupDSN", g_pcortex->m_settings.m_pszBackupDSN);
		//		file.SetIniString("Database", "BackupDBUser", g_pcortex->m_settings.m_pszBackupUser);
		//		file.SetIniString("Database", "BackupDBPassword", g_pcortex->m_settings.m_pszBackupPW);
				file.SetIniString("Database", "SettingsTableName", g_pcortex->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_pcortex->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_pcortex->m_settings.m_pszMessages);  // the Messages table name
				file.SetIniInt("Database", "ModificationCheckInterval", g_pcortex->m_settings.m_ulModsIntervalMS);  // in milliseconds
				file.SetIniInt("Database", "UsePeriodicQueries", g_pcortex->m_settings.m_bPeriodicQueries?1:0);  
				file.SetIniString("Database", "PeriodicQueries", g_pcortex->m_settings.m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.

				if(g_pcortex->m_settings.m_ulMainMode&CX_MODE_CLONE)
					strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
				else
				if(g_pcortex->m_settings.m_ulMainMode&CX_MODE_LISTENER)
					strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
				else  // default
					strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file
				file.SetSettings(pszFilename, false);  

			}
*/


			return CX_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return CX_ERROR;
}

