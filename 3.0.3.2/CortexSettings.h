// CortexSettings.h: interface for the CCortexSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_CORTEXSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "CortexDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CCortexSettings  
{
public:
	CCortexSettings();
	virtual ~CCortexSettings();
	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);

	char* m_pszName;  // familiar name of this instance.
//	unsigned long m_ulMainMode;

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	// ports
//	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// port ranges
	unsigned short m_usResourcePortMin;
	unsigned short m_usResourcePortMax;

	unsigned short m_usProcessPortMin;
	unsigned short m_usProcessPortMax;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Cortex
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	char* m_pszFileSpec;

	// database support, interactive menus, settings in CSF file overridden by DB
	bool m_bUseDB; 
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;


	// messaging for remote objects
//	bool m_bUseLogRemote;	// write a separate log file for each object

	int m_nSparkStaggerIntervalMS;

	char* m_pszMessages;
	char* m_pszExchange;
	char* m_pszSettings;

	bool  m_bPeriodicQueries;
	char* m_pszPeriodicQueries;  // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
	bool  m_bPeriodicShell;
	char* m_pszPeriodicShellCmds;  // a pipe-delimited string indicating intervals and shell commands (ShellExecute)
	bool  m_bPeriodicHttp;
	char* m_pszPeriodicHttp;  // a pipe-delimited string indicating intervals and HTTP gets to run...  results stored in m_pszPeriodicHttpResult
	char* m_pszPeriodicHttpResult;

	//admin
//	bool m_bUseAuthentication;	// user/password verification, otherwise totally open to all clients
//	char* m_pszUserName;  // cortex master username
//	char* m_pszPassword;	// cortex master password
//	char* m_pszSecurityFile1;
//	char* m_pszSecurityFile2;

	// self backup
	bool m_bUseClone;			// spark a clone (unless a clone itself)

	// aux backup
	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

};

#endif // !defined(AFX_CORTEXSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
