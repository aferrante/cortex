
CortexServiceps.dll: dlldata.obj CortexService_p.obj CortexService_i.obj
	link /dll /out:CortexServiceps.dll /def:CortexServiceps.def /entry:DllMain dlldata.obj CortexService_p.obj CortexService_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del CortexServiceps.dll
	@del CortexServiceps.lib
	@del CortexServiceps.exp
	@del dlldata.obj
	@del CortexService_p.obj
	@del CortexService_i.obj
