// CortexConsole.h : main header file for the CORTEXCONSOLE application
//

#if !defined(AFX_CORTEXCONSOLE_H__D237B287_1C20_4ADE_9763_7BE4B46E1D72__INCLUDED_)
#define AFX_CORTEXCONSOLE_H__D237B287_1C20_4ADE_9763_7BE4B46E1D72__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleApp:
// See CortexConsole.cpp for the implementation of this class
//

class CCortexConsoleApp : public CWinApp
{
public:
	CCortexConsoleApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexConsoleApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCortexConsoleApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONSOLE_H__D237B287_1C20_4ADE_9763_7BE4B46E1D72__INCLUDED_)
