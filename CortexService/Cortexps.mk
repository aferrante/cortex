
Cortexps.dll: dlldata.obj Cortex_p.obj Cortex_i.obj
	link /dll /out:Cortexps.dll /def:Cortexps.def /entry:DllMain dlldata.obj Cortex_p.obj Cortex_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del Cortexps.dll
	@del Cortexps.lib
	@del Cortexps.exp
	@del dlldata.obj
	@del Cortex_p.obj
	@del Cortex_i.obj
