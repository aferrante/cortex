// Cortex ConsoleDlg.h : header file
//

#if !defined(AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_)
#define AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CCortexConsoleDlg dialog

class CCortexConsoleDlg : public CDialog
{
// Construction
public:
	CCortexConsoleDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCortexConsoleDlg)
	enum { IDD = IDD_CORTEXCONSOLE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexConsoleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCortexConsoleDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONSOLEDLG_H__5BB59B7D_1798_4565_BEE1_D3318CC6E628__INCLUDED_)
