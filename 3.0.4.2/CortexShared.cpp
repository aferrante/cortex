// CortexShared.cpp: implementation of the CCortexMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Cortex.h"
#include "CortexShared.h"
#include <atlbase.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CCortexMessage Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMessage::CCortexMessage()
{
	int b=CX_XML_BUFFER_CORTEXBEGIN;
	while(b<CX_XML_BUFFER_COUNT)
	{
		m_pchResponse[b] = NULL;
		b++;
	}
	m_nTxStep =  CX_XML_BUFFER_CORTEXBEGIN;
	m_nFailure = CX_XML_ERR_SUCCESS;
	m_nType =    CX_XML_TYPE_UNK;
	m_pchType = NULL;
}

CCortexMessage::~CCortexMessage()
{
//	AfxMessageBox("foo");
	int b=CX_XML_BUFFER_CORTEXBEGIN;
	while(b<CX_XML_BUFFER_COUNT)
	{
		if(m_pchResponse[b])
		{
			try {free(m_pchResponse[b]);}
			catch(...) {}
		}
		b++;
	}
//	AfxMessageBox("foop");
	if(m_pchType)
	{
		try {free(m_pchType);}
		catch(...) {}
	}
//	AfxMessageBox("foox");
}

int CCortexMessage::FormatContent(int nBufferIndex, int nBufferMax, char* pchContent, ...)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT)&&(pchContent))
	{
		char* pchMessage = (char*)malloc(nBufferMax+1);
		if(pchMessage)
		{
			va_list marker;
			// create the formatted output string
			va_start(marker, pchContent); // Initialize variable arguments.
			_vsnprintf(pchMessage, nBufferMax, pchContent, (va_list) marker);
			va_end( marker );             // Reset variable arguments.

			if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
			m_pchResponse[nBufferIndex] = (char*) malloc(strlen(pchMessage)+1);
			if(m_pchResponse[nBufferIndex])
			{
				strcpy(m_pchResponse[nBufferIndex], pchMessage);
				try { free(pchMessage); } catch(...){}
				return CX_SUCCESS;
			}
			else
			{
				try { free(pchMessage); } catch(...){}
			}
		}
	}
	return CX_ERROR;
}


int CCortexMessage::SetType(char* pchContent)
{
	if(pchContent)
	{
		if(m_pchType)	{	try { free(m_pchType); } catch(...){}	}
		m_pchType = (char*) malloc(strlen(pchContent)+1);
		if(m_pchType)
		{
			strcpy(m_pchType, pchContent);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int CCortexMessage::SetContent(int nBufferIndex, char* pchContent)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT)&&(pchContent))
	{
		if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
		m_pchResponse[nBufferIndex] = (char*) malloc(strlen(pchContent)+1);
		if(m_pchResponse[nBufferIndex])
		{
			strcpy(m_pchResponse[nBufferIndex], pchContent);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

int CCortexMessage::ClearContent(int nBufferIndex)
{
	if((nBufferIndex>=CX_XML_BUFFER_CORTEXBEGIN)&&(nBufferIndex<CX_XML_BUFFER_COUNT))
	{
		if(m_pchResponse[nBufferIndex])	{	try { free(m_pchResponse[nBufferIndex]); } catch(...){}	}
		m_pchResponse[nBufferIndex] = NULL; // must assign NULL!
		return CX_SUCCESS;
	}
	return CX_ERROR;
}


char* CCortexMessage::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	char* pchText = NULL;
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					
					char* pchNode = W2T(_bstr_t(pValue->GetnodeValue()));
					if(pchNode)
					{
						pchText = (char*)malloc(strlen(pchNode)+1);
						if(pchText) strcpy(pchText, pchNode);
					}
				}
			}
		}
	}
	return pchText;
}


CCortexUtil::CCortexUtil()
{
}

CCortexUtil::~CCortexUtil()
{
}

char* CCortexUtil::ConvertWideToT(void* pbstrIn)
{
	// this function expects pbstrIn to actually be the address of a _bstr_t
USES_CONVERSION;
	if(pbstrIn)
	{
		_bstr_t* pbstr = (_bstr_t*)pbstrIn;
		char* pch = W2T(*pbstr); // pch* goes out of scope and frees when this funtion returns
		if(pch)
		{
			char* pchReturn = (char*)malloc(strlen(pch)+1);
			{
				if(pchReturn)
				{
					strcpy(pchReturn, pch);
					return pchReturn;
				}
			}
		}
	}
	return NULL;
}


int CCortexUtil::ConvertWideToT(char* pchBuffer, unsigned long ulMaxBufLen, void* pbstrIn)
{
	int nReturn = CX_ERROR;
	char* pchTag = ConvertWideToT(pbstrIn);
	if(pchTag)
	{
		nReturn = CX_SUCCESS+((strlen(pchTag)>ulMaxBufLen)?1:0); // truncated
		strncpy(pchBuffer, pchTag, ulMaxBufLen);
		try{ free(pchTag); } catch(...){}
	}
	else 
	{
		strcpy(pchBuffer, "");
	}
	return nReturn;
}


