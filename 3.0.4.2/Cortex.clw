; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCortexSplashDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Cortex.h"
LastPage=0

ClassCount=6
Class1=CAboutDlg
Class2=CCortexApp
Class3=CCortexHandler

ResourceCount=4
Resource1=IDD_CORTEX_SPLASH_DIALOG
Resource2=IDD_ABOUTBOX
Class4=CCortexDlg
Class5=CCortexSettings
Resource3=IDD_CORTEX_DIALOG
Class6=CCortexSplashDlg
Resource4=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=cortexdlg.h
ImplementationFile=cortexdlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CCortexApp]
Type=0
BaseClass=CWinApp
HeaderFile=Cortex.h
ImplementationFile=Cortex.cpp
Filter=N
VirtualFilter=AC
LastObject=CCortexApp

[CLS:CCortexHandler]
Type=0
BaseClass=CWnd
HeaderFile=CortexHandler.h
ImplementationFile=CortexHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CCortexHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SETTINGS
Command3=ID_CMD_SHOWWND
Command4=ID_CMD_EXIT
Command5=ID_CMD_ABOUT
Command6=ID_CMD_SETTINGS
Command7=ID_CMD_SHOWWND
Command8=ID_CMD_EXIT
CommandCount=8

[CLS:CCortexDlg]
Type=0
HeaderFile=CortexDlg.h
ImplementationFile=CortexDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_TREE1

[CLS:CCortexSettings]
Type=0
HeaderFile=CortexSettings.h
ImplementationFile=CortexSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CCortexSettings

[DLG:IDD_CORTEX_DIALOG]
Type=1
Class=CCortexDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1342242944
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

[DLG:IDD_CORTEX_SPLASH_DIALOG]
Type=1
Class=CCortexSplashDlg
ControlCount=4
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC_STATUSTEXT,static,1342308352
Control4=IDC_STATIC,static,1342179331

[CLS:CCortexSplashDlg]
Type=0
HeaderFile=CortexSplashDlg.h
ImplementationFile=CortexSplashDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CCortexSplashDlg

