// CortexMain.h: interface for the CCortexMain class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
#define AFX_CORTEXMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../Common/MSG/msg.h"
#include "../../Common/MSG/Messager.h"
#include "../../Common/TXT/FileUtil.h"
//#include "../../Common/HTTP/HTTP10.h"  // includes CMessagingObject
#include "CortexSettings.h"
#include "CortexData.h"


// global message function so that dialogs etc (external objects) can write to the log etc.
//void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations);

// global main thread - this is the business right here!
void CortexMainThread(void* pvArgs);

// cortex http thread - we pass in the main cortex object, do all the cgi programming and security checking.
//void CortexHTTPThread(void* pvArgs);

void CortexCommandHandlerThread(void* pvArgs);
void CortexStatusHandlerThread(void* pvArgs);
void CortexXMLHandlerThread(void* pvArgs);


class CCortexMain  
{
public:
	CCortexMain();
	virtual ~CCortexMain();

	CMessager					m_msgr;				// main messager // the one and only messager object
	CCortexSettings		m_settings;		// main mem storage for settings.  settings file objects are local to the CortexMainThread.
	CCortexData				m_data;				// internal variables
	CNetUtil					m_net;				// have a separate one for the status and command services.

//	CHTTP10						m_http;				// the main http file server object -  has an internal CNetUtil object, but...
	// the security object is declared locally in the CortexMainThread thread, and a pointer to it is fed 
	// to the CHTTP10 object.

	//core http functions
//	char*	CortexTranslate(CHTTPHeader* pHeader, char* pszBuffer);				// apply cortex scripting language
//	int		InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo=NULL);		// parse cgi

	// core net functions
	SOCKET*		SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);  // cortex initiates a request to an object server
	int		SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex replies to an object server after receiving data. (usually ack or nak)
	int		SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex answers a request from an object client

	// move following to a Messager destination object...
	int SendMsg(int nType, char* pszSender, char* pszError, ...);
	char* ConvertWideToT(void* pbstrIn);

};

#endif // !defined(AFX_CORTEXMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
