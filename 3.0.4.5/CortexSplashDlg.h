#if !defined(AFX_CORTEXSPLASHDLG_H__76EFA0A6_10BF_44B4_86BA_5BD4EF613C07__INCLUDED_)
#define AFX_CORTEXSPLASHDLG_H__76EFA0A6_10BF_44B4_86BA_5BD4EF613C07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CortexSplashDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCortexSplashDlg dialog

class CCortexSplashDlg : public CDialog
{
// Construction
public:
	CCortexSplashDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCortexSplashDlg)
	enum { IDD = IDD_CORTEX_SPLASH_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexSplashDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCortexSplashDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXSPLASHDLG_H__76EFA0A6_10BF_44B4_86BA_5BD4EF613C07__INCLUDED_)
