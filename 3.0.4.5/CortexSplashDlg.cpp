// CortexSplashDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Cortex.h"
#include "CortexSplashDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCortexSplashDlg dialog


CCortexSplashDlg::CCortexSplashDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCortexSplashDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCortexSplashDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCortexSplashDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCortexSplashDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCortexSplashDlg, CDialog)
	//{{AFX_MSG_MAP(CCortexSplashDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexSplashDlg message handlers

BOOL CCortexSplashDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString szParams=_T("");//, szReport=_T("");

	// main Dlg Title
	szParams.Format("Cortex powered by VDS");
	SetWindowText(szParams);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CCortexSplashDlg::Create(CWnd* pParentWnd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::Create(CCortexSplashDlg::IDD, pParentWnd);
}

