/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Jul 10 11:50:31 2007
 */
/* Compiler settings for CortexCOM.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __CortexCOM_h__
#define __CortexCOM_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __CortexComm_FWD_DEFINED__
#define __CortexComm_FWD_DEFINED__

#ifdef __cplusplus
typedef class CortexComm CortexComm;
#else
typedef struct CortexComm CortexComm;
#endif /* __cplusplus */

#endif 	/* __CortexComm_FWD_DEFINED__ */


#ifndef __ICortexComm_FWD_DEFINED__
#define __ICortexComm_FWD_DEFINED__
typedef interface ICortexComm ICortexComm;
#endif 	/* __ICortexComm_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 


#ifndef __CORTEXCOMLib_LIBRARY_DEFINED__
#define __CORTEXCOMLib_LIBRARY_DEFINED__

/* library CORTEXCOMLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CORTEXCOMLib;

EXTERN_C const CLSID CLSID_CortexComm;

#ifdef __cplusplus

class DECLSPEC_UUID("600EE745-1930-48A3-9913-F951EEC3DF33")
CortexComm;
#endif

#ifndef __ICortexComm_INTERFACE_DEFINED__
#define __ICortexComm_INTERFACE_DEFINED__

/* interface ICortexComm */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICortexComm;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B2752F1-62C4-4912-9448-5F6A2157BB3C")
    ICortexComm : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE OnStartPage( 
            /* [in] */ IUnknown __RPC_FAR *piUnk) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnEndPage( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE cxSetContext( 
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BOOL __RPC_FAR *pbSuccess) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE cxExchange( 
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutParams) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE cxUtil( 
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE sqlRetrieve( 
            /* [in] */ BSTR bstrSQL,
            /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE sqlExecute( 
            /* [in] */ BSTR bstrSQL,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutMessage) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICortexCommVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            ICortexComm __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            ICortexComm __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            ICortexComm __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OnStartPage )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ IUnknown __RPC_FAR *piUnk);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OnEndPage )( 
            ICortexComm __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *cxSetContext )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BOOL __RPC_FAR *pbSuccess);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *cxExchange )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutParams);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *cxUtil )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ BSTR bstrInParams,
            /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *sqlRetrieve )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ BSTR bstrSQL,
            /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *sqlExecute )( 
            ICortexComm __RPC_FAR * This,
            /* [in] */ BSTR bstrSQL,
            /* [retval][out] */ BSTR __RPC_FAR *pbOutMessage);
        
        END_INTERFACE
    } ICortexCommVtbl;

    interface ICortexComm
    {
        CONST_VTBL struct ICortexCommVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICortexComm_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICortexComm_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICortexComm_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICortexComm_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICortexComm_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICortexComm_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICortexComm_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICortexComm_OnStartPage(This,piUnk)	\
    (This)->lpVtbl -> OnStartPage(This,piUnk)

#define ICortexComm_OnEndPage(This)	\
    (This)->lpVtbl -> OnEndPage(This)

#define ICortexComm_cxSetContext(This,bstrInParams,pbSuccess)	\
    (This)->lpVtbl -> cxSetContext(This,bstrInParams,pbSuccess)

#define ICortexComm_cxExchange(This,bstrInParams,pbOutParams)	\
    (This)->lpVtbl -> cxExchange(This,bstrInParams,pbOutParams)

#define ICortexComm_cxUtil(This,bstrInParams,ppReturnRS)	\
    (This)->lpVtbl -> cxUtil(This,bstrInParams,ppReturnRS)

#define ICortexComm_sqlRetrieve(This,bstrSQL,ppReturnRS)	\
    (This)->lpVtbl -> sqlRetrieve(This,bstrSQL,ppReturnRS)

#define ICortexComm_sqlExecute(This,bstrSQL,pbOutMessage)	\
    (This)->lpVtbl -> sqlExecute(This,bstrSQL,pbOutMessage)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE ICortexComm_OnStartPage_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ IUnknown __RPC_FAR *piUnk);


void __RPC_STUB ICortexComm_OnStartPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


HRESULT STDMETHODCALLTYPE ICortexComm_OnEndPage_Proxy( 
    ICortexComm __RPC_FAR * This);


void __RPC_STUB ICortexComm_OnEndPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexComm_cxSetContext_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ BSTR bstrInParams,
    /* [retval][out] */ BOOL __RPC_FAR *pbSuccess);


void __RPC_STUB ICortexComm_cxSetContext_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexComm_cxExchange_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ BSTR bstrInParams,
    /* [retval][out] */ BSTR __RPC_FAR *pbOutParams);


void __RPC_STUB ICortexComm_cxExchange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexComm_cxUtil_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ BSTR bstrInParams,
    /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS);


void __RPC_STUB ICortexComm_cxUtil_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexComm_sqlRetrieve_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ BSTR bstrSQL,
    /* [retval][out] */ /* external definition not present */ _Recordset __RPC_FAR *__RPC_FAR *ppReturnRS);


void __RPC_STUB ICortexComm_sqlRetrieve_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ICortexComm_sqlExecute_Proxy( 
    ICortexComm __RPC_FAR * This,
    /* [in] */ BSTR bstrSQL,
    /* [retval][out] */ BSTR __RPC_FAR *pbOutMessage);


void __RPC_STUB ICortexComm_sqlExecute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICortexComm_INTERFACE_DEFINED__ */

#endif /* __CORTEXCOMLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
