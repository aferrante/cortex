/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Jul 10 11:50:31 2007
 */
/* Compiler settings for CortexCOM.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID LIBID_CORTEXCOMLib = {0x9819793D,0x7D00,0x4532,{0xA4,0xD1,0x5C,0x7B,0x28,0x1E,0x8E,0x81}};


const CLSID CLSID_CortexComm = {0x600EE745,0x1930,0x48A3,{0x99,0x13,0xF9,0x51,0xEE,0xC3,0xDF,0x33}};


const IID IID_ICortexComm = {0x7B2752F1,0x62C4,0x4912,{0x94,0x48,0x5F,0x6A,0x21,0x57,0xBB,0x3C}};


#ifdef __cplusplus
}
#endif

