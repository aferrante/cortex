// CortexComm.h : Declaration of the CCortexComm

#ifndef __CORTEXCOMM_H_
#define __CORTEXCOMM_H_

#include "resource.h"       // main symbols
#include <asptlb.h>         // Active Server Pages Definitions
#include "..\..\Common\TXT\BufferUtil.h"
#include "..\..\Common\LAN\NetUtil.h"

class CCortexModuleInfo  // used in comms to remote objects
{
public:
	CString m_pszModuleName;  // name of the module we are addressing
	CString m_pszHost;				// ip address or host name
	int   m_nPort;					// port on which to communicate

public:
	CCortexModuleInfo();
	virtual ~CCortexModuleInfo();
};


/////////////////////////////////////////////////////////////////////////////
// CCortexComm
class ATL_NO_VTABLE CCortexComm : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CCortexComm, &CLSID_CortexComm>,
	public ISupportErrorInfo,
	public IDispatchImpl<ICortexComm, &IID_ICortexComm, &LIBID_CORTEXCOMLib>
{
public:
	CCortexComm()
	{ 
		m_szHost = _T("");
		m_bOnStartPageCalled = FALSE;
		m_usPort = 0xffff;
		m_bCSFset = FALSE;
		m_bStub = FALSE;
		m_nNumModules = 0;
		m_ppModules = NULL;
	}

public:

DECLARE_REGISTRY_RESOURCEID(IDR_CORTEXCOMM)
DECLARE_NOT_AGGREGATABLE(CCortexComm)

BEGIN_COM_MAP(CCortexComm)
	COM_INTERFACE_ENTRY(ICortexComm)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ICortexComm
public:
	STDMETHOD(cxSetContext)(/*[in]*/ BSTR bstrInParams, /*[out,retval]*/ BOOL* pbSuccess);  // sets encoded IP/port for target module (can be cortex itself)
	STDMETHOD(cxExchange)(/*[in]*/ BSTR bstrInParams, /*[out,retval]*/ BSTR* pbOutParams);  // sends/receives url encoded param-value pairs from module
	STDMETHOD(cxUtil)(BSTR bstrInParams, _Recordset** ppReturnRS);  // creates a recordset with utility information
	STDMETHOD(sqlExecute)(BSTR bstrSQL, BSTR* pbOutMessage);  // sends a SQL string to a module to execute on its private DB (if it exists)
	STDMETHOD(sqlRetrieve)(BSTR bstrSQL, _Recordset** ppReturnRS);  // retrieves a recordset from a module from its private DB (if it exists)
	//Active Server Pages Methods
	STDMETHOD(OnStartPage)(IUnknown* IUnk);
	STDMETHOD(OnEndPage)();

private:
	CComPtr<IRequest> m_piRequest;					//Request Object
	CComPtr<IResponse> m_piResponse;				//Response Object
	CComPtr<ISessionObject> m_piSession;			//Session Object
	CComPtr<IServer> m_piServer;					//Server Object
	CComPtr<IApplicationObject> m_piApplication;	//Application Object
	BOOL m_bOnStartPageCalled;						//OnStartPage successful?

// CCortexComm internal variables and functions
private:
	CString m_szHost;						//hostname or IP
	unsigned short m_usPort;		// port
	BOOL m_bCSFset;							// CSF mode explicitly set
	BOOL m_bStub;							// CSF mode explicitly set
	CBufferUtil m_bu;						// buffer manip util
	CNetUtil m_net;
	CCortexModuleInfo** m_ppModules;
	int m_nNumModules;
	char* GetEventField(char* pchEventStart, char* pchEventEnd, char* szKey);
	int GetModuleInfo();
	int ClearModuleInfo();
	int GetModuleIndex(CString pszName);

};

#endif //__CORTEXCOMM_H_
