// CortexComm.cpp : Implementation of CCortexComm
#include "stdafx.h"
//#include "afxdisp.h"
#include "CortexCOM.h"
#include "CortexComm.h"
#include "..\..\Common\HTTP\HTTP10.h"
#include "..\..\Common\TXT\FileUtil.h"
#include "..\3.0.3.2\CortexDefines.h"
#include "..\..\Common\KEY\LicenseKey.h"
//#include "..\..\Common\API\Omnibus\OmniParse.h" // too hard to do with all the dependencies.  just copy relevant code into this file.

class CCAEventData  // used in data field search
{
public:
	char* m_pszFieldName;  // unadorned, no tags.
	char* m_pchStartTag;   // start position of the start tag including "<"
	char* m_pchEndTag;     // position of the end of the end tag including ">", = start position of next data.
	char* m_pszFieldInfo;  // contents of the data field including start and end tags.

public:
	CCAEventData();
	virtual ~CCAEventData();
};





int g_nCortexPort=-1;  // listen port for cortex setup.
CString g_szCortexHost = _T("");


#define NUMPARAMS 32
/////////////////////////////////////////////////////////////////////////////
// CCortexComm

STDMETHODIMP CCortexComm::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ICortexComm,
	};
	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CCortexComm::OnStartPage (IUnknown* pUnk)  
{
	if(!pUnk)
		return E_POINTER;

	CComPtr<IScriptingContext> spContext;
	HRESULT hr;

	// Get the IScriptingContext Interface
	hr = pUnk->QueryInterface(IID_IScriptingContext, (void **)&spContext);
	if(FAILED(hr))
		return hr;

	// Get Request Object Pointer
	hr = spContext->get_Request(&m_piRequest);
	if(FAILED(hr))
	{
		spContext.Release();
		return hr;
	}

	// Get Response Object Pointer
	hr = spContext->get_Response(&m_piResponse);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		return hr;
	}
	
	// Get Server Object Pointer
	hr = spContext->get_Server(&m_piServer);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		return hr;
	}
	
	// Get Session Object Pointer
	hr = spContext->get_Session(&m_piSession);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		m_piServer.Release();
		return hr;
	}

	// Get Application Object Pointer
	hr = spContext->get_Application(&m_piApplication);
	if(FAILED(hr))
	{
		m_piRequest.Release();
		m_piResponse.Release();
		m_piServer.Release();
		m_piSession.Release();
		return hr;
	}
	m_bOnStartPageCalled = TRUE;
	return S_OK;
}

STDMETHODIMP CCortexComm::OnEndPage ()  
{
	m_bOnStartPageCalled = FALSE;
	// Release all interfaces
	m_piRequest.Release();
	m_piResponse.Release();
	m_piServer.Release();
	m_piSession.Release();
	m_piApplication.Release();

	return S_OK;
}


STDMETHODIMP CCortexComm::cxExchange( BSTR bstrInParams, BSTR* pbOutParams)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())


	if(pbOutParams==NULL) return E_POINTER;

	CString szInput, szOutput;

	szInput = (LPCWSTR)bstrInParams;


  if (bstrInParams != NULL)  
  {
		LPSTR   p = (char*)szInput.GetBuffer(::SysStringLen(bstrInParams) + 1);
		BOOL    bUsedDefaultChar;

		::WideCharToMultiByte(CP_ACP, 0, bstrInParams, -1, 
						p, ::SysStringLen(bstrInParams)+1, 
						NULL, &bUsedDefaultChar);
		szInput.ReleaseBuffer();

/*
	FILE* fp = fopen("CX.txt", "wb");
	if(fp)
	{
		fprintf(fp, "%s\r\n\r\n", szInput);
	}
*/


		if(m_bCSFset) // decompile it here and go by CSF files.  (otherwise send the whole string to the host/port)
		{
	
			CHTTP10 http;

			char* pszContext = NULL;
			char** ppszNames = NULL;
			char** ppszValues = NULL;
			unsigned long ulNumPairs=0;
			char filepath[MAX_PATH];
			strcpy(filepath, "");


			pszContext = (char*) szInput.GetBuffer(0);

			if(http.URLDecode(&pszContext, &ppszNames, &ppszValues, &ulNumPairs)>=HTTP_SUCCESS)
			{
				// check context....

				szOutput=((pszContext)&&(strlen(pszContext)))?pszContext:"";
				unsigned long i=0;
				int nFound=0;

				int nType = -1;  // default
				int nSubType = -1;  // default
				int nModuleType = -1;  // default  // trumps subtype
				while(i<ulNumPairs)
				{
					if((ppszNames)&&(ppszNames[i])&&(strlen(ppszNames[i])))
					{ // loop thru looking for type;
		/*
						if(stricmp(ppszNames[i], "qtype")==0)
						{
							if(stricmp(ppszValues[i], "dependencies")==0)
							{
								nType = 1;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "dependencies";
								nFound++;
							}
						}
						else
		*/
						if(stricmp(ppszNames[i], "subtype")==0)
						{
							if(stricmp(ppszValues[i], "process")==0)
							{
								nSubType = 0;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "process";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "database")==0)
							{
								nSubType = 1;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "database";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "automation")==0)
							{
								nSubType = 2;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "automation";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "graphics")==0)
							{
								nSubType = 3;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "graphics";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "polling")==0)
							{
								nSubType = 7;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "polling";
								nFound++;
							}
						}
						else
						if(stricmp(ppszNames[i], "moduletype")==0)
						{
							if(stricmp(ppszValues[i], "Cortex")==0)
							{
								nModuleType = 0x100000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Promotor";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Promotor")==0)
							{
								nModuleType = 0x1000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Promotor";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Nucleus")==0)
							{
								nModuleType = 0x1000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Nucleus";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Direct")==0)
							{
								nModuleType = 0x2000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Direct";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Archivist")==0)
							{
								nModuleType = 0x3000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Archivist";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Annotator")==0)
							{
								nModuleType = 0x4000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Annotator";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Injector")==0)
							{
								nModuleType = 0x5000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Injector";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Advisor")==0)
							{
								nModuleType = 0x8000;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Advisor";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Sentinel")==0)
							{
								nModuleType = 0x0012;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Sentinel";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Radiance")==0)
							{
								nModuleType = 0x0023;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Radiance";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Prospero")==0)
							{
								nModuleType = 0x0033;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Prospero";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Barbero")==0)
							{
								nModuleType = 0x00b3;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Barbero";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Helios")==0)
							{
								nModuleType = 0x0042;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Helios";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Luminary")==0)
							{
								nModuleType = 0x0053;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Luminary";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Libretto")==0)
							{
								nModuleType = 0x0063;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Libretto";
								nFound++;
							}
							else
							if(stricmp(ppszValues[i], "Tabulator")==0)
							{
								nModuleType = 0x0077;
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += ppszNames[i];
								szOutput += "=";
								szOutput += "Tabulator";
								nFound++;
							}

						}
						else
						if(stricmp(ppszNames[i], "filepath")==0)
						{
							if(nFound>0) szOutput += "&";
							else szOutput += "?";
							szOutput += ppszNames[i];
							szOutput += "=";
							szOutput += ppszValues[i];
							strcpy(filepath, ppszValues[i]);
							nFound++;
						}
					}
					i++;
				}

				CFileUtil file;
				bool bFile = false;
				nType = -1;
				char filename[MAX_PATH];
				if(strlen(filepath)>0)
				{
					while((filepath[strlen(filepath)-1] == '\\')||(filepath[strlen(filepath)-1] == '/')) filepath[strlen(filepath)-1] = 0;
					sprintf(filename, "%s\\%s.csf", filepath, ((pszContext)&&(strlen(pszContext)))?pszContext:"default");
				}
				else
				{
					sprintf(filename, "C:\\cortex\\%s.csf", ((pszContext)&&(strlen(pszContext)))?pszContext:"default");
				}
				file.GetSettings(filename, false); 
				if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
				{
					bFile = true;
					char* pch = file.GetIniString("Main", "Type", "unknown");

					if(pch)
					{
						if(stricmp(pch, "Cortex")==0)					nType=0x100000;
						else if(stricmp(pch, "Promotor")==0)	nType=0x1000; // 
						else if(stricmp(pch, "Nucleus")==0)		nType=0x1000; // might need to change the type
						else if(stricmp(pch, "Direct")==0)		nType=0x2000;
						else if(stricmp(pch, "Archivist")==0)	nType=0x3000;  // archivist is now a process that deals with watchfolder, not a db resource
						else if(stricmp(pch, "Annotator")==0)	nType=0x4000;  
						else if(stricmp(pch, "Injector")==0)	nType=0x5000;
						else if(stricmp(pch, "Advisor")==0)		nType=0x8000;
						else if(stricmp(pch, "Sentinel")==0)	nType=0x0012; // last digit is type / automation = 2
						else if(stricmp(pch, "Radiance")==0)	nType=0x0023; //  3= graphics
						else if(stricmp(pch, "Prospero")==0)	nType=0x0033;
						else if(stricmp(pch, "Barbero")==0)		nType=0x00b3;
						else if(stricmp(pch, "Helios")==0)		nType=0x0042;
						else if(stricmp(pch, "Luminary")==0)	nType=0x0053;
						else if(stricmp(pch, "Libretto")==0)	nType=0x0063;
						else if(stricmp(pch, "Tabulator")==0)	nType=0x0077;

						free(pch);
					}
					else
					{
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += "error=%20could%20obtain%20settings%20from%20";
								szOutput += filename;
								nFound++;
					}

				}
				
				char* values[NUMPARAMS];
				int v=0;
				for(v=0; v<NUMPARAMS; v++) values[v]=NULL;
				
				if(bFile)
				{
					char keys[NUMPARAMS][MAX_PATH];
					char category[NUMPARAMS][MAX_PATH];
					char param[NUMPARAMS][MAX_PATH];
					char defvals[NUMPARAMS][MAX_PATH];

					int n = 0;
					strcpy(keys[n], "name");  strcpy(category[n], "Main");  strcpy(param[n], "Name");  strcpy(defvals[n], "unknown");  n++;  
					strcpy(keys[n], "type");  strcpy(category[n], "Main");  strcpy(param[n], "Type");  strcpy(defvals[n], "unknown");  n++;
					strcpy(keys[n], "project");  strcpy(category[n], "Main");  strcpy(param[n], "Project");  strcpy(defvals[n], "");  n++;
					strcpy(keys[n], "license");  strcpy(category[n], "License");  strcpy(param[n], "Key");  strcpy(defvals[n], "");  n++;

					switch(nType)
					{
					case 0x100000: // cortex
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Cortex");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Cortex"); n++; 
							strcpy(keys[n], "root"); strcpy(category[n], "FileServer");  strcpy(param[n], "Root");  strcpy(defvals[n], "C:\\Inetpub\\wwwroot\\cortex\\");  n++;
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0012: // sentinel
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Sentinel");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Sentinel"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0023: // radiance
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Radiance");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Radiance"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x1000: // promotor
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Promotor");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Promotor"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
/*
					case 0x1000: // Nucleus
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Nucleus");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Nucleus"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
*/
					case 0x2000: // Direct
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Direct");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Direct"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x3000: // Archivist
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Archivist");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Archivist"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
							strcpy(keys[n], "direct");   strcpy(category[n], "InstalledModules");  strcpy(param[n], "DirectInstalled");  strcpy(defvals[n], "0"); n++; 
						} break;
					case 0x4000: // Annotator
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Annotator");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Annotator"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x5000: // Injector
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Injector");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Injector"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x8000: // Advisor
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Advisor");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Advisor"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0033: // Prospero
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Prospero");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Prospero"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x00b3: // Barbero
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Barbero");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Barbero"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0042: // Helios
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Helios");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Helios"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0053: // Luminary
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Luminary");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Luminary"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0066: // Libretto
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Libretto");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Libretto"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case 0x0077: // Tabulator
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "Tabulator");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "Tabulator"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					case -1: // def
					default: // def
						{
							strcpy(keys[n], "dsn");  strcpy(category[n], "Database");  strcpy(param[n], "DSN");  strcpy(defvals[n], "unknown");  n++;
							strcpy(keys[n], "user"); strcpy(category[n], "Database");  strcpy(param[n], "DBUser");  strcpy(defvals[n], "sa");  n++;
							strcpy(keys[n], "pw");   strcpy(category[n], "Database");  strcpy(param[n], "DBPassword");  strcpy(defvals[n], "");  n++;
							strcpy(keys[n], "db");   strcpy(category[n], "Database");  strcpy(param[n], "DBDefault");  strcpy(defvals[n], "unknown"); n++; 
							strcpy(keys[n], "dependencies");  strcpy(category[n], "Dependencies");  strcpy(param[n], "Number");  strcpy(defvals[n], "0");  n++;
						} break;
					}

					CString szDependencies="dependencies=";
		//		char szDebug1[1024];
		//		char szDebug2[1024];
					bool binitial = true;
					
		//sprintf(szDebug2,"n=%d ",n);

					int q=0;
					while(q<n)
					{
						values[q] = file.GetIniString(category[q], param[q], defvals[q]);
		//sprintf(szDebug1, "%s got val=%s for [%s][%s][%s]", szDebug2, values[q], category[q], param[q], defvals[q]); strcpy(szDebug2,szDebug1);
			
						if((values[q])&&(stricmp(category[q], "Dependencies")==0)&&(stricmp(param[q], "Number")==0))
						{
							int nDeps = atoi(values[q]);
							if(nDeps>0)
							{
		//sprintf(szDebug1, "%s ndeps=%d ",szDebug2, nDeps); strcpy(szDebug2,szDebug1);
								char param[MAX_PATH];
								int d=0;
								while(d<nDeps)
								{
									sprintf(param, "Dependency%03d", d);
		//sprintf(szDebug1, "%s searching [%s] ",szDebug2, param); strcpy(szDebug2,szDebug1);
									char* pch = NULL;
									pch = file.GetIniString("Dependencies", param, "");
									if((pch)&&(strlen(pch)))
									{
										int nDepType = -1;
										char* pchName = NULL;

										pchName = file.GetIniString(pch, "Type", "");
										if(pchName)
										{
											if(stricmp(pchName, "Cortex")==0)					nDepType=0x100000;
											else if(stricmp(pchName, "Promotor")==0)	nDepType=0x1000; // 
											else if(stricmp(pchName, "Nucleus")==0)		nDepType=0x1000; // need to change
											else if(stricmp(pchName, "Direct")==0)		nDepType=0x2000;
											else if(stricmp(pchName, "Archivist")==0)	nDepType=0x3000;  // archivist is now a process that deals with watchfolder, not a db resource
											else if(stricmp(pchName, "Annotator")==0)	nDepType=0x4000;  
											else if(stricmp(pchName, "Injector")==0)	nDepType=0x5000;  
											else if(stricmp(pchName, "Advisor")==0)		nDepType=0x8000;  
											else if(stricmp(pchName, "Sentinel")==0)	nDepType=0x0012; // last digit is type / automation = 2
											else if(stricmp(pchName, "Radiance")==0)	nDepType=0x0023; //  3= graphics
											else if(stricmp(pchName, "Prospero")==0)	nDepType=0x0033;
											else if(stricmp(pchName, "Barbero")==0)		nDepType=0x00b3;
											else if(stricmp(pchName, "Helios")==0)		nDepType=0x0042;
											else if(stricmp(pchName, "Luminary")==0)	nDepType=0x0053;
											else if(stricmp(pchName, "Libretto")==0)	nDepType=0x0063;
											else if(stricmp(pchName, "Tabulator")==0)	nDepType=0x0077;


											
											free(pchName);
											pchName = NULL;
										}
		//sprintf(szDebug1, "%s type is 0x%04x  ",szDebug2, nDepType);strcpy(szDebug2,szDebug1);

										if(nModuleType>0)  //trumps subtype
										{
											if(nDepType>1)
											{
												if(nDepType == nModuleType)
												{
													pchName = file.GetIniString(pch, "Name", "");
													if(pchName)
													{
														if(strlen(pchName)>0)
														{
															if(!binitial)
															{
																szDependencies+=",";
															}
															szDependencies+=pchName;
															binitial = false;
														}
														free(pchName);
													}
												}
											}
										}
										else
										if(nSubType>=0)
										{
											if(nDepType>1)
											{
												if((nDepType&0x000f) == nSubType)
												{
													pchName = file.GetIniString(pch, "Name", "");
													if(pchName)
													{
														if(strlen(pchName)>0)
														{
															if(!binitial)
															{
																szDependencies+=",";
															}
															szDependencies+=pchName;
															binitial = false;
														}
														free(pchName);
													}
												}
											}
										}
										else
										{
		//sprintf(szDebug1, "%s searching for [%s][Name] ",szDebug2, pch);strcpy(szDebug2,szDebug1);
											pchName = file.GetIniString(pch, "Name", "");
											if(pchName)
											{
												if(strlen(pchName)>0)
												{
													if(!binitial)
													{
														szDependencies+=",";
													}
													szDependencies+=pchName;
													binitial = false;
												}
												free(pchName);
											}
										}
									}

									if(pch)
									{
										free (pch);
									}

									d++;
								}
							}
						}
						q++;
					}


				
					i=0;
					while(i<ulNumPairs)
					{
						if((ppszNames)&&(ppszNames[i])&&(strlen(ppszNames[i])))
						{
							if(stricmp(ppszNames[i], "dependencies")==0)
							{
								if(nFound>0) szOutput += "&";
								else szOutput += "?";
								szOutput += szDependencies;
		//						szOutput += szDebug1;
								nFound++; 
							}
							else
							if(stricmp(ppszNames[i], "license")==0)
							{
								int j=0;
								while(j<n)
								{
									if(stricmp(ppszNames[i], keys[j])==0)
									{
										if(nFound>0) szOutput += "&";
										else szOutput += "?";
										szOutput += ppszNames[i];

										if((values[j])&&(strlen(values[j])>0)) // we got a real key
										{
											CLicenseKey* pKey = new CLicenseKey;
											if(pKey)
											{
												pKey->m_pszLicenseString = (char*)malloc(strlen(values[j])+1);
												if(pKey->m_pszLicenseString)
												{
													strcpy(pKey->m_pszLicenseString, values[j]);
													pKey->InterpretKey();
													if(pKey->m_bValid)
													{
														CString szTemp;
														szOutput += "=valid:";
														szOutput += values[j];
														szOutput += ":";
														szOutput += pKey->m_bExpired?"X":"O";
														szTemp.Format(_T("%d:"),pKey->m_ulExpiryDate);
														szOutput += szTemp;
														szOutput += pKey->m_bExpireForgiveness?(pKey->m_ulExpiryDate+pKey->m_ulExpiryForgiveness>(unsigned long)time(NULL)?"O":"X"):"O";
														szTemp.Format(_T("%d:"),pKey->m_bExpireForgiveness?pKey->m_ulExpiryDate+pKey->m_ulExpiryForgiveness:0);
														szOutput += szTemp;
														szOutput += pKey->m_bMachineSpecific?(pKey->m_bValidMAC?"O":"X"):"O";
														szTemp.Format(_T("%02x%02x%02x%02x%02x%02x"),
															pKey->m_bMachineSpecific?pKey->m_ucMAC[0]:0,
															pKey->m_bMachineSpecific?pKey->m_ucMAC[1]:0,
															pKey->m_bMachineSpecific?pKey->m_ucMAC[2]:0,
															pKey->m_bMachineSpecific?pKey->m_ucMAC[3]:0,
															pKey->m_bMachineSpecific?pKey->m_ucMAC[4]:0,
															pKey->m_bMachineSpecific?pKey->m_ucMAC[5]:0
															);
														szOutput += szTemp;
														if((pKey->m_ulNumParams>0)&&(pKey->m_ppszParams)&&(pKey->m_ppszValues))
														{
															unsigned long lp=0;
															while(lp<pKey->m_ulNumParams)
															{
																if((pKey->m_ppszParams[lp])&&(pKey->m_ppszValues[lp]))
																{
																	szOutput += "&license_";
																	szOutput += pKey->m_ppszParams[lp];
																	szOutput += "=";
																	szOutput += pKey->m_ppszValues[lp];
																}
																lp++;
															}
														}
													}
													else
													{
														CString szTemp;
														szTemp.Format(_T("=invalid:%s"), values[i]);
														szOutput += szTemp;
													}
												}
												else
												{
													szOutput += "=error_memory";
												}
												delete pKey;
											}
											else
											{
												szOutput += "=error_no_object";
											}
										}
										else
										{
											szOutput += "=blank";
										}
										nFound++; 
										break;
									}
									j++;
								}
							}
							else
							{
								int j=0;
								while(j<n)
								{
									if(stricmp(ppszNames[i], keys[j])==0)
									{
										if(nFound>0) szOutput += "&";
										else szOutput += "?";
										szOutput += ppszNames[i];
										szOutput += "=";
										szOutput += values[j]?values[j]:"unknown";
										nFound++; 
										break;
									}
									j++;
								}
							}
						}

				
						i++;
					}
				}

				i=0;
				while(i<ulNumPairs)
				{
					if((ppszNames)&&(ppszNames[i])) free(ppszNames[i]);
					if((ppszValues)&&(ppszValues[i])) free(ppszValues[i]);
					
					i++;
				}


				v=0;
				while(v<NUMPARAMS)
				{
					if(values[v]) free(values[v]);
					v++;
				}

				if(ppszNames) free(ppszNames);
				if(ppszValues) free(ppszValues);
				if(pszContext) free(pszContext);
			}
			else
			{
				szOutput="error=url%20decode%20error";
			}

			szInput.ReleaseBuffer();
	/*
		if(fp)
		{
			fprintf(fp, "%s", szOutput);
			fflush(fp);
			fclose(fp);
		}
	*/
	//	CComBSTR bstrRtnVal = L"The input string was [";
	//	bstrRtnVal.Append(bstrInParams);
	//	bstrRtnVal.Append(L"]");
		}
		else  // use host.
		{
			// ok we are going to decompile the string and send commands based on the module info in the registry.

			// this is old - was going to ask the cortex module for the info, but now we just take from registry.
			// szOutput.Format(_T("The host is [%s:%d]"), m_szHost, m_usPort);

			CHTTP10 http;

			char* pszContext = NULL;
			char** ppszNames = NULL;
			char** ppszValues = NULL;
			unsigned long ulNumPairs=0;

			pszContext = (char*) szInput.GetBuffer(0);

			if(http.URLDecode(&pszContext, &ppszNames, &ppszValues, &ulNumPairs)>=HTTP_SUCCESS)
			{
				// check context....

				// format is:
				// ModuleName?type=00&cmd=00&sub=00&data=something&user=name&pw=password  where 00 is a hex code number like 3f

				szOutput=((pszContext)&&(strlen(pszContext)))?pszContext:"";
				unsigned long i=0;

				if(m_bStub)
				{
					szOutput += "?cmd=06&data=command%20succeeded";
				}
				else
				{

					int nModuleIndex = GetModuleIndex(szOutput);

					if((nModuleIndex>=0)&&(nModuleIndex<m_nNumModules)&&(m_ppModules)&&(m_ppModules[nModuleIndex]))
					{
						CNetData netdata;
/*
#define NET_TYPE_PROTOCOLMASK		0x1f
#define NET_TYPE_PROTOCOL1			0x00  // default protocol
#define NET_TYPE_PROTOCOL2			0x01  // default protocol plus security attribs.
#define NET_TYPE_PROTOCOL_HTTP	0x02  // webserver protocol
#define NET_TYPE_KEEPOPEN				0x20  // otherwise, one pair of RX/TX per connection - closes the connection at end of comm
#define NET_TYPE_HASDATA				0x40  // command has data
#define NET_TYPE_HASSUBC				0x80  // command has subcommand

//command and data
	unsigned char  m_ucType;      // defined type - indicates which protocol to use, structure of data
	unsigned char  m_ucCmd;       // the command byte
	unsigned char  m_ucSubCmd;    // the subcommand byte - if used with commmand byte gives up to 65535 possible commands - good enough for all practicality.  more than this can be accomplished with the attached data payload
	unsigned char* m_pucData;     // pointer to the payload data
	unsigned long m_ulDataLen;		// length of the payload data buffer.
	char* m_pszUser;     // pointer to the username, if the protocol supports it - zero terminated
	char* m_pszPassword; // pointer to the password, if the protocol supports it - zero terminated
				
*/
						BOOL bCmdset = FALSE;
						while(i<ulNumPairs)
						{
							if((ppszNames)&&(ppszNames[i])&&(strlen(ppszNames[i])))
							{
								if(stricmp(ppszNames[i], "type")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										netdata.m_ucType &= ~NET_TYPE_PROTOCOLMASK;
										netdata.m_ucType |= (unsigned char)m_bu.xtol(ppszValues[i], min(2,strlen(ppszValues[i]))); // truncates to 2 chars
									}
								}
								else
								if(stricmp(ppszNames[i], "cmd")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										bCmdset = TRUE;
										netdata.m_ucCmd = (unsigned char)m_bu.xtol(ppszValues[i], min(2,strlen(ppszValues[i]))); // truncates to 2 chars
									}
								}
								else
								if(stricmp(ppszNames[i], "sub")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										netdata.m_ucSubCmd = (unsigned char)m_bu.xtol(ppszValues[i], min(2,strlen(ppszValues[i]))); // truncates to 2 chars
										netdata.m_ucType |= NET_TYPE_HASSUBC;
									}
								}
								else
								if(stricmp(ppszNames[i], "data")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										netdata.m_pucData =	(unsigned char*)malloc(strlen(ppszValues[i])+1);
										if(netdata.m_pucData)
										{
											netdata.m_ucType |= NET_TYPE_HASDATA;
											netdata.m_ulDataLen = strlen(ppszValues[i]);
											memset(netdata.m_pucData, 0, netdata.m_ulDataLen+1);
											memcpy(netdata.m_pucData, ppszValues[i], netdata.m_ulDataLen);
										}
									}
								}
								else
								if(stricmp(ppszNames[i], "user")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										netdata.m_pszUser =	(char*)malloc(strlen(ppszValues[i])+1);
										if(netdata.m_pszUser)
										{
											strcpy(netdata.m_pszUser, ppszValues[i]);
										}
									}
								}
								else
								if(stricmp(ppszNames[i], "pw")==0)
								{
									if((ppszValues[i])&&(strlen(ppszValues[i])))
									{
										netdata.m_pszPassword =	(char*)malloc(strlen(ppszValues[i])+1);
										if(netdata.m_pszPassword)
										{
											strcpy(netdata.m_pszPassword, ppszValues[i]);
										}
									}
								}
							}
							i++;
						}

						// if its all set, send a command out, otherwise send an error back;

						if(bCmdset)
						{
	USES_CONVERSION;
							SOCKET s=NULL;
							int nReturn = m_net.SendData(&netdata, T2A(m_ppModules[nModuleIndex]->m_pszHost), m_ppModules[nModuleIndex]->m_nPort, 5000, 0, NET_SND_CMDTOSVR, &s);
							if(nReturn>=NET_SUCCESS)
							{
								//send ack
					//			AfxMessageBox("ack");
								m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

								// ok now we have return data.  lets assemble the response.
								CString szTempValue;
								szOutput += "?cmd=";

								szTempValue.Format(_T("%02x"), netdata.m_ucCmd );
								szOutput += szTempValue;

								if(netdata.m_ucType&NET_TYPE_HASSUBC)
								{
									szTempValue.Format(_T("&sub=%02x"), netdata.m_ucSubCmd );
									szOutput += szTempValue;
								}

								if(netdata.m_ucType&NET_TYPE_HASDATA)
								{
									szTempValue.Format(_T("&data="));
									szOutput += szTempValue;
									i=0;
									while(i<netdata.m_ulDataLen)
									{
										if(http.CheckSafe(*(netdata.m_pucData+i)) & HTTP_PATHSAFE)
										{
											szTempValue.Format(_T("%c"), *(netdata.m_pucData+i));
											szOutput += szTempValue;
										}
										else // have to encode
										{
											szTempValue.Format(_T("%02x"), (unsigned char)(*(netdata.m_pucData+i)) );// convert to unsigned to avoid negative numbers
											szOutput += szTempValue;
										}

										i++;
									}
								}
							}
							else
							{
								szOutput.Format(_T("error=code%%20%d%%20sending%%20command"), nReturn);
							}
						}
						else
						{
							//szOutput += "?error=";
							szOutput="error=no%20valid%20command";
						}
					}
					else
					{
						szOutput="error=module%20not%20found";
					}
				}

				i=0;
				while(i<ulNumPairs)
				{
					if((ppszNames)&&(ppszNames[i])) free(ppszNames[i]);
					if((ppszValues)&&(ppszValues[i])) free(ppszValues[i]);
					
					i++;
				}
				if(ppszNames) free(ppszNames);
				if(ppszValues) free(ppszValues);
				if(pszContext) free(pszContext);
			}

			szInput.ReleaseBuffer();
		}
	}
	else
	{
		szOutput="error=invalid%20input";
	}

	CComBSTR bstrRtnVal = szOutput.AllocSysString();

	*pbOutParams = bstrRtnVal.Detach();

	return S_OK;
}

STDMETHODIMP CCortexComm::sqlRetrieve(BSTR bstrSQL, _Recordset** ppReturnRS)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

  _RecordsetPtr pRs = NULL;

  try
  {
		pRs.CreateInstance(__uuidof(Recordset));

		pRs->CursorLocation=adUseClient;

//		SAFEARRAY*			pSa = NULL;
//		_variant_t      vtArray, vtFieldName("value");

//		pSa = SafeArrayCreateVector(VT_VARIANT, 1, 4); 
//		if (!pSa) _com_issue_error(E_OUTOFMEMORY);


//		vtArray.vt = VT_ARRAY | VT_VARIANT;
//		vtArray.parray = pSa;


		//while fields...
//	  long	ix[1];
//		ix[0] = 0;
//		SafeArrayPutElement(pSa, ix, &vtFieldName);

		pRs->Fields->Append(L"something", adVarChar, 6, adFldUnspecified);
		pRs->Fields->Append(L"somethingelse", adVarChar, 6, adFldUnspecified);

		int i=0;
		while(i<2)
		{
			pRs->AddNew();
//			pRs->AddNew(pSa, pSa);

			pRs->PutCollect(L"something", "value");
			pRs->PutCollect(L"somethingelse", "val2");

			pRs->Update();
			i++;
		}

		pRs->putref_ActiveConnection(NULL);
		*ppReturnRS = pRs.Detach();
	}

  catch(_com_error err)
  {
    ;
	}  


	return S_OK;
}

STDMETHODIMP CCortexComm::cxUtil(BSTR bstrInParams, _Recordset** ppReturnRS)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
USES_CONVERSION;

	if(ppReturnRS==NULL) return E_POINTER;
  _RecordsetPtr pRs = NULL;
	*ppReturnRS = pRs;

 	CString szInput, szOutput;

	szInput = (LPCWSTR)bstrInParams;
  if (bstrInParams != NULL)  
  {
		LPSTR   p = (char*)szInput.GetBuffer(::SysStringLen(bstrInParams) + 1);
		BOOL    bUsedDefaultChar;

		::WideCharToMultiByte(CP_ACP, 0, bstrInParams, -1, 
						p, ::SysStringLen(bstrInParams)+1, 
						NULL, &bUsedDefaultChar);
		szInput.ReleaseBuffer();

		CHTTP10 http;

		char* pszContext = NULL;
		char** ppszNames = NULL;
		char** ppszValues = NULL;
		unsigned long ulNumPairs=0, i=0;
		int nUtilIndex=-1;
		char filepath[MAX_PATH];
		strcpy(filepath, "");
		BOOL bAllowBlank = TRUE;
		BOOL bNTSC = TRUE;
		BOOL bLog = FALSE;
		BOOL bAMPM = FALSE;
		char* pszFields = NULL;
		int nSkipHeaderRecords = 0;
		int nOutputMode = 0;


		pszContext = (char*) szInput.GetBuffer(0);

		if(http.URLDecode(&pszContext, &ppszNames, &ppszValues, &ulNumPairs)>=HTTP_SUCCESS)
		{
			// check context....
			while(i<ulNumPairs)
			{
				if((ppszNames)&&(ppszNames[i])&&(strlen(ppszNames[i])))
				{ // loop thru looking for which utility to run;
					if(stricmp(ppszNames[i], "type")==0)
					{
						if(stricmp(ppszValues[i], "csv")==0) // generic csv
						{
							nUtilIndex = 0;
						}
						else
						if(stricmp(ppszValues[i], "tem")==0) // miranda intuition file
						{
							nUtilIndex = 1;
						}
						else
						if(stricmp(ppszValues[i], "osc")==0) // omnibus schedule file
						{
							nUtilIndex = 2;
						}
						else
						if(stricmp(ppszValues[i], "duration")==0) // am/pm not valid
						{
							bAMPM = FALSE;
							nUtilIndex = 3;
						}
						else
						if(stricmp(ppszValues[i], "time")==0) // am/pm valid
						{
							bAMPM = TRUE;
							nUtilIndex = 3;
						}
						else
						if(stricmp(ppszValues[i], "lst")==0) // harris playlist
						{
							nUtilIndex = 4;
						}
						else
						if(stricmp(ppszValues[i], "bcd")==0) // binary coded decimal
						{
							nUtilIndex = 5;
						}
						// else
					}
					else
					if(stricmp(ppszNames[i], "file")==0)
					{
						strcpy(filepath, ppszValues[i]);
					}
					else
					if(stricmp(ppszNames[i], "skip")==0)
					{
						if(ppszValues[i]) nSkipHeaderRecords = atoi(ppszValues[i]);
						if(nSkipHeaderRecords<0) nSkipHeaderRecords=0;
					}
					else
					if(stricmp(ppszNames[i], "blank")==0)
					{
						if(ppszValues[i])
						{
							if((strnicmp(ppszValues[i], "true", 4))&&(atoi(ppszValues[i])==0)) bAllowBlank = FALSE;
						}
					}
					else
					if(stricmp(ppszNames[i], "format")==0)
					{
						if(ppszValues[i])
						{
							if((stricmp(ppszValues[i], "PAL")==0)||(atoi(ppszValues[i])==25)) bNTSC = FALSE;
						}
					}
					else
					if(stricmp(ppszNames[i], "fields")==0)
					{
						pszFields = ppszValues[i];
					}
					else
					if(stricmp(ppszNames[i], "log")==0)
					{
						if(stricmp(ppszValues[i], "true")==0) bLog=TRUE;
					}
					else
					if(stricmp(ppszNames[i], "input")==0)
					{
						pszFields = ppszValues[i];
					}
					else
					if(stricmp(ppszNames[i], "output")==0)
					{
						if(stricmp(ppszValues[i], "sec")==0) nOutputMode = 1; //seconds
						// else default = milliseconds
					}
				}
				i++;
			}

			// valid range
			if((nUtilIndex >=0)&&(nUtilIndex <= 5))
			{
FILE* fpp=NULL;

if(bLog) {fpp = fopen("C:\\cxutil.txt", "ab");}
if((bLog)&&(fpp)) fprintf(fpp, "try\r\n");

				try
				{
if((bLog)&&(fpp)) fprintf(fpp, "pRs.CreateInstance(__uuidof(Recordset));\r\n");

					pRs.CreateInstance(__uuidof(Recordset));
if((bLog)&&(fpp)) fprintf(fpp, "pRs->CursorLocation=adUseClient;\r\n");


					pRs->CursorLocation=adUseClient;

//					_bstr_t bst(pszContext);
//					provider=msdatashape;
//					HRESULT hr = pRs->Open(_bstr_t(""), vtMissing, adOpenStatic, adLockOptimistic, adCmdUnspecified);
//					HRESULT hr = pRs->Open(_bstr_t(""), _bstr_t("data provider=none;"), adOpenForwardOnly, adLockBatchOptimistic, adCmdFile);

			//		SAFEARRAY*			pSa = NULL;
			//		_variant_t      vtArray, vtFieldName("value");

			//		pSa = SafeArrayCreateVector(VT_VARIANT, 1, 4); 
			//		if (!pSa) _com_issue_error(E_OUTOFMEMORY);


			//		vtArray.vt = VT_ARRAY | VT_VARIANT;
			//		vtArray.parray = pSa;


					//while fields...
			//	  long	ix[1];
			//		ix[0] = 0;
			//		SafeArrayPutElement(pSa, ix, &vtFieldName);
					int i=0;

					switch(nUtilIndex)
					{
					case 0: //csv
						{//xxx
if((bLog)&&(fpp))  fprintf(fpp, "ParseCSV on %s\r\n", filepath);
							FILE* fp = fopen(filepath, "rb");
							if(fp)
							{
								// determine file size
								fseek(fp, 0, SEEK_END);
								unsigned long ulFileLen = ftell(fp);

								char* pch = (char*) malloc(ulFileLen+1); // term zero
								if(pch)
								{
									fseek(fp, 0, SEEK_SET);
									fread(pch, sizeof(char), ulFileLen, fp);
									*(pch+ulFileLen) = 0; // term zero
									fclose(fp);

									CBufferUtil bu;

									char*** pppchBuffer = NULL;
									unsigned long ulRows=0;
									unsigned long ulColumns=0;
									if(bu.ParseCSV(pch, &pppchBuffer, &ulRows, &ulColumns)==0)
									{
										if(pppchBuffer)
										{
if((bLog)&&(fpp))  fprintf(fpp, "ParseCSV succeeded\r\n");

											i=0;
											while(i<(int)ulColumns)
											{
												CString col; col.Format(L"column%02d", i+1);
if((bLog)&&(fpp))  fprintf(fpp, "adding column%02d to schema as varchar(%d)\r\n", i+1, MAX_PATH);

												pRs->Fields->Append((_bstr_t)col, adVarChar, MAX_PATH, adFldUnspecified);
												i++;
											}

											int j=nSkipHeaderRecords;
											HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
											if (SUCCEEDED(hr))
											{
												while(j<(int)ulRows)
												{
													if(pppchBuffer[j])
													{
														BOOL bOK = FALSE;
														if(bAllowBlank)
														{
															bOK = TRUE;
														}
														else
														{
															i=0;
															while(i<(int)ulColumns)
															{
																if(pppchBuffer[j][i])
																{
																	if(strlen(pppchBuffer[j][i])>0)
																	{
																		bOK=TRUE; break;
																	}
																}
																i++;
															}
														}

														if(bOK)
														{
if((bLog)&&(fpp))  fprintf(fpp, "row %03d: ", j);
															pRs->AddNew();

															i=0;
															while(i<(int)ulColumns)
															{
																CString col; col.Format(L"column%02d", i+1);
																if(pppchBuffer[j][i])
																{
if((bLog)&&(fpp))  fprintf(fpp, "[%s] ",pppchBuffer[j][i]);

																	pRs->PutCollect((_bstr_t)col, A2W(pppchBuffer[j][i]));
																}
																else
																{
if((bLog)&&(fpp))  fprintf(fpp, "[(null)] ");
																	pRs->PutCollect((_bstr_t)col, _T(""));
																}
																i++;
															}
															pRs->Update();
														}
													}
													j++;
if((bLog)&&(fpp))  fprintf(fpp, "\r\n");
												}
											}
if((bLog)&&(fpp))  fprintf(fpp, "\r\n");
											j=0;
											while(j<(int)ulRows)
											{
												if(pppchBuffer[j])
												{
													i=0;
													while(i<(int)ulColumns)
													{
														if(pppchBuffer[j][i]) free(pppchBuffer[j][i]);
														i++;
													}
													delete [] pppchBuffer[j];
												}
												j++;
											}
											delete [] pppchBuffer;
										}
										else
										{ // pppchBuffer was null
if((bLog)&&(fpp))  fprintf(fpp, "ParseCSV buffer was NULL\r\n");
											pRs->Fields->Append(L"error", adVarChar, MAX_PATH, adFldUnspecified);
											HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
										}
									}
									else
									{
										// parse failed
if((bLog)&&(fpp))  fprintf(fpp, "ParseCSV failed\r\n");
										pRs->Fields->Append(L"error", adVarChar, MAX_PATH, adFldUnspecified);
										HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
									}
								} else 	fclose(fp);
							}
						} break;
					case 1: //tem (miranda template)
						{
if((bLog)&&(fpp))  fprintf(fpp, "Parse Miranda tem file on %s\r\n", filepath);
							pRs->Fields->Append(L"filename", adVarChar, MAX_PATH, adFldUnspecified);
							HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
							if (SUCCEEDED(hr))
							{
if((bLog)&&(fpp))  fprintf(fpp, "Open succeeded\r\n");


////////////////////////////////////////////
FILE* fp = fopen(filepath, "rb");
if(fp)
{
	// determine file size
	fseek(fp, 0, SEEK_END);
	unsigned long ulFileLen = ftell(fp);

	char* pch = (char*) malloc(ulFileLen+1); // term zero
	if(pch)
	{
		fseek(fp, 0, SEEK_SET);
		fread(pch, sizeof(char), ulFileLen, fp);
		*(pch+ulFileLen) = 0; // term zero
		fclose(fp);

		// now we have a buffer
		char pszFilename[MAX_PATH];
		char* ppszList[256]; // limitation!
		int numFiles = 0;

		char* pchElement = NULL;
		pchElement = strstr(pch, "<Box");
		while(pchElement)
		{
			char* pchFinish = NULL;
			pchFinish = strstr(pchElement, "</Box>");  // end of the box tag
			if(pchFinish)
			{
				// well formed tag, all the info is in there
				// let's parse it.
				char* pchField = NULL;
				pchField = strstr(pchElement, "FileName=\"");  // start of the filename.  Gets font files too.
				if((pchField)&&(pchField<pchFinish))
				{
					// we have an element
					pchField += strlen("FileName=\"");  // start of the filename.  Gets font files too.
					pchElement = strstr(pchField, "\"");  // end of the filename 

					if((pchElement)&&(pchElement<pchFinish))
					{
						int i=pchElement-pchField;
						if(i>0)
						{
							memset(pszFilename, 0, MAX_PATH);
							memcpy(pszFilename, pchField, i);

							char* pchChild = (char*)malloc(strlen(pszFilename)+1);
							if(pchChild)
							{
								sprintf(pchChild, "%s", pszFilename);
if((bLog)&&(fpp))  fprintf(fpp, "FILE: %s\r\n", pszFilename);

								bool bFoundDuplicate = false;
								i=0;
								while((!bFoundDuplicate)&&(i<numFiles))
								{
									if((ppszList)&&(ppszList[i]))
									{
										if(stricmp(pchChild,ppszList[i])==0)
										{
											bFoundDuplicate=true;
										} else i++;
									} else i++;
								}

								if(!bFoundDuplicate)
								{
									ppszList[numFiles]=pchChild;
									numFiles++;
if((bLog)&&(fpp))  fprintf(fpp, "adding new: %s\r\n", pchChild);
									pRs->AddNew();
									pRs->PutCollect(L"filename", A2W(pchChild));
									pRs->Update();
								}
								else
								{
									free(pchChild); // just let it go.
								}
							}
						}
					}
				}
				pchElement = strstr(pchFinish, "<Box");  // next box tag
			}
			else pchElement = NULL ; //breaks

		}
		i=0;
		while(i<numFiles)
		{
			if(ppszList[i]) free(ppszList[i]);
			i++;
		}
		free(pch);
	}
	else	fclose(fp);
}
////////////////////////////////////////////
							}  
							else
							{
//if((bLog)&&(fpp))  fprintf(fpp, "Open failed\r\n");
							}

						} break;
					case 2: //osc (omnibus schedule file)
						{
if((bLog)&&(fpp))  fprintf(fpp, "Parse Omnibus osc file on %s\r\n", filepath);
							pRs->Fields->Append(L"channelid", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_num", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_clip", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_title", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_type", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_timemode", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_duration", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_preset_date", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_preset_time", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"event_data", adVarChar, 4096, adFldUnspecified);
							HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
							if (SUCCEEDED(hr))
							{
//////////////////////////////////////////////////////////////
// OmniParse LoadFile

FILE* fp = fopen(filepath, "rb");
if(fp)
{
	fseek(fp, 0, SEEK_END);
	unsigned long ulFileLen = ftell(fp);
	char* pchFile = (char*) malloc(ulFileLen+1); // term zero
	if(pchFile)
	{
		fseek(fp, 0, SEEK_SET);

		fread(pchFile, sizeof(char), ulFileLen, fp);
		*(pchFile+ulFileLen) = 0; // term zero
		fclose(fp);

		char* pszChannelID = NULL;
		char* pch = NULL;
		char key[256];
		int nNumEvents = 0;

		// need to find channel id.
		int nChID = -1;

		sprintf(key, "%cchannel_id", 10);
		pch = strstr(pchFile, key);
		if(pch)
		{
			pch += 11;  // length of <10>channel_id
			pszChannelID = (char*)malloc(256);

			if(pszChannelID)
			{
				while(isspace(*pch)) pch++;
				int nch =0;
				while( (*pch != 10) && (*pch != 13) && (nch<255) )
				{
					*(pszChannelID + nch) = *pch;
					nch++;
					pch++;
				}
				*(pszChannelID + nch) = 0; //zero term
				nChID = atoi(pszChannelID);
			}
		}

		sprintf(key, "ITEM");
		pch = strstr(pchFile, key);
		while(pch)
		{
			char* pchEnd = NULL;
			char* pchSearch = pch;
if((bLog)&&(fpp))  fprintf(fpp, "Item delimiter found\r\n");

			while(!pchEnd)
			{
				sprintf(key, "%cend", 10);
				pchEnd = strstr(pchSearch, key);
				if(pchEnd == NULL) break;
				if( (*(pchEnd+4)!=10)&&(*(pchEnd+4)!=13) )
				{
					pchSearch = pchEnd+4;
					pchEnd = NULL;  // in case it's not really the end tag
				}
			}

			if(pchEnd)
			{
if((bLog)&&(fpp))  fprintf(fpp, "Item was terminated\r\n");
				sprintf(key, "%cITEM", 10);
				char* pchNew = strstr(pch+4, key);  //+4 to skip the current opener
				if(pchNew) // there is another item
				{
					if(pchEnd>pchNew) // current item is not terminated properly.
					{
						pch = pchNew;  // skip the one that wasnt terminated.
if((bLog)&&(fpp))  fprintf(fpp, "Incorrect item delimiter skipped\r\n");
					}
				}

				//pch is the start, pchEnd is the end.
				pRs->AddNew();
if((bLog)&&(fpp))  fprintf(fpp, "Adding new record\r\n");

				if(pszChannelID) 
				{
					pRs->PutCollect(L"channelid", A2W(pszChannelID));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect channelid %s\r\n", pszChannelID);
				}
				else
				{
					pRs->PutCollect(L"channelid", L"-1");
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect channelid -1\r\n");
				}

				pchNew = (char*)malloc(36);
				if(pchNew)
				{
					sprintf(pchNew, "%05d", nNumEvents+1);
					pRs->PutCollect(L"event_num", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_num %05d\r\n", nNumEvents+1);
					free( pchNew );
				}
					
				pchNew = GetEventField(pch, pchEnd, "clip");
				if(pchNew)
				{
					pRs->PutCollect(L"event_clip", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect clip %s\r\n", pchNew);
					free( pchNew );
				}

				pchNew = GetEventField(pch, pchEnd, "title");
				if(pchNew)
				{
					pRs->PutCollect(L"event_title", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_title %s\r\n", pchNew);
					free( pchNew );
				}

				pchNew = GetEventField(pch, pchEnd, "type");
				if(pchNew)
				{
					pRs->PutCollect(L"event_type", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_type %s\r\n", pchNew);
					free( pchNew );
				}

				pchNew = GetEventField(pch, pchEnd, "mode");
				if(pchNew)
				{
					pRs->PutCollect(L"event_timemode", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_timemode %s\r\n", pchNew);
					free( pchNew );
				}

				int nDuration=-1;
				pchNew = GetEventField(pch, pchEnd, "out_src");
				if(pchNew)
				{
					nDuration = (atoi(pchNew)*1000)/((bNTSC)?30:25);
					free(pchNew);
					pchNew = GetEventField(pch, pchEnd, "in_src");
					if(pchNew)
					{
						nDuration -= (atoi(pchNew)*1000)/((bNTSC)?30:25);
						free(pchNew);
					}
				}
				else
				{
					pchNew = GetEventField(pch, pchEnd, "tc_out");
					if(pchNew)
					{
						nDuration = 0;
						char* pchNum = pchNew;
						char* pchDelim = strchr(pchNum, ':');
						int nField = 0;
						while(pchDelim)
						{
							*pchDelim =0;
							
							switch(nField)
							{
							case 0: nDuration += atoi(pchNum)*(3600000); break;
							case 1: nDuration += atoi(pchNum)*(60000); break;
							case 2: nDuration += atoi(pchNum)*(1000); break;
							}

							nField++;
							pchNum = pchDelim+1;
							pchDelim = strchr(pchNum, ':');
						}
						nDuration += (atoi(pchNum)*1000)/((bNTSC)?30:25);

						free(pchNew);
						pchNew = GetEventField(pch, pchEnd, "tc_in");
						if(pchNew)
						{
							char* pchNum = pchNew;
							char* pchDelim = strchr(pchNum, ':');
							nField = 0;
							while(pchDelim)
							{
								*pchDelim =0;
								
								switch(nField)
								{
								case 0: nDuration -= atoi(pchNum)*(3600000); break;
								case 1: nDuration -= atoi(pchNum)*(60000); break;
								case 2: nDuration -= atoi(pchNum)*(1000); break;
								}

								nField++;
								pchNum = pchDelim+1;
								pchDelim = strchr(pchNum, ':');
							}
							nDuration -= (atoi(pchNum)*1000)/((bNTSC)?30:25);
							free(pchNew);
						}
					}
				}

				if(nDuration>=0)
				{
					sprintf(key, "%d", nDuration);
					pRs->PutCollect(L"event_duration", A2W(key));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_duration %d\r\n", nDuration);
				}
				else
				{
					pRs->PutCollect(L"event_duration", L"");
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_duration (blank)\r\n");
				}

				// in this case we are going to set up the preset time string in the
				// m_pContextData member (later we can calc the time for real)

/*
				pEvent->m_pContextData = (char*)malloc(256);
				if(pEvent->m_pContextData)
				{
					strcpy((char*)pEvent->m_pContextData, "");

					pchNew = GetEventField(pch, pchEnd, "preset_date");
					if(pchNew)
					{
						char ch=' ';
						switch(pEvent->m_usControl)
						{
						case 0: ch = 'M'; break;
						case 1: ch = 'A'; break;
						case 2: ch = '='; break;
						case 3: ch = '+'; break;
						case 4: ch = '-'; break;
						case 5: ch = 'F'; break;
						default: ch = ' '; break;
						}
						if((strcmp(pchNew, "01-01-1970")==0)||(ch=='+')||(ch=='-'))
						{
							sprintf((char*)pEvent->m_pContextData, "         %c ", ch);
						}
						else
						{
							sprintf((char*)pEvent->m_pContextData, "%s ", pchNew);
						}
						free(pchNew);
					}
					pchNew = GetEventField(pch, pchEnd, "preset_time");
					if(pchNew)
					{
						strcat((char*)pEvent->m_pContextData, pchNew);
						free(pchNew);
					}
				}
*/
if((bLog)&&(fpp))  fprintf(fpp, "GetEventField event_preset_date\r\n");
				pchNew = GetEventField(pch, pchEnd, "preset_date");
if((bLog)&&(fpp))  fprintf(fpp, "GetEventField event_preset_date returned\r\n");
				if(pchNew)
				{
if((bLog)&&(fpp))  fprintf(fpp, "GetEventField event_preset_date returned %s\r\n", pchNew);
					pRs->PutCollect(L"event_preset_date", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_preset_date %s\r\n", pchNew);
					free( pchNew );
				}

				pchNew = GetEventField(pch, pchEnd, "preset_time");
				if(pchNew)
				{
					pRs->PutCollect(L"event_preset_time", A2W(pchNew));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_preset_time %s\r\n",  pchNew);
					free( pchNew );
				}

				// ok now the hard part.
				// assemble the data member

// SetDataFields
/////////////////////////////////
CCAEventData**	ppfi=NULL;  // dynamic array of field info.
int							nNumFields=0;
BOOL bAlloc = FALSE;

if((pszFields == NULL)||(strlen(pszFields)<=0))
{
	pszFields = (char*)malloc(5);
	if(pszFields)
	{
		strcpy(pszFields, "data"); 
		bAlloc=TRUE;
	}
}

 // a comma separated list of tags inside the OSC file that will be assembled and put into the m_pszData member of the event items

//			AfxMessageBox(pszFields);
if(pszFields)
{																
	// first split up the pConn->m_pszDataFields into the tag names
	CSafeBufferUtil sbu;

	char* pchField = sbu.Token(pszFields, strlen(pszFields), "><, ");
	while(pchField)
	{
		if(strlen(pchField))
		{
			CCAEventData* pfi = new CCAEventData;
			if(pfi)
			{
				CCAEventData** ppnewfi = new CCAEventData*[nNumFields+1];
				if(ppnewfi)
				{
					if(ppfi)
					{
						for(int i=0; i<nNumFields; i++)
						{
							ppnewfi[i] = ppfi[i];
						}
						delete [] ppfi;
					}

					ppfi = ppnewfi;

	//			AfxMessageBox(pch);
					//pfi->m_pszFieldName = pchField;  
					pfi->m_pszFieldName = (char*)malloc(strlen(pchField)+1);
					if(pfi->m_pszFieldName) strcpy(pfi->m_pszFieldName, pchField);  
	//				AfxMessageBox(m_ppfi[m_nNumFields]->m_pszFieldName);

					ppfi[nNumFields] = pfi;
					nNumFields++;
				}
			}
		}

		//get the next one.
	//				AfxMessageBox(m_ppfi[m_nNumFields-1]->m_pszFieldName);
		pchField = sbu.Token(NULL, NULL, "><, ");
	//				AfxMessageBox(m_ppfi[m_nNumFields-1]->m_pszFieldName);

	/*
		for(int i=0; i<m_nNumFields; i++)
		{
			char fn[256];
			sprintf(fn, "%d: 0x%08x: %s", i, m_ppfi[i]->m_pszFieldName, m_ppfi[i]->m_pszFieldName);
			AfxMessageBox(fn);
		}
	*/			
	}
	if (bAlloc) free(pszFields);
}
// SetDataFields
/////////////////////////////////

				char* pszData=NULL;

				if((nNumFields>0)&&(ppfi))
				{
					int n=0;
					while(n<nNumFields)
					{
						if((ppfi[n])&&(ppfi[n]->m_pszFieldName)&&(strlen(ppfi[n]->m_pszFieldName)>0))
						{
							pchNew = GetEventField(pch, pchEnd, ppfi[n]->m_pszFieldName);
							if(pchNew)
							{
								char* pchData = NULL;
								if(pszData)
								{
									pchData = (char*)malloc(strlen(pszData)+(strlen(ppfi[n]->m_pszFieldName)*2)+6+strlen(pchNew));
									if(pchData)
									{
										sprintf(pchData, "%s<%s>%s</%s>", pszData, ppfi[n]->m_pszFieldName, pchNew, ppfi[n]->m_pszFieldName);
										free(pszData);
										pszData = pchData;
									}
								}
								else
								{
									pchData = (char*)malloc((strlen(ppfi[n]->m_pszFieldName)*2)+6+strlen(pchNew));
									sprintf(pchData, "<%s>%s</%s>", ppfi[n]->m_pszFieldName, pchNew, ppfi[n]->m_pszFieldName);
									pszData = pchData;
								}

								free(pchNew);
							}
						}
						n++;
					}
					if(pszData)
					{
						pRs->PutCollect(L"event_data", A2W(pszData));
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_data %s\r\n", pszData);

						free(pszData);
					}
					else
					{
						pRs->PutCollect(L"event_data", L"");
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_data (blank)\r\n");
					}
				}
				else
				{
					pRs->PutCollect(L"event_data", L"");
if((bLog)&&(fpp))  fprintf(fpp, "PutCollect event_data (blank)\r\n");
				}
/*
unsigned short m_usType;  // internal colossus type
char* m_pszID;    // the clip ID
char* m_pszTitle;
char* m_pszData;  //must encode zero.
char* m_pszReconcileKey;       // the "story ID"
unsigned char  m_ucSegment;    // not used
unsigned long  m_ulOnAirTimeMS;
unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
unsigned long  m_ulDurationMS;
unsigned short m_usStatus;  // status (omnibus code)
unsigned short m_usControl; // time mode

double m_dblOnAirTimeInternal;  // for omnibus, this is frames since jan 1970
double m_dblUpdated;            // the last time the system updated the event. format is: unixtime in the integer part, number of milliseconds in the decimal part. ex: half a second past midnight Jan 2, 1970 would be 86400.500
*/

				pRs->Update();

				nNumEvents++;
if((bLog)&&(fpp))  fprintf(fpp, "Added record number %d\r\n", nNumEvents);

			}
			sprintf(key, "%cITEM", 10);
			pch = strstr(pch+4, key);
		}

		if(pszChannelID) free(pszChannelID);
		free(pchFile);
	}
	else fclose(fp);
}

// OmniParse LoadFile
////////////////////////////////////////////////////////////////////////////////////


							}
						} break;
					case 3: //time parse!
						{
							pRs->Fields->Append(L"time", adVarChar, MAX_PATH, adFldUnspecified);
							HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
							if (SUCCEEDED(hr))
							{
								if(pszFields)
								{
if((bLog)&&(fpp))  fprintf(fpp, "Time input: %s\r\n", pszFields);
									int nDur = 0;
									int nTemp = 0;
									int nTimeLen = strlen(pszFields);
									if(nTimeLen>0)
									{
										BOOL bAfternoon = FALSE;
										if(bAMPM)
										{
											if(strstr(pszFields, "PM"))
											{
												bAfternoon = TRUE;
											}
											else
											if(strstr(pszFields, "pm"))
											{
												bAfternoon = TRUE;
											}
										}

//										while( (*(pszFields+nTimeLen-1)<48)&&(*(pszFields+nTimeLen-1)>57)&&(nTimeLen>=0)) nTimeLen--;
//										*(pszFields+nTimeLen)=0;

										char* pchDot = strchr(pszFields, '.');
										if(pchDot==NULL)
										{
											// count the :
											nTemp = m_bu.CountChar(pszFields, nTimeLen, ':');
											if(nTemp>2)
												pchDot = strrchr(pszFields, ':');
										}
										if(pchDot) // we have frames or milliseconds
										{
											*pchDot = 0; // zero term
											pchDot++;
											char* pchEnd = pchDot;
											while( ((*pchEnd)>=48) && ((*pchEnd)<=57) )
											{
												pchEnd++;
											}
											*pchEnd = 0;

											if((pchEnd-pchDot) == 3) // we have milliseconds
											{
												nDur = atoi(pchDot);
											}
											else
											if((pchEnd-pchDot) == 2) // we have frames
											{
												nTemp=atoi(pchDot);
												if(bNTSC)
												{
													if(nTemp<0) nTemp = 0;
													if(nTemp>29) nTemp = 29; // truncate!
													nTemp *= 1000;
													nTemp /= 30; // total 33.333333
												}
												else
												{
													if(nTemp<0) nTemp = 0;
													if(nTemp>24) nTemp = 24; // truncate!
													nTemp *= 25; // = 1000/40 = 25
												}
												nDur = nTemp;
											}
											else // some sort of fraction.
											{
												*(pchDot+3)=0;
												nDur=atoi(pchDot);
											}
											
										}
										// now let's check seconds.
										pchDot = strrchr(pszFields, ':');
										if(pchDot)
										{
											*pchDot = 0; // zero term
											pchDot++;
											nDur += (atoi(pchDot)*1000);

											// now let's check minutes.
											pchDot = strrchr(pszFields, ':');
											if(pchDot)
											{
												*pchDot = 0; // zero term
												pchDot++;
												nDur += (atoi(pchDot)*60000);

												// now let's check hours.
/*
// shouldn't be any leading characters
												pchDot = strrchr(pszFields, ':');
												if(pchDot)
												{
													*pchDot = 0; // zero term
													pchDot++;
													nDur += (atoi(pchDot)*60000);
												}
												else
*/
//												{
													if(strlen(pszFields)) // we have something left over
													{
														nDur += (atoi(pszFields)*3600000);
													}
//												}
											}
											else
											{
												if(strlen(pszFields)) // we have something left over
												{
													nDur += (atoi(pszFields)*60000);
												}
											}
										}
										else
										{
											if(strlen(pszFields)) // we have something left over
											{
												nDur += (atoi(pszFields)*1000);
											}
										}

										if(bAfternoon) nDur += 43200000;

										// add record
if((bLog)&&(fpp))  fprintf(fpp, "Time output: %d ms\r\n", nDur);

										pRs->AddNew();
										sprintf(filepath, "%d", nDur);
if((bLog)&&(fpp))  fprintf(fpp, "return value: [%s]\r\n", filepath);
										pRs->PutCollect(L"time", A2W(filepath));
										pRs->Update();

									}
								}
								else
								{
									if((bLog)&&(fpp))  fprintf(fpp, "No valid time input\r\n");
								}
							}
						} break;
					case 4: //lst (harris playlist)
						{
							pRs->Fields->Append(L"event_num", adVarChar, MAX_PATH, adFldUnspecified);
							HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
							if (SUCCEEDED(hr))
							{
							}
						} break;
					case 5: //bcd (harris binary coded decimal)
						{
if((bLog)&&(fpp))  fprintf(fpp, "Parse Harris binary coded decimal %s\r\n", pszFields?pszFields:"(null)");
							pRs->Fields->Append(L"milliseconds", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"seconds", adVarChar, MAX_PATH, adFldUnspecified);
							pRs->Fields->Append(L"string", adVarChar, MAX_PATH, adFldUnspecified);
							HRESULT hr = pRs->Open(L"", vtMissing, adOpenStatic, adLockOptimistic, adCmdText);
							if (SUCCEEDED(hr))
							{
								unsigned long ulCodedDecimal = -1;
								if((pszFields)&&(strlen(pszFields)))
								{
									ulCodedDecimal = atol(pszFields);

									int h=0,s=0,m=0,f=0;

									f = ( (   ulCodedDecimal & 0x0000000f )         + ( 10 * ( (ulCodedDecimal & 0x000000f0) >>  4 ) ) );
									s = ( ( ( ulCodedDecimal & 0x00000f00 ) >> 8  ) + ( 10 * ( (ulCodedDecimal & 0x0000f000) >> 12 ) ) );
									m = ( ( ( ulCodedDecimal & 0x000f0000 ) >> 16 ) + ( 10 * ( (ulCodedDecimal & 0x00f00000) >> 20 ) ) );
									h = ( ( ( ulCodedDecimal & 0x0f000000 ) >> 24 ) + ( 10 * ( (ulCodedDecimal & 0xf0000000) >> 28 ) ) );


/*
									sprintf(filepath, "%08x", nCodedDecimal);
									f = m_bu.xtol(filepath+6, 2); filepath[6] = 0;
									s = m_bu.xtol(filepath+4, 2); filepath[4] = 0;
									m = m_bu.xtol(filepath+2, 2); filepath[2] = 0;
									h = m_bu.xtol(filepath+6, 2);
*/
									double dblOut = (double)(h*3600000 + m*60000 + s*1000 + (f*1000)/((bNTSC)?30:25));
									pRs->AddNew();
								
									sprintf(filepath, "%d", (unsigned long)(dblOut));
									pRs->PutCollect(L"milliseconds", A2W(filepath));

									sprintf(filepath, "%.3f", (dblOut/1000.0));
									pRs->PutCollect(L"seconds", A2W(filepath));

									sprintf(filepath, "%02d:%02d:%02d.%02d", h,m,s,f);
									pRs->PutCollect(L"string", A2W(filepath));
									pRs->Update();

								}
							}
						} break;
					}
					// detach was here.
				}

				catch(_com_error err)
				{
					;
				}
				
if((bLog)&&(fpp))   fclose(fpp);

			}
		}
		i=0;
		while(i<ulNumPairs)
		{
			if((ppszNames)&&(ppszNames[i])) free(ppszNames[i]);
			if((ppszValues)&&(ppszValues[i])) free(ppszValues[i]);
			
			i++;
		}
		if(ppszNames) free(ppszNames);
		if(ppszValues) free(ppszValues);
		if(pszContext) free(pszContext);


	}
	try
	{
		pRs->MoveFirst( );
	}
	catch( ... )
	{
	}

	pRs->PutRefActiveConnection(NULL);
	*ppReturnRS = pRs.Detach();

	return S_OK;
}

STDMETHODIMP CCortexComm::sqlExecute(BSTR bstrSQL, BSTR* pbOutMessage)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP CCortexComm::cxSetContext(BSTR bstrInParams, BOOL* pbSuccess)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if(bstrInParams==NULL) return E_POINTER;

	CString szInput;

	szInput = (LPCWSTR)bstrInParams;


	LPSTR   p = (char*)szInput.GetBuffer(::SysStringLen(bstrInParams) + 1);
	BOOL    bUsedDefaultChar;

	::WideCharToMultiByte(CP_ACP, 0, bstrInParams, -1, 
					p, ::SysStringLen(bstrInParams)+1, 
					NULL, &bUsedDefaultChar);

/*
FILE* fp = fopen("CX.txt", "wb");
if(fp)
{
	fprintf(fp, "%s\r\n\r\n", szInput);
	fclose(fp);
}
*/

	if(stricmp(p, "CSF")==0)  // set this to CSF file usage
	{
		m_szHost = _T("");
		m_usPort = 0xffff;
		m_bCSFset = TRUE;
		m_bStub = FALSE;

		if(pbSuccess!=NULL) *pbSuccess = TRUE;
		szInput.ReleaseBuffer();
		return S_OK;
	}

	if(stricmp(p, "REG")==0)  // set this to NOT CSF file usage
	{
		g_nCortexPort = AfxGetApp()->GetProfileInt( _T("Comm"), _T("port"), 9973 );
		g_szCortexHost = AfxGetApp()->GetProfileString( _T("Comm"), _T("listen"), _T("localhost") );

		if((g_nCortexPort>0)&&(g_szCortexHost.GetLength()>0))
		{
			m_szHost = g_szCortexHost;
			m_usPort = (unsigned short)g_nCortexPort;
		}
		m_bCSFset = FALSE;
		m_bStub = FALSE;

		if(pbSuccess!=NULL) *pbSuccess = TRUE;
		szInput.ReleaseBuffer();
		return S_OK;
	}

	if(stricmp(p, "STUB")==0)  // set this to NOT CSF file usage, but use fake return data
	{
		g_nCortexPort = AfxGetApp()->GetProfileInt( _T("Comm"), _T("port"), 9973 );
		g_szCortexHost = AfxGetApp()->GetProfileString( _T("Comm"), _T("listen"), _T("localhost") );

		if((g_nCortexPort>0)&&(g_szCortexHost.GetLength()>0))
		{
			m_szHost = g_szCortexHost;
			m_usPort = (unsigned short)g_nCortexPort;
		}
		m_bCSFset = FALSE;
		m_bStub = TRUE;

		if(pbSuccess!=NULL) *pbSuccess = TRUE;
		szInput.ReleaseBuffer();
		return S_OK;
	}

	int nLen = strlen(p);
	if(nLen<=0)
	{
		if(pbSuccess!=NULL) *pbSuccess = FALSE;
		szInput.ReleaseBuffer();
		return S_FALSE;
	}

	// have to check to make sure the string is a valid encoded string.
	int n=0;
	while(n<nLen)
	{
		if(
			  (*(p+n)!=CX_B64PADCH)
			&&(strchr(CX_B64ALPHA, *(p+n)) == NULL)
			)
		{
			if(pbSuccess!=NULL) *pbSuccess = FALSE;
			szInput.ReleaseBuffer();
			return S_FALSE;
		}
		n++;
	}

	m_bCSFset = FALSE;  

	char* pchInput;
	char* pchResult;

	pchInput = (char*)malloc(nLen+1);
	if(pchInput==NULL)
	{
		if(pbSuccess!=NULL) *pbSuccess = FALSE;
		szInput.ReleaseBuffer();
		return E_OUTOFMEMORY;
	}

	sprintf(pchInput, "%s", p);
	szInput.ReleaseBuffer();

	pchResult = pchInput;
	unsigned long ulBufLen = strlen(pchInput);

	HRESULT hr = E_INVALIDARG;

	if(m_bu.Base64Decode(&pchResult, &ulBufLen, false, CX_B64ALPHA, CX_B64PADCH)>=RETURN_SUCCESS)
	{
		if(ulBufLen>0)
		{
			char* pch = strchr(pchResult, ':');
			char* pchHost = pchResult;
			CString szTestHost;
			unsigned short usTestPort = CX_PORT_CMD;
			if(pch)
			{
				*pch = 0;
				szTestHost.Format(_T("%s"), pchResult);
				pch++;
				usTestPort = atoi(pch);
			}
			else
			{
				szTestHost.Format(_T("%s"), pchResult);
				usTestPort = CX_PORT_CMD;
			}

			if(szTestHost.GetLength()<=0)
			{
				m_usPort = 0xffff;
				hr = S_FALSE;
			}
			else
			{
				// test the host name for validity
				if(strchr(pchHost, '.')!=NULL)  // periods only allowed in IP address
				{
					// check for valid IP
					if (m_bu.CountChar(pchHost, strlen(pchHost), '.')==3)
					{
						while(*pchHost != 0)
						{
							if(strchr(NET_VALIDIPCHARS, *pchHost)==NULL)
							{
								break;
							}
							pchHost++;
						}
						if(*pchHost == 0)
						{
							// all chars valid - should test for 0-255 validity, later
							m_szHost = szTestHost;
							m_usPort = usTestPort;
							hr = S_OK;
						}
					}
				}
				else  //check for valid host name
				{
					if(*pchHost != '-')
					{
						while(*pchHost != 0)
						{
							if(strchr(NET_VALIDHOSTCHARS, *pchHost)==NULL)
							{
								break;
							}
							pchHost++;
						}
						if(*pchHost == 0)
						{
							// all chars valid
							m_szHost = szTestHost;
							m_usPort = usTestPort;
							hr = S_OK;
						}
					}
				}

				hr = S_FALSE;
			}
		}
		else
			hr = S_FALSE;
		free(pchResult);
	}

	free(pchInput);
	if(pbSuccess!=NULL)
	{
		if(hr == S_OK)
			*pbSuccess = TRUE;
		else
			*pbSuccess = FALSE;
	}
	return hr; 
}


// internal util
char* CCortexComm::GetEventField(char* pchEventStart, char* pchEventEnd, char* szKey)
{
	if((pchEventStart==NULL)||(pchEventEnd==NULL)||(szKey==NULL)||(strlen(szKey)<=0)) return NULL;

	char key[36];
	_snprintf(key, 32, "%c%s", 10, szKey);
	char* pch = strstr(pchEventStart, key);
	if((pch)&&(pch<pchEventEnd))  // has to exist within the event bounds
	{
		pch += strlen(key);
		char* pchReturn = (char*)malloc(256);
		if(pchReturn)
		{
			while(isspace(*pch)) pch++;
			int nch =0;
			while( (*pch != 10) && (*pch != 13) && (nch<255) )
			{
				*(pchReturn + nch) = *pch;
				nch++;
				pch++;
			}
			*(pchReturn + nch) = 0; //zero term
			return pchReturn;
		}
	}
	return NULL;
}

int CCortexComm::GetModuleInfo()
{
	// get module info from registry
	ClearModuleInfo();
	CWinApp* papp = AfxGetApp();
	if(papp)
	{
		m_nNumModules = papp->GetProfileInt( _T("Modules"), _T("NumModules"), 0 );
		if(m_nNumModules>0)
		{
			CCortexModuleInfo** p = new CCortexModuleInfo*[m_nNumModules];
			if(p)
			{
				m_ppModules = p;
				CString szKey;
				int i=0;

				while(i<m_nNumModules)
				{
					szKey.Format(_T("Module_%03d_Name"), i);
					m_ppModules[i]->m_pszModuleName = papp->GetProfileString( _T("Modules"), szKey, _T("") );
					szKey.Format(_T("Module_%03d_Host"), i);
					m_ppModules[i]->m_pszHost = papp->GetProfileString( _T("Modules"), szKey, _T("") );
					szKey.Format(_T("Module_%03d_Port"), i);
					m_ppModules[i]->m_nPort = papp->GetProfileInt( _T("Modules"), szKey, 0 );

					i++;
				}
				return 0;
			}
		} else return 0;
	}

	return -1;
}

int CCortexComm::ClearModuleInfo()
{
	if((m_ppModules)&&(m_nNumModules>0))
	{
		int i=0;
		while(i<m_nNumModules)
		{
			if(m_ppModules[i])
			{
				delete m_ppModules[i];
			}
			i++;
		}
		delete [] m_ppModules;
		m_ppModules = NULL;
	}
	return 0;
}

int CCortexComm::GetModuleIndex(CString pszName)
{
	if((m_ppModules)&&(m_nNumModules>0)&&(pszName.GetLength()))
	{
		int i=0;
		while(i<m_nNumModules)
		{
			if((m_ppModules[i])&&(m_ppModules[i]->m_pszModuleName)&&(m_ppModules[i]->m_pszModuleName.CompareNoCase(pszName)==0))
			{
				return i;
			}
			i++;
		}
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////
// CCAEventData Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCAEventData::CCAEventData()
{
	m_pszFieldName = NULL;
	m_pchStartTag = NULL;
	m_pchEndTag = NULL;
	m_pszFieldInfo = NULL;
}

CCAEventData::~CCAEventData()
{
	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate buffer;
	if(m_pszFieldInfo) free(m_pszFieldInfo); // must use malloc to allocate buffer;
	//must NOT free m_pchStartTag and m_pchEndTag.  these are just pointers for search positions
}




//////////////////////////////////////////////////////////////////////
// CCortexModuleInfo Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCortexModuleInfo::CCortexModuleInfo()
{
	m_pszModuleName=_T("");  // name of the module we are addressing
	m_pszHost=_T("");				// ip address or host name
	m_nPort=-1;					// port on which to communicate
}

CCortexModuleInfo::~CCortexModuleInfo()
{
//	if(m_pszModuleName) free(m_pszModuleName); // must use malloc to allocate buffer;
//	if(m_pszHost) free(m_pszHost); // must use malloc to allocate buffer;
}




