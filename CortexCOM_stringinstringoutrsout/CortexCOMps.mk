
CortexCOMps.dll: dlldata.obj CortexCOM_p.obj CortexCOM_i.obj
	link /dll /out:CortexCOMps.dll /def:CortexCOMps.def /entry:DllMain dlldata.obj CortexCOM_p.obj CortexCOM_i.obj kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib 

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL $<

clean:
	@del CortexCOMps.dll
	@del CortexCOMps.lib
	@del CortexCOMps.exp
	@del dlldata.obj
	@del CortexCOM_p.obj
	@del CortexCOM_i.obj
