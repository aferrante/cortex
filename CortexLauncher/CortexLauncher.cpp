// CortexLauncher.cpp : Implementation of WinMain


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f CortexLauncherps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "CortexLauncher.h"
#include <time.h>
#include <process.h>

#include "CortexLauncher_i.c"


#include <stdio.h>

CServiceModule _Module;
BOOL g_bKillThread = FALSE;

BEGIN_OBJECT_MAP(ObjectMap)
END_OBJECT_MAP()


LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2)
{
    while (p1 != NULL && *p1 != NULL)
    {
        LPCTSTR p = p2;
        while (p != NULL && *p != NULL)
        {
            if (*p1 == *p)
                return CharNext(p1);
            p = CharNext(p);
        }
        p1 = CharNext(p1);
    }
    return NULL;
}

// Although some of these functions are big they are declared inline since they are only used once

inline HRESULT CServiceModule::RegisterServer(BOOL bRegTypeLib, BOOL bService)
{
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr))
        return hr;

    // Remove any previous service since it may point to
    // the incorrect file
    Uninstall();

    // Add service entries
    UpdateRegistryFromResource(IDR_CortexLauncher, TRUE);

    // Adjust the AppID for Local Server or Service
    CRegKey keyAppID;
    LONG lRes = keyAppID.Open(HKEY_CLASSES_ROOT, _T("AppID"), KEY_WRITE);
    if (lRes != ERROR_SUCCESS)
        return lRes;

    CRegKey key;
    lRes = key.Open(keyAppID, _T("{AF2D4347-5669-46E8-B22F-3BBDB17F7B92}"), KEY_WRITE);
    if (lRes != ERROR_SUCCESS)
        return lRes;
    key.DeleteValue(_T("LocalService"));
    
    if (bService)
    {
        key.SetValue(_T("CortexLauncher"), _T("LocalService"));
        key.SetValue(_T("-Service"), _T("ServiceParameters"));
        // Create service
        Install();

				HKEY hkey;

				lRes = RegOpenKeyEx(HKEY_LOCAL_MACHINE,	_T("System\\CurrentControlSet\\Services\\CortexLauncher"),0,KEY_ALL_ACCESS,&hkey);
				if(lRes == ERROR_SUCCESS)
				{ // set value
					lRes = RegSetValueEx(	hkey,	_T("Description"),0,REG_SZ,
						(const BYTE *)_T("Cortex Launcher service"), 
					        (wcslen(_T("Cortex Launcher service"))+1)*(sizeof(wchar_t))	);
				}
    }

    // Add object entries
    hr = CComModule::RegisterServer(bRegTypeLib);

    CoUninitialize();
    return hr;
}

inline HRESULT CServiceModule::UnregisterServer()
{
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr))
        return hr;

    // Remove service entries
    UpdateRegistryFromResource(IDR_CortexLauncher, FALSE);
    // Remove service
    Uninstall();
    // Remove object entries
    CComModule::UnregisterServer(TRUE);
    CoUninitialize();
    return S_OK;
}

inline void CServiceModule::Init(_ATL_OBJMAP_ENTRY* p, HINSTANCE h, UINT nServiceNameID, const GUID* plibid)
{
    CComModule::Init(p, h, plibid);

    m_bService = TRUE;

    LoadString(h, nServiceNameID, m_szServiceName, sizeof(m_szServiceName) / sizeof(TCHAR));

    // set up the initial service status 
    m_hServiceStatus = NULL;
    m_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    m_status.dwCurrentState = SERVICE_STOPPED;
    m_status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    m_status.dwWin32ExitCode = 0;
    m_status.dwServiceSpecificExitCode = 0;
    m_status.dwCheckPoint = 0;
    m_status.dwWaitHint = 0;



		// write out a bunch of defaults:
		PushProfileString(_T("Local"), _T("Object 999 Path"), _T("C:\\Cortex\\Cortex.exe")); 
		PushProfileString(_T("Local"), _T("Object 999 Args"), _T("")); 
		PushProfileInt(_T("Local"), _T("Object 999 Delay"), 5000); 
}

LONG CServiceModule::Unlock()
{
    LONG l = CComModule::Unlock();
    if (l == 0 && !m_bService)
        PostThreadMessage(dwThreadID, WM_QUIT, 0, 0);
    return l;
}

BOOL CServiceModule::IsInstalled()
{
    BOOL bResult = FALSE;

    SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (hSCM != NULL)
    {
        SC_HANDLE hService = ::OpenService(hSCM, m_szServiceName, SERVICE_QUERY_CONFIG);
        if (hService != NULL)
        {
            bResult = TRUE;
            ::CloseServiceHandle(hService);
        }
        ::CloseServiceHandle(hSCM);
    }
    return bResult;
}

inline BOOL CServiceModule::Install()
{
    if (IsInstalled())
        return TRUE;

    SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (hSCM == NULL)
    {
        MessageBox(NULL, _T("Couldn't open service manager"), m_szServiceName, MB_OK);
        return FALSE;
    }

    // Get the executable file path
    TCHAR szFilePath[_MAX_PATH];
    ::GetModuleFileName(NULL, szFilePath, _MAX_PATH);

    SC_HANDLE hService = ::CreateService(
        hSCM, m_szServiceName, m_szServiceName,
        SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS,
        SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL,
        szFilePath, NULL, NULL, _T("RPCSS\0"), NULL, NULL);

    if (hService == NULL)
    {
        ::CloseServiceHandle(hSCM);
        MessageBox(NULL, _T("Couldn't create service"), m_szServiceName, MB_OK);
        return FALSE;
    }

    ::CloseServiceHandle(hService);
    ::CloseServiceHandle(hSCM);
    return TRUE;
}

inline BOOL CServiceModule::Uninstall()
{
    if (!IsInstalled())
        return TRUE;

    SC_HANDLE hSCM = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (hSCM == NULL)
    {
        MessageBox(NULL, _T("Couldn't open service manager"), m_szServiceName, MB_OK);
        return FALSE;
    }

    SC_HANDLE hService = ::OpenService(hSCM, m_szServiceName, SERVICE_STOP | DELETE);

    if (hService == NULL)
    {
        ::CloseServiceHandle(hSCM);
        MessageBox(NULL, _T("Couldn't open service"), m_szServiceName, MB_OK);
        return FALSE;
    }
    SERVICE_STATUS status;
    ::ControlService(hService, SERVICE_CONTROL_STOP, &status);

    BOOL bDelete = ::DeleteService(hService);
    ::CloseServiceHandle(hService);
    ::CloseServiceHandle(hSCM);

    if (bDelete)
        return TRUE;

    MessageBox(NULL, _T("Service could not be deleted"), m_szServiceName, MB_OK);
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////
// Logging functions
void CServiceModule::LogEvent(LPCTSTR pFormat, ...)
{
    TCHAR    chMsg[256];
    HANDLE  hEventSource;
    LPTSTR  lpszStrings[1];
    va_list pArg;

    va_start(pArg, pFormat);
    _vstprintf(chMsg, pFormat, pArg);
    va_end(pArg);

    lpszStrings[0] = chMsg;

    if (m_bService)
    {
        /* Get a handle to use with ReportEvent(). */
        hEventSource = RegisterEventSource(NULL, m_szServiceName);
        if (hEventSource != NULL)
        {
            /* Write to event log. */
            ReportEvent(hEventSource, EVENTLOG_INFORMATION_TYPE, 0, 0, NULL, 1, 0, (LPCTSTR*) &lpszStrings[0], NULL);
            DeregisterEventSource(hEventSource);
        }
    }
    else
    {
        // As we are not running as a service, just write the error to the console.
        _putts(chMsg);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Service startup and registration
inline void CServiceModule::Start()
{
    SERVICE_TABLE_ENTRY st[] =
    {
        { m_szServiceName, _ServiceMain },
        { NULL, NULL }
    };
    if (m_bService && !::StartServiceCtrlDispatcher(st))
    {
        m_bService = FALSE;
    }
    if (m_bService == FALSE)
        Run();
}

inline void CServiceModule::ServiceMain(DWORD /* dwArgc */, LPTSTR* /* lpszArgv */)
{
    // Register the control request handler
    m_status.dwCurrentState = SERVICE_START_PENDING;
    m_hServiceStatus = RegisterServiceCtrlHandler(m_szServiceName, _Handler);
    if (m_hServiceStatus == NULL)
    {
        LogEvent(_T("Handler not installed"));
        return;
    }
    SetServiceStatus(SERVICE_START_PENDING);

    m_status.dwWin32ExitCode = S_OK;
    m_status.dwCheckPoint = 0;
    m_status.dwWaitHint = 0;

    // When the Run function returns, the service has stopped.
    Run();

    SetServiceStatus(SERVICE_STOPPED);
    LogEvent(_T("Service stopped"));
}

inline void CServiceModule::Handler(DWORD dwOpcode)
{
    switch (dwOpcode)
    {
    case SERVICE_CONTROL_STOP:
        SetServiceStatus(SERVICE_STOP_PENDING);
        PostThreadMessage(dwThreadID, WM_QUIT, 0, 0);
				g_bKillThread = TRUE;
        break;
    case SERVICE_CONTROL_PAUSE:
        break;
    case SERVICE_CONTROL_CONTINUE:
        break;
    case SERVICE_CONTROL_INTERROGATE:
        break;
    case SERVICE_CONTROL_SHUTDOWN:
				g_bKillThread = TRUE;
        break;
    default:
        LogEvent(_T("Bad service request"));
    }
}

void WINAPI CServiceModule::_ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv)
{
    _Module.ServiceMain(dwArgc, lpszArgv);
}
void WINAPI CServiceModule::_Handler(DWORD dwOpcode)
{
    _Module.Handler(dwOpcode); 
}

void CServiceModule::SetServiceStatus(DWORD dwState)
{
    m_status.dwCurrentState = dwState;
    ::SetServiceStatus(m_hServiceStatus, &m_status);
}

void CServiceModule::Run()
{
    _Module.dwThreadID = GetCurrentThreadId();

    HRESULT hr = CoInitialize(NULL);
//  If you are running on NT 4.0 or higher you can use the following call
//  instead to make the EXE free threaded.
//  This means that calls come in on a random RPC thread
//  HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

    _ASSERTE(SUCCEEDED(hr));

    // This provides a NULL DACL which will allow access to everyone.
    CSecurityDescriptor sd;
    sd.InitializeFromThreadToken();
    hr = CoInitializeSecurity(sd, -1, NULL, NULL,
        RPC_C_AUTHN_LEVEL_PKT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);
    _ASSERTE(SUCCEEDED(hr));

    hr = _Module.RegisterClassObjects(CLSCTX_LOCAL_SERVER | CLSCTX_REMOTE_SERVER, REGCLS_MULTIPLEUSE);
    _ASSERTE(SUCCEEDED(hr));

    LogEvent(_T("Service started"));
    if (m_bService)
        SetServiceStatus(SERVICE_RUNNING);


/////////////////////////////////////////////////
// original loop
/*
    MSG msg;
    while (GetMessage(&msg, 0, 0, 0))
        DispatchMessage(&msg);
*/
// original loop
/////////////////////////////////////////////////

		unsigned long nNumExe = PullProfileInt(_T("Local"), _T("ObjectCount"), 0);
//					FILE* fp1 = fopen("C:\\cl.txt", "ab");
//					if(fp1){ fprintf(fp1, "got %d local objects\r\n", nNumExe); fclose(fp1);}

		PushProfileInt(_T("Local"), _T("ObjectCount"), nNumExe);  // write it back too.

		if(nNumExe>0)
		{
			unsigned long nCount =0, nDelay=0;
			wchar_t buffer[1024];
			wchar_t* pszParams = NULL;
			wchar_t* pszArgs = NULL;

			while((nCount<nNumExe)&&(!g_bKillThread))
			{
				wsprintf(buffer, _T("Object %03d Args"), nCount);
				pszArgs = PullProfileString(_T("Local"), buffer, _T("")); 
				wsprintf(buffer, _T("Object %03d Path"), nCount);
				pszParams = PullProfileString(_T("Local"), buffer, _T("")); 
				if(pszParams)
				{
//					FILE* fp = fopen("C:\\cl.txt", "ab");
//					if(fp){ fprintf(fp, "%S\r\n", pszParams); fclose(fp);}
					if(wcslen(pszParams)>0)
					{

						wsprintf(buffer, _T("Object %03d Delay"), nCount);
						nDelay = PullProfileInt(_T("Local"), buffer, 0); 
	
						if(nDelay>0)
						{
//							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nDelay, pszParams);  

							int nClock = clock() + nDelay;
							int nPrint = clock() + 1000;
							while((nClock>clock())&&(!g_bKillThread))
							{
								Sleep(1);
								if(clock()>nPrint)
								{
									nPrint = clock() + 1000;
//									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nClock-clock(), pszParams);  
								}
//								_ftime( &cortex.m_data.m_timebTick );
							}
						}

//					fp = fopen("C:\\cl.txt", "ab");
//					if(fp){ fprintf(fp, "spawning %S\r\n", pszParams); fclose(fp);}


//						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Starting %s...", pszParams);  

						if(!g_bKillThread)
						{
							if(_wspawnl( _P_NOWAIT , pszParams, pszParams, pszArgs, NULL )<0)
							{
								//report failure
	//					AfxMessageBox("failure to spawn exe");
								// need to add an icon handler here too... 
//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s.", pszParams); 
//					fp = fopen("C:\\cl.txt", "ab");
//					if(fp){ fprintf(fp, "failure spawning %S\r\n", pszParams); fclose(fp);}
	
							}
/*
							else
							{
	//					AfxMessageBox(pszFilename);
								// need to put a short pause in here, so we dont get all the commands all at once.
								Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS); // how much is too much?
								// need to make a loop to make this interruptible.
							}

*/
	//					AfxMessageBox(pszParams);
						}



					}
					if(pszParams)  // if obj !=NULL, then we've allocated and moved the buffer into exename, so don't want to free it.
					{
						try
						{
							delete [] pszParams;  
						}
						catch(...)
						{
						}
					}
					pszParams=NULL;
				}

				if(pszArgs)
				{
					try
					{
						delete [] pszArgs;  
					}
					catch(...)
					{
					}
				}
				pszArgs=NULL;

				nCount++;
			}




		}

	


    _Module.RevokeClassObjects();

    CoUninitialize();
}

/////////////////////////////////////////////////////////////////////////////
//
extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, 
    HINSTANCE /*hPrevInstance*/, LPTSTR lpCmdLine, int /*nShowCmd*/)
{
    lpCmdLine = GetCommandLine(); //this line necessary for _ATL_MIN_CRT
    _Module.Init(ObjectMap, hInstance, IDS_SERVICENAME, &LIBID_CORTEXLAUNCHERLib);
    _Module.m_bService = TRUE;

    TCHAR szTokens[] = _T("-/");

    LPCTSTR lpszToken = FindOneOf(lpCmdLine, szTokens);
    while (lpszToken != NULL)
    {
        if (lstrcmpi(lpszToken, _T("UnregServer"))==0)
            return _Module.UnregisterServer();

        // Register as Local Server
        if (lstrcmpi(lpszToken, _T("RegServer"))==0)
            return _Module.RegisterServer(TRUE, FALSE);
        
        // Register as Service
        if (lstrcmpi(lpszToken, _T("Service"))==0)
            return _Module.RegisterServer(TRUE, TRUE);
        
        lpszToken = FindOneOf(lpszToken, szTokens);
    }

    // Are we Service or Local Server
    CRegKey keyAppID;
    LONG lRes = keyAppID.Open(HKEY_CLASSES_ROOT, _T("AppID"), KEY_READ);
    if (lRes != ERROR_SUCCESS)
        return lRes;

    CRegKey key;
    lRes = key.Open(keyAppID, _T("{AF2D4347-5669-46E8-B22F-3BBDB17F7B92}"), KEY_READ);
    if (lRes != ERROR_SUCCESS)
        return lRes;

    TCHAR szValue[_MAX_PATH];
    DWORD dwLen = _MAX_PATH;
    lRes = key.QueryValue(szValue, _T("LocalService"), &dwLen);

    _Module.m_bService = FALSE;
    if (lRes == ERROR_SUCCESS)
        _Module.m_bService = TRUE;

    _Module.Start();

    // When we get here, the service has been stopped
    return _Module.m_status.dwWin32ExitCode;
}


unsigned long CServiceModule::PullProfileInt(TCHAR* szKey, TCHAR* szValue, int nDefaultData)
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if((szKey)&&(szValue)&&(wcslen(szKey))&&(wcslen(szValue)))
	{
		TCHAR szFullKey[1024];
		LONG regResult = ERROR_SUCCESS;
		HKEY hKey;
		_snwprintf(szFullKey, 1023, _T("Software\\Video Design Software\\CortexLauncher\\%s"), szKey);

		regResult=RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		szFullKey,
		0,KEY_ALL_ACCESS,&hKey);

		if(regResult == ERROR_SUCCESS)
		{
			unsigned long type=REG_DWORD, size=1024;
			char res[1024]="";

			regResult=RegQueryValueEx(hKey,szValue,NULL,&type,(LPBYTE)&res[0],&size);
			RegCloseKey(hKey);
			if(regResult == ERROR_SUCCESS)
			{
				if(size>=4) // its a DWORD so better be 4.
				{ 
/*
	FILE* fp = fopen("C:\\cl.txt", "ab");
	if(fp)
	{
		fprintf(fp, "int query succeeded [%S] %d\r\n", szFullKey, ((res[0]&0x000000ff)|((res[1]<<8)&0x0000ff00)|((res[2]<<16)&0x00ff0000)|((res[3]<<24)&0xff000000)));
		fclose(fp);
	}
*/
					return ((res[0]&0x000000ff)|((res[1]<<8)&0x0000ff00)|((res[2]<<16)&0x00ff0000)|((res[3]<<24)&0xff000000));
				}
			}
/*
			else
			{
	FILE* fp = fopen("C:\\cl.txt", "ab");
	if(fp)
	{
		fprintf(fp, "int query failed [%S] %d\r\n", szFullKey, regResult);
		fclose(fp);
	}
			}
*/
		}
	}
	return nDefaultData;
}

TCHAR* CServiceModule::PullProfileString(TCHAR* szKey, TCHAR* szValue, TCHAR* pszDefaultData)
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if((szKey)&&(szValue)&&(wcslen(szKey))&&(wcslen(szValue)))
	{
		TCHAR szFullKey[1024];
		LONG regResult = ERROR_SUCCESS;
		HKEY hKey;
		_snwprintf(szFullKey, 1023, _T("Software\\Video Design Software\\CortexLauncher\\%s"), szKey);

		regResult=RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		szFullKey,
		0,KEY_ALL_ACCESS,&hKey);

		if(regResult == ERROR_SUCCESS)
		{
			unsigned long type=REG_SZ, size=4096;
			char res[4096]="";

			regResult=RegQueryValueEx(hKey,szValue,NULL,&type,(LPBYTE)&res[0],&size);
			RegCloseKey(hKey);
			if(regResult == ERROR_SUCCESS)
			{
				if(size>=0)
				{

/*
	FILE* fp = fopen("C:\\cl.txt", "ab");
	if(fp)
	{
		fprintf(fp, "size is %d = %S\r\n", size, res);
		fclose(fp);
	}
*/
					TCHAR* pszRV = new TCHAR[size+2];
					if(pszRV)
					{
						char* p = (char*) pszRV;
						if(p)
						{
							memset(p,0,min(size+2,4096));
							memcpy(p, res, size);

							return pszRV;
						}
					}
				}
			}
/*
			else
			{
	FILE* fp = fopen("C:\\cl.txt", "ab");
	if(fp)
	{
		fprintf(fp, "string query failed [%S][%S] %d\r\n", szFullKey,szValue, regResult);
		fclose(fp);
	}
			}
*/
		}
/*
		else
		{
	FILE* fp = fopen("C:\\cl.txt", "ab");
	if(fp)
	{
		fprintf(fp, "string open failed [%S]\r\n", szFullKey);
		fclose(fp);
	}
		}
*/
	}
	return pszDefaultData;
}

int CServiceModule::PushProfileInt(TCHAR* szKey, TCHAR* szValue, unsigned long ulData)
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if((szKey)&&(szValue)&&(wcslen(szKey))&&(wcslen(szValue)))
	{
		TCHAR szFullKey[1024];
		LONG regResult = ERROR_SUCCESS;
		HKEY hKey;
		_snwprintf(szFullKey, 1023, _T("Software\\Video Design Software\\CortexLauncher\\%s"), szKey);
		regResult = RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		szFullKey,
		0,
		NULL,
		0,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		NULL
				);
/*
RegCreateKeyEx(
  __in        HKEY hKey,
  __in        LPCTSTR lpSubKey,
  __reserved  DWORD Reserved,
  __in_opt    LPTSTR lpClass,
  __in        DWORD dwOptions,
  __in        REGSAM samDesired,
  __in_opt    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
  __out       PHKEY phkResult,
  __out_opt   LPDWORD lpdwDisposition
);
*/
		if(regResult==ERROR_SUCCESS)
		{ // set value
			char res[8]="";
			res[0]= (char)(ulData&0xff);
			res[1]= (char)((ulData>>8)&0xff);
			res[2]= (char)((ulData>>16)&0xff);
			res[3]= (char)((ulData>>24)&0xff);

			regResult = RegSetValueEx(
				hKey,
				szValue,
				0,
				REG_DWORD,
				(const BYTE *)(&res[0]),
				4
				);
/*
LONG WINAPI RegSetValueEx(
  __in        HKEY hKey,
  __in_opt    LPCTSTR lpValueName,
  __reserved  DWORD Reserved,
  __in        DWORD dwType,
  __in_opt    const BYTE *lpData,
  __in        DWORD cbData
);*/
			if(regResult==ERROR_SUCCESS)
			{
				RegCloseKey(hKey);
				return 0; //success
			}
		}
		RegCloseKey(hKey);
	}
	return -1;
}

int CServiceModule::PushProfileString(TCHAR* szKey, TCHAR* szValue, TCHAR* szData)
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if((szKey)&&(szValue)&&(wcslen(szKey))&&(wcslen(szValue)))
	{
		TCHAR szFullKey[1024];
		LONG regResult = ERROR_SUCCESS;
		HKEY hKey;
		_snwprintf(szFullKey, 1023, _T("Software\\Video Design Software\\CortexLauncher\\%s"), szKey);
		regResult = RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		szFullKey,
		0,
		NULL,
		0,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		NULL
				);
/*
RegCreateKeyEx(
  __in        HKEY hKey,
  __in        LPCTSTR lpSubKey,
  __reserved  DWORD Reserved,
  __in_opt    LPTSTR lpClass,
  __in        DWORD dwOptions,
  __in        REGSAM samDesired,
  __in_opt    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
  __out       PHKEY phkResult,
  __out_opt   LPDWORD lpdwDisposition
);
*/
		if(regResult==ERROR_SUCCESS)
		{ // set value

//			wsprintf((TCHAR*)res2, _T("NumModules = %d"), (int)res);
			regResult = RegSetValueEx(
				hKey,
				szValue,
				0,
				REG_SZ,
				(const BYTE *)szData,
				((wcslen(szData)+1)*sizeof(TCHAR))
				);
/*
LONG WINAPI RegSetValueEx(
  __in        HKEY hKey,
  __in_opt    LPCTSTR lpValueName,
  __reserved  DWORD Reserved,
  __in        DWORD dwType,
  __in_opt    const BYTE *lpData,
  __in        DWORD cbData
);*/
			if(regResult==ERROR_SUCCESS)
			{
				RegCloseKey(hKey);
				return 0; //success
			}
		}
		RegCloseKey(hKey);
	}
	return -1;
}

