
CortexLauncherps.dll: dlldata.obj CortexLauncher_p.obj CortexLauncher_i.obj
	link /dll /out:CortexLauncherps.dll /def:CortexLauncherps.def /entry:DllMain dlldata.obj CortexLauncher_p.obj CortexLauncher_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del CortexLauncherps.dll
	@del CortexLauncherps.lib
	@del CortexLauncherps.exp
	@del dlldata.obj
	@del CortexLauncher_p.obj
	@del CortexLauncher_i.obj
