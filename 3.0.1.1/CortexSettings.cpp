// CortexSettings.cpp: implementation of the CCortexSettings.
//
//////////////////////////////////////////////////////////////////////

#include "CortexDefines.h"
#include "CortexSettings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexSettings::CCortexSettings()
{
	m_pszName = NULL;
	m_ulMainMode = CX_MODE_DEFAULT;

	// ports
	m_usFilePort		= CX_PORT_FILE;
	m_usCommandPort	= CX_PORT_CMD;
	m_usStatusPort	= CX_PORT_STATUS;

	// port ranges
	m_usResourcePortMin		= CX_PORT_RESMIN;
	m_usResourcePortMax		= CX_PORT_RESMAX;

	m_usProcessPortMin		= CX_PORT_PRCMIN;
	m_usProcessPortMax		= CX_PORT_PRCMAX;

	// messaging for Cortex
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host

	// messaging for remote objects
	m_bUseLogRemote = false;	// write a separate log file for each object

	//admin
	m_bUseAuthentication = true;	// user/password verification, otherwise totally open to all clients

	// backup
	m_bUseClone = false;			// spark a clone (unless a clone itself)

}

CCortexSettings::~CCortexSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
}
