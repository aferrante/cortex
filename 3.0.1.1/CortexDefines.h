// CortexDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(CORTEXDEFINES_H_INCLUDED)
#define CORTEXDEFINES_H_INCLUDED

#include <stdlib.h>


#ifndef NULL
#define NULL 0
#endif

// modes
#define CX_MODE_DEFAULT			0x00000000  // exclusive
#define CX_MODE_LISTENER		0x00000001  // exclusive
#define CX_MODE_CLONE				0x00000002  // exclusive
#define CX_MODE_QUIET				0x00000004  // ORable - means, no statup UI and no message boxes.
#define CX_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override

// default port values.
#define CX_PORT_FILE				80		
#define CX_PORT_CMD					10560		
#define CX_PORT_STATUS			10561		

#define CX_PORT_RESMIN			10660		
#define CX_PORT_RESMAX			20659		

#define CX_PORT_PRCMIN			20660		
#define CX_PORT_PRCMAX			30659		

#define CX_PORT_INVALID			0	

// types
#define CX_TYPE_UNDEF				0x0000  // undefined	


// status
#define CX_STATUS_UNINIT				0x00000000  // uninitialized	

// owners are zero-based enumerated, but we reserve the following values
#define CX_OWNER_INVALID				0xffffffff  // not owned, or owner unknown
#define CX_OWNER_CORTEX					0xfffffffe  // owned by this instance of cortex (not another cortex somewhere)

// default intervals
#define CX_TIME_PING				5000		// get device status every 5 seconds
#define CX_TIME_FAIL				60000		// object failure after 1 minute timeout


//return values
#define CX_SUCCESS   0
#define CX_ERROR	   -1


// default filenames
#define CX_SETTINGS_FILE_DEFAULT	  "cortex.csf"
#define CX_SETTINGS_FILE_LISTENER	  "listen.csf"
#define CX_SETTINGS_FILE_CLONE		  "clone.csf"
#define CX_SETTINGS_FILE_SPARK			"cortex.csl"


// object status states:


// common commands.  objects must be careful not to override these.
#define CX_CMD_GETFILE	0x30

#define CX_CMD_USEAUTH	0xf0

#endif // !defined(CORTEXDEFINES_H_INCLUDED)
