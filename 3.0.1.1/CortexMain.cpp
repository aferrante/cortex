// CortexMain.cpp: implementation of the CCortexMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Cortex.h"  // just included to have access to windowing environment
#include "CortexDlg.h"  // just included to have access to windowing environment
#include "CortexHandler.h"  // just included to have access to windowing environment

#include "CortexMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;

extern CMessager* g_pmsgr;  // from Messager.cpp

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMain::CCortexMain()
{
}

CCortexMain::~CCortexMain()
{
}

char*	CCortexMain::CortexTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply cortex scripting language
{
	return pszBuffer;
}

int		CCortexMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return CX_SUCCESS;
}


void CortexMainThread(void* pvArgs)
{
	CCortexApp* pApp = (CCortexApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment


	//startup.
	g_bThreadStarted = true;

	// windows stuff
	CCortexDlg* pdlg = ((CCortexDlg*)(((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg));
	CWnd* pwndStatus = ((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT);
	CListCtrlEx* plist = (CListCtrlEx*)(((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_LIST1));
	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Cortex\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}


	pdlg->SetProgress(CXDLG_WAITING);

	if(pwndStatus) pwndStatus->SetWindowText("Initializing...");
	if(plist) plist->InsertItem(0, "Initializing...");  //I can insert one at the top, becuase this is the absolute first item the list should have.

	//create the main object.
	CCortexMain cortex;
//AfxMessageBox("x");
	InitMessager(&cortex.m_msgr);
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// modes.
			if(stricmp(pch, "/c")==0) // clone mode  (check for this first, it invalidates some of the rest)
			{
				//TODO create clone mode!
				cortex.m_settings.m_ulMainMode |= CX_MODE_CLONE;  // trumps listener
			}
			else
			if(stricmp(pch, "/l")==0) // listener mode
			{
				//TODO create listener mode
				cortex.m_settings.m_ulMainMode |= CX_MODE_LISTENER;  // trumped by clone
			}
			else
			if(stricmp(pch, "/q")==0) // quiet mode (works in listener or default, clone is by def quiet)
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_QUIET;	
			}
			else
			if(stricmp(pch, "/v")==0) // volatile mode settings used this session are not retained by default - does not override explicit saves via http interface
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_VOLATILE;	
			}
			else

			// params
			if(strnicmp(pch, "/s:", 3)==0) // server listen port(s) override
			{
			}
			else
			if(strnicmp(pch, "/r:", 3)==0) // resource port range override
			{
			}
			else
			if(strnicmp(pch, "/p:", 3)==0) // process port range override
			{
			}

			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

	if(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE)
		strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
	else
	if(cortex.m_settings.m_ulMainMode&CX_MODE_LISTENER)
		strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
	else  // default
		strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// get settings
		cortex.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		if(cortex.m_settings.m_bUseLog)
		{
			// for logfiles, we need params, and they must be in this format:
			//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			pszParams = file.GetIniString("Messager", "LogFileIni", "Cortex|YM");
			int nRegisterCode=0;

			nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
			if (nRegisterCode != MSG_SUCCESS) 
			{
				// inform the windowing environment
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
				AfxMessageBox(errorstring);
				pwndStatus->SetWindowText(errorstring);
			}

			free(pszParams); pszParams=NULL;
		}
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}


	cortex.m_msgr.DM(MSG_ICONINFO, "", "", "--------------------------------------------------\n\
-------------- Cortex 3.0.1.1 start --------------");  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
	cortex.m_http.InitializeMessaging(::DispatchEncodedMessage);
	cortex.m_net.InitializeMessaging(::DispatchEncodedMessage);


// load up the rest of the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		cortex.m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);
		cortex.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
		cortex.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
		cortex.m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

		cortex.m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

		cortex.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		cortex.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		cortex.m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
		cortex.m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 1)?true:false;

		if(pszParams) free(pszParams);

// get the security settings from a different file (encrypted) default to yes.
		_snprintf(pszPath, MAX_PATH, "%scortex.esf", pszCurrentDir);  // esf = encrypted settings file
		pszParams = file.GetIniString("Mode", "Encrypted", pszPath);  //reasonable default. - could search the file system, though.
		if(pszParams)
		{
			CFileUtil encfile;
			encfile.GetSettings(pszParams, true); 
			if(encfile.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				cortex.m_settings.m_bUseAuthentication = encfile.GetIniInt("Authentication", "Use", 1)?true:false;
			}
			free(pszParams); pszParams=NULL;
		}
	}


//init command and status listeners.
	if(pwndStatus) pwndStatus->SetWindowText("initializing command server...");

	if(cortex.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
		}
	}

	if(pwndStatus) pwndStatus->SetWindowText("initializing status server...");

	if(cortex.m_settings.m_usStatusPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
		}
	}



// init file server....
	if(pwndStatus) pwndStatus->SetWindowText("initializing file server...");
	if(plist) plist->SetItemText(0, 0, "initializing file server...");  

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// got settings already
		if(pszParams) free(pszParams); 
		_snprintf(pszPath, MAX_PATH, "%sroot", pszCurrentDir);  

		pszParams = file.GetIniString("FileServer", "Root", pszPath);  //reasonable default. - could search the file system, though.
//		pszParams = file.GetIniString("FileServer", "Root", "C:\\temp\\Temp\\VDI temp\\Harris\\IBC demo\\root");  // for testing only
		
	// if security (from settings) then create secure obj and attach to http obj
//	cortex.m_http.m_pSecure = new CSecurity;
//cortex.m_http.m_pSecure.InitSeurity();...


		// start the webserver
		if(pszParams)
		{
			if(cortex.m_http.SetRoot(pszParams)!=HTTP_SUCCESS)
			{
				if(pwndStatus) pwndStatus->SetWindowText("Error: could not set root settings.");
				if(plist) plist->SetItemText(0, 0, "Error: could not set root settings.");  
			}
			else
			{
				if(cortex.m_http.GetHost()!=HTTP_SUCCESS)
				{
					if(pwndStatus) pwndStatus->SetWindowText("Error: could not get host name.");
					if(plist) plist->SetItemText(0, 0, "Error: could not get host name.");  
				}
				else
				{

					_snprintf(pszPath, MAX_PATH, "http://%s/index.htm", cortex.m_http.m_pszHost);  
					pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);

//					AfxMessageBox(pApp->m_pszSettingsURL);

					// get port from settings
					if(cortex.m_http.InitServer(80, CortexHTTPThread, &cortex, &cortex.m_http, errorstring) <HTTP_SUCCESS)
					{
						if(pwndStatus) pwndStatus->SetWindowText(errorstring);
					}
					else
					{
						sprintf(errorstring,"Listening on %s:80", cortex.m_http.m_pszHost);
						if(pwndStatus) pwndStatus->SetWindowText(errorstring);
					}
				}
			}
			free(pszParams); pszParams = NULL;
		}
		else
		{
			if(pwndStatus) pwndStatus->SetWindowText("Error: could not get root settings.");
			if(plist) plist->SetItemText(0, 0, "Error: could not get root settings.");  
		}
	}
	else
	{
		// report failure!
	}
	pdlg->SetProgress(CXDLG_CLEAR);

	// now run the spark list.
	// this is the list of executables that get sparked
	pdlg->SetProgressColor(RGB(192,192,0));
	pdlg->SetProgress(CXDLG_WAITING);

	if(!(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE))
	{
		if(pwndStatus) pwndStatus->SetWindowText("Executing registered objects.");
		strcpy(pszFilename, CX_SETTINGS_FILE_SPARK);  // cortex spark list
		CFileUtil fileSpark;
		fileSpark.GetSettings(pszFilename, false); // not encrypted
		if(fileSpark.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			// got settings already
			if(pszParams) free(pszParams); 
			int nNumExe = 0, nCount =0;

			// local objects first
			nNumExe = fileSpark.GetIniInt("Local", "ObjectCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Object %03d Path", nCount);
				pszParams = fileSpark.GetIniString("Local", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.
						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename becuase we have it already
//						if(_execl( pszParams, pszFilename, NULL )<0) // crashes
						if(_spawnl( _P_NOWAIT , pszParams, pszFilename, NULL )<0)
						{
							//report failure
					AfxMessageBox("failure to spawn exe");
						}
						else
						{
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(100); // how much is too much?
						}
//					AfxMessageBox(pszParams);

					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}

			// remote objects
			nNumExe = 0, nCount =0;
/*
// remotes - do later.
			nNumExe = fileSpark.GetIniInt("Remote", "HostCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Remote Host %03d", nCount);
				pszParams = fileSpark.GetIniString("Remote", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.

						// have to send a spark command exchange to each remote host.
						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename becuase we have it already
						if(0)
						{
							//report failure
						}
						else
						{
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(100); // how much is too much?
						}
					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}
			*/
		}
		//else report error.
	}
	

	pdlg->SetProgress(CXDLG_CLEAR);
	pdlg->SetProgressColor(RGB(0,192,0));

	_timeb timestamp;
	tm* theTime;
	int x=0, dx=1;
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &timestamp );

		theTime = localtime( &timestamp.time	);
		strftime( pszFilename, 30, "%H:%M:%S", theTime );

		sprintf(pszFilename, "%s  %s:%d.%d.%d",
			pszFilename,
			cortex.m_http.m_pszHost,
			cortex.m_settings.m_usFilePort,
			cortex.m_settings.m_usCommandPort,
			cortex.m_settings.m_usStatusPort); // just using filename because we have it already
		if(pwndStatus) pwndStatus->SetWindowText(pszFilename);


		Sleep(3);
//		x+=dx;  
//		if((x>199)||(x<1)) dx=-dx;
//		pdlg->SetProgress(x);
		pdlg->SetProgress(CXDLG_EVILEYE);  // default settings.
	}

// shut down all the running objects;

	// here's a hard coded one for now

	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no nede to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = cortex.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		cortex.m_net.OpenConnection(cortex.m_http.m_pszHost, 10888, &s); // branding hardcode
		cortex.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		cortex.m_net.CloseConnection(s); // branding hardcode
	}


	pdlg->SetProgress(CXDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down file server.");
	cortex.m_http.EndServer();
//	AfxMessageBox("shutting down command server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down command server.");
	cortex.m_net.StopServer(cortex.m_settings.m_usCommandPort, 5000, errorstring);
//	AfxMessageBox("shutting down status server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down status server.");
	cortex.m_net.StopServer(cortex.m_settings.m_usStatusPort, 5000, errorstring);
	if(pwndStatus) pwndStatus->SetWindowText("exiting...");

	// save settings.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
/*
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniInt("FileServer", "ListenPort", cortex.m_settings.m_usFilePort);
		file.SetIniInt("CommandServer", "ListenPort", cortex.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", cortex.m_settings.m_usStatusPort);

		file.SetIniInt("Resources", "MinPort", cortex.m_settings.m_usResourcePortMin);
		file.SetIniInt("Resources", "MaxPort", cortex.m_settings.m_usResourcePortMax);

		file.SetIniInt("Processes", "MinPort", cortex.m_settings.m_usProcessPortMin);
		file.SetIniInt("Processes", "MaxPort", cortex.m_settings.m_usResourcePortMin);

		file.SetIniInt("Messager", "UseEmail", cortex.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", cortex.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "LogRemote", cortex.m_settings.m_bUseLogRemote?1:0);
		file.SetIniInt("Messager", "UseClone", cortex.m_settings.m_bUseClone?1:0);
*/
		file.SetSettings("cortex.csf", false);
	}


	//exiting
	cortex.m_msgr.DM(MSG_ICONINFO, "", "", "-------------- Cortex 3.0.1.1 exit ---------------\n\
--------------------------------------------------\n");  //(Dispatch message)
	pdlg->SetProgress(CXDLG_CLEAR);

	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(100); // another small delay at end
}


void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
*/
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}

// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void CortexHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CCortexMain* pCortex = (CCortexMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pCortex->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
*/
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
*/
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

*/


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								//if(phttp->m_pSecure)  // have to check the main object for permission.
								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // iff error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: Aurora/1.01dev" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev\r\nContent-Length: \r\n\r\n");

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assmebles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);

										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01dev");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

*/
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}




