HTML insert directives for cortex:

"<!-- cortex" is the open tag for cortex insertion
"cortex //-->" is the end tag.

these tags are stripped from the file, everything between them is preserved as is, except with the following possibilities:

escape sequences are:

%% = Put a % character
%@ = begin/end standard base64 encode
%* = begin/end VDS alphabet base64 encode
%H = hostname or IP to which communication should be directed
%P = Port to which communication should be directed
%h = hostname or IP from whence the request came.
%a = authorization string from whence the request came (plain text format user:password).
%c = relative path to object (codebase).
%u = unixtime of service (ASCII).
%d = execute a cortex directive.  use %d like quotes to delimit the directive name.

So, to initialize an ActiveX control for authoring, to tell it what host to address, the HTML template
might look like this:


<table width=100% height=100% border=0 cellpadding=0 cellspacing=0 bgcolor=#bb3333>
<!-- cortex
<tr><td height=1 colspan=2 valign=middle><img src="img/clear.gif" width=29 height=21 align=top><font face="Verdana" ><B>The sample authoring application<B></td></tr>
<tr><td width=3><img src="img/clear.gif" width=3></td><td valign=top width=100%% ><OBJECT ID="Testx" CLASSID="CLSID:E5BB65EF-36FC-4AB7-B201-4F74EE7AE871" CODEBASE="Testx.OCX">
<PARAM NAME="Init" VALUE="%*%H:%P:%a%*"> 
cortex //-->
</OBJECT></td></tr>
</table>

notice that within the tags, it becomes width=100%%

%*%H:%P:%a%* does the following:
let's say the follwing values exist:

hostname = 192.23.64.222
port = 20022
authorization = johnqpublic:mypassword

the string "192.23.64.222:20022:johnqpublic:mypassword" is assembled, then base 64 encoded with the
VDS alphabet.  (this way authorization is a somewhat more secure)

The OCX can then take this string and decompile it at the other end, and make its requests to
192.23.64.222:20022 with the authorization of the person who requested the HTML.

this is equivalent:
<table width=100% height=100% border=0 cellpadding=0 cellspacing=0 bgcolor=#bb3333>
<tr><td height=1 colspan=2 valign=middle><img src="img/clear.gif" width=29 height=21 align=top><font face="Verdana" ><B>The sample authoring application<B></td></tr>
<tr><td width=3><img src="img/clear.gif" width=3></td><td valign=top width=100% ><OBJECT ID="Testx" CLASSID="CLSID:E5BB65EF-36FC-4AB7-B201-4F74EE7AE871" CODEBASE="Testx.OCX">
<PARAM NAME="Init" VALUE="<!-- cortex%*%H:%P:%a%*cortex //-->"> 
</OBJECT></td></tr>
</table>


If a treemenu is being built, there may be links to other pages. that must get imported from each module.
cortex stores the files for each module in its own directory.
therefore, there will be a relative path to those files that must be inserted into the href, %c is used.

example of a directive:
<table>
<tr><td>something here</td></tr>
<!-- cortex
<tr><td>%dinsert_help_menu%d</td></tr>
cortex //-->
<tr><td>something else here</td></tr>

in the middle of the table, cortex would insert the contents of all of the help menu files it has. 
(They are formatted appropriately).

this is equivalent:
<table>
<tr><td>something here</td></tr>
<tr><td><!-- cortex%dinsert_help_menu%dcortex //--></td></tr>
<tr><td>something else here</td></tr>

