// Cortex.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CortexMain.h"
#include "Cortex.h"
#include "CortexHandler.h"
#include "CortexDlg.h"
#include <process.h>
//#include "CortexSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// global control vars for main thread
extern bool g_bKillThread;
extern bool g_bThreadStarted;


// ******************************************
// THINGS you have to do to get this project to compile correctly
// 1) Add to files: bmp.cpp, bmp.h, EmailHandler.cpp/.h, 
//    FileHandler.cpp/.h, ListCtrlEx.cpp/.h, MessageDispatcher.cpp/.h, MDdefines.h,
//    NAHandler.cpp/.h, networking.cpp/.h, sendmail.cpp/.h, TextUtils.cpp/.h,
//    UIHandler.cpp/.h
//    The inlcludes for all these are in "$$root$$.h", and you can remove them if
//    your project does not need them.  However, the default project is built with:
//    an App Class, a Dlg class, a Handler Class, and a Settings class.
//    The App Class is the App, the Dlg is the Dialog which is displayed when the
//    systray icon is hit.  This is the class that is the "main dialog" and has all the
//    Messge dispatching, etc.  Howevr, it is not the application's main window.  The
//    main window is not visible, it is the Handler class.  It is also persistent, and
//    basically handles creation, destruction, the icons and handles the mouse clicks
//    in the systray.  The Settings class handles all the Settings, the dialog is not
//    persistent although the data is.

// ******************************************
// There is a list of commented out includes in the "$$root$$.h" file.  
// Generally, add includes to that file, as all files in the project include that file.


/////////////////////////////////////////////////////////////////////////////
// CCortexApp

BEGIN_MESSAGE_MAP(CCortexApp, CWinApp)
	//{{AFX_MSG_MAP(CCortexApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexApp construction

CCortexApp::CCortexApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
//	m_ulFlags = CXWF_DEFAULT;
	m_pszSettingsURL = NULL;
	m_ulMainMode = CX_MODE_DEFAULT;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCortexApp object

CCortexApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCortexApp initialization

BOOL CCortexApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// uncomment the following to initialize RichEdit Controls
//	AfxInitRichEdit();

	// prevent multiple instances

	bool bAllowMultiple = false;
/*
  if (m_lpCmdLine[0] != '\0')
  {
		if( (strstr(m_lpCmdLine, "/c")) || (strstr(m_lpCmdLine, "/C")) )  // clone mode.  Allow another instance (until we check later for existence of a listener)
		bAllowMultiple = true;
  }
*/

	// parse the command line to see if there are any overrides.
  if (m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// modes.
			if(stricmp(pch, "/c")==0) // clone mode  (check for this first, it invalidates some of the rest)
			{
				//TODO create clone mode!
				bAllowMultiple = true;
				m_ulMainMode |= CX_MODE_CLONE;  // trumps listener
			}
			else
			if(stricmp(pch, "/l")==0) // listener mode
			{
				//TODO create listener mode
				m_ulMainMode |= CX_MODE_LISTENER;  // trumped by clone
			}
			else
			if(stricmp(pch, "/q")==0) // quiet mode (works in listener or default, clone is by def quiet)
			{
				m_ulMainMode |= CX_MODE_QUIET;	
			}
			else
			if(stricmp(pch, "/v")==0) // volatile mode settings used this session are not retained by default - does not override explicit saves via http interface
			{
				m_ulMainMode |= CX_MODE_VOLATILE;	
			}
			else

			// params
			if(strnicmp(pch, "/s:", 3)==0) // server listen port(s) override
			{
			}
			else
			if(strnicmp(pch, "/r:", 3)==0) // resource port range override
			{
			}
			else
			if(strnicmp(pch, "/p:", 3)==0) // process port range override
			{
			}

			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }


	
	::CreateMutex(NULL,TRUE,m_pszExeName);
	if((GetLastError()==ERROR_ALREADY_EXISTS)&&(!bAllowMultiple)) return FALSE;  

  
	// uncomment the following to write PID to file (change filename)
  FILE *fp; fp = fopen("cortex.pid", "wt");
  if (fp) { fprintf(fp, "%ld\n", _getpid()); fclose(fp); } 


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  //SetRegistryKey("Video Design Interactive");  // can use this for licensing later on, or use a hidden file somewhere

	CCortexDlg* pDlg = new CCortexDlg();
	VERIFY(pDlg->Create());
//	pDlg->DoModal();

// handler window for systray
	CCortexHandler* pWnd = new CCortexHandler();
	pWnd->m_pMainDlg = pDlg;
	VERIFY(pWnd->Create());
	m_pMainWnd = pWnd;

	// here start the main (almost non-MFC) thread.
	// we can feed a pointer to the app into it to give it access to the status windows.
	// all settings are in the thread.
	g_bThreadStarted = false;
	if(_beginthread(CortexMainThread, 0, (void*) this)==-1)
	{
		AfxMessageBox("Could not begin main thread!\nMust exit.");
		return FALSE;
	}
//	while(!g_bThreadStarted){Sleep(10);}
//	AfxMessageBox("Q");

	return TRUE;
}

int CCortexApp::ExitInstance() 
{
	g_bKillThread=true;
	while(g_bThreadStarted) Sleep(1);
	if(m_pszSettingsURL) free(m_pszSettingsURL);
	return CWinApp::ExitInstance();
}

char* CCortexApp::GetSettingsFilename() 
{
	char* pch = NULL;
	FILE* fp = fopen(CX_SETTINGS_FILE_SETTINGS, "rb");
	if(fp)
	{
		// determine file size
		fseek(fp,0,SEEK_END);
		unsigned long ulLen = ftell(fp);

		if((ulLen>0)&&(ulLen<MAX_PATH))
		{	
			fseek(fp,0,SEEK_SET);
			// allocate buffer and read in file contents
			pch = (char*) malloc(ulLen+1); // for 0
			if(pch!=NULL)
			{
	//			CString foo; foo.Format("len=%d", ulLen);
	//	AfxMessageBox(foo);
				fread(pch,sizeof(char),ulLen,fp);
				memset(pch+ulLen, 0, 1);
//		AfxMessageBox(pch);
			}
		}
		fclose(fp);
	}
	if(pch==NULL)
	{
		pch = (char*) malloc(MAX_PATH+1); // for 0
		if(pch!=NULL)
		{
			if(m_ulMainMode&CX_MODE_CLONE)
				strcpy(pch, CX_SETTINGS_FILE_CLONE);  // cortex settings file
			else
			if(m_ulMainMode&CX_MODE_LISTENER)
				strcpy(pch, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
			else  // default
				strcpy(pch, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file
		}
	}

	return pch;
}

