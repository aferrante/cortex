// CortexData.cpp: implementation of the CCortexData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include "CortexData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CCortexObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexObject::CCortexObject()
{
	m_pszName		= NULL;
	m_usType		= CX_TYPE_UNDEF;

	// destinations within object
	m_ppszDestName		= NULL;
	m_ppszDestParams	= NULL;  // has to be generic for all types
	m_ulNumDest				= 0; // number of destinations.

	// dependencies within object
	m_ppszResourceName		= NULL;
	m_ppszResourceParams	= NULL;  // has to be generic for all types
	m_ulNumResources			= NULL; // number of dependency resources.

	// IP and ports
	m_pszHost				= NULL;
	m_usFilePort		= CX_PORT_INVALID;
	m_usCommandPort	= CX_PORT_INVALID;
	m_usStatusPort	= CX_PORT_INVALID;

	// cortex assigned status
	m_ulStatus	= CX_STATUS_UNINIT;
	m_ulOwner		= CX_OWNER_INVALID;

	// there is a negotiation to set the following from default values.
	m_ulStatusIntervalMS	= CX_TIME_PING;		// the interval, in milliseconds, at which we expect to hear status.
	m_ulFailureIntervalMS	= CX_TIME_FAIL;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	//m_timebLastStatus; // the time of the last status received (parseable)
	//m_timebLastTime;		// the last time received (object's local time)
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for object status
	m_pszStatus = NULL;	// parseable string
	m_pszInfo = NULL;		// human readable info string
}

CCortexObject::~CCortexObject()
{

}

void CCortexObject::Free()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_ppszDestName)
	{
		unsigned long i=0;
		while(i<m_ulNumDest)
		{
			if(m_ppszDestName[i]) free(m_ppszDestName[i]); // must use malloc to allocate
			if(m_ppszDestParams[i]) free(m_ppszDestParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszDestName);
	}
	if(m_ppszResourceName)
	{
		unsigned long i=0;
		while(i<m_ulNumResources)
		{
			if(m_ppszResourceName[i]) free(m_ppszResourceName[i]); // must use malloc to allocate
			if(m_ppszResourceParams[i]) free(m_ppszResourceParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszResourceName);
	}
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
}





//////////////////////////////////////////////////////////////////////
// CCortexData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexData::CCortexData()
{
	m_usResourcePortLast	= CX_PORT_INVALID;
	m_usProcessPortLast		= CX_PORT_INVALID;

	m_ppObj = NULL;
	m_ulNumObjects = 0;
	m_ulNumResources = 0;  // this is for convenience only
	m_ulNumProcesses = 0;  // this is for convenience only

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
	m_pszInfo = NULL;		// human readable info string
}

CCortexData::~CCortexData()
{
	if(m_ppObj)
	{
		unsigned long i=0;
		while(i<m_ulNumObjects)
		{
			if(m_ppObj[i]) delete m_ppObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
}
