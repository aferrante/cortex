// CortexDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(CORTEXDEFINES_H_INCLUDED)
#define CORTEXDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define CX_CURRENT_VERSION		"3.0.4.4"
#define CX_AUTH_USER					"cortex"  // for cortex to tell an object that it is cortex that is sending commands
#define CX_AUTH_PWD						"medulla" // cortex's password
#define CX_B64ALPHA						")x-{fJ|M2mqwO( /+tS:.<*WGoF&%`7Uc$]=Hhj;D^_0}5b3NQ[l1LsAX8d>4g\\y"   // have to escape the backslash
#define CX_B64PADCH						'@'


// modes
#define CX_MODE_DEFAULT			0x00000000  // exclusive
#define CX_MODE_LISTENER		0x00000001  // exclusive
#define CX_MODE_CLONE				0x00000002  // exclusive
#define CX_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define CX_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define CX_MODE_MASK				0x0000000f  //

// default port values.
//#define CX_PORT_FILE				80
#define CX_PORT_CMD					10560
#define CX_PORT_STATUS			10561

#define CX_PORT_RESMIN			10660
#define CX_PORT_RESMAX			20659

#define CX_PORT_PRCMIN			20660
#define CX_PORT_PRCMAX			30659

#define CX_PORT_INVALID			0

// types
#define CX_TYPE_UNDEF				0x0000  // undefined
#define CX_TYPE_RESOURCE		0x8000  // resource
#define CX_TYPE_PROCESS			0x4000  // process

// resource types
#define CX_TYPE_DB					0x0001  // database
#define CX_TYPE_AUTOMATION	0x0002  // automation
#define CX_TYPE_CG					0x0003  // CG
#define CX_TYPE_AUDIO				0x0004  // audio
#define CX_TYPE_MIXER				0x0005  // mixer
#define CX_TYPE_FEED				0x0006  // feed
#define CX_TYPE_POLL				0x0007  // polling data manager
#define CX_TYPE_USER				0x000f  // user defined

// process types
#define CX_TYPE_PERIODIC		0x0010  // periodic process, like a show
#define CX_TYPE_ETERNAL			0x0020  // eternal process.
#define CX_TYPE_INFO				0x0100  // process that acts as an information resource for other processes.
#define CX_TYPE_META				0x0200  // process that keeps tabs on other processes and resources, without grabbing ownership.


// status
#define CX_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define CX_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)
#define CX_STATUS_ERROR								0x00000020  // error (red icon)
#define CX_STATUS_OK									0x00000030  // ready (green icon)
#define CX_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);
#define CX_ICON_MASK									0x00000070  // mask

#define CX_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)


#define CX_FLAGS_NONE									0x00000000  // nothing
#define CX_FLAGS_RESPARK							0x00000001  // on connection loss, kill and re-spark
#define CX_FLAGS_ERRORSENT						0x00000100  // on extended connection loss, messages sent
#define CX_FLAGS_CONNERRORSENT				0x00000200  // on connection broken, messages sent

// remove file server
//#define CX_STATUS_FILESVR_START				0x00000100  // starting the file server
//#define CX_STATUS_FILESVR_RUN					0x00000200  // file server running
//#define CX_STATUS_FILESVR_END					0x00000300  // file server shutting down
//#define CX_STATUS_FILESVR_ERROR				0x00000400  // file server error
//#define CX_STATUS_FILESVR_MASK				0x00000700  // file server mask bits

#define CX_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define CX_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define CX_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define CX_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define CX_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define CX_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define CX_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define CX_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define CX_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define CX_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define CX_STATUS_THREAD_START				0x00100000  // starting the main thread
#define CX_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define CX_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define CX_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define CX_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define CX_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define CX_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define CX_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define CX_STATUS_FAIL_DB							0x20000000  // could not get DB
#define CX_STATUS_FAIL_SMTP						0x40000000  // could not register email
#define CX_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

// owners are zero-based enumerated, but we reserve the following values
#define CX_OWNER_INVALID				0xffffffff  // not owned, or owner unknown
#define CX_OWNER_CORTEX					0xfffffffe  // owned by this instance of cortex (not another cortex somewhere)

// default intervals
#define CX_TIME_PING				5000		// get device status every 5 seconds
#define CX_TIME_FAIL				60000		// object failure after 1 minute timeout

#define CX_DB_MOD_MAX				0xffff


//return values
#define CX_SUCCESS   0
#define CX_ERROR	   -1


//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2  


// default filenames
#define CX_SETTINGS_FILE_SETTINGS	  "cortex.csr"		// csr = cortex settings redirect
#define CX_SETTINGS_FILE_DEFAULT	  "cortex.csf"		// csf = cortex settings file
#define CX_SETTINGS_FILE_LISTENER	  "listen.csf"		// csf = cortex settings file
#define CX_SETTINGS_FILE_CLONE		  "clone.csf"			// csf = cortex settings file
#define CX_SETTINGS_FILE_SPARK			"cortex.csl"		// csl = cortex spark list
#define CX_SETTINGS_FILE_ENCRYPT		"cortex.esf"		// esf = encrypted settings file
#define CX_SETTINGS_FILE_SECURE1		"cortex.epa"		// epa = ecrypted password archive
#define CX_SETTINGS_FILE_SECURE2		"cortex.epb"		// epb = ecrypted password backup


// common commands.  objects must be careful not to override these.
#define CX_CMD_NULL					0x00  // null or empty command
#define CX_CMD_ACK					0x06  // same as ascii, used as response only
#define CX_CMD_NAK					0x15  // same as ascii, used as response only

#define CX_CMD_HASAUTH			0x80  // usually a subcommand, ORable, means, has auth info in data.

// commands used by cortex to command cortex listeners
#define CX_CMD_SPARK				0x31  // run the spark list. main cortex hostname:commandport in data.
#define CX_CMD_KILL					0x32  // send kill commands to the exes on the spark list.
#define CX_CMD_KILLSELF			0x33  // end process, possibly for upgrade. (upgrading process should shell execute after upgrade)

// communications params
// commands used by cortex to command resources and processes
#define CX_CMD_CHGCOMM			0x01  // change communication parameters.
// following are ORable subs.
#define CX_CMD_HOST					0x01  // change the host (can be used for switching to backup)
#define CX_CMD_PORT					0x02  // change the ports // has data "commandport:statusport"


// familiar name labels
#define CX_CMD_GETNAME			0x02  // get the familiar names associated with a process or resource.
#define CX_CMD_CHGNAME			0x03  // change the familiar name for a process or resource (if we have one thats identical, must append a suffix to make unique)  new name given in data
// following are exclusive subs.
#define CX_CMD_OBJ					0x01  // default, get the familiar name for the object
#define CX_CMD_DEST					0x02  // get the familiar name for all destinations in a resource.


// other object information
#define CX_CMD_GETINFO			0x04  // get the information associated with a cortex object
#define CX_CMD_ASSIGN				0x05  // set ownership and other info
// following are exclusive subs.
#define CX_CMD_TYPE					0x01  // get the enumerated type value of the object.
#define CX_CMD_STATUS				0x02  // get current status.
#define CX_CMD_OWNER				0x03  // get/set the name of the current owner.
#define CX_CMD_STATUSINT		0x04  // get/set status interval.
#define CX_CMD_FAILINT			0x05  // get/set failure interval.
#define CX_CMD_SETTING			0x06  // get/set setting value.
#define CX_CMD_AUTHCRED			0x07  // set cortex authorization credentials to identify cortex to a module

// files
#define CX_CMD_GETFILE			0x06	// get files associated with module.
// following are exclusive subs.
#define CX_CMD_LIST_ADMIN   0x01  // retrieves a list of files and templates necessary for admin
#define CX_CMD_LIST_AUTH	  0x02  // retrieves a list of files and templates necessary for auth
#define CX_CMD_LIST_STATUS  0x03  // retrieves a list of files and templates necessary for status
#define CX_CMD_LIST_LOGS	  0x04  // retrieves a list of files and templates necessary for logs
#define CX_CMD_LIST_HELP	  0x05  // retrieves a list of files and templates necessary for help
#define CX_CMD_LIST_QUERY	  0x06  // retrieves a list of files that match a query
#define CX_CMD_FILE					0x08  // retrieves a single file. (used after receiving lists, retrieves deficiencies)

#define CX_CMD_BYE				  0x0f  // command module to shut down.
#define CX_CMD_GETSTATUS	  0x99 // gets status info


// commands used by resources and processes to request things from cortex
#define CX_REQ_HELLO				0x01  // startup... has data "host:commandport:statusport:type:name:initialstatus", type is  CX_TYPE_RESOURCE or CX_TYPE_PROCESS
#define CX_REQ_GIVESTATUS		0x02  // gives formatted status of module and destinations, etc
#define CX_REQ_REQOWN		    0x03  // requests ownership of a destination
#define CX_REQ_RELOWN		    0x04  // releases ownership of a destination
#define CX_REQ_AUTHCHK		  0x05  // check credentials against the cortex main security files

// commands used by cortex to answer resources and processes. (main cmd should be ACK or NAK plus flags)
#define CX_REPLY_OK					0x00  // was able to comply
#define CX_REPLY_NOT				0x01  // was not able to comply.


//Graphics Destination types (decimal)
#define CX_DESTTYPE_UNKNOWN                    0000  // type unknown

//Orad
#define CX_DESTTYPE_ORAD_DVG                   1000  // Orad DVG
#define CX_DESTTYPE_ORAD_HDVG                  1001  // Orad HDVG


// Miranda (Oxtel)
#define CX_DESTTYPE_MIRANDA_IS2                2001  // Imagestore 2
#define CX_DESTTYPE_MIRANDA_INT                2002  // Intuition
#define CX_DESTTYPE_MIRANDA_IS300              2003  // Imagestore 300
// HD types were this, but changed (to below)
//#define CX_DESTTYPE_MIRANDA_HDIS               2004  // Imagestore HD
//#define CX_DESTTYPE_MIRANDA_HDINT              2005  // Intuition HD
#define CX_DESTTYPE_MIRANDA_HDIS               2101  // Imagestore HD
#define CX_DESTTYPE_MIRANDA_HDINT              2102  // Intuition HD
#define CX_DESTTYPE_MIRANDA_HDIS300            2103  // Imagestore 300 HD


//Chyron
// removed, no need.
//#define CX_DESTTYPE_CHYRON_LEX                 3000  // Duet LEX
//#define CX_DESTTYPE_CHYRON_HYPERX              3001  // Duet HyperX
//#define CX_DESTTYPE_CHYRON_MICROX              3002  // Duet MicroX

#define CX_DESTTYPE_CHYRON_CHANNELBOX          3003  // Channel Box
#define CX_DESTTYPE_CHYRON_CALBOX              3004  // CAL Box
#define CX_DESTTYPE_CHYRON_LYRIC               3005  // Generic Lyric Intelligent Interface, any hardware
#define CX_DESTTYPE_CHYRON_HDCHANNELBOX        3103  // Channel Box HD
#define CX_DESTTYPE_CHYRON_HDCALBOX            3104  // CAL Box HD
#define CX_DESTTYPE_CHYRON_HDLYRIC             3105  // Generic Lyric Intelligent Interface, any hardware, HD

//Evertz
#define CX_DESTTYPE_EVERTZ_9625LG              4000
#define CX_DESTTYPE_EVERTZ_9625LGA             4001
#define CX_DESTTYPE_EVERTZ_9725LGA             4002
#define CX_DESTTYPE_EVERTZ_9725LG              4003
#define CX_DESTTYPE_EVERTZ_HD9625LG            4100
#define CX_DESTTYPE_EVERTZ_HD9625LGA           4101
#define CX_DESTTYPE_EVERTZ_HD9725LG            4102
#define CX_DESTTYPE_EVERTZ_HD9725LGA           4103

// Harris (Leitch)
#define CX_DESTTYPE_HARRIS_ICONII              5000    // Icon station with Intelligent Interface
#define CX_DESTTYPE_HARRIS_ICONXML             5001    // Icon station with XML Interface
#define CX_DESTTYPE_HARRIS_HDICONII            5100    // Icon station with Intelligent Interface HD
#define CX_DESTTYPE_HARRIS_HDICONXML           5101    // Icon station with XML Interface HD

#define CX_HARRIS_ICONII_LSFORMAT_NUM4				0 // 4 digit numerical LayoutSalvoFormat
#define CX_HARRIS_ICONII_LSFORMAT_NUM8				1 // 8 digit numerical LayoutSalvoFormat
#define CX_HARRIS_ICONII_LSFORMAT_ALPHA				2 // alphanumerical string LayoutSalvoFormat

// Aivd (Pinnacle)
#define CX_DESTTYPE_AVID_DEKOCASTXML					 6000    // Pinnacle (Avid) DekoCast XML
#define CX_DESTTYPE_AVID_HDDEKOCASTXML				 6100    // Pinnacle (Avid) DekoCast XML HD


//Automation types


//Harris
#define CX_AUTOTYPE_HARRIS_ADC					1000 // Harris ADC using API
#define CX_AUTOTYPE_HARRIS_DSERIES			1001 // D-series (Encoda)

// Omnibus
#define CX_AUTOTYPE_OMNIBUS_COLOSSUS		2000 // Colossus Adaptor XML
#define CX_AUTOTYPE_OMNIBUS_ITX					2001 // ITX XML (Colossus type)

// Pro-Bel
#define CX_AUTOTYPE_PROBEL_MORPHEUS			3000 // Pro Bel Morpheus

// Pebble Beach Systems
#define CX_AUTOTYPE_PEBLBCH_PBA					4000 // Pebble Beach Systems (Pebble Beach Automation - yes, that is their name for it)

// Sundance
#define CX_AUTOTYPE_AVID_TITAN					5000 // Avid (Sundance) Titan automation
#define CX_AUTOTYPE_AVID_FBNXT					5001 // Avid (Sundance) FastBreak NXT Automation 

// Florical
#define CX_AUTOTYPE_FLORICAL_AIRBOSS		6000 // Florical


// debug defines
#define CX_DEBUG_CONNECT				0x00000004
#define CX_DEBUG_CONNECTSTATUS  0x00000008
#define CX_DEBUG_COMM						0x00000200



//  XML handler defines
#define CX_XML_VERSION			"2.1"


#define CX_XML_NODECOUNT			5
#define CX_XML_NODE_CORTEX		0
#define CX_XML_NODE_RX				1
#define CX_XML_NODE_CMD				2
#define CX_XML_NODE_OPTIONS		3
#define CX_XML_NODE_DATA			4

#define CX_XML_BUFFER_COUNT						12
#define CX_XML_BUFFER_CORTEXBEGIN			0
#define CX_XML_BUFFER_TX							1
#define CX_XML_BUFFER_ACKBEGIN				2
#define CX_XML_BUFFER_MSG							3
#define CX_XML_BUFFER_FLAGSBEGIN			4
#define CX_XML_BUFFER_FLAGS						5
#define CX_XML_BUFFER_FLAGSEND				6
#define CX_XML_BUFFER_DATABEGIN				7
#define CX_XML_BUFFER_DATA						8
#define CX_XML_BUFFER_DATAEND					9
#define CX_XML_BUFFER_ACKEND					10
#define CX_XML_BUFFER_CORTEXEND				11


#define CX_XML_ERR_SUCCESS						0 // success
#define CX_XML_ERR_XMLPARSE						-1 // XML parse error
#define CX_XML_ERR_NOCHILD						-2 // XML format error
#define CX_XML_ERR_NOCMD							-3 // Cortex XML format error
#define CX_XML_ERR_BADCMD							-4 // Unsupported command


#define CX_XML_TYPE_UNK						0 // unknown
#define CX_XML_TYPE_ACK						1	// 4.1 ack Acknowledges a message with success
#define CX_XML_TYPE_NAK						2	// 4.2 nak Acknowledges a message with failure, or cancels
#define CX_XML_TYPE_PERSIST				3	// 4.3 persist Requests a persistent connection
#define CX_XML_TYPE_QUIT					4	// 4.4 quit Disconnects a persistent connection
#define CX_XML_TYPE_STATUS				5	// 4.5 status Obtains status information regarding the module
#define CX_XML_TYPE_BYE						6	// 4.6 bye Causes the module to shut down
#define CX_XML_TYPE_GETVERSION		7	// 4.7 get_version gets the version of the module as well as the build date
#define CX_XML_TYPE_PING					8	// 4.8 ping just sends an ack to indicate it is alive

// Cortex specific XML commands
#define CX_XML_TYPE_GETMODULES		101 // 6.1 get_modules Obtains a list of all currently connected modules, and information about each module
#define CX_XML_TYPE_GETLOCAL			102 // 6.2 get_local Obtains a list of local modules which Cortex attempts to initiate at startup, and manage if they successfully connect.
#define CX_XML_TYPE_GETREMOTE			103 // 6.3 get_remote Obtains a list of local remote hosts on which Cortex attempts to initiate a remote startup procedure, at local startup.
#define CX_XML_TYPE_GETQUERY			104 // 6.4 get_query Obtains a list of registered periodic database queries and their parameters
#define CX_XML_TYPE_GETSHELL			105 // 6.5 get_shell Obtains a list of registered periodic shell commands and their parameters




#endif // !defined(CORTEXDEFINES_H_INCLUDED)
