// CortexMain.cpp: implementation of the CCortexMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Cortex.h"  // just included to have access to windowing environment
#include "CortexDlg.h"  // just included to have access to windowing environment
#include "CortexHandler.h"  // just included to have access to windowing environment


#include "CortexMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

#include "CortexShared.h"  // included to have xml messaging objects and other shared things
//#include "CortexSplashDlg.h"

#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus=false;

CCortexMain* g_pcortex=NULL;

extern CCortexApp theApp;

extern CMessager* g_pmsgr;  // from Messager.cpp

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMain::CCortexMain()
{
}

CCortexMain::~CCortexMain()
{
}


/*

char*	CCortexMain::CortexTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply cortex scripting language
{
	return pszBuffer;
}

int		CCortexMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return CX_SUCCESS;
}
*/

SOCKET*	CCortexMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // cortex initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
//CString foo; foo.Format("sending to %s:%d",pchHost, usPort);AfxMessageBox(foo);

	if(m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps)>=NET_SUCCESS)
	{
		m_net.SendData(NULL, *ps, 5000, 0, NET_SND_CLNTACK);
	}

	return ps;
}

int		CCortexMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex replies to an object server after receiving data. (usually ack or nak)
{
	return CX_SUCCESS;
}

int		CCortexMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex answers a request from an object client
{
	return CX_SUCCESS;
}

int CCortexMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return CX_SUCCESS;
		}
	}
	return CX_ERROR;
}

void CortexMainThread(void* pvArgs)
{
	CCortexApp* pApp = (CCortexApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.

	//create the main objects.
	CCortexMain cortex;

	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_START;
	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNINIT;

	cortex.m_data.GetHost();

	g_pcortex = &cortex;
	g_bThreadStarted = true;

	CDBUtil* pdb = NULL;

	// windows stuff  *** removed!
//	CCortexDlg* pdlg = ((CCortexDlg*)(((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg));
//	CListCtrlEx* plist = (CListCtrlEx*)(&(pdlg->m_lce));
//	CWnd* pwndStatus = ((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT);

	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Cortex\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	char dberrorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	strcpy(dberrorstring, "");


	// get settings.

	cortex.m_settings.Settings(true); //read


	// at this point, we have not done anything real yet.
	// we can choose to not "start up" immediately, in which case we put up a splash screen for a bit
	// no need for splash screen, just use the main wnd.
	// and count down to init.

	if(cortex.m_settings.m_nStartupDelayMS>0)  
	{

/*		
		CCortexSplashDlg* pcsdlg = NULL;

		if(cortex.m_settings.m_nStartupDelayMS>2000)  // arbitrary 2 second threshold for human needing some eye candy while waiting.
		{
			pcsdlg = new CCortexSplashDlg;
			if(pcsdlg)
			{
				if(pcsdlg->Create(theApp.GetMainWnd())==NULL)
				{
					try{ delete pcsdlg; } catch(...){}
					pcsdlg = NULL;
				}
			}
		}

		if(pcsdlg)
		{
			pcsdlg->ShowWindow(SW_SHOW);
		}
*/
		int nClock = clock() + cortex.m_settings.m_nStartupDelayMS;
		int nDelay = nClock - clock();
//		while((nClock>clock())&&(!g_bKillThread))
		while((nDelay>0)&&(!g_bKillThread))
		{
			Sleep(10);

			nDelay = nClock - clock();
//		if(pcsdlg)
			if((theApp.m_pMainWnd)&&(((CCortexHandler*)theApp.m_pMainWnd)->m_pMainDlg))
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is delaying %d second%s before initializing...", ((nDelay+500)/1000), ((nDelay>1499)?"s":""));
//				pcsdlg->GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText(errorstring);

				((CCortexDlg*)((CCortexHandler*)theApp.m_pMainWnd)->m_pMainDlg)->m_szStatusText = errorstring;
			}

		}
		((CCortexDlg*)((CCortexHandler*)theApp.m_pMainWnd)->m_pMainDlg)->m_szStatusText = "     Main mode enabled.";  // back to default

/*		
		if(pcsdlg)
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is initializing...");
			pcsdlg->GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText(errorstring);
			Sleep(250); // show the init state for a blip
			pcsdlg->ShowWindow(SW_HIDE);
			pcsdlg->DestroyWindow();
			try{ delete pcsdlg; } catch(...){}
		}
*/
	}



///////////////////////////////////
//  need this for other things than main settings....
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
/*
AfxMessageBox("3");
	if(g_pcortex)
	{
		char* pchF=g_pcortex->m_settings.GetSettingsFilename();
		if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	}
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file
*/

//	CFileUtil file;
//	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
//
///////////////////////////////////

//	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
//	{
		// get settings
//		cortex.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
//		cortex.m_settings.m_pszName = file.GetIniString("Main", "Name", "Cortex");
//		cortex.m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);

//		cortex.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");
//		cortex.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
/*
		// compile license key params
		if(g_pcortex->m_data.m_key.m_pszLicenseString) free(g_pcortex->m_data.m_key.m_pszLicenseString);
		g_pcortex->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(cortex.m_settings.m_pszLicense)+1);
		if(g_pcortex->m_data.m_key.m_pszLicenseString)
		sprintf(g_pcortex->m_data.m_key.m_pszLicenseString, "%s", cortex.m_settings.m_pszLicense);

		g_pcortex->m_data.m_key.InterpretKey();

		if(g_pcortex->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pcortex->m_data.m_key.m_ulNumParams)
			{
				if((g_pcortex->m_data.m_key.m_ppszParams)
					&&(g_pcortex->m_data.m_key.m_ppszValues)
					&&(g_pcortex->m_data.m_key.m_ppszParams[i])
					&&(g_pcortex->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pcortex->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_pcortex->m_data.m_nMaxLicensedDevices = atoi(g_pcortex->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!cortex.m_data.m_key.m_bExpires)
					||((cortex.m_data.m_key.m_bExpires)&&(!cortex.m_data.m_key.m_bExpired))
					||((cortex.m_data.m_key.m_bExpires)&&(cortex.m_data.m_key.m_bExpireForgiveness)&&(cortex.m_data.m_key.m_ulExpiryDate+cortex.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!cortex.m_data.m_key.m_bMachineSpecific)
					||((cortex.m_data.m_key.m_bMachineSpecific)&&(cortex.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				cortex.m_data.SetStatusText(errorstring, CX_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				cortex.m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			cortex.m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
		}
		
/*		
		cortex.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
		cortex.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
		cortex.m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

		cortex.m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
		cortex.m_settings.m_usProcessPortMax = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

		cortex.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		cortex.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
//		cortex.m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
		cortex.m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 0)?true:false;
		cortex.m_settings.m_bUseDB = file.GetIniInt("Mode", "UseDB", 0)?true:false;

		cortex.m_settings.m_nSparkStaggerIntervalMS = file.GetIniInt("Timing", "SparkInterval", 1000);
		
		cortex.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", cortex.m_settings.m_pszName?cortex.m_settings.m_pszName:"Cortex");
		cortex.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		cortex.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
//		cortex.m_settings.m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", cortex.m_settings.m_pszName?cortex.m_settings.m_pszName:"CortexBackup");
//		cortex.m_settings.m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
//		cortex.m_settings.m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
		cortex.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		cortex.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		cortex.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
		cortex.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
		cortex.m_settings.m_bPeriodicQueries = file.GetIniInt("Database", "UsePeriodicQueries", 0)?true:false; 
		if(cortex.m_settings.m_bPeriodicQueries)
		{
			cortex.m_settings.m_pszPeriodicQueries = file.GetIniString("Database", "PeriodicQueries", "600|sp_PrimaryToBackup");    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
			CSafeBufferUtil sbu;
			char* pch = sbu.Token(cortex.m_settings.m_pszPeriodicQueries, strlen(cortex.m_settings.m_pszPeriodicQueries), "|");
			while(pch)
			{
				unsigned long ulInterval = atol(pch);
				unsigned long ulOffset=0;   // number of seconds to offset
				pch = strchr(pch, '+');  //search for offset
				if(pch)
				{
					pch++;
					ulOffset=atol(pch);
				}

				pch = sbu.Token(NULL, NULL, "|");

				if((pch)&&(strlen(pch)>0))
				{
					CPeriodicQuery* pQuery = new CPeriodicQuery;
					if(pQuery)
					{
						pQuery->m_pszQuery = (char*)malloc(strlen(pch)+1);
						if(pQuery->m_pszQuery)
						{
							strcpy(pQuery->m_pszQuery, pch);
							pQuery->m_ulInterval = ulInterval; // number of seconds between calls
							pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

							CPeriodicQuery** ppQueries = new CPeriodicQuery*[cortex.m_data.m_nNumQueries+1];
							if(ppQueries)
							{
								int i=0;
								if((cortex.m_data.m_ppPeriodicQuery)&&(cortex.m_data.m_nNumQueries))
								{
									while(i<cortex.m_data.m_nNumQueries)
									{
										ppQueries[i] = cortex.m_data.m_ppPeriodicQuery[i];
										i++;
									}
									delete [] cortex.m_data.m_ppPeriodicQuery;
								}
								ppQueries[i] = pQuery;
								cortex.m_data.m_nNumQueries = i+1;
								cortex.m_data.m_ppPeriodicQuery = ppQueries;

							}
							else delete pQuery;
						}
						else delete pQuery;
					}
				}
				pch = sbu.Token(NULL, NULL, "|");
			}
		}

		pszParams = file.GetIniString("FileServer", "SettingsURL", "default.asp");

		if(pszParams)
		{
			if(cortex.m_data.m_pszHost)
			{
				if(pApp->m_pszSettingsURL) free(pApp->m_pszSettingsURL);
				_snprintf(pszPath, MAX_PATH, "http://%s/%s", cortex.m_data.m_pszHost, pszParams);  
				pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
				if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);
			}

			free(pszParams);
		}
		*/
// get the security settings from a different file (encrypted) default to yes.
/*
		_snprintf(pszPath, MAX_PATH, "%s%s", pszCurrentDir, CX_SETTINGS_FILE_ENCRYPT);  // esf = encrypted settings file
		pszParams = file.GetIniString("Mode", "Encrypted", pszPath);  //reasonable default. - could search the file system, though.
		if(pszParams)
		{
			CFileUtil encfile;
			encfile.GetSettings(pszParams, true); 
			if(encfile.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				cortex.m_settings.m_bUseAuthentication = encfile.GetIniInt("Authentication", "Use", 1)?true:false;
				cortex.m_settings.m_pszUserName = encfile.GetIniString("Authentication", "MasterID", CX_AUTH_USER);
				cortex.m_settings.m_pszPassword = encfile.GetIniString("Authentication", "MasterPW", CX_AUTH_PWD);
				cortex.m_settings.m_pszSecurityFile1 = encfile.GetIniString("Authentication", "File", CX_SETTINGS_FILE_SECURE1);
				cortex.m_settings.m_pszSecurityFile2 = encfile.GetIniString("Authentication", "Backup", CX_SETTINGS_FILE_SECURE2);
			}
			free(pszParams); pszParams=NULL;
		}
*/
/*
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}
*/
	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;

	if(cortex.m_settings.m_bUseLog)
	{
		bUseLog = cortex.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Cortex|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			cortex.m_settings.m_pszProcessedFileSpec?cortex.m_settings.m_pszProcessedFileSpec:cortex.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

	Sleep(10);
	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "--------------------------------------------------\n\
-------------- Cortex %s start --------------", CX_CURRENT_VERSION);  //(Dispatch message)

	if(cortex.m_settings.m_bUseEmail)
	{
		bUseEmail = cortex.m_settings.m_bUseEmail;

				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
		nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec,
			errorstring);
//AfxMessageBox(cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}



//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	cortex.m_http.InitializeMessaging(&cortex.m_msgr);
	if(cortex.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = cortex.m_settings.m_bLogNetworkErrors;
		if(cortex.m_net.InitializeMessaging(&cortex.m_msgr)==0)
		{
			cortex.m_data.m_bNetworkMessagingInitialized=true;
		}
	}

	if(cortex.m_settings.m_bUseDB)
	{
		pdb = new CDBUtil;
		if(pdb == NULL)
		{
			// report error
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not create database object.");
			cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_DB|CX_STATUS_ERROR));
			cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_init", errorstring);  //(Dispatch message)
		}
	}

	CDBconn* pdbConn = NULL;
	if(pdb)
	{

		pdbConn = pdb->CreateNewConnection(cortex.m_settings.m_pszDSN, cortex.m_settings.m_pszUser, cortex.m_settings.m_pszPW);
		if(pdbConn)
		{
			if(pdb->ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_DB|CX_STATUS_ERROR));
				cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_connect", errorstring);  //(Dispatch message)
			}
			else
			{
				cortex.m_settings.m_pdbConn = pdbConn;
				cortex.m_settings.m_pdb = pdb;
				cortex.m_data.m_pdbConn = pdbConn;
				cortex.m_data.m_pdb = pdb;
				if(cortex.m_settings.GetFromDatabase(errorstring)<CX_SUCCESS)
				{
					cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_DB|CX_STATUS_ERROR));
					cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_get", errorstring);  //(Dispatch message)
				}
				else
				{
					cortex.m_data.m_nLastSettingsMod = cortex.m_data.m_nSettingsMod;

					if((!cortex.m_settings.m_bUseEmail)&&(bUseEmail))
					{
						bUseEmail = false;
						// reset it
						cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
						cortex.m_msgr.RemoveDestination("email");

					}
					if((!cortex.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
					{
						// reset it
						cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down network logging.");  //(Dispatch message)
						if(cortex.m_data.m_bNetworkMessagingInitialized)
						{
							cortex.m_net.UninitializeMessaging();  // void return
							cortex.m_data.m_bNetworkMessagingInitialized=false;
						}

					}
					if((!cortex.m_settings.m_bUseLog)&&(bUseLog))
					{
						bUseLog = false;
						// reset it
						cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

						Sleep(250); // let the message get there.
						cortex.m_msgr.RemoveDestination("log");

					}
				}
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", cortex.m_settings.m_pszDSN, cortex.m_settings.m_pszUser, cortex.m_settings.m_pszPW); 
			cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_DB|CX_STATUS_ERROR));
			cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:database_init", errorstring);  //(Dispatch message)

			//**MSG
		}

	}


//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", cortex.m_settings.m_usCommandPort); 
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_CMDSVR_START);
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)

	if(cortex.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "CortexCommandServer");

		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = CortexCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &cortex;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &cortex.m_net;					// pointer to the object with the Message function.


		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			cortex.m_data.SetStatusText(errorstring, CX_STATUS_CMDSVR_ERROR);
			cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)
			cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:command_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", cortex.m_settings.m_usCommandPort);
			cortex.m_data.SetStatusText(errorstring, CX_STATUS_CMDSVR_RUN);
			cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_init", errorstring);  //(Dispatch message)
		}
	}


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", cortex.m_settings.m_usStatusPort); 
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_STATUSSVR_START);
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:xml_server_init", errorstring);  //(Dispatch message)

	if(cortex.m_settings.m_usStatusPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName)strcpy(pServer->m_pszName, "CortexXMLServer");

		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = CortexXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &cortex;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &cortex.m_net;					// pointer to the object with the Message function.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			cortex.m_data.SetStatusText(errorstring, CX_STATUS_STATUSSVR_ERROR);
			cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:xml_server_init", errorstring);  //(Dispatch message)
			cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", cortex.m_settings.m_usStatusPort);
			cortex.m_data.SetStatusText(errorstring, CX_STATUS_STATUSSVR_RUN);
			cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:xml_server_init", errorstring);  //(Dispatch message)
		}
	}



// init file server....
/*
	cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing file server on %d", cortex.m_settings.m_usFilePort); 
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
*/
/*
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// got settings already
		if(pszParams) free(pszParams); 
		_snprintf(pszPath, MAX_PATH, "%sroot", pszCurrentDir);  

		pszParams = file.GetIniString("FileServer", "Root", pszPath);  //reasonable default. - could search the file system, though.
//		pszParams = file.GetIniString("FileServer", "Root", "C:\\temp\\Temp\\VDI temp\\Harris\\IBC demo\\root");  // for testing only
		
/ *
	// if security (from settings) then create secure obj and attach to http obj
		if(cortex.m_settings.m_bUseAuthentication)
		{
			cortex.m_http.m_pSecure = new CSecurity;
			cortex.m_http.m_pSecure->InitSecurity(
				cortex.m_settings.m_pszSecurityFile1,
				cortex.m_settings.m_pszSecurityFile2,
				cortex.m_settings.m_pszUserName,
				cortex.m_settings.m_pszPassword
				);
		}


		// start the webserver

		if(pszParams)
		{
			if(cortex.m_http.SetRoot(pszParams)!=HTTP_SUCCESS)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set root settings.");
				cortex.m_data.SetStatusText(errorstring, CX_STATUS_FILESVR_ERROR);
			}
			else
			{
				if(cortex.m_http.GetHost()!=HTTP_SUCCESS)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set host name.");
					cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
					cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
					cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				}
				else
				{

					_snprintf(pszPath, MAX_PATH, "http://%s/index.htm", cortex.m_http.m_pszHost);  
					pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);

//					AfxMessageBox(pApp->m_pszSettingsURL);

					// get port from settings
					if(cortex.m_http.InitServer(cortex.m_settings.m_usFilePort, CortexHTTPThread, &cortex, &cortex.m_http, errorstring) <HTTP_SUCCESS)
					{
						cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
						cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
					}
					else
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "File server listening on %s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usFilePort);
						cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
						cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_RUN;
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
					}
				}
			}
			free(pszParams); pszParams = NULL;
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set root settings.");
			cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
			cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
			cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
		}
* /
	}
	else
	{
		// report failure!
//		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error: could not set file server settings.");
//		cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
//		cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_ERROR;
//		cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
	}
*/
	_ftime( &cortex.m_data.m_timebTick );
	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is starting registered objects...");
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_THREAD_SPARK);

	// now run the spark list.
	// this is the list of executables that get sparked

	if(!(theApp.m_ulMainMode&CX_MODE_CLONE))
	{
		strcpy(pszFilename, CX_SETTINGS_FILE_SPARK);  // cortex spark list
		CFileUtil fileSpark;
		fileSpark.GetSettings(pszFilename, false); // not encrypted
		if(fileSpark.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			// got settings already
			if(pszParams) free(pszParams); 
			int nNumExe = 0, nCount =0, nDelay=0;

			// local objects first
			nNumExe = fileSpark.GetIniInt("Local", "ObjectCount", 0); 

			while((nCount<nNumExe)&&(!g_bKillThread))
			{
				sprintf(pszFilename, "Object %03d Path", nCount);
				CCortexStartupObject* pObj = NULL;
				pszParams = fileSpark.GetIniString("Local", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_data.m_pszHost))
//					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						pObj = new CCortexStartupObject;
						if(pObj)
						{
							pObj->m_pszExeName = pszParams;
							//copy host
							pObj->m_pszHost = (char*)malloc(strlen(cortex.m_data.m_pszHost)+1);
							if(pObj->m_pszHost) strcpy(pObj->m_pszHost, cortex.m_data.m_pszHost);

							sprintf(pszFilename, "Object %03d Name", nCount);
							char* pszName = fileSpark.GetIniString("Local", pszFilename, ""); 
							if(pszName)
							{
								if(strlen(pszName))
								{
									pObj->m_pszName = pszName;
								}
								else 
								{
									try
									{
										free(pszName);
									}
									catch(...)
									{
									}
								}
							}

							sprintf(pszFilename, "Object %03d Ping", nCount);
							nDelay = fileSpark.GetIniInt("Local", pszFilename, CX_TIME_PING); 
							pObj->m_ulStatusIntervalMS = nDelay;

							sprintf(pszFilename, "Object %03d Fail", nCount);
							nDelay = fileSpark.GetIniInt("Local", pszFilename, CX_TIME_FAIL); 
							pObj->m_ulFailureIntervalMS = nDelay;

							sprintf(pszFilename, "Object %03d Flags", nCount);
							nDelay = fileSpark.GetIniInt("Local", pszFilename, CX_FLAGS_NONE); 
							pObj->m_ulFlags = nDelay;


							if(cortex.m_data.AddStartupObject(pObj) == CX_ERROR)
							{
								try
								{
									delete pObj;
								}
								catch(...)
								{
								}

								pObj = NULL;
							}

						}

						sprintf(pszFilename, "Object %03d Delay", nCount);
						nDelay = fileSpark.GetIniInt("Local", pszFilename, -1); 
	
						if(nDelay>0)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nDelay, pszParams);  
							cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:objects_init", errorstring);  //(Dispatch message)
							cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

							int nClock = clock() + nDelay;
							int nPrint = clock() + 1000;
							while((nClock>clock())&&(!g_bKillThread))
							{
								Sleep(1);
								if(clock()>nPrint)
								{
									nPrint = clock() + 1000;
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nClock-clock(), pszParams);  
									cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
								}
								_ftime( &cortex.m_data.m_timebTick );
							}
						}

						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Starting %s...", pszParams);  
						cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:objects_init", errorstring);
						cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:objects_init", errorstring);  //(Dispatch message)
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.
						sprintf(pszFilename, "%s:%d", cortex.m_data.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
//						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
//						if(_execl( pszParams, pszFilename, NULL )<0) // crashes

						if(!g_bKillThread)
						{
							if(_spawnl( _P_NOWAIT , pszParams, pszParams, pszFilename, NULL )<0)
							{
								//report failure
	//					AfxMessageBox("failure to spawn exe");
								// need to add an icon handler here too... 
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s.", pszParams);  
								cortex.m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
								cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:objects_init", errorstring);  //(Dispatch message)
								cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:objects_init", errorstring);

							}
							else
							{
	//					AfxMessageBox(pszFilename);
								// need to put a short pause in here, so we dont get all the commands all at once.
								Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS); // how much is too much?
								// need to make a loop to make this interruptible.
							}


	//					AfxMessageBox(pszParams);
						}
					}
					if((pObj==NULL)&&(pszParams))  // if obj !=NULL, then we've allocated and moved the buffer into exename, so don't want to free it.
					{
						try
						{
							free(pszParams);  
						}
						catch(...)
						{
						}
					}
					pszParams=NULL;
				}
				nCount++;
			}

			// remote objects
			nNumExe = 0, nCount =0;

// remotes - do later.
			nNumExe = fileSpark.GetIniInt("Remote", "HostCount", 0); 

			while((nCount<nNumExe)&&(!g_bKillThread))
			{
				sprintf(pszFilename, "Remote Host %03d", nCount);
				CCortexStartupObject* pObj = NULL;  // have to support this later.
				pszParams = fileSpark.GetIniString("Remote", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_data.m_pszHost))
//					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{

						pObj = new CCortexStartupObject;
						if(pObj)
						{
							pObj->m_pszHost = pszParams;
							pObj->m_pszExeName = NULL;

							sprintf(pszFilename, "Remote Host %03d Name", nCount);
							char* pszName = fileSpark.GetIniString("Remote", pszFilename, ""); 
							if(pszName)
							{
								if(strlen(pszName))
								{
									pObj->m_pszName = pszName;
								}
								else 
								{
									try
									{
										free(pszName);
									}
									catch(...)
									{
									}
								}
							}

							sprintf(pszFilename, "Remote Host %03d Ping", nCount);
							nDelay = fileSpark.GetIniInt("Remote", pszFilename, CX_TIME_PING); 
							pObj->m_ulStatusIntervalMS = nDelay;

							sprintf(pszFilename, "Remote Host %03d Fail", nCount);
							nDelay = fileSpark.GetIniInt("Remote", pszFilename, CX_TIME_FAIL); 
							pObj->m_ulFailureIntervalMS = nDelay;

							sprintf(pszFilename, "Remote Host %03d Flags", nCount);
							nDelay = fileSpark.GetIniInt("Local", pszFilename, CX_FLAGS_NONE); 
							pObj->m_ulFlags = nDelay;


							if(cortex.m_data.AddStartupObject(pObj) == CX_ERROR)
							{
								try
								{
									delete pObj;
								}
								catch(...)
								{
								}

								pObj = NULL;
							}

						}

						sprintf(pszFilename, "Remote Host %03d Delay", nCount);
						nDelay = fileSpark.GetIniInt("Remote", pszFilename, -1); 

						sprintf(pszFilename, "Remote Host %03d Port", CX_PORT_CMD);
						int nPort = fileSpark.GetIniInt("Remote", pszFilename, -1); 
						if(pObj) pObj->m_usPort = (unsigned short) nPort;

						if(nDelay>0)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nDelay, pszParams);  
							cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:remote_objects_init", errorstring);  //(Dispatch message)
							cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

							int nClock = clock() + nDelay;
							int nPrint = clock() + 1000;
							while((nClock>clock())&&(!g_bKillThread))
							{
								Sleep(1);
								if(clock()>nPrint)
								{
									nPrint = clock() + 1000;
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Delaying %d milliseconds before starting %s...", nClock-clock(), pszParams);  
									cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
								}
								_ftime( &cortex.m_data.m_timebTick );
							}
						}
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Starting remote %s...", pszParams);  
						cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:remote_objects_init", errorstring);
						cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
						cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:remote_objects_init", errorstring);  //(Dispatch message)
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.

						// have to send a spark command exchange to each remote host.
//						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
						sprintf(pszFilename, "%s:%d", cortex.m_data.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already

//						x send a net command
						if(!g_bKillThread)
						{
							// do something with host and port....
							if(0)
							{
								//report failure
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s.", pszParams);  
								cortex.m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
								cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:remote_objects_init", errorstring);  //(Dispatch message)
								cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:remote_objects_init", errorstring);
							}
							else
							{
								// need to put a short pause in here, so we dont get all the commands all at once.
								Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS); // how much is too much?
							}
						}
					}
					if((pObj==NULL)&&(pszParams))  // if obj !=NULL, then we've allocated and moved the buffer into exename, so don't want to free it.
					{
						try
						{
							free(pszParams);  
						}
						catch(...)
						{
						}
					}
					pszParams=NULL;
				}
				nCount++;
			}
		}
		//else report error.
	}
	
	if((cortex.m_data.m_ulFlags&CX_ICON_MASK) != CX_STATUS_ERROR)
	{
		cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
		cortex.m_data.m_ulFlags |= CX_STATUS_OK;  // green - we want run to be blue when something in progress
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex main thread running.");  
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_THREAD_RUN);
	cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:init", "Cortex %s is started. The main thread is running.", CX_CURRENT_VERSION);
	// after this point, we want to update the UI with the "status display", something like:

// [Global Status]
//	listening on host:port.port
// [Resource Status]
//  resource 1: status whatever
//  resource 2: status whatever
// [Process Status]
//  process 1: status whatever
//  process 2: status whatever

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );

	cortex.m_data.m_ulQueriesStart = timebCheckMods.time;
	cortex.m_data.m_ulShellStart = timebCheckMods.time;
	cortex.m_data.m_ulHttpStart = timebCheckMods.time;
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &cortex.m_data.m_timebTick );


		if(
			  (!cortex.m_data.m_bProcessSuspended)
			&&(cortex.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!cortex.m_data.m_key.m_bExpires)
				||((cortex.m_data.m_key.m_bExpires)&&(!cortex.m_data.m_key.m_bExpired))
				||((cortex.m_data.m_key.m_bExpires)&&(cortex.m_data.m_key.m_bExpireForgiveness)&&(cortex.m_data.m_key.m_ulExpiryDate+cortex.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!cortex.m_data.m_key.m_bMachineSpecific)
				||((cortex.m_data.m_key.m_bMachineSpecific)&&(cortex.m_data.m_key.m_bValidMAC))
				)
			)
		{
			if(
					(cortex.m_settings.m_bUseDB)
				&&(cortex.m_settings.m_bPeriodicQueries)
				&&(cortex.m_data.m_ppPeriodicQuery)
				&&(cortex.m_data.m_nNumQueries>0)
				&&(!g_bKillThread)
				&&(pdbConn)
				&&(pdbConn->m_bConnected)
				)
			{
				int i=0;
	EnterCriticalSection(&cortex.m_data.m_critQueries);

				while(i<cortex.m_data.m_nNumQueries)
				{
					if( (cortex.m_data.m_ppPeriodicQuery[i])
						&&(cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject)
						&&(strlen(cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject))
						)
					{
						if(
								(
									(cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent==0)
								&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ulQueriesStart+cortex.m_data.m_ppPeriodicQuery[i]->m_ulOffset)
								)

							||
								(
									(cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent>0)
								&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent+cortex.m_data.m_ppPeriodicQuery[i]->m_ulInterval)
								)
							)
						{
							strcpy(dberrorstring, "");

	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:periodic_query", "Executing [%s]", cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject);  //(Dispatch message)

	try
	{
							if(pdb->ExecuteSQL(pdbConn, cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject, dberrorstring)<DB_SUCCESS)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure running %s: %s", cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject, dberrorstring);  
								cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:periodic_query", errorstring);  //(Dispatch message)
								cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:periodic_query", errorstring);
							}
	}
	catch (...)
	{
		cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:periodic_query", "Exception running query [%s]: %s", cortex.m_data.m_ppPeriodicQuery[i]->m_pszObject, dberrorstring);  //(Dispatch message)
	}
							
							cortex.m_data.m_ppPeriodicQuery[i]->m_ulLastSent = cortex.m_data.m_timebTick.time;
						}
					}
					i++;
				}
	LeaveCriticalSection(&cortex.m_data.m_critQueries);

			}


			if(
					(cortex.m_settings.m_bPeriodicShell)
				&&(cortex.m_data.m_ppPeriodicShell)
				&&(cortex.m_data.m_nNumShellCmds>0)
				&&(!g_bKillThread)
				)
			{
				int i=0;
	EnterCriticalSection(&cortex.m_data.m_critShell);
				while(i<cortex.m_data.m_nNumShellCmds)
				{
					if( (cortex.m_data.m_ppPeriodicShell[i])
						&&(cortex.m_data.m_ppPeriodicShell[i]->m_pszObject)
						&&(strlen(cortex.m_data.m_ppPeriodicShell[i]->m_pszObject))
						)
					{
						if(
								(
									(cortex.m_data.m_ppPeriodicShell[i]->m_ulLastSent==0)
								&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ulShellStart+cortex.m_data.m_ppPeriodicShell[i]->m_ulOffset)
								)

							||
								(
									(cortex.m_data.m_ppPeriodicShell[i]->m_ulLastSent>0)
								&&((unsigned long)cortex.m_data.m_timebTick.time>=cortex.m_data.m_ppPeriodicShell[i]->m_ulLastSent+cortex.m_data.m_ppPeriodicShell[i]->m_ulInterval)
								)
							)
						{

							if(_spawnl( _P_NOWAIT , cortex.m_data.m_ppPeriodicShell[i]->m_pszObject, cortex.m_data.m_ppPeriodicShell[i]->m_pszObject, NULL )<0)
							{
								//report failure

								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure spawning %s.", cortex.m_data.m_ppPeriodicShell[i]->m_pszObject);  
								cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:periodic_shell", errorstring);  //(Dispatch message)
								cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:periodic_shell", errorstring);

							}
							
							cortex.m_data.m_ppPeriodicShell[i]->m_ulLastSent = cortex.m_data.m_timebTick.time;
						}
					}
					i++;
				}
	LeaveCriticalSection(&cortex.m_data.m_critShell);

			}





// let's ping any connected modules
EnterCriticalSection(&g_pcortex->m_data.m_critObj);

			if(cortex.m_data.m_ppObj)
			{
				int i=0;
				while(i<cortex.m_data.m_nNumObjects)
				{
					if(
							(cortex.m_data.m_ppObj[i])
						&&(
						(cortex.m_data.m_timebTick.time > cortex.m_data.m_ppObj[i]->m_timebLastStatus.time + (int)((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulStatusIntervalMS:CX_TIME_PING)/1000) )
							||((cortex.m_data.m_timebTick.time == cortex.m_data.m_ppObj[i]->m_timebLastStatus.time + (int)((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulStatusIntervalMS:CX_TIME_PING)/1000))&&(cortex.m_data.m_timebTick.millitm >= cortex.m_data.m_ppObj[i]->m_timebLastStatus.millitm + ((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulStatusIntervalMS:CX_TIME_PING)%1000)))
							)
						)
					{
						CNetData* pdata = new CNetData;
						if(pdata)
						{
							pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data
							pdata->m_ucCmd = CX_CMD_GETSTATUS;       // the command byte
							pdata->m_ucSubCmd = 0;       // the subcommand byte
							pdata->m_pucData =  NULL;
							pdata->m_ulDataLen = 0;

							int nReturn = CX_ERROR;
							if(cortex.m_data.m_ppObj[i]->m_socketCmd == NULL)
							{
								nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_pszHost, cortex.m_data.m_ppObj[i]->m_usCommandPort, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, &cortex.m_data.m_ppObj[i]->m_socketCmd);
								if(nReturn>=NET_SUCCESS)
								{
									cortex.m_msgr.DM(MSG_ICONINFO|MSG_PRI_LOW, NULL, "Cortex:module_status", "Cortex has established a connection to %s on %s:%d", 
									cortex.m_data.m_ppObj[i]->m_pszName,
									cortex.m_data.m_ppObj[i]->m_pszHost,
									cortex.m_data.m_ppObj[i]->m_usCommandPort
									);
								}

							}
							else
							{
								nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
							}
					//AfxMessageBox("sending");
							if(nReturn>=NET_SUCCESS)
							{
								cortex.m_data.m_ppObj[i]->m_ulFlags &= ~CX_FLAGS_CONNERRORSENT;

								//send ack 
					//			AfxMessageBox("ack");
								cortex.m_net.SendData(NULL, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT|NET_SND_KEEPOPENLCL);

								if(cortex.m_settings.m_ulDebug&CX_DEBUG_CONNECTSTATUS)
								{
									cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:module_status", "%s connect status: %s %d", 
										cortex.m_data.m_ppObj[i]->m_pszName, pdata->m_pucData, pdata->m_ulDataLen);
								}

								// use the results.
								if((pdata->m_pucData)&&(pdata->m_ulDataLen))
								{
									cortex.m_data.m_ppObj[i]->SetStatusText((char*)pdata->m_pucData, (unsigned long)pdata->m_ucSubCmd, cortex.m_data.m_ppObj[i]->m_ulStatusCounter+1);
								}
								else
								{
									cortex.m_data.m_ppObj[i]->SetStatusText("Unknown", CX_STATUS_UNKNOWN, cortex.m_data.m_ppObj[i]->m_ulStatusCounter+1);
								}

								_ftime(&cortex.m_data.m_ppObj[i]->m_timebLastTime);

							}
							else
							{
					//			AfxMessageBox("could not send");
								cortex.m_net.CloseConnection(cortex.m_data.m_ppObj[i]->m_socketCmd);
								cortex.m_data.m_ppObj[i]->m_socketCmd = NULL;
								cortex.m_data.m_ppObj[i]->SetStatusText("Lost connection", CX_STATUS_UNKNOWN, cortex.m_data.m_ppObj[i]->m_ulStatusCounter+1);
								if(!(cortex.m_data.m_ppObj[i]->m_ulFlags&CX_FLAGS_CONNERRORSENT))
								{
									cortex.m_msgr.DM(MSG_ICONINFO|MSG_PRI_LOW, NULL, "Cortex:module_status", "Cortex has lost connection to %s on %s:%d", 
										cortex.m_data.m_ppObj[i]->m_pszName,
										cortex.m_data.m_ppObj[i]->m_pszHost,
										cortex.m_data.m_ppObj[i]->m_usCommandPort
										);  // low pri because could jus tbe temp network issue - after a longer time, it gets set to med.
									cortex.m_data.m_ppObj[i]->m_ulFlags |= CX_FLAGS_CONNERRORSENT;
								}

							}

							_ftime(&cortex.m_data.m_ppObj[i]->m_timebLastStatus);
							
							delete pdata;
						}
					}

					if(
							(cortex.m_data.m_ppObj[i])
						&&(
								(cortex.m_data.m_timebTick.time > cortex.m_data.m_ppObj[i]->m_timebLastTime.time + (int)((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulFailureIntervalMS:CX_TIME_FAIL)/1000) )
							||((cortex.m_data.m_timebTick.time == cortex.m_data.m_ppObj[i]->m_timebLastTime.time + (int)((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulFailureIntervalMS:CX_TIME_FAIL)/1000))&&(cortex.m_data.m_timebTick.millitm >= cortex.m_data.m_ppObj[i]->m_timebLastTime.millitm + ((cortex.m_data.m_ppObj[i]->m_pStartupObj?cortex.m_data.m_ppObj[i]->m_pStartupObj->m_ulFailureIntervalMS:CX_TIME_FAIL)%1000)))
							)
						)
					{
						cortex.m_data.m_ppObj[i]->SetStatusText("Lost connection", CX_STATUS_ERROR, cortex.m_data.m_ppObj[i]->m_ulStatusCounter+1);
						_ftime(&cortex.m_data.m_ppObj[i]->m_timebLastTime); // keeps getting here every interval....


						if(!(cortex.m_data.m_ppObj[i]->m_ulFlags&CX_FLAGS_ERRORSENT))
						{
							// should probably try to respark stuff here, or send alerts etc...
							cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_MEDIUM, NULL, "Cortex:module_failure", "Cortex has lost connection to %s on %s:%d", 
								cortex.m_data.m_ppObj[i]->m_pszName,
								cortex.m_data.m_ppObj[i]->m_pszHost,
								cortex.m_data.m_ppObj[i]->m_usCommandPort
								);
							cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:module_failure", "Cortex has lost connection to %s on %s:%d", 
								cortex.m_data.m_ppObj[i]->m_pszName,
								cortex.m_data.m_ppObj[i]->m_pszHost,
								cortex.m_data.m_ppObj[i]->m_usCommandPort
								);

						}


						// messaged on error.
						// now, kill PID in case there's a stalled app,
						// then respark

						cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:object_reinit", "checking ptr: %d at %d",cortex.m_data.m_ppObj[i]->m_pStartupObj, clock());  //(Dispatch message)
						if(cortex.m_data.m_ppObj[i]->m_pStartupObj)
						{
							CCortexStartupObject* pStartupObj = cortex.m_data.m_ppObj[i]->m_pStartupObj;
							if((pStartupObj->m_pszExeName)&&(strlen(pStartupObj->m_pszExeName))&&(pStartupObj->m_ulFlags&CX_FLAGS_RESPARK))
							{
								// kill process first.
								if(cortex.m_data.m_ppObj[i]->m_nPID>0)
								{
									HANDLE hProcess =  OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, TRUE, cortex.m_data.m_ppObj[i]->m_nPID);
									if(hProcess)
									{
										if(TerminateProcess(hProcess,0)<0)
										{
											cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:object_reinit", "Failure %d to terminate process %d", GetLastError(), cortex.m_data.m_ppObj[i]->m_nPID);  //(Dispatch message)
										}
									}
									else
									{
										cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:object_reinit", "Failure %d to open process %d", GetLastError(), cortex.m_data.m_ppObj[i]->m_nPID);  //(Dispatch message)
									}
								}

								// now respark
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Attempting to restart %s...", pStartupObj->m_pszExeName);  
		//						cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:object_reinit", errorstring);
								cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:object_reinit", errorstring);  //(Dispatch message)
								if(!(cortex.m_data.m_ppObj[i]->m_ulFlags&CX_FLAGS_ERRORSENT)) cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:object_reinit", errorstring);

								// cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
								// need to give host name and command port as args.  
								// the rest the exes can get over the cmd channel.
								sprintf(pszFilename, "%s:%d", cortex.m_data.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
		//						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename because we have it already
		//						if(_execl( pszParams, pszFilename, NULL )<0) // crashes

								if(!g_bKillThread)
								{
									if(_spawnl( _P_NOWAIT , pStartupObj->m_pszExeName, pStartupObj->m_pszExeName, pszFilename, NULL )<0)
									{
										//report failure
			//					AfxMessageBox("failure to spawn exe");
										// need to add an icon handler here too... 
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "* Failure starting %s.  Error %d", pStartupObj->m_pszExeName, errno);  
										// cortex.m_data.SetStatusText(errorstring, CX_STATUS_ERROR);
										cortex.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Cortex:object_reinit", errorstring);  //(Dispatch message)
										if(!(cortex.m_data.m_ppObj[i]->m_ulFlags&CX_FLAGS_ERRORSENT)) cortex.SendMsg(CX_SENDMSG_ERROR, "Cortex:object_reinit", errorstring);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Successfully restarted %s.", pStartupObj->m_pszExeName);  
				//						cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:object_reinit", errorstring);
										cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:object_reinit", errorstring);  //(Dispatch message)
										// cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
										cortex.SendMsg(CX_SENDMSG_INFO|MSG_PRI_HIGH, "Cortex:object_reinit", errorstring);
									}
								}
							}
						}
						cortex.m_data.m_ppObj[i]->m_ulFlags|=CX_FLAGS_ERRORSENT;

					}
					i++;
				}
			}
LeaveCriticalSection(&g_pcortex->m_data.m_critObj);






		}


		if(
					(cortex.m_data.m_timebTick.time > timebCheckMods.time )
				||((cortex.m_data.m_timebTick.time == timebCheckMods.time)&&(cortex.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
//				&&(!cortex.m_data.m_bProcessSuspended)
			)
		{
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "time to check");  Sleep(5);//(Dispatch message)
			timebCheckMods.time = cortex.m_data.m_timebTick.time + cortex.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = cortex.m_data.m_timebTick.millitm + (unsigned short)(cortex.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	cortex.m_msgr.DM(MSG_ICONHAND, NULL, "Cortex:debug", "checkmods %d, %d", timebCheckMods.time, cortex.m_settings.m_ulModsIntervalMS);  //(Dispatch message)
				strcpy(errorstring, "");

				if(cortex.m_data.CheckDatabaseMods(errorstring)==CX_ERROR)
				{
					if(!cortex.m_data.m_bCheckModsWarningSent)
					{
						cortex.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						cortex.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(cortex.m_data.m_bCheckModsWarningSent)
					{
						cortex.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					cortex.m_data.m_bCheckModsWarningSent=false;
				}


				if(cortex.m_data.m_timebTick.time > cortex.m_data.m_timebAutoPurge.time + cortex.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&cortex.m_data.m_timebAutoPurge);

					if(cortex.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(cortex.m_data.CheckMessages(errorstring)==CX_ERROR)
						{
							if(!cortex.m_data.m_bCheckMsgsWarningSent)
							{
								cortex.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								cortex.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(cortex.m_data.m_bCheckMsgsWarningSent)
						{
							cortex.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						cortex.m_data.m_bCheckMsgsWarningSent=false;
					}

/*
					if(cortex.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(cortex.m_data.CheckAsRun(errorstring)==cortex_ERROR)
						{
							if(!cortex.m_data.m_bCheckAsRunWarningSent)
							{
								cortex.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								cortex.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(cortex.m_data.m_bCheckMsgsWarningSent)
						{
							cortex.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						cortex.m_data.m_bCheckAsRunWarningSent=false;
					}
*/
				}




				if(cortex.m_data.m_nSettingsMod != cortex.m_data.m_nLastSettingsMod)
				{
					if(cortex.m_settings.GetFromDatabase()>=CX_SUCCESS)
					{
						cortex.m_data.m_nLastSettingsMod = cortex.m_data.m_nSettingsMod;

						// check for stuff to change

						// network messaging
						if(cortex.m_settings.m_bLogNetworkErrors) 
						{
							if(!cortex.m_data.m_bNetworkMessagingInitialized)
							{
								if(cortex.m_net.InitializeMessaging(&cortex.m_msgr)==0)
								{
									cortex.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(cortex.m_data.m_bNetworkMessagingInitialized)
							{
								cortex.m_net.UninitializeMessaging();  // void return
								cortex.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!cortex.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								cortex.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=cortex.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((cortex.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec))
									{
										if(strcmp(cortex.m_msgr.m_ppDest[nIndex]->m_pszParams, (cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = cortex.m_msgr.ModifyDestination(
												"email", 
												cortex.m_settings.m_pszProcessedMailSpec?cortex.m_settings.m_pszProcessedMailSpec:cortex.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!cortex.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								cortex.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									cortex.m_settings.m_pszProcessedFileSpec?cortex.m_settings.m_pszProcessedFileSpec:cortex.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=cortex.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((cortex.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(cortex.m_settings.m_pszProcessedFileSpec?cortex.m_settings.m_pszProcessedFileSpec:cortex.m_settings.m_pszFileSpec))
									{
										if(strcmp(cortex.m_msgr.m_ppDest[nIndex]->m_pszParams, (cortex.m_settings.m_pszProcessedFileSpec?cortex.m_settings.m_pszProcessedFileSpec:cortex.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = cortex.m_msgr.ModifyDestination(
												"log", 
												cortex.m_settings.m_pszProcessedFileSpec?cortex.m_settings.m_pszProcessedFileSpec:cortex.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//cortex.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												cortex.m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}





					}
				}

				// increment the Keep alive.
				cortex.m_data.IncrementDatabaseMods("Keep_Alive");
			}
		}



		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1);

	}

	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_END;

	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:uninit", "Cortex is shutting down.");  //(Dispatch message)
	cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:uninit", "Cortex %s is shutting down.", CX_CURRENT_VERSION);

	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is shutting down.");  
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

EnterCriticalSection(&g_pcortex->m_data.m_critObj);
// shut down all the running objects;
	if(cortex.m_data.m_ppObj)
	{
		int i=0;
		// have to shut down all the processes first
		while(i<cortex.m_data.m_nNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_PROCESS))
			{
				cortex.m_data.m_ppObj[i]->SetStatusText("Shutting down...", CX_STATUS_UNKNOWN, cortex.m_data.m_ppObj[i]->m_ulStatusCounter+1);

				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  
				cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:process_object_uninit", errorstring);
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:process_object_uninit", "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  //(Dispatch message)
			
				CNetData* pdata = new CNetData;
				if(pdata)
				{

					pdata->m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
					pdata->m_ucCmd = CX_CMD_BYE;       // the command byte
					pdata->m_ucSubCmd = 0;       // the subcommand byte
					pdata->m_pucData =  NULL;
					pdata->m_ulDataLen = 0;

					int nReturn=0;
			//AfxMessageBox("sending");
					if(cortex.m_data.m_ppObj[i]->m_socketCmd == NULL)
					{
						nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_pszHost, cortex.m_data.m_ppObj[i]->m_usCommandPort, 5000, 0, NET_SND_CMDTOSVR, &cortex.m_data.m_ppObj[i]->m_socketCmd);
					}
					else
					{
						nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CMDTOSVR);
					}
					if(nReturn>=NET_SUCCESS)
					{
						//send ack
			//			AfxMessageBox("ack");
						cortex.m_net.SendData(NULL, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CLNTACK);
					}
					else
					{
			//			AfxMessageBox("could not send");
					}
					cortex.m_net.CloseConnection(cortex.m_data.m_ppObj[i]->m_socketCmd);

					delete pdata;
				}
				cortex.m_data.m_nNumProcesses--; // do this here for now. - later, let it be a request on the command server, letting cortex know shutdown has been done successfully
/*
				CNetData* pReturnData = NULL;

				SOCKET* ps = cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					pReturnData,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
					CX_AUTH_USER, CX_AUTH_PWD);
					if(pReturnData) delete pReturnData;
					cortex.m_net.CloseConnection(*ps);
*/
			}
			Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS);
			i++;
		}

		while(cortex.m_data.m_nNumProcesses>0)  // wait for the processes to shutdown
		{
			_ftime( &cortex.m_data.m_timebTick );

			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
				AfxGetApp()->PumpMessage();
			Sleep(1);
		}

		// then shut down all the resources.
		i=0;
		while(i<cortex.m_data.m_nNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_RESOURCE))
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  
				cortex.SendMsg(CX_SENDMSG_INFO, "Cortex:resource_object_uninit", errorstring);
				cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
				cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:resource_object_uninit", "Sending quit command to %s...", cortex.m_data.m_ppObj[i]->m_pszName?cortex.m_data.m_ppObj[i]->m_pszName:"unknown process");  //(Dispatch message)

				CNetData* pdata = new CNetData;
				if(pdata)
				{

					pdata->m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
					pdata->m_ucCmd = CX_CMD_BYE;       // the command byte
					pdata->m_ucSubCmd = 0;       // the subcommand byte
					pdata->m_pucData =  NULL;
					pdata->m_ulDataLen = 0;

					int nReturn=0;
			//AfxMessageBox("sending");
					if(cortex.m_data.m_ppObj[i]->m_socketCmd == NULL)
					{
						nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_pszHost, cortex.m_data.m_ppObj[i]->m_usCommandPort, 5000, 0, NET_SND_CMDTOSVR, &cortex.m_data.m_ppObj[i]->m_socketCmd);
					}
					else
					{
						nReturn = cortex.m_net.SendData(pdata, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CMDTOSVR);
					}
					if(nReturn>=NET_SUCCESS)
					{
						//send ack
			//			AfxMessageBox("ack");
						cortex.m_net.SendData(NULL, cortex.m_data.m_ppObj[i]->m_socketCmd, 5000, 0, NET_SND_CLNTACK);
					}
					else
					{
			//			AfxMessageBox("could not send");
					}
					cortex.m_net.CloseConnection(cortex.m_data.m_ppObj[i]->m_socketCmd);

					delete pdata;
				}
				cortex.m_data.m_nNumResources--;// do this here for now. - later, let it be a request on the command server, letting cortex know shutdown has been done successfully
/*
				CNetData* pReturnData = NULL;
				SOCKET* ps = cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					pReturnData,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
//					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
					CX_AUTH_USER, CX_AUTH_PWD);
				if(pReturnData) delete pReturnData;
				cortex.m_net.CloseConnection(*ps);
*/
			}
			Sleep(cortex.m_settings.m_nSparkStaggerIntervalMS);
			i++;
		}
//CString foo; foo.Format("resources: %d",cortex.m_data.m_nNumResources);AfxMessageBox(foo);
		while(cortex.m_data.m_nNumResources>0)  // wait for the resources to shutdown
		{
			_ftime( &cortex.m_data.m_timebTick );

			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
				AfxGetApp()->PumpMessage();
			Sleep(1);
		}
//		AfxMessageBox("X");
	}
LeaveCriticalSection(&g_pcortex->m_data.m_critObj);
/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no nede to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = cortex.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		cortex.m_net.OpenConnection(cortex.m_http.m_pszHost, 10888, &s); // branding hardcode
		cortex.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		cortex.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	pdlg->SetProgress(CXDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	cortex.m_data.m_ulFlags &= ~CX_STATUS_FILESVR_MASK;
//	cortex.m_data.m_ulFlags |= CX_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);
//	_ftime( &cortex.m_data.m_timebTick );
//	cortex.m_http.EndServer();
	_ftime( &cortex.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:command_server_uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_CMDSVR_END);
	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_net.StopServer(cortex.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &cortex.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down xml server....");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:xml_server_uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, CX_STATUS_STATUSSVR_END);
	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_net.StopServer(cortex.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &cortex.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is exiting.");  
	cortex.m_msgr.DM(MSG_ICONNONE, NULL, "Cortex:uninit", errorstring);  //(Dispatch message)
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

	g_bKillStatus = true;

	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	cortex.m_settings.Settings(false); //write
/*

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
//		file.SetIniInt("FileServer", "ListenPort", cortex.m_settings.m_usFilePort);
		file.SetIniString("License", "Key", cortex.m_settings.m_pszLicense);
		file.SetIniInt("CommandServer", "ListenPort", cortex.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", cortex.m_settings.m_usStatusPort);
		file.SetIniString("FileServer", "IconPath", cortex.m_settings.m_pszIconPath);

		file.SetIniInt("Resources", "MinPort", cortex.m_settings.m_usResourcePortMin);
		file.SetIniInt("Resources", "MaxPort", cortex.m_settings.m_usResourcePortMax);

		file.SetIniInt("Processes", "MinPort", cortex.m_settings.m_usProcessPortMin);
		file.SetIniInt("Processes", "MaxPort", cortex.m_settings.m_usProcessPortMax);

		file.SetIniInt("Messager", "UseEmail", cortex.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", cortex.m_settings.m_bUseNetwork?1:0);
//		file.SetIniInt("Messager", "LogRemote", cortex.m_settings.m_bUseLogRemote?1:0);
		file.SetIniInt("Mode", "UseClone", cortex.m_settings.m_bUseClone?1:0);
		file.SetIniInt("Mode", "UseDB", cortex.m_settings.m_bUseDB?1:0);

		file.SetIniInt("Timing", "SparkInterval", cortex.m_settings.m_nSparkStaggerIntervalMS);

		file.SetIniString("Database", "DSN", cortex.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", cortex.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", cortex.m_settings.m_pszPW);
//		file.SetIniString("Database", "BackupDSN", cortex.m_settings.m_pszBackupDSN);
//		file.SetIniString("Database", "BackupDBUser", cortex.m_settings.m_pszBackupUser);
//		file.SetIniString("Database", "BackupDBPassword", cortex.m_settings.m_pszBackupPW);
		file.SetIniString("Database", "SettingsTableName", cortex.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", cortex.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", cortex.m_settings.m_pszMessages);  // the Messages table name
		file.SetIniInt("Database", "ModificationCheckInterval", cortex.m_settings.m_ulModsIntervalMS);  // in milliseconds
		file.SetIniInt("Database", "UsePeriodicQueries", cortex.m_settings.m_bPeriodicQueries?1:0);  
		file.SetIniString("Database", "PeriodicQueries", cortex.m_settings.m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.

		if(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE)
			strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
		else
		if(cortex.m_settings.m_ulMainMode&CX_MODE_LISTENER)
			strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
		else  // default
			strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file
		file.SetSettings(pszFilename, false);  

	}
*/

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Cortex is exiting");
	cortex.m_data.m_ulFlags &= ~CX_ICON_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	cortex.m_data.SetStatusText(errorstring, cortex.m_data.m_ulFlags);

	//exiting
	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "-------------- Cortex %s exit ---------------\n\
--------------------------------------------------\n", CX_CURRENT_VERSION);  //(Dispatch message)
///	pdlg->SetProgress(CXDLG_CLEAR); // no point

	_ftime( &cortex.m_data.m_timebTick );
	cortex.m_data.m_ulFlags &= ~CX_STATUS_THREAD_MASK;
	cortex.m_data.m_ulFlags |= CX_STATUS_THREAD_ENDED;

	Sleep(250); // let the message get there.
	cortex.m_msgr.RemoveDestination("log");
	cortex.m_msgr.RemoveDestination("email");

	if(pdb)
	{
		pdb->RemoveConnection(pdbConn);
		delete pdb;
	}

	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &cortex.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_pcortex = NULL;
	nClock = clock() + cortex.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &cortex.m_data.m_timebTick );}
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void CortexHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CCortexMain* pCortex = (CCortexMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pCortex->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: CortexServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: CortexServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(CX_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: CortexServer/%s", CX_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: CortexServer/%s", CX_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void CortexCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;

					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_REQ_HELLO:
						{
//AfxMessageBox("receiving");

							CSafeBufferUtil sbu;

							data.m_ucCmd = NET_CMD_ACK;

							if(data.m_pucData!=NULL)
							{
//AfxMessageBox( (char*)data.m_pucData );
								CCortexObject* pObj = new CCortexObject;
								if(pObj)
								{
									char* pch = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, ":", MODE_SINGLEDELIM);
									if(pch)
									{
										pObj->m_pszHost = (char*)malloc(strlen(pch)+1);
										if(pObj->m_pszHost) strcpy(pObj->m_pszHost, pch);
									}
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usCommandPort = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usStatusPort = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_usType = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if((pch)&&(strlen(pch)))	pObj->m_nPID = atoi(pch);
									pch = sbu.Token(NULL, NULL, ":", MODE_SINGLEDELIM);
									if(pch)
									{
										pObj->m_pszName = (char*)malloc(strlen(pch)+1);
										if(pObj->m_pszName) strcpy(pObj->m_pszName, pch);
									}
									// add more fields, for status and status text
									pObj->SetStatusText("pending", CX_STATUS_UNKNOWN, 1);  
									pObj->m_ulFlags &= ~(CX_FLAGS_ERRORSENT|CX_FLAGS_CONNERRORSENT);

									EnterCriticalSection(&g_pcortex->m_data.m_critObj);
//AfxMessageBox( "adding" );

									int q=g_pcortex->m_data.FindObject(pObj);
									if(q>=0) // already exists
									{
										// may need to create new connections and such.
										// let's re-init since we got a new hello.
										g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:objects_hello", "%s object %s on %s reconnected", ((pObj->m_usType&CX_TYPE_PROCESS)?"Process":"Resource"), (pObj->m_pszName?pObj->m_pszName:"[unknown]"), (pObj->m_pszHost?pObj->m_pszHost:"[unknown]"));  //(Dispatch message)
						g_pcortex->SendMsg(CX_SENDMSG_INFO, "Cortex:objects_hello", "%s object %s on %s reconnected", ((pObj->m_usType&CX_TYPE_PROCESS)?"Process":"Resource"), (pObj->m_pszName?pObj->m_pszName:"[unknown]"), (pObj->m_pszHost?pObj->m_pszHost:"[unknown]")); 

										pObj->m_pStartupObj = g_pcortex->m_data.m_ppObj[q]->m_pStartupObj; //reassign startup pointer
										try
										{
											delete g_pcortex->m_data.m_ppObj[q];
										}
										catch (...)
										{
										}

										g_pcortex->m_data.m_ppObj[q] = pObj;

										_ftime(&pObj->m_timebLastStatus);
										_ftime(&pObj->m_timebLastTime);

									}
									else
									if(g_pcortex->m_data.AddObject(pObj)<CX_SUCCESS)
									{
										//**MSG
						g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, "Cortex:objects_hello", "%s object %s on %s connected, failure to add", "%s object %s on %s reconnected", ((pObj->m_usType&CX_TYPE_PROCESS)?"Process":"Resource"), (pObj->m_pszName?pObj->m_pszName:"[unknown]"), (pObj->m_pszHost?pObj->m_pszHost:"[unknown]"));  //(Dispatch message);  //(Dispatch message)
										try
										{
											delete pObj;
										}
										catch(...)
										{
										}
									}
									else
									{
										//**MSG success
						g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, "Cortex:objects_hello", "%s object %s on %s connected", ((pObj->m_usType&CX_TYPE_PROCESS)?"Process":"Resource"), (pObj->m_pszName?pObj->m_pszName:"[unknown]"), (pObj->m_pszHost?pObj->m_pszHost:"[unknown]"));  //(Dispatch message)
						g_pcortex->SendMsg(CX_SENDMSG_INFO, "Cortex:objects_hello", "%s object %s on %s connected", ((pObj->m_usType&CX_TYPE_PROCESS)?"Process":"Resource"), (pObj->m_pszName?pObj->m_pszName:"[unknown]"), (pObj->m_pszHost?pObj->m_pszHost:"[unknown]")); 

										_ftime(&pObj->m_timebLastStatus);
										_ftime(&pObj->m_timebLastTime);

									EnterCriticalSection(&g_pcortex->m_data.m_critStartObj);
										int n= g_pcortex->m_data.FindStartupObject(pObj);
										if(n>=0)
										{
											pObj->m_pStartupObj = g_pcortex->m_data.m_ppStartObj[n];
										}
									LeaveCriticalSection(&g_pcortex->m_data.m_critStartObj);
									}
									LeaveCriticalSection(&g_pcortex->m_data.m_critObj);


								}

								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.


						} break;
/*
					case CX_CMD_BYE:  // not supported by main cortex
						{
	g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
*/
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going

						// NO, dont do this.
						// this is fine for a non-persistent server, but it will report all sorts of errors while just 
						// waiting and timing out on a persistent one.
/*
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
*/
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

void CortexStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;

					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


void CortexXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szCortexSource[MAX_PATH]; 
	strcpy(szCortexSource, "CortexXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_pcortex) g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szCortexSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );

		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_pcortex)
			{
				g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, errorstring);  //(Dispatch message)
				g_pcortex->SendMsg(CX_SENDMSG_ERROR, szCortexSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_pcortex)
		{
			g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);  //(Dispatch message)
			g_pcortex->SendMsg(CX_SENDMSG_INFO, szCortexSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_pcortex)&&(g_pcortex->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;

									if(pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);
														
										//	bCloseCommand = false;  // inserted for mem leak debug

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 

												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));
											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);}
																					catch(...)
																					{				
																						g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in RXID free");  //(Dispatch message)
																					}

																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Cortex specific commands
																				if(strcmp("get_modules", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETMODULES;
																				}
																				else
																				if(strcmp("get_local", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETLOCAL;
																				}
																				else
																				if(strcmp("get_remote", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETREMOTE;
																				}
																				else
																				if(strcmp("get_query", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETQUERY;
																				}
																				else
																				if(strcmp("get_shell", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETSHELL;
																				}
																					
////                                    end Cortex specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////


																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
//																				strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));

																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
//																				strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_pcortex->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_pcortex->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Cortex specific XML commands
														case CX_XML_TYPE_GETMODULES://		11 // 6.1 get_modules Obtains a list of all currently connected modules, and information about each module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_modules\" code=\"%d\">", g_pcortex->m_data.m_nNumObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;

EnterCriticalSection(&g_pcortex->m_data.m_critObj);
																if((g_pcortex->m_data.m_ppObj)&&(g_pcortex->m_data.m_nNumObjects))
																{

																	int m = 0;
																	while(m<g_pcortex->m_data.m_nNumObjects)
																	{
																		if(g_pcortex->m_data.m_ppObj[m])
																		{
																			CCortexObject* pObj = g_pcortex->m_data.m_ppObj[m];

																			char* pchStatus = pObj->GetStatusText(&ulDataLen);
																			if(pchStatus)
																			{
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<module><name>%s</name>\
<type>0x%04x</type>\
<host>%s</host>\
<binport>%d</binport>\
<xmlport>%d</xmlport>\
<status><code>0x%08x</code><text>%s</text></status>\
</module>",
																					pObj->m_pszName,
																					pObj->m_usType,
																					pObj->m_pszHost,
																					pObj->m_usCommandPort,
																					pObj->m_usStatusPort,
																					ulDataLen,
																					pchStatus																					
																				);
																					
																			}
																			else
																			{
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<module><name>%s</name>\
<type>0x%04x</type>\
<host>%s</host>\
<binport>%d</binport>\
<xmlport>%d</xmlport>\
<status><code>0xffffffff</code><text>unavailable</text></status>\
</module>",
																					pObj->m_pszName,
																					pObj->m_usType,
																					pObj->m_pszHost,
																					pObj->m_usCommandPort,
																					pObj->m_usStatusPort
																				
																				);

																		
																			}

																			if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																			msg.FormatContent(CX_XML_BUFFER_FLAGS, MAX_MESSAGE_LENGTH, "%s%s", 
																				(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																				(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																				);
																			
																			msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_FLAGS] );

																		}
																		m++;
																	}

																}
LeaveCriticalSection(&g_pcortex->m_data.m_critObj);
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);


															} break; //case CX_XML_TYPE_GETMODULES
														case CX_XML_TYPE_GETLOCAL://			12 // 6.2 get_local Obtains a list of local modules which Cortex attempts to initiate at startup, and manage if they successfully connect.
															{
															} break; //case CX_XML_TYPE_GETLOCAL
														case CX_XML_TYPE_GETREMOTE://			13 // 6.3 get_remote Obtains a list of local remote hosts on which Cortex attempts to initiate a remote startup procedure, at local startup.
															{
															} break; //case CX_XML_TYPE_UNK
														case CX_XML_TYPE_GETQUERY://			14 // 6.4 get_query Obtains a list of registered periodic database queries and their parameters
															{
															} break; //case CX_XML_TYPE_GETQUERY
														case CX_XML_TYPE_GETSHELL://			15 // 6.5 get_shell Obtains a list of registered periodic shell commands and their parameters
															{
															} break; //case CX_XML_TYPE_GETSHELL

// end Cortex specific XML commands
////////////////////////////////////////////////////////
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pcortex)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, errorstring);  //(Dispatch message)
				g_pcortex->SendMsg(CX_SENDMSG_ERROR, szCortexSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								}

								// log it

								//debug file write
								if((g_pcortex)&&(g_pcortex->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } 
										catch(...)
										{				
											g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception freeing buffer %d", b);  //(Dispatch message)
										}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); }
									catch(...)
									{				
										g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in RpcStringFree");  //(Dispatch message)
									}

								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } 
									catch(...)
									{				
										g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in XML free");  //(Dispatch message)
									}

								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } 
									catch(...)
									{				
										g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in RXID free");  //(Dispatch message)
									}

								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_pcortex)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);  //(Dispatch message)
						g_pcortex->SendMsg(CX_SENDMSG_INFO, szCortexSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_pcortex)
				{
					g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);  //(Dispatch message)
					g_pcortex->SendMsg(CX_SENDMSG_INFO, szCortexSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Cortex:XMLHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_pcortex->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pcortex)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, errorstring);  //(Dispatch message)
				g_pcortex->SendMsg(CX_SENDMSG_ERROR, szCortexSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_pcortex)&&(g_pcortex->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_pcortex->m_settings.m_pszName?g_pcortex->m_settings.m_pszName:"Cortex"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_pcortex)
		{
			g_pcortex->m_msgr.DM(MSG_ICONINFO, NULL, szCortexSource, errorstring);  //(Dispatch message)
			g_pcortex->SendMsg(CX_SENDMSG_INFO, szCortexSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_pcortex) g_pcortex->m_msgr.DM(MSG_ICONERROR, NULL, szCortexSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CCortexHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();


//		AfxMessageBox("end thread");
}



