// CortexData.h: interface for the CCortexData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <sys/timeb.h>
#include "CortexDefines.h"


class CCortexObject  
{
public:
	CCortexObject();
	virtual ~CCortexObject();
	void Free();

	char* m_pszName;
	unsigned short m_usType;

	// destinations within object
	char** m_ppszDestName;
	char** m_ppszDestParams;  // has to be generic for all types
	unsigned long m_ulNumDest; // number of destinations.

	// dependencies within object
	char** m_ppszResourceName;
	char** m_ppszResourceParams;  // has to be generic for all types
	unsigned long m_ulNumResources; // number of dependency resources.

	// IP and ports
	char* m_pszHost;
	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// cortex assigned status
	unsigned long m_ulStatus;
	unsigned long m_ulOwner;

	// there is a negotiation to set the following from default values.
	unsigned long m_ulStatusIntervalMS;		// the interval, in milliseconds, at which we expect to hear status.
	unsigned long m_ulFailureIntervalMS;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	_timeb m_timebLastStatus; // the time of the last status received (parseable)
	_timeb m_timebLastTime;		// the last time received (object's local time)
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for object status
	char* m_pszStatus;	// parseable string
	char* m_pszInfo;		// human readable info string
};



class CCortexData  
{
public:
	CCortexData();
	virtual ~CCortexData();

	// cortex object brokering
	unsigned short m_usResourcePortLast;
	unsigned short m_usProcessPortLast;

	CCortexObject** m_ppObj;
	unsigned long m_ulNumObjects;
	unsigned long m_ulNumResources;  // this is for convenience only
	unsigned long m_ulNumProcesses;  // this is for convenience only

	_timeb m_timebLastStatus; // the time of the last status given
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)
	char* m_pszStatus;	// parseable string
	char* m_pszInfo;		// human readable info string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_CORTEXDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
