// CortexMain.cpp: implementation of the CCortexMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Cortex.h"  // just included to have access to windowing environment
#include "CortexDlg.h"  // just included to have access to windowing environment
#include "CortexHandler.h"  // just included to have access to windowing environment

#include "CortexMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;

extern CMessager* g_pmsgr;  // from Messager.cpp

void StatusMessage(void* pList, char* pszFormat, ...);
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCortexMain::CCortexMain()
{
}

CCortexMain::~CCortexMain()
{
}

char*	CCortexMain::CortexTranslate(CHTTPHeader* pHeader, char* pszBuffer, unsigned long* pulBuflen, unsigned long ulFlags)			// apply cortex scripting language
{
	if((pszBuffer)&&(pHeader))
	{
//		AfxMessageBox(pszBuffer);
		char tagbuf[32];
		char tagbuf2[32];
		strcpy(tagbuf,"<!--cortex");
		strcpy(tagbuf2,"cortex-->");
		char* ptag = strstr(pszBuffer, tagbuf); 
		if(ptag==NULL)
		{
			strcpy(tagbuf,"<!-- cortex");
			strcpy(tagbuf2,"cortex -->");
			ptag = strstr(pszBuffer, tagbuf); 
		}
		

		if(ptag!=NULL)
		{
//		AfxMessageBox("foo2");
			char* pchOutBuf = (char*)malloc((*pulBuflen)+1); // allocate the same buffer size for now.  
			//we are removing a lot of cortex tags, so that should make up for any params we insert (but we will check it)
			if(pchOutBuf)
			{
				char context[64];
				strcpy(context, "main");
				unsigned long ulInMarker = 0;
				unsigned long ulOutMarker = 0;
				while((ulInMarker<(*pulBuflen))&&(*(pszBuffer+ulInMarker)!=0)) // null term string!
				{
					if(
							((*(pszBuffer+ulInMarker))=='<')
						&&(ulInMarker+strlen(tagbuf)<(*pulBuflen))
						&&(strncmp( pszBuffer+ulInMarker, tagbuf, strlen(tagbuf))==0)
						)
					{
						// absorb the tag
						ulInMarker+=strlen(tagbuf);
						while((ulInMarker<(*pulBuflen))&&(isspace(*(pszBuffer+ulInMarker)))) ulInMarker++;

						//now, continue
						while((ulInMarker<(*pulBuflen))&&(*(pszBuffer+ulInMarker)!=0))
						{
							// check for the end tag
							if(
									((*(pszBuffer+ulInMarker))=='c')
								&&(ulInMarker+strlen(tagbuf2)<(*pulBuflen))
								&&(strncmp( pszBuffer+ulInMarker, tagbuf2, strlen(tagbuf2))==0)
								)
							{
								// absorb the end tag
								ulInMarker+=strlen(tagbuf2);
								while((ulOutMarker>1)&&(isspace(*(pchOutBuf+(ulOutMarker-1))))) ulOutMarker--;  // eat up back whitespace
								break;
							}
							else 
							// check for directive
							if((*(pszBuffer+ulInMarker))=='%')
							{
								// parse escape sequence
								ulInMarker++;
								switch(*(pszBuffer+ulInMarker))
								{
								case 'v':
									{
										// insert a value... have to GET the value.

										// hardcode the cases:
										if(stricmp(context, "brandingauth")==0) // this case doesnt happen, there are no %v's in the file
										{
											// get settings from the branding file

										}
										else
										if(stricmp(context, "sentineladmin")==0) 
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											CBufferUtil bu;
											char* pch = NULL;
											pch = (char*)bu.memrncmp((unsigned char*) pszBuffer, ulInMarker, (unsigned char*) "name=\"", strlen("name=\""));
											if(pch)
											{
												char name[64];

												unsigned long x=0;
												pch+=strlen("name=\"");
												while((*pch!='\"')&&(x<63))
												{
													name[x]=*pch;
													x++;
													pch++;
												}
												name[x]=0;

												//now we know what we are looking for.
												if(stricmp(name, "server")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ServerName", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "list")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ListNumber", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "login")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ClientName", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}

											}
											else
											{
												strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
												ulOutMarker+= strlen("value N/A");
											}
										}
										else
										if(stricmp(context, "dekocastadmin")==0) 
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											CBufferUtil bu;
											char* pch = NULL;
											pch = (char*)bu.memrncmp((unsigned char*) pszBuffer, ulInMarker, (unsigned char*) "name=\"", strlen("name=\""));
											if(pch)
											{
												char name[64];

												unsigned long x=0;
												pch+=strlen("name=\"");
												while((*pch!='\"')&&(x<63))
												{
													name[x]=*pch;
													x++;
													pch++;
												}
												name[x]=0;

												//now we know what we are looking for.
												if(stricmp(name, "host")==0)
												{
													char* pchValue = settings.GetIniString("Deko Settings", "DekoHost", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "port")==0)
												{
													char* pchValue = settings.GetIniString("Deko Settings", "DekoPort", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}

											}
											else
											{
												strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
												ulOutMarker+= strlen("value N/A");
											}
										}
										else
										if(stricmp(context, "databaseadmin")==0) 
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											CBufferUtil bu;
											char* pch = NULL;
											pch = (char*)bu.memrncmp((unsigned char*) pszBuffer, ulInMarker, (unsigned char*) "name=\"", strlen("name=\""));
											if(pch)
											{
												char name[64];

												unsigned long x=0;
												pch+=strlen("name=\"");
												while((*pch!='\"')&&(x<63))
												{
													name[x]=*pch;
													x++;
													pch++;
												}
												name[x]=0;

												//now we know what we are looking for.
												if(stricmp(name, "dsn")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "DSN", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "user")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "User", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "pw")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "Pw", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "pw2")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "Pw", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "data")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "MainTable", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "mdata")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "LookupTable", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}

											}
											else
											{
												strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
												ulOutMarker+= strlen("value N/A");
											}
										}
										else
										if(stricmp(context, "brandingadmin")==0) 
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											CBufferUtil bu;
											char* pch = NULL;
											pch = (char*)bu.memrncmp((unsigned char*) pszBuffer, ulInMarker, (unsigned char*) "name=\"", strlen("name=\""));
											if(pch)
											{
												char name[64];

												unsigned long x=0;
												pch+=strlen("name=\"");
												while((*pch!='\"')&&(x<63))
												{
													name[x]=*pch;
													x++;
													pch++;
												}
												name[x]=0;

												//now we know what we are looking for.
												if(stricmp(name, "cport")==0)
												{
													char* pchValue = settings.GetIniString("Main Settings", "ReadyPort", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}
												else
												if(stricmp(name, "look")==0)
												{
													char* pchValue = settings.GetIniString("Main Settings", "Lookahead", "cxdefaultval");
													if(strcmp(pchValue,"cxdefaultval")==0)
													{
														strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
														ulOutMarker+= strlen("value N/A");
													}
													else
													{
														strncpy((pchOutBuf+ulOutMarker), pchValue, strlen(pchValue));
														ulOutMarker+= strlen(pchValue);
													}
													free(pchValue);
												}

											}
											else
											{
												strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
												ulOutMarker+= strlen("value N/A");
											}
										}
										else
										{
											// get settings from the settings! (should be "main")
											CBufferUtil bu;
											char* pch = NULL;
											pch = (char*)bu.memrncmp((unsigned char*) pszBuffer, ulInMarker, (unsigned char*) "name=\"", strlen("name=\""));
											if(pch)
											{
												char name[64];
												char value[64];

												unsigned long x=0;
												pch+=strlen("name=\"");
												while((*pch!='\"')&&(x<63))
												{
													name[x]=*pch;
													x++;
													pch++;
												}
												name[x]=0;

												//now we know what we are looking for.
												if(stricmp(name, "hport")==0)
												{
													sprintf(value,"%d", m_settings.m_usFilePort);
													strncpy((pchOutBuf+ulOutMarker), value, strlen(value));
													ulOutMarker+= strlen(value);
												}
												else
												if(stricmp(name, "cport")==0)
												{
													sprintf(value,"%d", m_settings.m_usCommandPort);
													strncpy((pchOutBuf+ulOutMarker), value, strlen(value));
													ulOutMarker+= strlen(value);
												}
												else
												if(stricmp(name, "sport")==0)
												{
													sprintf(value,"%d", m_settings.m_usStatusPort);
													strncpy((pchOutBuf+ulOutMarker), value, strlen(value));
													ulOutMarker+= strlen(value);
												}

											}
											else
											{
												strncpy((pchOutBuf+ulOutMarker), "value N/A", strlen("value N/A"));
												ulOutMarker+= strlen("value N/A");
											}

										}


									} break;
								case 'd':
									{
										// set the directive.
										ulInMarker++;
										unsigned long x=0;
										char directive[64];
										while((ulInMarker<(*pulBuflen))&&(*(pszBuffer+ulInMarker)!=0)&&(*(pszBuffer+ulInMarker)!='=')&&(*(pszBuffer+ulInMarker)!=' '))
										{

											directive[x] = (*(pszBuffer+ulInMarker));
											x++;
											ulInMarker++;
										}
										directive[x] = 0;

										if(stricmp(directive, "_context")==0)
										{
											x=0; ulInMarker++;
											while((ulInMarker<(*pulBuflen))&&(*(pszBuffer+ulInMarker)!=0)&&(*(pszBuffer+ulInMarker)!=' '))
											{
												context[x] = (*(pszBuffer+ulInMarker));
												x++;
												ulInMarker++;
											}
											context[x] = 0;
										}
										else
										if(stricmp(directive, "_other")==0)
										{
											// no other directives defined yet.
										}
										
									} break;
								case 'H':
									{
										// insert the host name.
										// need to check the buffer length before we can insert
										//TODO

										if(stricmp(context, "brandingauth")==0) // this case doesnt happen, there are no %v's in the file
										{
											// get settings from the branding file

											// actually just use the same host, because its the same (for now)
											strncpy((pchOutBuf+ulOutMarker), m_http.m_pszHost, strlen(m_http.m_pszHost));
											ulOutMarker+= strlen(m_http.m_pszHost);

										}
										else  // main host.
										{
											strncpy((pchOutBuf+ulOutMarker), m_http.m_pszHost, strlen(m_http.m_pszHost));
											ulOutMarker+= strlen(m_http.m_pszHost);
										}

									} break;
								case 'P':
									{
										// insert the host name.
										// need to check the buffer length before we can insert
										//TODO

										char port[32];
									
										if(stricmp(context, "brandingauth")==0) // this case doesnt happen, there are no %v's in the file
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											int nPort = settings.GetIniInt("Main Settings", " ReadyPort", 10888);

											sprintf(port, "%d", 	nPort);

										}
										else  // main host.
										{
											sprintf(port, "%d", 	m_settings.m_usFilePort);
										}
										strncpy((pchOutBuf+ulOutMarker), port, strlen(port));
										ulOutMarker+= strlen(port);

									} break;
								default:
									{
										( (*(pchOutBuf+ulOutMarker)) = (*(pszBuffer+ulInMarker)) );
										ulOutMarker++;
									} break;
								}
								ulInMarker++;

							}
							else
							// just copy.
							{
								( (*(pchOutBuf+ulOutMarker)) = (*(pszBuffer+ulInMarker)) );
								ulOutMarker++;
								ulInMarker++;
							}
						}
					}
					else
					{
						( (*(pchOutBuf+ulOutMarker)) = (*(pszBuffer+ulInMarker)) );
						ulOutMarker++;
						ulInMarker++;
					}

				}
				*pulBuflen = ulOutMarker;
				return pchOutBuf;
			}
		}
//		else return pszBuffer; // just return the original buffer, nothing to parse.
	}
	return NULL;  // return NULL if no directives.
}

int		CCortexMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return CX_SUCCESS;
}

int		CCortexMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // cortex initiates a request to an object server
{
	return CX_SUCCESS;
}

int		CCortexMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex replies to an object server after receiving data. (usually ack or nak)
{
	return CX_SUCCESS;
}

int		CCortexMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// cortex answers a request from an object client
{
	return CX_SUCCESS;
}


void CortexMainThread(void* pvArgs)
{
	CCortexApp* pApp = (CCortexApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment


	//startup.
	g_bThreadStarted = true;

	// windows stuff
	CCortexDlg* pdlg = ((CCortexDlg*)(((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg));
	CWnd* pwndStatus = ((CCortexHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT);
	CListCtrlEx* plist = (CListCtrlEx*)(&(pdlg->m_lce));
	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Cortex\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}

	pdlg->SetProgress(CXDLG_WAITING);

	if(pwndStatus) pwndStatus->SetWindowText("Initializing...");
	StatusMessage(plist, "Initializing..."); 

	//create the main object.
	CCortexMain cortex;
//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// modes.
			if(stricmp(pch, "/c")==0) // clone mode  (check for this first, it invalidates some of the rest)
			{
				//TODO create clone mode!
				cortex.m_settings.m_ulMainMode |= CX_MODE_CLONE;  // trumps listener
			}
			else
			if(stricmp(pch, "/l")==0) // listener mode
			{
				//TODO create listener mode
				cortex.m_settings.m_ulMainMode |= CX_MODE_LISTENER;  // trumped by clone
			}
			else
			if(stricmp(pch, "/q")==0) // quiet mode (works in listener or default, clone is by def quiet)
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_QUIET;	
			}
			else
			if(stricmp(pch, "/v")==0) // volatile mode settings used this session are not retained by default - does not override explicit saves via http interface
			{
				cortex.m_settings.m_ulMainMode |= CX_MODE_VOLATILE;	
			}
			else

			// params
			if(strnicmp(pch, "/s:", 3)==0) // server listen port(s) override
			{
			}
			else
			if(strnicmp(pch, "/r:", 3)==0) // resource port range override
			{
			}
			else
			if(strnicmp(pch, "/p:", 3)==0) // process port range override
			{
			}

			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

	if(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE)
		strcpy(pszFilename, CX_SETTINGS_FILE_CLONE);  // cortex settings file
	else
	if(cortex.m_settings.m_ulMainMode&CX_MODE_LISTENER)
		strcpy(pszFilename, CX_SETTINGS_FILE_LISTENER);  // cortex settings file
	else  // default
		strcpy(pszFilename, CX_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
//AfxMessageBox("x");
	Sleep(5);  // needed to not crash on XP for some reason... maybe flush after file read?
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// get settings
		cortex.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		if(cortex.m_settings.m_bUseLog)
		{
			// for logfiles, we need params, and they must be in this format:
			//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			pszParams = file.GetIniString("Messager", "LogFileIni", "Cortex|YM||");
			int nRegisterCode=0;

//AfxMessageBox("x0");
			nRegisterCode = cortex.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
//AfxMessageBox("x1");
			if (nRegisterCode != MSG_SUCCESS) 
			{
				// inform the windowing environment
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
				AfxMessageBox(errorstring);
				pwndStatus->SetWindowText(errorstring);
			}

			free(pszParams); pszParams=NULL;
		}
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "--------------------------------------------------\n\
-------------- Cortex %s start --------------", CX_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
	cortex.m_http.InitializeMessaging(&cortex.m_msgr);
	cortex.m_net.InitializeMessaging(&cortex.m_msgr);


// load up the rest of the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		cortex.m_settings.m_pszName = file.GetIniString("Main", "Name", "Cortex");
		cortex.m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", CX_PORT_FILE);
		cortex.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CX_PORT_CMD);
		cortex.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CX_PORT_STATUS);

		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", CX_PORT_RESMIN);
		cortex.m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", CX_PORT_RESMAX);

		cortex.m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", CX_PORT_PRCMIN);
		cortex.m_settings.m_usResourcePortMin = file.GetIniInt("Processes", "MaxPort", CX_PORT_PRCMAX);

		cortex.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		cortex.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		cortex.m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;
		cortex.m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 1)?true:false;

		if(pszParams) free(pszParams);

// get the security settings from a different file (encrypted) default to yes.
		_snprintf(pszPath, MAX_PATH, "%s%s", pszCurrentDir, CX_SETTINGS_FILE_ENCRYPT);  // esf = encrypted settings file
		pszParams = file.GetIniString("Mode", "Encrypted", pszPath);  //reasonable default. - could search the file system, though.
		if(pszParams)
		{
			CFileUtil encfile;
			encfile.GetSettings(pszParams, true); 
			if(encfile.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				cortex.m_settings.m_bUseAuthentication = encfile.GetIniInt("Authentication", "Use", 1)?true:false;
				cortex.m_settings.m_pszUserName = encfile.GetIniString("Authentication", "MasterID", CX_AUTH_USER);
				cortex.m_settings.m_pszPassword = encfile.GetIniString("Authentication", "MasterPW", CX_AUTH_PWD);
				cortex.m_settings.m_pszSecurityFile1 = encfile.GetIniString("Authentication", "File", CX_SETTINGS_FILE_SECURE1);
				cortex.m_settings.m_pszSecurityFile2 = encfile.GetIniString("Authentication", "Backup", CX_SETTINGS_FILE_SECURE2);
			}
			free(pszParams); pszParams=NULL;
		}
	}


//init command and status listeners.
	if(pwndStatus) pwndStatus->SetWindowText("initializing command server...");
	StatusMessage(plist, "Initializing command server on %d",cortex.m_settings.m_usCommandPort);

	if(cortex.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
			StatusMessage(plist, errorstring);
		}
		else
		{
			StatusMessage(plist, "Command server listening on %d",cortex.m_settings.m_usCommandPort);
		}
	}

	if(pwndStatus) pwndStatus->SetWindowText("initializing status server...");
	StatusMessage(plist, "Initializing status server on %d",cortex.m_settings.m_usStatusPort);

	if(cortex.m_settings.m_usStatusPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = cortex.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		if(cortex.m_net.StartServer(pServer, &cortex.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
			StatusMessage(plist, errorstring);
		}
		else
		{
			StatusMessage(plist, "Status server listening on %d",cortex.m_settings.m_usStatusPort);
		}
	}



// init file server....
	if(pwndStatus) pwndStatus->SetWindowText("initializing file server...");
	StatusMessage(plist, "Initializing file server...");

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// got settings already
		if(pszParams) free(pszParams); 
		_snprintf(pszPath, MAX_PATH, "%sroot", pszCurrentDir);  

		pszParams = file.GetIniString("FileServer", "Root", pszPath);  //reasonable default. - could search the file system, though.
//		pszParams = file.GetIniString("FileServer", "Root", "C:\\temp\\Temp\\VDI temp\\Harris\\IBC demo\\root");  // for testing only
		
	// if security (from settings) then create secure obj and attach to http obj
		if(cortex.m_settings.m_bUseAuthentication)
		{
			cortex.m_http.m_pSecure = new CSecurity;
			cortex.m_http.m_pSecure->InitSecurity(
				cortex.m_settings.m_pszSecurityFile1,
				cortex.m_settings.m_pszSecurityFile2,
				cortex.m_settings.m_pszUserName,
				cortex.m_settings.m_pszPassword
				);
		}

		// start the webserver
		if(pszParams)
		{
			if(cortex.m_http.SetRoot(pszParams)!=HTTP_SUCCESS)
			{
				if(pwndStatus) pwndStatus->SetWindowText("Error: could not set root settings.");
				StatusMessage(plist, "Error: could not set root settings.");
			}
			else
			{
				if(cortex.m_http.GetHost()!=HTTP_SUCCESS)
				{
					if(pwndStatus) pwndStatus->SetWindowText("Error: could not get host name.");
					StatusMessage(plist, "Error: could not get host name.");  
				}
				else
				{

					_snprintf(pszPath, MAX_PATH, "http://%s/index.htm", cortex.m_http.m_pszHost);  
					pApp->m_pszSettingsURL = (char*)malloc(strlen(pszPath)+1);
					if(pApp->m_pszSettingsURL) strcpy(pApp->m_pszSettingsURL, pszPath);

//					AfxMessageBox(pApp->m_pszSettingsURL);

					// get port from settings
					if(cortex.m_http.InitServer(cortex.m_settings.m_usFilePort, CortexHTTPThread, &cortex, &cortex.m_http, errorstring) <HTTP_SUCCESS)
					{
						if(pwndStatus) pwndStatus->SetWindowText(errorstring);
						StatusMessage(plist, errorstring);
					}
					else
					{
						sprintf(errorstring,"Listening on %s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usFilePort);
						if(pwndStatus) pwndStatus->SetWindowText(errorstring);
						StatusMessage(plist, errorstring);
					}
				}
			}
			free(pszParams); pszParams = NULL;
		}
		else
		{
			if(pwndStatus) pwndStatus->SetWindowText("Error: could not get root settings.");
			StatusMessage(plist, "Error: could not get root settings.");  
		}
	}
	else
	{
		// report failure!
	}
	pdlg->SetProgress(CXDLG_CLEAR);

	// now run the spark list.
	// this is the list of executables that get sparked
	pdlg->SetProgressColor(RGB(192,192,0));
	pdlg->SetProgress(CXDLG_WAITING);

	if(!(cortex.m_settings.m_ulMainMode&CX_MODE_CLONE))
	{
		if(pwndStatus) pwndStatus->SetWindowText("Executing registered objects.");
		strcpy(pszFilename, CX_SETTINGS_FILE_SPARK);  // cortex spark list
		CFileUtil fileSpark;
		fileSpark.GetSettings(pszFilename, false); // not encrypted
		if(fileSpark.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			// got settings already
			if(pszParams) free(pszParams); 
			int nNumExe = 0, nCount =0;

			// local objects first
			nNumExe = fileSpark.GetIniInt("Local", "ObjectCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Object %03d Path", nCount);
				pszParams = fileSpark.GetIniString("Local", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						StatusMessage(plist, "Starting %s...", pszParams);  

						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.
						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename becuase we have it already
//						if(_execl( pszParams, pszFilename, NULL )<0) // crashes
						if(_spawnl( _P_NOWAIT , pszParams, pszFilename, NULL )<0)
						{
							//report failure
//					AfxMessageBox("failure to spawn exe");
						}
						else
						{
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(100); // how much is too much?
						}
//					AfxMessageBox(pszParams);

					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}

			// remote objects
			nNumExe = 0, nCount =0;
/*
// remotes - do later.
			nNumExe = fileSpark.GetIniInt("Remote", "HostCount", 0); 

			while(nCount<nNumExe)
			{
				sprintf(pszFilename, "Remote Host %03d", nCount);
				pszParams = fileSpark.GetIniString("Remote", pszFilename, ""); 
				if(pszParams)
				{
					if((strlen(pszParams)>0)&&(cortex.m_http.m_pszHost))
					{
						// need to give host name and command port as args.  
						// the rest the exes can get over the cmd channel.

						// have to send a spark command exchange to each remote host.
						sprintf(pszFilename, "%s:%d", cortex.m_http.m_pszHost, cortex.m_settings.m_usCommandPort); // just using filename becuase we have it already
						if(0)
						{
							//report failure
						}
						else
						{
							// need to put a short pause in here, so we dont get all the commands all at once.
							Sleep(100); // how much is too much?
						}
					}
					if(pszParams) free(pszParams);  pszParams=NULL;
				}
				nCount++;
			}
			*/
		}
		//else report error.
	}
	

	pdlg->SetProgress(CXDLG_CLEAR);
	pdlg->SetProgressColor(RGB(0,192,0));

	_timeb timestamp;
	tm* theTime;
	int x=0, dx=1;
	StatusMessage(plist, "Main mode enabled.");  
	// after this point, we want to update the UI with the "status display", something like:

// [Global Status]
//	listening on host:port.port.port
// [Resource Status]
//  resource 1: status whatever
//  resource 2: status whatever
// [Process Status]
//  process 1: status whatever
//  process 2: status whatever

	_ftime( &timestamp );
	int nStatustime=timestamp.time+10;  //ten seconds of displaying the init status before going to real status.

	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &timestamp );

		theTime = localtime( &timestamp.time	);
		strftime( pszFilename, 30, "%H:%M:%S", theTime );

		sprintf(pszFilename, "%s  %s:%d.%d.%d",
			pszFilename,
			cortex.m_http.m_pszHost,
			cortex.m_settings.m_usFilePort,
			cortex.m_settings.m_usCommandPort,
			cortex.m_settings.m_usStatusPort); // just using filename as a variable because we have it already
		if(pwndStatus) pwndStatus->SetWindowText(pszFilename);


		Sleep(3);
		pdlg->SetProgress(CXDLG_EVILEYE);  // default settings.

		if(timestamp.time>nStatustime)  // once per second
		{
			if(pdlg) pdlg->SetStatus(&cortex);
			nStatustime=timestamp.time;
		}
	}

	pdlg->SetProgressColor(RGB(192,0,0));

// shut down all the running objects;
	if(cortex.m_data.m_ppObj)
	{
		unsigned long i=0;
		// have to shut down all the processes first
		while(i<cortex.m_data.m_ulNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_PROCESS))
			{
				cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					NULL,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
			}
			i++;
		}
		while(cortex.m_data.m_ulNumProcesses)  // wait for the processes to shutdown
		{
			_ftime( &timestamp );

			theTime = localtime( &timestamp.time	);
			strftime( pszFilename, 30, "%H:%M:%S", theTime );

			sprintf(pszFilename, "%s  %s:%d.%d.%d",
				pszFilename,
				cortex.m_http.m_pszHost,
				cortex.m_settings.m_usFilePort,
				cortex.m_settings.m_usCommandPort,
				cortex.m_settings.m_usStatusPort); // just using filename as a variable because we have it already
			if(pwndStatus) pwndStatus->SetWindowText(pszFilename);


			Sleep(3);
			pdlg->SetProgress(CXDLG_EVILEYE);  // default settings.

			if(timestamp.time>nStatustime)  // once per second
			{
				if(pdlg) pdlg->SetStatus(&cortex);
				nStatustime=timestamp.time;
			}
		}

		// then shut down all the resources.
		i=0;
		while(i<cortex.m_data.m_ulNumObjects)
		{
			if((cortex.m_data.m_ppObj[i])&&(cortex.m_data.m_ppObj[i]->m_usType&CX_TYPE_RESOURCE))
			{
				cortex.SendClientRequest(
					cortex.m_data.m_ppObj[i]->m_pszHost, 
					cortex.m_data.m_ppObj[i]->m_usCommandPort, 
					NULL,  // we dont need any return data.  we arent even going to check errors
					NET_TYPE_PROTOCOL1, // dont keep conn open, no extra data, nothing, just say goodbye gracie
					CX_CMD_BYE, 
					CX_CMD_NULL, 
					NULL, 
					0, 
					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszUser:CX_AUTH_USER):NULL, 
					cortex.m_settings.m_bUseAuthentication?(((cortex.m_http.m_pSecure)&&(cortex.m_http.m_pSecure->m_ppUsers)&&(cortex.m_http.m_pSecure->m_ppUsers[0]))?cortex.m_http.m_pSecure->m_ppUsers[0]->m_pszPassword:CX_AUTH_PWD):NULL);  // the magic cortex pw
			}
			i++;
		}
		while(cortex.m_data.m_ulNumResources)  // wait for the resources to shutdown
		{
			_ftime( &timestamp );

			theTime = localtime( &timestamp.time	);
			strftime( pszFilename, 30, "%H:%M:%S", theTime );

			sprintf(pszFilename, "%s  %s:%d.%d.%d",
				pszFilename,
				cortex.m_http.m_pszHost,
				cortex.m_settings.m_usFilePort,
				cortex.m_settings.m_usCommandPort,
				cortex.m_settings.m_usStatusPort); // just using filename as a variable because we have it already
			if(pwndStatus) pwndStatus->SetWindowText(pszFilename);


			Sleep(3);
			pdlg->SetProgress(CXDLG_EVILEYE);  // default settings.

			if(timestamp.time>nStatustime)  // once per second
			{
				if(pdlg) pdlg->SetStatus(&cortex);
				nStatustime=timestamp.time;
			}
		}
	}

	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = cortex.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		cortex.m_net.OpenConnection(cortex.m_http.m_pszHost, 10888, &s); // branding hardcode
		cortex.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		cortex.m_net.CloseConnection(s); // branding hardcode
	}


	pdlg->SetProgress(CXDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down file server.");
	cortex.m_http.EndServer();
//	AfxMessageBox("shutting down command server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down command server.");
	cortex.m_net.StopServer(cortex.m_settings.m_usCommandPort, 5000, errorstring);
//	AfxMessageBox("shutting down status server.");
	if(pwndStatus) pwndStatus->SetWindowText("shutting down status server.");
	cortex.m_net.StopServer(cortex.m_settings.m_usStatusPort, 5000, errorstring);
	if(pwndStatus) pwndStatus->SetWindowText("exiting...");

/*

	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniInt("FileServer", "ListenPort", cortex.m_settings.m_usFilePort);
		file.SetIniInt("CommandServer", "ListenPort", cortex.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", cortex.m_settings.m_usStatusPort);

		file.SetIniInt("Resources", "MinPort", cortex.m_settings.m_usResourcePortMin);
		file.SetIniInt("Resources", "MaxPort", cortex.m_settings.m_usResourcePortMax);

		file.SetIniInt("Processes", "MinPort", cortex.m_settings.m_usProcessPortMin);
		file.SetIniInt("Processes", "MaxPort", cortex.m_settings.m_usResourcePortMin);

		file.SetIniInt("Messager", "UseEmail", cortex.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", cortex.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "LogRemote", cortex.m_settings.m_bUseLogRemote?1:0);
		file.SetIniInt("Messager", "UseClone", cortex.m_settings.m_bUseClone?1:0);

		file.SetSettings("cortex.csf", false);  // have to have correct filename
	}

*/

	//exiting
	cortex.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "-------------- Cortex %s exit ---------------\n\
--------------------------------------------------\n", CX_CURRENT_VERSION);  //(Dispatch message)
	pdlg->SetProgress(CXDLG_CLEAR);

	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(100); // another small delay at end
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void CortexHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CCortexMain* pCortex = (CCortexMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pCortex->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
*/
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
*/
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

*/


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if(!(phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL)))
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								if(
										(strstr(header.m_pszPath, "cortex.cgi")!=NULL)
									&&(header.m_ulNumArgs>0)
									&&(header.m_ppszArgNames!=NULL)
									&&(header.m_ppszArgValues!=NULL)
									)
									dwAttrib = FILE_ATTRIBUTE_NORMAL; // not really, but need to bypass
								else
									dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // iff error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: Aurora/1.01" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: Aurora/1.01\r\nContent-Length: \r\n\r\n");

									// here we have to check if this is a cgi directive, rather than a file.
									if(
											(strstr(header.m_pszPath, "cortex.cgi")!=NULL)
										&&(header.m_ulNumArgs>0)
										&&(header.m_ppszArgNames!=NULL)
										&&(header.m_ppszArgValues!=NULL)
										)
									{

										//AfxMessageBox("cgi");
										// parse out the stuff, then change the path and mimetype, etc 
										// so that it contains the referring page etc.

										// first find which module it is.
										char module[256];
										strcpy(module, "cortexadmin");
										unsigned long ulArg = 0;
										bool bModule = false;

										while((ulArg<header.m_ulNumArgs)&&(!bModule))
										{
											if(stricmp(header.m_ppszArgNames[ulArg], "module")==0)
											{
												bModule = true;
												strcpy(module, header.m_ppszArgValues[ulArg]);
											}
											ulArg++;
										}

										bModule = false;  // repurpose for path.
										ulArg=0;
										while((ulArg<header.m_ulNumArgs)&&(!bModule))
										{
											if(stricmp(header.m_ppszArgNames[ulArg], "source")==0)
											{
												bModule = true;
												_snprintf(header.m_pszPath, MAX_PATH, "%s", http.m_pszRoot );
												strncat(header.m_pszPath, "/", MAX_PATH);
												strncat(header.m_pszPath, header.m_ppszArgValues[ulArg], MAX_PATH);
												_fullpath(header.m_pszPath, header.m_pszPath, MAX_PATH);  // in case of relative references
												char ext[_MAX_EXT];
												_splitpath(header.m_pszPath, NULL, NULL, NULL, ext);
												header.m_pszMimeType =  http.MimeType(ext);  // get the mime type.
											}
											ulArg++;
										}

										if(!bModule) // serve the main page.
										{
											_snprintf(header.m_pszPath, MAX_PATH, "%s", http.m_pszRoot );
											strncat(header.m_pszPath, "splash.htm", MAX_PATH);
											strncat(header.m_pszPath, "/", MAX_PATH);
											_fullpath(header.m_pszPath, header.m_pszPath, MAX_PATH);  // in case of relative references
											char ext[_MAX_EXT];
											_splitpath(header.m_pszPath, NULL, NULL, NULL, ext);
											header.m_pszMimeType =  http.MimeType(ext);  // get the mime type.
										}
//										AfxMessageBox(header.m_pszPath);


										// now, do the necessary stuff based on context.
										if(stricmp(module, "cortexadmin")==0)
										{
											// only thing they can do is change listen ports.
											// lets ignore for now.
										}
										else
										if(stricmp(module, "sentineladmin")==0)
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											int nPort = settings.GetIniInt("Main Settings", "ReadyPort", 10888);

											ulArg=0;
											bool bMod = false;
											while(ulArg<header.m_ulNumArgs)
											{
												if(stricmp(header.m_ppszArgNames[ulArg], "server")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ServerName", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Harris Settings", "ServerName", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "list")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ListNumber", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Harris Settings", "ListNumber", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "login")==0)
												{
													char* pchValue = settings.GetIniString("Harris Settings", "ClientName", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Harris Settings", "ClientName", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												ulArg++;
											}
											settings.SetSettings("branding.csf");

											if(bMod)
											{
												FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, branding isnt started
												if(fpb)
												{
													fclose(fpb);
													// send a cmd to branding to reload settings
													unsigned char buffer[8] = {0,(unsigned char)0x93,0,0,0,0,0,0}; // branding hardcode 0x93 = reset automation
													buffer[0] = pCortex->m_net.Checksum((buffer+1), 5); // branding hardcode
													SOCKET s; // branding hardcode
													pCortex->m_net.OpenConnection(pCortex->m_http.m_pszHost, nPort, &s); // branding hardcode
													pCortex->m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
													pCortex->m_net.CloseConnection(s); // branding hardcode
												}
											}
										}
										else
										if(stricmp(module, "dekocastadmin")==0)
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											int nPort = settings.GetIniInt("Main Settings", "ReadyPort", 10888);

											ulArg=0;
											bool bMod = false;
											while(ulArg<header.m_ulNumArgs)
											{
												if(stricmp(header.m_ppszArgNames[ulArg], "host")==0)
												{
													char* pchValue = settings.GetIniString("Deko Settings", "DekoHost", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Deko Settings", "DekoHost", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "port")==0)
												{
													char* pchValue = settings.GetIniString("Deko Settings", "DekoPort", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Deko Settings", "DekoPort", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												ulArg++;
											}
											settings.SetSettings("branding.csf");

											if(bMod)
											{
												FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, branding isnt started
												if(fpb)
												{
													fclose(fpb);
													// send a cmd to branding to reload settings
													unsigned char buffer[8] = {0,(unsigned char)0x94,0,0,0,0,0,0}; // branding hardcode 0x94 = reset deko
													buffer[0] = pCortex->m_net.Checksum((buffer+1), 5); // branding hardcode
													SOCKET s; // branding hardcode
													pCortex->m_net.OpenConnection(pCortex->m_http.m_pszHost, nPort, &s); // branding hardcode
													pCortex->m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
													pCortex->m_net.CloseConnection(s); // branding hardcode
												}
											}
										}
										else
										if(stricmp(module, "databaseadmin")==0)
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											int nPort = settings.GetIniInt("Main Settings", "ReadyPort", 10888);

											ulArg=0;
											bool bMod = false;
											while(ulArg<header.m_ulNumArgs)
											{
												if(stricmp(header.m_ppszArgNames[ulArg], "dsn")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "DSN", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("DB Settings", "DSN", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "user")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "User", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("DB Settings", "User", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "pw")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "Pw", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("DB Settings", "Pw", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "data")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "MainTable", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("DB Settings", "MainTable", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "mdata")==0)
												{
													char* pchValue = settings.GetIniString("DB Settings", "LookupTable", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("DB Settings", "LookupTable", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												ulArg++;
											}
											settings.SetSettings("branding.csf");

											if(bMod)
											{
												FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, branding isnt started
												if(fpb)
												{
													fclose(fpb);
													// send a cmd to branding to reload settings
													unsigned char buffer[8] = {0,(unsigned char)0x95,0,0,0,0,0,0}; // branding hardcode 0x95 = reset db
													buffer[0] = pCortex->m_net.Checksum((buffer+1), 5); // branding hardcode
													SOCKET s; // branding hardcode
													pCortex->m_net.OpenConnection(pCortex->m_http.m_pszHost, nPort, &s); // branding hardcode
													pCortex->m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
													pCortex->m_net.CloseConnection(s); // branding hardcode
												}
											}
										}
										else
										if(stricmp(module, "brandingadmin")==0)
										{
											// get settings from the branding file
											CFileUtil settings;
											settings.GetSettings("branding.csf");
											int nPort = settings.GetIniInt("Main Settings", "ReadyPort", 10888);

											ulArg=0;
											bool bMod = false;
											while(ulArg<header.m_ulNumArgs)
											{
												if(stricmp(header.m_ppszArgNames[ulArg], "cport")==0)
												{
													char* pchValue = settings.GetIniString("Main Settings", "ReadyPort", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Main Settings", "ReadyPort", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												else
												if(stricmp(header.m_ppszArgNames[ulArg], "look")==0)
												{
													char* pchValue = settings.GetIniString("Main Settings", "Lookahead", "cxdefaultval");
													if(strcmp(pchValue, header.m_ppszArgValues[ulArg]))
													{
														settings.SetIniString("Main Settings", "Lookahead", header.m_ppszArgValues[ulArg]);
														bMod = true;
													}
													free(pchValue);
												}
												ulArg++;
											}
											settings.SetSettings("branding.csf");

											if(bMod)
											{
												FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, branding isnt started
												if(fpb)
												{
													fclose(fpb);
													// send a cmd to branding to reload settings
													unsigned char buffer[8] = {0,(unsigned char)0x92,0,0,0,0,0,0}; // branding hardcode 0x92 = reset main
													buffer[0] = pCortex->m_net.Checksum((buffer+1), 5); // branding hardcode
													SOCKET s; // branding hardcode
													pCortex->m_net.OpenConnection(pCortex->m_http.m_pszHost, nPort, &s); // branding hardcode
													pCortex->m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
													pCortex->m_net.CloseConnection(s); // branding hardcode
												}
											}
										}

										
									}

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;


									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
										fseek(pFile, 0, SEEK_SET);
										char* pszFileBuffer = (char*) malloc(ulFileLen+1);
										if(pszFileBuffer)
										{
											fread(pszFileBuffer, sizeof(char), ulFileLen, pFile);
											*(pszFileBuffer+ulFileLen)=0; //null term
										} 
										//else  error
										fclose(pFile);

										char* pchParsedBuffer = pCortex->CortexTranslate(&header, pszFileBuffer, &ulFileLen, 0);

										if(pchParsedBuffer!=NULL)
										{
											free(pszFileBuffer); // free the unparsed buffer
										}
										else
										{
											pchParsedBuffer = pszFileBuffer;  // set the buffer to the buffer
										}

										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												memcpy(pch+(ulBufferLen-ulFileLen), pchParsedBuffer, ulFileLen);
												*(pch+ulBufferLen) = 0; // term zero
												free(pchParsedBuffer);
											}
											
//											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
//											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												strcpy((char*)pch, "HTTP/1.0 200 OK\r\nServer: Aurora/1.01");
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
												free(pchParsedBuffer);
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

*/
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


void StatusMessage(void* pList, char* pszFormat, ...)
{
	if((pList)&&(pszFormat)&&(strlen(pszFormat)))
	{
		_timeb timestamp;
		_ftime( &timestamp );

		char buffer[MAX_MESSAGE_LENGTH];
		tm* theTime = localtime( &timestamp.time	);
		strftime(buffer, 30, "%H:%M:%S.", theTime );
		int nOffest = strlen(buffer);

		sprintf(buffer+nOffest,"%03d   ",timestamp.millitm);
		nOffest = strlen(buffer);
		va_list marker;
		// create the formatted output string
		va_start(marker, pszFormat); // Initialize variable arguments.
		_vsnprintf((char *) (buffer+nOffest), MAX_MESSAGE_LENGTH-1, pszFormat, (va_list) marker);
		va_end( marker );             // Reset variable arguments.

		CListCtrlEx* plist = (CListCtrlEx*)(pList);
		plist->InsertItem(plist->GetItemCount(), buffer);  

	}
}

