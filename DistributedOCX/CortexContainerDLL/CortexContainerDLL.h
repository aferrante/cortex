// CortexContainerDLL.h : main header file for the CORTEXCONTAINERDLL DLL
//

#if !defined(AFX_CORTEXCONTAINERDLL_H__0248122C_810B_40ED_80E6_8905A59CC3B1__INCLUDED_)
#define AFX_CORTEXCONTAINERDLL_H__0248122C_810B_40ED_80E6_8905A59CC3B1__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "..\CortexContainer\CortexContainerDefines.h"
#include "..\..\..\..\Common\LAN\NetUtil.h"  //network comm protocol
#include "..\..\..\..\Common\TXT\BufferUtil.h"  // buffer utilities 

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDLLApp
// See CortexContainerDLL.cpp for the implementation of this class
//

class CCortexContainerDLLApp : public CWinApp
{
public:
	BOOL m_bDlgCreated;
	BOOL m_bDlgDestroyed;
	CWnd* m_pwndStatusText;
	CDialog* m_pdlgMain;
	CString m_szInit;

public:
	CCortexContainerDLLApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexContainerDLLApp)
	//}}AFX_VIRTUAL

	int Control(void** pvObject, unsigned long ulFlags);

	//{{AFX_MSG(CCortexContainerDLLApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINERDLL_H__0248122C_810B_40ED_80E6_8905A59CC3B1__INCLUDED_)
