// CortexContainerDLL.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "CortexContainerDLL.h"
#include "CortexContainedDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDLLApp

BEGIN_MESSAGE_MAP(CCortexContainerDLLApp, CWinApp)
	//{{AFX_MSG_MAP(CCortexContainerDLLApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDLLApp construction

CCortexContainerDLLApp::CCortexContainerDLLApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_bDlgCreated = FALSE;
	m_bDlgDestroyed = FALSE;
	m_pwndStatusText = NULL;
	m_pdlgMain = NULL;
	m_szInit = "";
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCortexContainerDLLApp object

CCortexContainerDLLApp theApp;


int CCortexContainerDLLApp::Control(void** pvObject, unsigned long ulFlags)
{
	switch(ulFlags)
	{
	case CXC_GIVEPARAMS://		0x00000000  // give the init parameters to the DLL.  must be called first!
		{
			if ((pvObject==NULL)||(*pvObject==NULL)) return CXC_ERROR;

			m_szInit.Format("%s", (char*)(*pvObject));

		} break;
	case CXC_GIVESTATUS://		0x00000001  // give a pointer to the status window to the DLL.
		{
			if ((pvObject==NULL)||(*pvObject==NULL)) return CXC_ERROR;

			m_pwndStatusText = (CWnd*)(*pvObject);

		} break;
	case CXC_STARTDIALOG://		0x00000002  // get the dialog pointer.  once we have it, we do a ShowWindow and resize it to the container dimensions.
		{
			if ((pvObject==NULL)||(*pvObject==NULL)) return CXC_ERROR;
			if((m_bDlgCreated)&&(m_pdlgMain))
			{
				(*pvObject) = m_pdlgMain;
			}
			else
			{
				m_bDlgCreated = FALSE;
				if(m_pdlgMain) m_pdlgMain->DestroyWindow( );
				m_pdlgMain = new CCortexContainedDlg;

				if(m_pdlgMain)
				{
					if(!((CCortexContainedDlg*)m_pdlgMain)->Create((CWnd*)(*pvObject))) 
					{
						delete m_pdlgMain;
						m_pdlgMain = NULL;
						return CXC_ERROR;
					}
					else
					{
						//OK, now let's set up the dlg info, for convenience.

						((CCortexContainedDlg*)m_pdlgMain)->m_usPort = 0;
						((CCortexContainedDlg*)m_pdlgMain)->m_pszHost = NULL;
						((CCortexContainedDlg*)m_pdlgMain)->m_pszUserName = NULL;
						((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword = NULL;
						((CCortexContainedDlg*)m_pdlgMain)->m_pszParams = NULL;


						CSafeBufferUtil sbu;
						char  pszDelim[2] = {'|',0};

						char* pszBuffer = (char*)malloc(m_szInit.GetLength()+1);
						if(pszBuffer)
						{
							sprintf( pszBuffer, "%s", m_szInit);
							unsigned long ulBufLen = strlen(pszBuffer);

							char* pch = sbu.Token(pszBuffer, ulBufLen, pszDelim);
							if(pch!=NULL)
							{
								((CCortexContainedDlg*)m_pdlgMain)->m_pszHost = (char*)malloc(strlen(pch)+1);
								if(((CCortexContainedDlg*)m_pdlgMain)->m_pszHost) 
									strcpy(((CCortexContainedDlg*)m_pdlgMain)->m_pszHost, pch); 

								pch = sbu.Token(NULL, ulBufLen, pszDelim);
								if(pch!=NULL)
								{
									((CCortexContainedDlg*)m_pdlgMain)->m_usPort = (unsigned short)atol(pch);
									pch = sbu.Token(NULL, ulBufLen, pszDelim);
									if(pch!=NULL)
									{
										((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword = strchr(pch, 10);
										if(((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword!=NULL)
										{
											((CCortexContainedDlg*)m_pdlgMain)->m_pszUserName 
												= (char*)malloc(((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword-pch+1);
											if(((CCortexContainedDlg*)m_pdlgMain)->m_pszUserName)
											{
												strncpy(((CCortexContainedDlg*)m_pdlgMain)->m_pszUserName, pch, (((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword)-pch); 
												*(
													 (((CCortexContainedDlg*)m_pdlgMain)->m_pszUserName)
													+(((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword-pch+1)
													) = 0; 
											}
											pch = (((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword)+1;

											((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword = (char*)malloc(strlen(pch)+1);
											if(((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword) strcpy(((CCortexContainedDlg*)m_pdlgMain)->m_pszPassword, pch); 
										}

										pch += strlen(pch)+1; // should be at the beginning of DLL filename.
										// so, we can search for another pipe, if we find one and there anything after it, it goes into "other parameters"
										pch = strchr(pch, '|');
										if(pch!=NULL)
										{
											if(strlen(pch+1)>0)
											{
												pch++;
												((CCortexContainedDlg*)m_pdlgMain)->m_pszParams = (char*)malloc(strlen(pch)+1);
												if(((CCortexContainedDlg*)m_pdlgMain)->m_pszParams)
												{
													// copy the params in.
													strcpy(((CCortexContainedDlg*)m_pdlgMain)->m_pszParams, pch);
													((CCortexContainedDlg*)m_pdlgMain)->m_bInitialized = TRUE;
												}
											}
										}
										else  // no extra params
										{
											// we should be good to here.
											((CCortexContainedDlg*)m_pdlgMain)->m_bInitialized = TRUE;
										}
									}
								}
							}
						}

						m_bDlgCreated = TRUE;
						*pvObject = m_pdlgMain;
					}
				}
			}
		} break;
	case CXC_ENDDIALOG://			0x00000003  // signal to destroy dialog and do any other cleanup.  sets the pointer to a bool, so we can wait for window destruction before exiting.
		{
			if(!m_bDlgDestroyed)
			{
				if(m_bDlgCreated)
				{
					((CCortexContainedDlg*)m_pdlgMain)->ExitProcedure();  // this procedure is allowed to wait until all destruction completed, 
					// except for final step:
					m_pdlgMain->DestroyWindow();
					delete m_pdlgMain;
					m_pdlgMain = NULL;
					m_bDlgCreated = FALSE;

					m_bDlgDestroyed = TRUE;
				}
			}
		} break;
	default: 	return CXC_ERROR; break;
	}
	return CXC_SUCCESS;
}
