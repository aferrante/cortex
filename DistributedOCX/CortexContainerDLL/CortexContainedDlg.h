#if !defined(AFX_CORTEXCONTAINEDDLG_H__FE6F14F0_934F_4480_AE07_4FA305CDEA22__INCLUDED_)
#define AFX_CORTEXCONTAINEDDLG_H__FE6F14F0_934F_4480_AE07_4FA305CDEA22__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CortexContainedDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCortexContainedDlg dialog

class CCortexContainedDlg : public CDialog
{
public:
	BOOL m_bInitialized;
	unsigned short m_usPort;
	char* m_pszHost;
	char* m_pszUserName;
	char* m_pszPassword;
	char* m_pszParams;


// Construction
public:
	CCortexContainedDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCortexContainedDlg();   // standard destructor

	void ExitProcedure();

// Dialog Data
	//{{AFX_DATA(CCortexContainedDlg)
	enum { IDD = IDD_DIALOG_MAIN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexContainedDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCortexContainedDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINEDDLG_H__FE6F14F0_934F_4480_AE07_4FA305CDEA22__INCLUDED_)
