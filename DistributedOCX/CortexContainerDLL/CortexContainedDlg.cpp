// CortexContainedDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CortexContainerDLL.h"
#include "CortexContainedDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CCortexContainerDLLApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCortexContainedDlg dialog


CCortexContainedDlg::CCortexContainedDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCortexContainedDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCortexContainedDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bInitialized = FALSE;
	m_usPort = 0;
	m_pszHost = NULL;
	m_pszUserName = NULL;
	m_pszPassword = NULL;
	m_pszParams = NULL;
}

CCortexContainedDlg::~CCortexContainedDlg()
{
	if(m_pszHost)			free(m_pszHost);
	if(m_pszUserName)	free(m_pszUserName);
	if(m_pszPassword)	free(m_pszPassword);
	if(m_pszParams)		free(m_pszParams);
}

void CCortexContainedDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCortexContainedDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCortexContainedDlg, CDialog)
	//{{AFX_MSG_MAP(CCortexContainedDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexContainedDlg message handlers

void CCortexContainedDlg::OnCancel() 
{
	// intercepts esc key
//CDialog::OnCancel();
}

void CCortexContainedDlg::OnOK() 
{
	// intercepts enter key
//CDialog::OnOK();
}

BOOL CCortexContainedDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCortexContainedDlg::IDD, pParentWnd);
}

BOOL CCortexContainedDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	// the init data members should be already set by this point

	GetDlgItem(IDC_STATIC_INFO)->SetWindowText(theApp.m_szInit);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCortexContainedDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	
}

void CCortexContainedDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

// called just before destroying window.  allows cleanup.
void CCortexContainedDlg::ExitProcedure()
{

}
