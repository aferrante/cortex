// CortexContainerCtl.cpp : Implementation of the CCortexContainerCtrl ActiveX Control class.

#include "stdafx.h"
#include "CortexContainer.h"
#include "CortexContainerDlg.h"
#include "CortexContainerCtl.h"
//#include "CortexContainerPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CCortexContainerCtrl, COleControl)
CCortexContainerDlg g_dlg;
CCortexContainerCtrl* g_pctrl=NULL;


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CCortexContainerCtrl, COleControl)
	//{{AFX_MSG_MAP(CCortexContainerCtrl)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CCortexContainerCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CCortexContainerCtrl)
	DISP_PROPERTY_NOTIFY(CCortexContainerCtrl, "Init", m_szInit, OnInitChanged, VT_BSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CCortexContainerCtrl, COleControl)
	//{{AFX_EVENT_MAP(CCortexContainerCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages
/*
// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CCortexContainerCtrl, 1)
	PROPPAGEID(CCortexContainerPropPage::guid)
END_PROPPAGEIDS(CCortexContainerCtrl)
*/

/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCortexContainerCtrl, "CORTEXCONTAINER.CortexContainerCtrl.1",
	0xae697cb4, 0x476d, 0x4f9b, 0xaa, 0xaa, 0xd6, 0xa9, 0x48, 0x64, 0x31, 0x4d)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CCortexContainerCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DCortexContainer =
		{ 0x76b5abb4, 0xf225, 0x4be6, { 0xb7, 0xda, 0x3e, 0x12, 0x9d, 0xad, 0xb7, 0xab } };
const IID BASED_CODE IID_DCortexContainerEvents =
		{ 0x6ce5e52d, 0xb1f2, 0x44b7, { 0xbd, 0x8f, 0x43, 0x69, 0x5b, 0xf1, 0x45, 0x9e } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwCortexContainerOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CCortexContainerCtrl, IDS_CORTEXCONTAINER, _dwCortexContainerOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::CCortexContainerCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CCortexContainerCtrl

BOOL CCortexContainerCtrl::CCortexContainerCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_CORTEXCONTAINER,
			IDB_CORTEXCONTAINER,
			afxRegApartmentThreading,
			_dwCortexContainerOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::CCortexContainerCtrl - Constructor

CCortexContainerCtrl::CCortexContainerCtrl()
{
	InitializeIIDs(&IID_DCortexContainer, &IID_DCortexContainerEvents);

	// TODO: Initialize your control's instance data here.
	g_pctrl=this;
	m_bInit=TRUE;

	// TODO: Initialize your control's instance data here.
	SetInitialSize(GetSystemMetrics(SM_CXFULLSCREEN), GetSystemMetrics(SM_CYFULLSCREEN));
//		SetInitialSize(640, 480); // has to be bigger than the dialog initially
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::~CCortexContainerCtrl - Destructor

CCortexContainerCtrl::~CCortexContainerCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::OnDraw - Drawing function

void CCortexContainerCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	if(m_bInit) // just set the size the first time around
	{
		m_bInit=FALSE;
		CRect rc;
		g_dlg.GetWindowRect(&rc);
		SetControlSize(rc.Width(), rc.Height());
	}

}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::DoPropExchange - Persistence support

void CCortexContainerCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
  PX_String(pPX, _T("Init"), m_szInit, _T(""));

}

CString CCortexContainerCtrl::GetInitData()
{
	return m_szInit;
}



/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl::OnResetState - Reset control to default state

void CCortexContainerCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl message handlers

int CCortexContainerCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	g_dlg.Create(this);
	g_dlg.ShowWindow(SW_SHOW); 
	
	return 0;
}

void CCortexContainerCtrl::OnDestroy() 
{
	COleControl::OnDestroy();
	
	// TODO: Add your message handler code here
	g_dlg.ShowWindow(SW_HIDE);

// send a message to the loaded dialog to terminate.
	if(g_dlg.m_lpfnCtrl) g_dlg.m_lpfnCtrl((void**)NULL, DLLCMD_MAINDLG_END);

	g_dlg.DestroyWindow();

}

void CCortexContainerCtrl::OnInitChanged() 
{
	// TODO: Add notification handler code
	SetModifiedFlag();
}
