#if !defined(AFX_CORTEXCONTAINERCTL_H__665E295A_EC5C_41B8_997D_182ED2F3A9B1__INCLUDED_)
#define AFX_CORTEXCONTAINERCTL_H__665E295A_EC5C_41B8_997D_182ED2F3A9B1__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// CortexContainerCtl.h : Declaration of the CCortexContainerCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerCtrl : See CortexContainerCtl.cpp for implementation.

class CCortexContainerCtrl : public COleControl
{
	DECLARE_DYNCREATE(CCortexContainerCtrl)

// Constructor
public:
	CCortexContainerCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexContainerCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

	BOOL m_bInit;
	CString GetInitData();

// Implementation
protected:
	~CCortexContainerCtrl();

	DECLARE_OLECREATE_EX(CCortexContainerCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CCortexContainerCtrl)      // GetTypeInfo
//	DECLARE_PROPPAGEIDS(CCortexContainerCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CCortexContainerCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CCortexContainerCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CCortexContainerCtrl)
	CString m_szInit;
	afx_msg void OnInitChanged();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

// Event maps
	//{{AFX_EVENT(CCortexContainerCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CCortexContainerCtrl)
		// NOTE: ClassWizard will add and remove enumeration elements here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINERCTL_H__665E295A_EC5C_41B8_997D_182ED2F3A9B1__INCLUDED)
