#ifndef CORTEXCONTAINER_DEFINES_INCLUDED
#define CORTEXCONTAINER_DEFINES_INCLUDED


// the DLL is of the type defined in Client DLL
#include "../../../Applications/Generic/Client/ClientDefines.h"

// However, the following defines are necessary to load said DLL into a Cortex control container,
// and have communications back to Cortex and its modules.

// the DLL to be loaded has one exported function (defined in ClientDefines.h)
// typedef int (CALLBACK* LPFNDLLCTRL)(void**, UINT); //DLLCtrl


// this function takes as args:
// 1) the address of a pointer to a dialog (&(CDialog*)) 
//    which will be set to point to the UI supplied in the DLL
// 2) a command UINT, set to one of the following values:


#define CXCMD_GIVEPARAMS		0x00000010  // give the init parameters to the DLL (overrides any settings file that may exist)
#define CXCMD_GIVESTATUS		0x00000011  // give a pointer to the status window to the DLL.


#endif //CORTEXCONTAINER_DEFINES_INCLUDED