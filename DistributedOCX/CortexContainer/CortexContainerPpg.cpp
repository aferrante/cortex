// CortexContainerPpg.cpp : Implementation of the CCortexContainerPropPage property page class.

#include "stdafx.h"
#include "CortexContainer.h"
#include "CortexContainerPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CCortexContainerPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CCortexContainerPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CCortexContainerPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CCortexContainerPropPage, "CORTEXCONTAINER.CortexContainerPropPage.1",
	0x2ad89f18, 0x9237, 0x4ebc, 0xa5, 0x9b, 0x7f, 0xf8, 0x1f, 0x2d, 0xdb, 0x4d)


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerPropPage::CCortexContainerPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CCortexContainerPropPage

BOOL CCortexContainerPropPage::CCortexContainerPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_CORTEXCONTAINER_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerPropPage::CCortexContainerPropPage - Constructor

CCortexContainerPropPage::CCortexContainerPropPage() :
	COlePropertyPage(IDD, IDS_CORTEXCONTAINER_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CCortexContainerPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerPropPage::DoDataExchange - Moves data between page and properties

void CCortexContainerPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CCortexContainerPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerPropPage message handlers
