//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CortexContainer.rc
//
#define IDS_CORTEXCONTAINER             1
#define IDB_CORTEXCONTAINER             1
#define IDS_CORTEXCONTAINER_PPG         2
#define IDS_CORTEXCONTAINER_PPG_CAPTION 200
#define IDD_PROPPAGE_CORTEXCONTAINER    200
#define IDC_STATIC_PULL                 201
#define IDD_DIALOG_MAIN                 201
#define IDB_BITMAP_PULL                 202
#define IDC_STATIC_FRAME                202
#define IDC_STATIC_STATUS               203
#define IDC_STATIC_PROG                 204
#define IDC_STATIC_EYE                  205
#define IDC_STATIC_INFO                 206
#define IDC_BUTTON_RETRY                207

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         208
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
