#if !defined(AFX_CORTEXCONTAINERDLG_H__BC750647_24D3_4483_A183_955E4DA14968__INCLUDED_)
#define AFX_CORTEXCONTAINERDLG_H__BC750647_24D3_4483_A183_955E4DA14968__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CortexContainerDlg.h : header file
//

#define ID_FRM		0
#define ID_INFO		1
#define ID_PROG		2
#define ID_EYE		3
#define ID_STATUS	4
#define ID_BN			5

#define CCD_NUMCTRLS	6

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDlg dialog

class CCortexContainerDlg : public CDialog
{
// Construction
public:
	CCortexContainerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCortexContainerDlg();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CCortexContainerDlg)
	enum { IDD = IDD_DIALOG_MAIN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCortexContainerDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	CPoint m_ptMouseDown;
	HCURSOR m_hcurArrow;
	HCURSOR	m_hcurSize;
	HCURSOR	m_hcurNS;
	HCURSOR	m_hcurEW;
	BOOL m_bSizing;
	CSize m_sizeDlg;
	CSize m_sizeMouse;
	BOOL m_bVis;

	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[CCD_NUMCTRLS];

	int m_nDxEye;

public:
	void SetProgress(unsigned long ulFlags);

	// things to load the dll...
	char* m_pchIP;
	unsigned short m_usPort;
	char* m_pszUserName;
	char* m_pszPassword;
	char  m_pszPath[MAX_PATH];
	char  m_pszFile[MAX_PATH];
	BOOL m_bLoadingDll;
	BOOL m_bDllInitialized;

	CDialog* m_pdlg;  // pointer to the DLL dialog
	LPFNDLLCTRL m_lpfnCtrl;


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCortexContainerDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonRetry();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINERDLG_H__BC750647_24D3_4483_A183_955E4DA14968__INCLUDED_)
