#if !defined(AFX_CORTEXCONTAINERPPG_H__F1310142_23E7_4ED7_91E6_0C4C76C754D3__INCLUDED_)
#define AFX_CORTEXCONTAINERPPG_H__F1310142_23E7_4ED7_91E6_0C4C76C754D3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// CortexContainerPpg.h : Declaration of the CCortexContainerPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CCortexContainerPropPage : See CortexContainerPpg.cpp.cpp for implementation.

class CCortexContainerPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CCortexContainerPropPage)
	DECLARE_OLECREATE_EX(CCortexContainerPropPage)

// Constructor
public:
	CCortexContainerPropPage();

// Dialog Data
	//{{AFX_DATA(CCortexContainerPropPage)
	enum { IDD = IDD_PROPPAGE_CORTEXCONTAINER };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CCortexContainerPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINERPPG_H__F1310142_23E7_4ED7_91E6_0C4C76C754D3__INCLUDED)
