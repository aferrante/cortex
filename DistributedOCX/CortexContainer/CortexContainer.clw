; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCortexContainerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CortexContainer.h"
CDK=Y

ClassCount=3
Class1=CCortexContainerCtrl
Class2=CCortexContainerPropPage

ResourceCount=2
LastPage=0
Resource1=IDD_PROPPAGE_CORTEXCONTAINER
Class3=CCortexContainerDlg
Resource2=IDD_DIALOG_MAIN

[CLS:CCortexContainerCtrl]
Type=0
HeaderFile=CortexContainerCtl.h
ImplementationFile=CortexContainerCtl.cpp
Filter=W
BaseClass=COleControl
VirtualFilter=wWC
LastObject=CCortexContainerCtrl

[CLS:CCortexContainerPropPage]
Type=0
HeaderFile=CortexContainerPpg.h
ImplementationFile=CortexContainerPpg.cpp
Filter=D

[DLG:IDD_PROPPAGE_CORTEXCONTAINER]
Type=1
Class=CCortexContainerPropPage
ControlCount=0

[DLG:IDD_DIALOG_MAIN]
Type=1
Class=CCortexContainerDlg
ControlCount=9
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC_PULL,static,1342177294
Control4=IDC_STATIC_FRAME,static,1207959559
Control5=IDC_STATIC_STATUS,static,1342308352
Control6=IDC_STATIC_PROG,static,1342177294
Control7=IDC_STATIC_EYE,static,1342177294
Control8=IDC_STATIC_INFO,static,1342308352
Control9=IDC_BUTTON_RETRY,button,1208025088

[CLS:CCortexContainerDlg]
Type=0
HeaderFile=CortexContainerDlg.h
ImplementationFile=CortexContainerDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_BUTTON_RETRY

