#if !defined(AFX_CORTEXCONTAINER_H__220109DF_31A2_4D79_901F_066ED9B4B414__INCLUDED_)
#define AFX_CORTEXCONTAINER_H__220109DF_31A2_4D79_901F_066ED9B4B414__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// CortexContainer.h : main header file for CORTEXCONTAINER.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

#include "CortexContainerDefines.h" 


/////////////////////////////////////////////////////////////////////////////
// CCortexContainerApp : See CortexContainer.cpp for implementation.

class CCortexContainerApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORTEXCONTAINER_H__220109DF_31A2_4D79_901F_066ED9B4B414__INCLUDED)
