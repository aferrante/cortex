// CortexContainerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CortexContainer.h"
#include "CortexContainerCtl.h"
#include "CortexContainerDlg.h"
#include "..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h"  //bmp utilities for interface
#include "..\..\..\Common\LAN\NetUtil.h"  //network comm protocol
#include "..\..\..\Common\TXT\BufferUtil.h"  // buffer utilities 

#include "..\..\..\Cortex\3.0.1.1\CortexDefines.h"

#include <direct.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define DLG_MINSIZEX  150
#define DLG_MINSIZEY  100

#define CXDLG_CLEAR		0x00000000
#define CXDLG_WAITING 0x10000000
#define CXDLG_EVILEYE 0x20000000

#define CXDLG_INITTIMER 27

extern CCortexContainerCtrl* g_pctrl;

// proto
UINT InitializerThread( LPVOID pParam );
UINT DownloadThread( LPVOID pParam );



/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDlg dialog


CCortexContainerDlg::CCortexContainerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCortexContainerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCortexContainerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hcurArrow=LoadCursor(NULL,IDC_ARROW);
	m_hcurSize=LoadCursor(NULL, IDC_SIZENWSE);
	m_hcurEW=LoadCursor(NULL,IDC_SIZEWE);
	m_hcurNS=LoadCursor(NULL,IDC_SIZENS);
	m_bSizing=FALSE;
	m_bVis=FALSE;
	m_bNewSizeInit = TRUE;
	m_nDxEye = 1;
	m_pdlg = NULL;
	m_bLoadingDll = FALSE;
	m_pchIP = NULL;
	m_usPort=0;
	m_pszUserName = NULL;
	m_pszPassword = NULL;
	strcpy(m_pszPath, "");
	strcpy(m_pszFile, "");
	m_lpfnCtrl = NULL;
	m_bDllInitialized = FALSE;
}

CCortexContainerDlg::~CCortexContainerDlg()
{
	if(m_pchIP) free(m_pchIP);
	if(m_pszUserName) free(m_pszUserName);
	if(m_pszPassword) free(m_pszPassword);
}


void CCortexContainerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCortexContainerDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCortexContainerDlg, CDialog)
	//{{AFX_MSG_MAP(CCortexContainerDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_RETRY, OnButtonRetry)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCortexContainerDlg message handlers

void CCortexContainerDlg::OnCancel() 
{
	// prevents closing from escape button
	// CDialog::OnCancel();
}

void CCortexContainerDlg::OnOK() 
{
	// intercepts enter key
	// CDialog::OnOK();
}

BOOL CCortexContainerDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CCortexContainerDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x=DLG_MINSIZEX;
	lpMMI->ptMinTrackSize.y=DLG_MINSIZEY;
}


void CCortexContainerDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if(m_bNewSizeInit)
	{
		int nCtrlID=0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<CCD_NUMCTRLS;i++)  
		{
			switch(i)
			{
			case ID_FRM:		nCtrlID=IDC_STATIC_FRAME;break;
			case ID_INFO:		nCtrlID=IDC_STATIC_INFO;break;
			case ID_PROG:		nCtrlID=IDC_STATIC_PROG;break;
			case ID_EYE:		nCtrlID=IDC_STATIC_EYE;break;
			case ID_STATUS: nCtrlID=IDC_STATIC_STATUS;break;
			case ID_BN:			nCtrlID=IDC_BUTTON_RETRY;break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}

		m_bVis=TRUE;
		m_bNewSizeInit = FALSE;
	}
}

void CCortexContainerDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int dx,dy;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	int nCtrlID=0;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_STATIC_FRAME);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_FRM].left,  
			m_rcCtrl[ID_FRM].top,  
			m_rcCtrl[ID_FRM].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_FRM].Height()-dy, // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		if((m_bDllInitialized)&&(m_pdlg))
		{
			m_pdlg->SetWindowPos( 
				&wndTop,
				m_rcCtrl[ID_FRM].left,  
				m_rcCtrl[ID_FRM].top,  
				m_rcCtrl[ID_FRM].Width()-dx,  // goes with right edge
				m_rcCtrl[ID_FRM].Height()-dy, // goes with bottom edge
				SWP_NOZORDER|SWP_NOMOVE
				);
		}

		pWnd=GetDlgItem(IDC_STATIC_INFO);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_INFO].left,  
			m_rcCtrl[ID_INFO].top-dy/2,  
			m_rcCtrl[ID_INFO].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_INFO].Height(),
			SWP_NOZORDER
			);

		pWnd=GetDlgItem(IDC_STATIC_PROG);
		if(pWnd!=NULL)
		{
			CRect rcProg;
			pWnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pWnd->SetWindowPos( 
			&wndTop,
			rcProg.left,  
			m_rcCtrl[ID_INFO].top-(dy/2)+(m_rcCtrl[ID_PROG].top-m_rcCtrl[ID_INFO].top),  // goes with bottom edge
			rcProg.Width(), // leave the width alone, it's a progress bar!
			rcProg.Height(), 
			SWP_NOZORDER
			);
		}

		pWnd=GetDlgItem(IDC_STATIC_EYE);
		if(pWnd!=NULL)
		{
			CRect rcProg;
			pWnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pWnd->SetWindowPos( 
			&wndTop,
			rcProg.left,  
			m_rcCtrl[ID_INFO].top-(dy/2)+(m_rcCtrl[ID_EYE].top-m_rcCtrl[ID_INFO].top),  // goes with bottom edge
			rcProg.Width(), // leave the width alone, it's a progress bar!
			rcProg.Height(), 
			SWP_NOZORDER
			);
		}

		pWnd=GetDlgItem(IDC_STATIC_STATUS);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STATUS].left,  
			m_rcCtrl[ID_STATUS].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_STATUS].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_STATUS].Height(), 
			SWP_NOZORDER
			);

		pWnd=GetDlgItem(IDC_BUTTON_RETRY);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BN].left-dx/2,  
			m_rcCtrl[ID_BN].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_BN].Width(),
			m_rcCtrl[ID_BN].Height(), 
			SWP_NOZORDER|SWP_NOSIZE
			);


		for (int i=0;i<CCD_NUMCTRLS;i++)  
		{
			switch(i)
			{
			case ID_FRM:		nCtrlID=IDC_STATIC_FRAME;break;
			case ID_INFO:		nCtrlID=IDC_STATIC_INFO;break;
			case ID_PROG:		nCtrlID=IDC_STATIC_PROG;break;
			case ID_EYE:		nCtrlID=IDC_STATIC_EYE;break;
			case ID_STATUS: nCtrlID=IDC_STATIC_STATUS;break;
			case ID_BN:			nCtrlID=IDC_BUTTON_RETRY;break;
			}
			if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
		}
	}		


	if(((nType==SIZE_MAXIMIZED)||(nType==SIZE_RESTORED))&&(m_sizeDlg!=CSize(0,0))&&(m_bVis))
	{
		// move components around
		CRect rc;
		int dx=m_sizeDlg.cx-cx;
		int dy=m_sizeDlg.cy-cy;

		CWnd* pWnd;
		pWnd = GetDlgItem(IDC_STATIC_PULL);
		if(pWnd)
		{
			pWnd->GetWindowRect(&rc);
			ScreenToClient(&rc);
			pWnd->SetWindowPos(NULL, rc.left-dx, rc.top-dy, 0,0, SWP_NOZORDER|SWP_NOSIZE);
		}

		// reset dlg size
		m_sizeDlg.cx=cx;
		m_sizeDlg.cy=cy;
	}
	
	if(g_pctrl!=NULL)
	{
		g_pctrl->SetControlSize(cx , cy);
	}
}

void CCortexContainerDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rc;
	GetWindowRect(&rc);

//	CString foo; foo.Format("dlg:%d, %d\n", rc.Width(), rc.Height() );
//	AfxMessageBox(foo);
//	GetDlgItem(IDC_STATIC_X)->SetWindowText(foo);

	if(g_pctrl!=NULL)
	{
		if(g_pctrl->IsModified())
		{
//			GetDlgItem(IDC_STATIC_INIT)->SetWindowText(g_pctrl->GetInitData());
		}
	}


	GetDlgItem(IDC_STATIC_PULL)->GetWindowRect(&rc);
	ScreenToClient(&rc);

	if(	(point.x>rc.left)&&(point.y>rc.top)	)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurSize!=hCur) SetCursor(m_hcurSize);
	}
	else if (point.x>=rc.right)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurEW!=hCur) SetCursor(m_hcurEW);
	}
	else if (point.y>=rc.bottom)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurNS!=hCur) SetCursor(m_hcurNS);
	}
	else
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurArrow!=hCur) SetCursor(m_hcurArrow);
	}

	if(m_bSizing)
	{
		GetWindowRect(&rc);
		CSize size(1,1);
		if(rc.Width()+((point.x-m_ptMouseDown.x)*m_sizeMouse.cx)<DLG_MINSIZEX) size.cx=0;
		if(rc.Height()+((point.y-m_ptMouseDown.y)*m_sizeMouse.cy)<DLG_MINSIZEY) size.cy=0;

		SetWindowPos(NULL,0,0,
			rc.Width()+((point.x-m_ptMouseDown.x)*m_sizeMouse.cx*size.cx), rc.Height()+((point.y-m_ptMouseDown.y)*m_sizeMouse.cy*size.cy),
			SWP_NOMOVE|SWP_NOZORDER);
		if(size.cx) m_ptMouseDown.x = point.x; // else leave it the old x
		if(size.cy) m_ptMouseDown.y = point.y; // else leave it the old y
//		m_ptMouseDown = point;
	}	
	CDialog::OnMouseMove(nFlags, point);
}

void CCortexContainerDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CRect rc;
	GetDlgItem(IDC_STATIC_PULL)->GetWindowRect(&rc);
	ScreenToClient(&rc);
	if(	(point.x>rc.left)&&(point.y>rc.top)	)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(1,1);
		HCURSOR hCur = GetCursor();
		if(m_hcurSize!=hCur) SetCursor(m_hcurSize);
	}
	else if (point.x>rc.right)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(1,0);
		HCURSOR hCur = GetCursor();
		if(m_hcurEW!=hCur) SetCursor(m_hcurEW);
	}
	else if (point.y>rc.bottom)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(0,1);
		HCURSOR hCur = GetCursor();
		if(m_hcurNS!=hCur) SetCursor(m_hcurNS);
	}
	CDialog::OnLButtonDown(nFlags, point);
}

void CCortexContainerDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	m_bSizing=FALSE;
	ReleaseCapture();
	HCURSOR hCur = GetCursor();
	if(m_hcurArrow!=hCur) SetCursor(m_hcurArrow);
	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CCortexContainerDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CWinApp *pWinApp = AfxGetApp();

	CRect rc;
	GetWindowRect(&rc);
	m_sizeDlg.cx = rc.Width();
	m_sizeDlg.cy = rc.Height();

		// Load all bitmaps
	CBmpUtil bu;
	CBitmap bmp;
	HBITMAP hbmp;

  bmp.LoadBitmap(IDB_BITMAP_PULL);
	hbmp = bu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), bu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();

	// provide transparency to the corner pull
	if(hbmp!=NULL) ((CStatic*)GetDlgItem(IDC_STATIC_PULL))->SetBitmap(hbmp);


	CWnd* pwnd = GetDlgItem(IDC_STATIC_PROG);
	if(pwnd)
	{
		CRect rcProg;
		pwnd->GetClientRect(&rcProg);
		
		hbmp = bu.ColorBitmap(GetDC()->GetSafeHdc(), rcProg.Width(), rcProg.Height(), RGB(0, 0, 192));
		((CStatic*)pwnd)->SetBitmap(hbmp);
	}

	pwnd = GetDlgItem(IDC_STATIC_EYE);
	if(pwnd)
	{
		CRect rcProg;
		pwnd->GetClientRect(&rcProg);
		
		hbmp = bu.ColorBitmap(GetDC()->GetSafeHdc(), rcProg.Width(), rcProg.Height(), RGB(192, 0, 0));
		((CStatic*)pwnd)->SetBitmap(hbmp);
	}

	GetDlgItem(IDC_STATIC_INFO)->SetWindowText("Loading interface...");
	GetDlgItem(IDC_STATIC_PROG)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_STATIC_EYE)->ShowWindow(SW_HIDE);


	// have to start a wait thread.
	// once we get the init info, we can load the necessary DLLs

	GetDlgItem(IDC_BUTTON_RETRY)->ShowWindow(SW_HIDE);	// just in case
	GetDlgItem(IDC_BUTTON_RETRY)->EnableWindow(FALSE);	// just in case


	AfxBeginThread(InitializerThread, (void*) this);

	SetTimer(CXDLG_INITTIMER, 250, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCortexContainerDlg::SetProgress(unsigned long ulFlags)
{
	if(ulFlags&CXDLG_EVILEYE) 
	{
		CWnd* pwnd = GetDlgItem(IDC_STATIC_EYE);
		if(pwnd)
		{
			//width is expressed as a percentage.
			int nWidth;
			if(((ulFlags&0x7f)>0)&&((ulFlags&0x7f)<100))
				nWidth = (ulFlags&0x7f);
			else
				nWidth = 10;

			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_INFO)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);

			//recalc width to pixels;
			nWidth = (rcFull.Width()*nWidth/100);


			if(rcProg.Width() != nWidth)//starting.
			{
				if((rcProg.left+nWidth)>= rcFull.right)
				{
					rcProg.left = rcFull.right-nWidth-1;
				}
				if( rcProg.left <= rcFull.left)
				{
					rcProg.left = rcFull.left+1;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER);
				//speed is expressed as a pixel value.
				if((((ulFlags&0xff00)>>8)>0)&&((int)((ulFlags&0xff00)>>8)<nWidth))  // max one full step per call
					m_nDxEye = ((ulFlags&0xff00)>>8);
				else
					m_nDxEye = 1;
			}
			else
			{
				if((rcProg.right+m_nDxEye>rcFull.right)||(rcProg.left+m_nDxEye<rcFull.left))
				{
					m_nDxEye=-m_nDxEye;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left+m_nDxEye, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER|SWP_NOSIZE);
			}
		}
	}
	else 
	if(ulFlags==CXDLG_CLEAR)
	{
		GetDlgItem(IDC_STATIC_PROG)->ShowWindow(SW_HIDE);
	}
	else
	if((ulFlags&0x7F)<=100)
	{// set a percentage
		// do the progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROG);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_INFO)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// right edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left, rcProg.top,
				rcFull.Width()*(ulFlags&0x7F)/100, rcProg.Height(),
				SWP_NOZORDER);
		}
	}
	else
	if(((ulFlags&0xff)>100)&&((ulFlags&0xff)<200)) // range 1 to 99. described by 101 to 199.
	{// set a percentage
		// do the left side progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROG);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_INFO)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// left edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left+(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.top,
				rcFull.Width()-(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.Height(),  //width done this way because of int truncate
				SWP_NOZORDER);
		}
	}
}

UINT InitializerThread( LPVOID pParam )
{

	CCortexContainerDlg* pDlg = (CCortexContainerDlg*) pParam;
	CWnd* pwnd = pDlg->GetDlgItem(IDC_STATIC_INFO);
	if(pwnd) pwnd->SetWindowText("Getting initialization parameters...");


	pDlg->SetProgress(CXDLG_CLEAR);
	CString szInitData="";

	BOOL bInfoGotten = FALSE;
	while(!bInfoGotten)
	{
		pDlg->SetProgress(CXDLG_EVILEYE);  // default settings.

		if(g_pctrl!=NULL)
		{
			if(g_pctrl->IsModified())
			{
				szInitData = g_pctrl->GetInitData();
				bInfoGotten = TRUE;
			}
		}
		Sleep(1);
	}

	if((bInfoGotten)&&(szInitData.GetLength()>0)) // decode the info
	{
		bInfoGotten = false;
		if(pwnd) pwnd->SetWindowText("Loading interface...");

		// decode, try to load dll locally, if not, get it remotely.

		// the info we want is:
		// ip, port, userid, pw, dll name.
		//  this information is pipe delimited, then vds alpha 64 base encoded.
		// ip | port | user name [char 10] password | dll name (without path - default search path on server side,
		// and different default path used on client side)
		// userid and pw are delimited by char 10, in case of blank password its not a problem.

		char* pszBuffer = (char*)malloc(szInitData.GetLength()+1);
		if(pszBuffer)
		{
			sprintf(pszBuffer, "%s", szInitData);
			unsigned long ulBufLen = strlen(pszBuffer);


			CBufferUtil bu;
			if(bu.Base64Decode(&pszBuffer, &ulBufLen, true)>=RETURN_SUCCESS)
			{
				// first try to load the dll.
				if(bu.CountChar(pszBuffer, ulBufLen, '|')>2)
				{
					CSafeBufferUtil sbu;
					char  pszDelim[2] = {'|',0};

					char* pch = sbu.Token(pszBuffer, ulBufLen, pszDelim);
					if(pch!=NULL)
					{
						pDlg->m_pchIP = (char*)malloc(strlen(pch)+1);
						if(pDlg->m_pchIP) strcpy(pDlg->m_pchIP, pch); 

						pch = sbu.Token(NULL, ulBufLen, pszDelim);
						if(pch!=NULL)
						{
							pDlg->m_usPort = (unsigned short)atol(pch);
							pch = sbu.Token(NULL, ulBufLen, pszDelim);
							if(pch!=NULL)
							{
								pDlg->m_pszPassword = strchr(pch, 10);
								if(pDlg->m_pszPassword!=NULL)
								{
									pDlg->m_pszUserName = (char*)malloc(pDlg->m_pszPassword-pch+1);
									if(pDlg->m_pszUserName)
									{
										strncpy(pDlg->m_pszUserName, pch, (pDlg->m_pszPassword)-pch); 
										*((pDlg->m_pszUserName)+(pDlg->m_pszPassword-pch+1)) = 0; 
										*(pDlg->m_pszPassword) = 0;
									}
									pch = (pDlg->m_pszPassword)+1;

									pDlg->m_pszPassword = (char*)malloc(strlen(pch)+1);
									if(pDlg->m_pszPassword) strcpy(pDlg->m_pszPassword, pch); 
								}

								pch = sbu.Token(NULL, ulBufLen, pszDelim);
								if(pch!=NULL)
								{
									if(GetWindowsDirectory(pDlg->m_pszPath, MAX_PATH))
									{
										int nLen = strlen(pDlg->m_pszPath);
										if(pDlg->m_pszPath[nLen]!='\\')
										{
											pDlg->m_pszPath[nLen++]='\\';
											pDlg->m_pszPath[nLen]=0;
										}
										_snprintf(pDlg->m_pszPath, MAX_PATH-1, "Cortex\\%s", pch);  // we store all the interface dlls in %WINDOWS%\Cortex\*.*
										_snprintf(pDlg->m_pszFile, MAX_PATH-1, "%s", pch);  // to give as a param to the downloader if nec

										// attempt the load
										HINSTANCE hinst = AfxLoadLibrary(pDlg->m_pszPath);
										if(hinst==NULL)  // not there, so lets try to download it
										{
											pDlg->m_bLoadingDll = TRUE;
											AfxBeginThread(DownloadThread, pParam);
											while(pDlg->m_bLoadingDll)
											{		
												pDlg->SetProgress(CXDLG_EVILEYE);  // default settings.
												Sleep(1);
											}

											if(strlen(pDlg->m_pszPath)>0)  // means download was success.
											{
												// now try again
												hinst = AfxLoadLibrary(pDlg->m_pszPath);
											}
										}

										if(hinst!=NULL)  // success
										{
											// get process addresses.
											pDlg->m_lpfnCtrl = (LPFNDLLCTRL)GetProcAddress(hinst, DLLFUNC_NAME);
											if(pDlg->m_lpfnCtrl)
											{
												// give the DLL the comm and other params.
												pDlg->m_lpfnCtrl((void**)&pszBuffer, CXCMD_GIVEPARAMS);  // buffer is already decoded

												// give a pointer to the status window to the DLL.
												CWnd* pstatus = pDlg->GetDlgItem(IDC_STATIC_STATUS);
												pDlg->m_lpfnCtrl((void**)&pstatus, CXCMD_GIVESTATUS);

												//start the dialog
												pDlg->m_pdlg = pDlg; // supply a pointer to the parent window.
												if(pDlg->m_lpfnCtrl((void**)(&(pDlg->m_pdlg)), DLLCMD_MAINDLG_BEGIN)>=CLIENT_SUCCESS)
												{
													// got the pointer!
													bInfoGotten = true;
												}
												else
												{
													pDlg->m_pdlg = NULL;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else free(pszBuffer);
		}
	}

	if((!bInfoGotten)||(szInitData.GetLength()<=0))
	{
		bInfoGotten = false;
		if(szInitData.GetLength()<=0)
		{
			if(pwnd) pwnd->SetWindowText("Interface load unsuccessful - could not get initialization parameters.");
		}
		else
		{
			if(pwnd) pwnd->SetWindowText("Interface load unsuccessful.");
		}
		pDlg->GetDlgItem(IDC_BUTTON_RETRY)->ShowWindow(SW_SHOW);	
		pDlg->GetDlgItem(IDC_BUTTON_RETRY)->EnableWindow(TRUE);	
	}

	pDlg->SetProgress(CXDLG_CLEAR);
	pwnd = pDlg->GetDlgItem(IDC_STATIC_EYE);
	if(pwnd) pwnd->ShowWindow(SW_HIDE);  //get rid of the eye and exit

	if(bInfoGotten) pDlg->m_bDllInitialized = TRUE;
	return 0;
}

UINT DownloadThread( LPVOID pParam )
{
	CCortexContainerDlg* pDlg = (CCortexContainerDlg*) pParam;
	CWnd* pwnd = pDlg->GetDlgItem(IDC_STATIC_INFO);

	char status[NET_ERRORSTRING_LEN];

	_snprintf(status, NET_ERRORSTRING_LEN-1, "Downloading %s...", pDlg->m_pszFile);
	if(pwnd) pwnd->SetWindowText(status);

	pDlg->SetProgress(15);
	CNetUtil net;
	CNetData data;

	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_HASSUBC;  // defined type - indicates which protocol to use, structure of data
	data.m_ucCmd = CX_CMD_GETFILE;       // the command byte
	
	
	if((pDlg->m_pszUserName)&&(strlen(pDlg->m_pszUserName)>0))
		data.m_ucSubCmd = CX_CMD_USEAUTH;			// the subcommand byte
	else
		data.m_ucSubCmd = NET_CMD_NULL;				// the subcommand byte

	data.m_pucData = NULL;

	if(data.m_ucSubCmd == NET_CMD_NULL)
	{
		data.m_ulDataLen = strlen(pDlg->m_pszFile);
		if(data.m_ulDataLen>0)
		{
			char* pch = (char*)malloc(data.m_ulDataLen+1);     // pointer to the payload data
			if(pch)
			{
				strcpy(pch, pDlg->m_pszFile);
				data.m_pucData = (unsigned char*)pch;
			}
		}
	}
	else  // put authentication in.
	{
		data.m_ulDataLen = strlen(pDlg->m_pszFile)+strlen(pDlg->m_pszUserName)+strlen(pDlg->m_pszPassword)+2; // delims
		if(data.m_ulDataLen>0)
		{
			char* pch = (char*)malloc(data.m_ulDataLen+1);     // pointer to the payload data
			if(pch)
			{
				// format is:
				// userid [char 10] pw [char 10] filename
				sprintf(pch, "%s%c%s%c%s", pDlg->m_pszUserName, 10, pDlg->m_pszPassword, 10, pDlg->m_pszFile);
				data.m_pucData = (unsigned char*)pch;
			}
		}
	}

	SOCKET s;
	int nReturn = net.SendData(&data, pDlg->m_pchIP, pDlg->m_usPort, 5000, 0, NET_SND_CMDTOSVR, &s, status);
	if(nReturn<NET_SUCCESS)
	{
		strcpy(pDlg->m_pszPath, "");  //if failure, set to empty string.
		if(data.m_ucCmd == NET_CMD_NAK) // means we received a neg reply, so comm ok, but it doesnt expect a reply.
		{
			_snprintf(status, NET_ERRORSTRING_LEN-1, "Load failed - communications OK, received negative reply.");
			if(pwnd) pwnd->SetWindowText(status);
		}
		else 
		{
			if(pwnd) pwnd->SetWindowText(status);  // display network error
		}
	}
	else  //write the file!
	{
		pDlg->SetProgress(85);
		// send the ack
		net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK, status);
		pDlg->SetProgress(90);

		//file contents are in data.m_pucData
		//make dirs if nec.
		char path_buffer[_MAX_PATH];
		char windows_path_buffer[_MAX_PATH];
		char drive[_MAX_DRIVE];
		char dir[_MAX_DIR];

		if(GetWindowsDirectory(windows_path_buffer, MAX_PATH))
		{
			int nLen = strlen(windows_path_buffer);
			if(windows_path_buffer[nLen]!='\\')
			{
				windows_path_buffer[nLen++]='\\';
				windows_path_buffer[nLen]=0;
			}
		}

		strcpy(path_buffer, pDlg->m_pszPath);
		_splitpath( path_buffer, drive, dir, NULL, NULL );	


		sprintf(path_buffer, "%s%s", drive, dir);
		// have to make dir.
		// if dir already exists, no problem, _mkdir just returns and we continue
		// so, we can always just call mkdir

		for (int i=strlen(windows_path_buffer); i<(int)strlen(path_buffer); i++)
		{
			if((path_buffer[i]=='/')||(path_buffer[i]=='\\'))
			{
				path_buffer[i] = 0;
				if(strlen(path_buffer)>0)
					_mkdir(path_buffer);
				path_buffer[i] = '\\';
			}
		}

		FILE* fp = fopen(pDlg->m_pszPath, "wb");
		if(fp)
		{
			pDlg->SetProgress(95);
			fwrite(data.m_pucData, 1, data.m_ulDataLen, fp );
			fflush(fp);
			fclose(fp);
			_snprintf(status, NET_ERRORSTRING_LEN-1, "File %s created.", pDlg->m_pszFile);
			if(pwnd) pwnd->SetWindowText(status);
		}
		else
		{
			strcpy(pDlg->m_pszPath, "");
			_snprintf(status, NET_ERRORSTRING_LEN-1, "Load failed - could not open file for writing.");
			if(pwnd) pwnd->SetWindowText(status);
		}
	}

	pDlg->SetProgress(99);
	net.CloseConnection(s);

	pDlg->m_bLoadingDll = FALSE;  // indicate we are done loading.

	return 0;
}

void CCortexContainerDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==CXDLG_INITTIMER)
	{
		// check for the init of the DLL dialog.  once its loaded, set the size and show it.
		if((m_bDllInitialized)&&(m_pdlg))
		{
			KillTimer(CXDLG_INITTIMER);

			CRect rc;
			GetDlgItem(IDC_STATIC_FRAME)->GetWindowRect(&rc);
			ScreenToClient(&rc);
			m_pdlg->SetWindowPos( 
				&wndTop,
				rc.left,  
				rc.top,  
				rc.Width(),
				rc.Height(),
				SWP_NOZORDER
				);

			m_pdlg->ShowWindow(SW_SHOW);
		}
	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CCortexContainerDlg::OnButtonRetry() 
{
	GetDlgItem(IDC_BUTTON_RETRY)->ShowWindow(SW_HIDE);	
	GetDlgItem(IDC_BUTTON_RETRY)->EnableWindow(FALSE);	
	AfxBeginThread(InitializerThread, (void*) this);
}
