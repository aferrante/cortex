<%

'--------------------------------------------
'	Constants
'--------------------------------------------

dbpwdfile = "c:\cortex\direct.csf" 'Location of the file that contains the database password.
numperpage = 2
datepattern = "Date-DD-MMM-YYYY-24"

'--------------------------------------------
'	Variables
'--------------------------------------------
public p_channelarray
public nullarray(1,0)
public db
public bkupdb
'dim r
'dim m
'dim f
'dim d
'dim rlocal

nullarray(0,0) = NULL
nullarray(1,0) = NULL

public pwdlenmin
pwdlenmin = 6

public pwdlenmax
pwdlenmax = 12

public framerate
framerate = 29.97 'NTSC drop frame

'--------------------------------------------
'	Functions
'--------------------------------------------

function validuser()

	if len(session("username")) < 1 then
		%>
		<script language="JavaScript">
			parent.location.replace("default.asp");
		</script>
		<%
	end if
end function


Sub getdbvars()

	'Reads in the database password from a text file.
	'chr(13) Carriage return code

	'if len(session("dsn")) < 1 then
		dim fs, f, pos, connectstr, filevar, val

		Set fs=Server.CreateObject("Scripting.FileSystemObject")
		if fs.FileExists(dbpwdfile) = True then
			Set f=fs.OpenTextFile(dbpwdfile, 1)
			filecontents = (f.ReadAll)

			'PRIMARY DATABASE
			'Find DSN
			filevar = "PrimaryDSN"
			pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
			'connectstr = connectstr & mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
			session("dsn") = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)

			'Find Username
			filevar = "PrimaryDBUser"
			pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
			val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
			'connectstr = connectstr & "," & val
			session("dbusername") = val

			'Find Password
			filevar = "PrimaryDBPassword"
			pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
			val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
			'connectstr = connectstr & "," & val
			session("dbpassword") = val

			'Use Backup DB?
			filevar = "UseBackup"
			pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
			val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
			'connectstr = connectstr & "," & val
			usebackup = val

			session("bkupdsn") = ""
			session("bkupdbusername") = ""
			session("bkupdbpassword") = ""
			session("usebkupdb") = 0

			if len(usebackup) > 0 and isnumeric(usebackup) then
				if cint(usebackup) <> 0 then

					session("usebkupdb") = 1

					'BACKUP DATABASE
					'Find DSN
					filevar = "BackupDSN"
					pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
					'connectstr = connectstr & mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
					session("bkupdsn") = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)

					'Find Username
					filevar = "BackupDBUser"
					pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
					val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
					'connectstr = connectstr & "," & val
					session("bkupdbusername") = val

					'Find Password
					filevar = "BackupDBPassword"
					pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
					val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
					'connectstr = connectstr & "," & val
					session("bkupdbpassword") = val
				end if
			end if

			f.Close
			Set f=Nothing

			if len(session("dsn")) < 1 and len(session("bkupdsn")) > 0 then
				%>
				<script language="JavaScript">
					if (confirm("The primary DSN information does not exist in the CSF file.  Would you like to use the Backup database?") != 1) {
						location.replace("errormsg.asp?errorcode=2");
					};
				</script>
				<%
			end if

			if len(session("dsn")) < 1 and len(session("bkupdsn")) < 1 then
				response.redirect "errormsg.asp?errorcode=3"
			end if

		else
			response.redirect "errormsg.asp?errorcode=1"
		end if
		Set fs=Nothing
'end if

end sub


sub ConnectToDBs()

	On Error Resume Next

	set db = CreateObject("ADODB.Connection")
	db.open session("dsn"), session("dbusername"), session("dbpassword")

	if err.number <> 0 then
		' Reset error handling
		if len(session("bkupdsn")) > 0 then
			%>
			<script language="JavaScript">
				if (confirm("A connection to the primary database has failed. Would you like to use the Backup database?") != 1) {
					location.replace("errormsg.asp?errorcode=4");
				}
			</script>
			<%
		else
			if len(session("dsn")) < 1 then
				%>
				<script language="JavaScript">
				 parent.location.replace("default.asp");
				</script>
				<%
			else
				response.redirect "errormsg.asp?errorcode=5&desc=" & err.description
			end if
		end if

		session("dsn") = ""
		On Error Goto 0
	end if

	On Error Resume Next
	if session("usebkupdb") = 1 then
		if len(session("bkupdsn")) > 0 then
			set bkupdb = CreateObject("ADODB.Connection")
			bkupdb.open session("bkupdsn"), session("bkupdbusername"), session("bkupdbpassword")

			if err.number <> 0 then
				' Reset error handling
				On Error Goto 0

				if len(session("dsn")) > 0 then
					%>
					<script language="JavaScript">
						alert("WARNING - The a connection to the backup database has failed.  Changes will only be made to the primary database.");
					</script>
					<%
				else
					response.redirect "errormsg.asp?errorcode=7"
				end if

				session("bkupdsn") = ""
			end if
		else
			response.redirect "errormsg.asp?errorcode=10"
		end if
	end if
end sub



function executeSQL(strsql, sqltype)


	On Error Goto 0
	 'Enable error handling
     On Error Resume Next

	if sqltype = 0 then	'the sql is a select statement

		if len(session("dsn")) > 0 then

			set executeSQL = db.execute(strsql)

			if err.number <> 0 then
				response.redirect "errormsg.asp?errorcode=8&desc=" & err.description

				' Reset error handling
				On Error Goto 0
			else
				exit function
			end if

		elseif len(session("bkupdsn")) > 0 then

			set executeSQL = db.execute(strsql)

			if err.number <> 0 then
				response.redirect "errormsg.asp?errorcode=8&desc=" & err.description

				' Reset error handling
				On Error Goto 0
			else
				exit function
			end if
		else
			response.redirect "errormsg.asp?errorcode=8&desc=" & err.description
		end if
	else

		'select queries only use primary, sqltype greater than 0 are for inserts, updates, deletes which should be done in both dbs if available.

		On Error Resume Next

		if sqltype > 0 and len(session("dsn")) > 0 then
			db.execute(strsql)
			if err.number <> 0 then
				response.redirect "errormsg.asp?errorcode=8&desc="

				' Reset error handling
				On Error Goto 0
			end if
		end if

		On Error Resume Next
		if sqltype > 0 and len(session("bkupdsn")) > 0 then
			bkupdb.execute(strsql)
			if err.number <> 0 then
				response.redirect "errormsg.asp?errorcode=9"

				' Reset error handling
				On Error Goto 0
			end if
		end if
	end if
end function



sub CloseDBs()

	On Error Resume Next
	db.close
	bkdb.close
	On Error Goto 0
end sub

function iif(cond, val1, val2)

	if cond then
		iif = val1
	else
		iif = val2
	end if

end function


function OptionListVideoStandards(Highlight, fromDB)

	dim optionlist, cHighlight

	if fromDB = 1 then
		cHighlight = cint(hex(Highlight)/10) * 10
	else
		cHighlight = Highlight
	end if

	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(cHighlight = 0 or len(cHighlight) < 1, "SELECTED", "") & ">NTSC Drop Frame (29.97 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=10 " & iif(cHighlight = 10, "SELECTED", "") & ">NTSC No Drop Frame/NDF (30 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=20 " & iif(cHighlight = 20, "SELECTED", "") & ">PAL (25 fps)</option>" & vbcrlf

	OptionListVideoStandards = optionlist

end function


function LoadOptionListChannels()

	dim r
	set r = executeSQL("select id, description from Channels order by id", 0)

	if not r.eof then
		p_channelarray = r.getrows()
	else
		p_channelarray = nullarray
	end if

end function




function OptionListChannels(Highlight)

	dim i, optionlist

	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0 or len(Highlight) < 1, "SELECTED", "") & "></option>" & vblf

	if not isnull(p_channelarray(0,0)) then
		for i = 0 to ubound(p_channelarray, 2)
			optionlist = optionlist & "<option value=" & p_channelarray(0,i) & " " & iif(highlight = p_channelarray(0,i), "SELECTED", "") & ">" & p_channelarray(0,i) & "-" & p_channelarray(1,i) & "</option>" & vblf
		next
	end if

	OptionListChannels = optionlist

end function



function OptionListFileExtensions(Highlight)

	dim optionlist, r

	set r = executeSQL("select criterion, mod, flag from exchange where mod > 0 order by criterion", 0)

	cHighlight = iif(len(highlight) > 0, highlight, 0)

	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif((cHighlight + 0 = 0 and len(highlight) > 0), "SELECTED", "") & ">Unknown</option>" & vblf


	do while not r.eof
		optionlist = optionlist & "<option value=""" & r("mod") & """ " & iif(cint(chighlight) = r("mod"), "SELECTED", "") & ">" & r("criterion") & " - " & r("flag") & "</option>" & vblf
	r.movenext
	loop

	OptionListFileExtensions = optionlist

end function




Function OptionListKeyerLayerOp(Highlight)

	optionlist = ""

	optionlist = optionlist & "<option value="""" " & iif(Highlight = "", "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=""<="" " & iif(Highlight = "<=", "SELECTED", "") & ">less than or equal to</option>" & vbcrlf
	optionlist = optionlist & "<option value="">="" " & iif(Highlight = ">=", "SELECTED", "") & ">greater than or equal to</option>" & vbcrlf
	optionlist = optionlist & "<option value=""=="" " & iif(Highlight = "==", "SELECTED", "") & ">equal to</option>" & vbcrlf

	OptionListKeyerLayerOp = optionlist

end function

Function GetKeyerLayerOpText(Highlight)

	if Highlight = "<=" then
		GetKeyerLayerOpText = "less than or equal to"
	elseif Highlight = ">=" then
		GetKeyerLayerOpText = "greater than or equal to"
	elseif Highlight = "==" then
		GetKeyerLayerOpText = "equal to"
	else
		GetKeyerLayerOpText = ""
	end if

end function

Function OptionListDestTypes(Highlight)

	optionlist = ""

	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0, "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=1 " & iif(Highlight = 1, "SELECTED", "") & ">Imagestore</option>" & vbcrlf
	optionlist = optionlist & "<option value=2 " & iif(Highlight = 2, "SELECTED", "") & ">Intuition</option>" & vbcrlf

	OptionListDestTypes = optionlist

end function


Function GetDestType(curtype)

	if len(curtype) < 1 then
		GetDestType = ""
	else
		if curtype = 1 then
			GetDestType = "Imagestore"
		elseif curtype = 2 then
			GetDestType = "Intuition"
		else
			GetDestType = ""
		end if
	end if
end function



function PageTitleIndent()

	PageTitleIndent= IndentMe(5)

end function

function IndentMe(indentnum)

	dim i, tmp

	tmp = ""
	for i=1 to indentnum
		tmp = tmp & "&nbsp;&nbsp;&nbsp;&nbsp;"
	next

	IndentMe = tmp
end function

function fixstring(curstring)

	if len(curstring) > 0 then
		fixstring = replace(curstring, "'", "''")
	else
		fixstring = ""
	end if
end function


function Validate(curstr, curtype)

	if isnull(curstr) = False then
		dim re
		set re = new regexp

		if curtype = "IP" then
			re.Pattern = "^([0-9]{1,3}\.){3}([0-9]{1,3}){1}$"
		elseif curtype = "DBColName" then
			re.Pattern = "^([0-9]|[a-z]|[_]|\s)+$"
		elseif curtype = "Duration" then
			re.Pattern = "^(([0-9]){2}:{1}){2}(([0-9]){2}\.{1}){1}([0-9]{2}){1}$"
		elseif curtype = "Date-DD-MMM-YYYY-24" then
			re.Pattern = "^[0-9]{1,2}\s{1}([a-z]|[A-Z]){3}\s{1}[0-9]{4}(\s{1}([0-9]{2}:){2}[0-9]{2}){0,1}$"
		end if

		re.IgnoreCase = true
		re.Global = true
		Validate = re.test(curstr)
	else
		Validate = False
	end if
end function


function paddatepart(curstr)
	if len(curstr) = 2 then
		paddatepart = curstr
	else
		paddatepart = "0" & curstr
	end if
end function


function unUDate(intTimeStamp)
	dim tmp
	if len(intTimeStamp) > 0 then
		tmp = cdate(DateAdd("s", intTimeStamp, "01/01/1970 00:00:00") & iif(intTimeStamp mod 86400 = 0, " 12:00:00 AM", ""))
		unUDate = paddatepart(day(tmp)) & " " & monthname(month(tmp), True) & " " & Year(tmp) & " " & paddatepart(hour(tmp)) & ":" & paddatepart(minute(tmp)) & ":" & paddatepart(second(tmp))

	'	unUDate = Day(tmp) & " " & monthname(month(tmp), True) & " " & Year(tmp) & " " & hour(tmp) & ":" & minute(tmp) & ":" & second(tmp)
	else
		unUDate = ""
	end if
end function


function UDate(curDate)

	if len(curDate) > 0 then
		UDate = DateDiff("s", "01/01/1970 00:00:00", curDate)
	else
		UDate = ""
	end if
end function


function MStoHMSF(milliseconds, basis)

	if len(milliseconds) > 0 then
		if milliseconds > 0 then
			dim duration, part
			duration = ""
			part = formatnumber(int(milliseconds/3600000), 0, -1, 0, 0)
			if len(part)<2 then
				duration = duration + "0"
			end if
			duration = duration + part + ":"

			part = formatnumber(int(milliseconds/60000) mod 60, 0, -1, 0, 0)
			if len(part)<2 then
				duration = duration + "0"
			end if
			duration = duration + part + ":"
			part = formatnumber((int(milliseconds/1000) mod 60)mod 60, 0, -1, 0, 0)
			if len(part)<2 then
				duration = duration + "0"
			end if
			duration = duration + part + "."

			select case int(basis) 'pal=25, ntsc = 30, ndf = 29
			case 25: 'PAL	(25 fps)
				part = formatnumber(int((milliseconds mod 1000)/40),0, -1, 0, 0)  '(int)(1000.0/25.0) = 40
			case 30:' NTSC no drop frame (30 fps)
				part = formatnumber(int((milliseconds mod 1000)/33),0, -1, 0, 0)  '(int)(1000.0/30.0)  = (int)(33.333333) = 33
			case else: 'default to NTSC drop frame   (29.97 fps)
				part = formatnumber(int((milliseconds mod 1000)/33),0, -1, 0, 0)  '(int)(1000.0/29.97) = (int)(33.366700) = 33

			end select
			if len(part)<2 then
				duration = duration + "0"
			end if
			duration = duration + part

			MStoHMSF = duration
		else
			MStoHMSF = ""
		end if
	else
		MStoHMSF = ""
	end if
end function


function HMSFtoMS(dur, basis)

	'12:45:78.90
	dim ms, hours, minutes, seconds, frames

	hours = 	cint(mid(dur, 1, 2))
	minutes = 	cint(mid(dur, 4, 2))
	seconds = 	cint(mid(dur, 7, 2))
	frames = 	cint(mid(dur, 10, 2))

	ms = int(basis + 0.1)
	if hours>23 or hours<0  or minutes>59 or minutes<0 or seconds>59 or seconds<0 or seconds>59 or frames<0 or frames >= ms then
		ms = -1
	else
		ms = (hours*3600000+minutes*60000+seconds*1000+(int)(((frames)*1000.0)/basis)) ' parens for rounding
  	end if
	HMSFtoMS = ms
end function




function PrintPageNumbers(numofpages, pagevars, curpage)
	dim i, tmp

	if numofpages > 0 then
		numofpages = (numofpages/numperpage) + iif(numofpages mod numperpage <> 0, 1, 0)
		tmp = "Page "
		for i=1 to numofpages
			tmp = tmp & "<a style=""color: black;"" href=""" & request.servervariables("SCRIPT_NAME") & "?pagenum=" & i & pagevars & """>" & iif(i = curpage, "<b>" & i & "</b>", i) & "</a>&nbsp;"
		next
	else
		tmp = ""
	end if

	PrintPageNumbers = tmp
end function


function OptionListDataTypes(Highlight)

	optionlist = ""

	optionlist = optionlist & "<option value="""" " & iif(Highlight = "", "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=""int"" " & iif(Highlight = "int", "SELECTED", "") & ">Integer</option>" & vbcrlf
	optionlist = optionlist & "<option value="""" " & iif(Highlight = "varchar(256)", "SELECTED", "") & ">Text</option>" & vbcrlf

	OptionListDataTypes = optionlist


end function


function getdevicename(curipaddress)

	dim r
	set r = executeSQL("select description from Destinations where ip = '" & curipaddress & "'", 0)

	if not r.eof then
		getdevicename = r("description")
	else
		getdevicename = ""
	end if

end function


function determinesortby(curcol, cursortby)

	if (instr(1, cstr(cursortby), " desc") > 0) or (cstr(curcol) <> cstr(cursortby)) then
		determinesortby = curcol
	else
		determinesortby = curcol & " desc"
	end if

end function

dim pwin
pwin = "password"

function benc( ins )

	dim dlen
	dlen = len(ins)
	const alpha = ")x-{fJ|M2mqwO( /+tS:.<*WGoF&%`7Uc$]=Hhj;D^_0}5b3NQ[l1LsAX8d>4g\y"
	const padch = "@"
	const padch2 = "~"
	dim out, n
	out = ""

	for n = 1 to dlen step 3
		dim triple, quad

		'Create one long from this 3 bytes.
		triple = &H10000 * asc(Mid(ins, n, 1)) + _
		  &H100 * retch(Mid(ins, n + 1, 1)) + retch(Mid(ins, n + 2, 1))

		'Oct splits the long To 8 groups with 3 bits
		triple = Oct(triple)

		'Add leading zeros
		triple = String(8 - Len(triple), "0") & triple

		'Convert To base64
		quad = Mid(alpha, CLng("&o" & Mid(triple, 1, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 3, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 5, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 7, 2)) + 1, 1)

		'Add the part To OutPut string
		out = out + quad

	next

	select case dlen mod 3
		case 0: '0 bit final - add padchs for encrypt
		  out = padch + Left(out, Len(out) - 2)  + padch2
		case 1: '8 bit final - split padchs for encrypt
		  out = padch2 +Left(out, Len(out) - 2)  + padch
		case 2: '16 bit final - prepend padchs for encrypt
		  out = padch + Left(out, Len(out) - 1)
	end select
	benc = StrReverse(out)
end function

function retch( inch )
	if inch = "" then
		retch = 0
	else
		retch = asc(inch)
	end if
end function




%>