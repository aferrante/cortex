<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>
<body>
<%
call getdbvars()
Call ConnectToDBs()

errormsg = ""

'DELETING - do not update other destinations while deleting.
deletepos = instr(1, request.form, "delete_dest")

if deletepos > 0 then

	deletepos = deletepos + 11 'move to the end of the variable name
	deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the destination ip

	set r = executeSQL("select Channels.flags from Destinations join Channels on destinations.channelid = Channels.id where destinations.ip = '" & deletepos & "'", 0)

	activechannel = 0
	if not r.eof then
		if cint(r("flags")) mod 2 = 1 then
			activechannel = 1
		end if
	end if

	if activechannel = 0 then
		strsql = "delete from destinations where ip = '" & deletepos & "'"

		'response.write strsql
		call executeSQL(strsql, 1)

		set r = executeSQL("select table_name from INFORMATION_SCHEMA.TABLES where table_name =  'Device_" & deletepos & "'", 0)

		if not r.eof then
			strsql = "drop table [Device_" & deletepos & "]"
			call executeSQL(strsql, 1)
		end if
	else
		errormsg = errormsg & "- Miranda Device " & deletepos & " cannot be deleted because the channel is active.  Please deactivate the channel before deleting."
	end if

'ADD A DESTINATIONS
elseif instr(1, request.form, "add_dest") > 0 then

	if Validate(request.form("ipaddressNEW"), "IP") = False then
		errormsg = errormsg & "- IP address is not in a valid format.<br>"
	else
		set r = executeSQL("select * from Destinations where ip = '" & request.form("ipaddressNEW") & "'", 0)

		if not r.eof then
			errormsg = "- Miranda Device " & request.form("ipaddressNEW") & " already exists in the database."
		end if
	end if

	if len(request.form("descNEW")) < 1 then
		errormsg = errormsg & "- Description is required.<br>"
	end if

	if len(request.form("diskspaceNEW")) < 1 or isnumeric(request.form("diskspaceNEW")) = False then
		errormsg = errormsg & "- % Disk Space Limit is not a numeric value.<br>"
	else
		if request.form("diskspaceNEW") < 5 or request.form("diskspaceNEW") > 99 then
			errormsg = errormsg & "- % Disk Space Limit is out of range.  The minimum value is 5 and the maximum value is 99.<br>"
		end if
	end if

	if request.form("channelidNEW") = 0 then
		errormsg = errormsg & "- Associated Omnibus Channel Number is required.<br>"
	end if

	if len(request.form("keyerlayeropNEW")) < 1 then
		errormsg = errormsg & "- Keyer Layer is required.<br>"
	end if

	if len(request.form("keyerlayerNEW")) < 1 or isnumeric(request.form("keyerlayerNEW")) = False then
		errormsg = errormsg & "- Keyer Layer is not a numeric value.<br>"
	else
		if request.form("keyerlayerNEW") < 0 then
			errormsg = errormsg & "- Keyer Layer is out of range.  The minimum value is 0.<br>"
		end if
	end if

	if len(errormsg) < 1 then

		ext = ""
		if request.form("typeNEW") = 1 then
			ext = "V:oxt,oxa|A:oxw,oxe"
		elseif request.form("typeNEW") = 2 then
			ext = "V:tem"
		end if

		if len(request.form("typeNEW")) > 0 then
			curtype = request.form("typeNEW")
		else
			curtype = NULL
		end if

		strsql = "insert into Destinations (ip, keys, criteria, diskspace, type, description, channelid, keyerlayer, keyerlayerop, extensions) values ('" & request.form("ipaddressNEW") & "', '<roID>,<data>', '<roID>nd0==" & request.form("channelidNEW") & ",<data>d|2d:1" & request.form("keyerlayeropNEW") & request.form("keyerlayerNEW") & "', " & request.form("diskspaceNEW") & ", " & curtype & ", '" & fixstring(request.form("descNEW")) & "', " & request.form("channelidNEW") & ", " & request.form("keyerlayerNEW") & ", '" & request.form("keyerlayeropNEW") & "', '" & ext & "')"

		'response.write strsql

		call executeSQL(strsql, 1)


	end if


'UPDATE ALL DESTINATIONS
elseif request.form("update") = "Update" then

	idarray = split(request.form("ids"), ",")

		for i=0 to ubound(idarray) -1
			curid = idarray(i)

			if Validate(request.form("ipaddress" & idarray(i)), "IP") = False then
					errormsg = errormsg & "Line " & i + 1 & " - IP address is not in a valid format.<br>"
			else
				if request.form("ipaddress" & idarray(i)) <> idarray(i) then

					set r = executeSQL("select * from Destinations where ip = '" & request.form("ipaddress" & idarray(i)) & "' and ip <> '" & idarray(i) & "'", 0)

					if not r.eof then
						errormsg = "Line " & i + 1 & " - Miranda Device " & request.form("ipaddress" & idarray(i)) & " already exists in the database."
					end if
				end if
			end if

			if len(request.form("desc" & idarray(i))) < 1 then
				errormsg = errormsg & "Line " & i + 1 & " - Description is required.<br>"
			end if

			if len(request.form("diskspace" & idarray(i))) < 1 or isnumeric(request.form("diskspace" & idarray(i))) = False then
				errormsg = errormsg & "Line " & i + 1 & " - % Disk Space Limit is not a numeric value.<br>"
			else
				if request.form("diskspace" & idarray(i)) < 5 or request.form("diskspace" & idarray(i)) > 99 then
					errormsg = errormsg & "Line " & i + 1 & " - % Disk Space Limit is out of range.  The minimum value is 5 and the maximum value is 99.<br>"
				end if
			end if

		next

	if len(errormsg) < 1 then

		for i=0 to ubound(idarray) -1
				curid = idarray(i)

				strsql = "update destinations set ip = '" & request.form("ipaddress" & idarray(i)) & "', description = '" & fixstring(request.form("desc" & idarray(i))) & "', diskspace = " & request.form("diskspace" & idarray(i)) & " where ip = '" & idarray(i) & "'"

				'response.write strsql & "<BR><BR>"

				call executeSQL(strsql, 1)

				if request.form("ipaddress" & idarray(i)) <> idarray(i) then

					set r = executeSQL("select table_name from INFORMATION_SCHEMA.TABLES where table_name =  'Device_" & idarray(i) & "'", 0)

					if not r.eof then
						strsql = "EXEC sp_rename '[" & r("table_name") & "]', 'device_" & request.form("ipaddress" & idarray(i)) & "', 'OBJECT'"
						'response.write strsql
						call executeSQL(strsql, 1)
					end if
				end if
			next

	end if




end if

'Determine Sort Clause

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "Destinations.description"))

set r = executeSQL("select Destinations.ip, Destinations.criteria, Destinations.diskspace, Destinations.type, Destinations.description, Destinations.kb_free, Destinations.kb_total, Destinations.channelid, Destinations.keyerlayer, Destinations.keyerlayerop,Channels.flags, Channels.description as channeldesc, devicetables.table_name as tablename from Destinations left join Channels on destinations.channelid = Channels.id left join (select table_name, right(table_name, len(table_name) - 7) as ip from INFORMATION_SCHEMA.TABLES where table_name like 'Device_%') as devicetables on destinations.ip = devicetables.ip order by " & sortby, 0)

set d = executeSQL("select table_name from INFORMATION_SCHEMA.TABLES where table_name like 'Device_%' order by table_name", 0)

call LoadOptionListChannels()

%>

<form method=post action=<%=request.servervariables("SCRIPT_NAME")%> name=form onreset="resetStyles();">
<input type=hidden name=sortby value="<%=sortby%>">

<table border=0 cellspacing=0 cellpadding=2>
	<thead>
		<tr valign=top>
			<td colspan=3><h2><nobr><%=PageTitleIndent%>Miranda Devices</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
			<td align=right colspan=3>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				</nobr>
			</td>
		</tr>

		<tr class="tableheader">
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.ip", sortby)%>" title="Sort By IP Address" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">IP Address</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.description", sortby)%>" title="Sort By Description" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Description</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.diskspace", sortby)%>" title="Sort By % Disk Space Limit" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">% Disk Space Limit</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.channelid", sortby)%>" title="Sort By Associated Omnibus Channel Number" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Associated Omnibus Channel Number</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.type", sortby)%>" title="Sort By Device Type" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Device Type</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.keyerlayer", sortby)%>" title="Sort By Keyer Layer" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Keyer Layer</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.kb_free", sortby)%>" title="Sort By KB Free" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">KB Free</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("Destinations.kb_total", sortby)%>" title="Sort By KB Total" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">KB Total</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("devicetables.ip", sortby)%>" title="Sort By View" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">View</a></th>
			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			
		</tr>
	</thead>

	<tbody>


	<!--Begin Add New Destination-->

	<tr class="tablerowAddNew" valign=center>
		<td class="tablecell" align=center>
			<input type=text name=ipaddressNew size=15 value="<%=iif(len(errormsg) > 0, request.form("ipaddressNew"), "")%>" onchange="changedValue('ipaddressNew');">
		</td>
		<td class="tablecell">
			<input id="descNEW" type=text name=descNEW size=20 value="<%=iif(len(errormsg) > 0, request.form("descNEW"), "")%>" onchange="changedValue('descNEW');">
		</td>
		<td class="tablecell" align=center>
			<input type=text name=diskspaceNEW size=3 value="<%=iif(len(errormsg) > 0, request.form("diskspaceNEW"), "")%>" onchange="changedValue('diskspaceNEW');">
		</td>
		<td class="tablecell">
			<select name=channelidNEW size=1 smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();" onblur="BuildDestDesc();">
				<%=OptionListChannels(iif(len(errormsg) > 0, request.form("channelidNEW") + 0, 0))%>
			</select>
		</td>
		<td class="tablecell">
			<select id="typeNEW" name=typeNEW size=1 smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();" onblur="DestTypeFunctionCalls();">
				<%=OptionListDestTypes(iif(len(errormsg) > 0, request.form("typeNEW") + 0, 0))%>
			</select>
		</td>
		<td class="tablecell">
			<nobr>
			<select id="keyerlayeropNEW" name=keyerlayeropNEW size=1 smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();">
				<%=OptionListKeyerLayerOp(iif(len(errormsg) > 0, request.form("keyerlayeropNEW") & "", ""))%>
			</select>
			<input id="keyerlayerNEW" type=text name=keyerlayerNEW size=2 value="<%=iif(len(errormsg) > 0, request.form("keyerlayerNEW"), "")%>">
			</nobr>
		</td>
		<td class="tablecell">&nbsp;</td>
		<td class="tablecell">&nbsp;</td>
		<td class="tablecell">&nbsp;</td>

		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_dest" value="Add New"></td>
	</tr>

	<!--End Add New Destination-->

	<%

ids = ""
i = 0

do while not r.eof


	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell" align=center>
			<input id="New" type=text name="<%="ipaddress" & r("ip")%>" size=15 value="<%=iif(len(errormsg) > 0, request.form("ipaddress" & r("ip")), r("ip"))%>" onchange="changedValue('<%="ipaddress" & r("ip")%>');">
		</td>
		<td class="tablecell">
			<input id="New" type=text name="<%="desc" & r("ip")%>" size=20 value="<%=iif(len(errormsg) > 0, request.form("desc" & r("ip")), r("description"))%>" onchange="changedValue('<%="desc" & r("ip")%>');">
		</td>
		<td class="tablecell" align=center>
			<input id="New" type=text name="<%="diskspace" & r("ip")%>" size=3 value="<%=iif(len(errormsg) > 0, request.form("diskspace" & r("ip")), r("diskspace"))%>" onchange="changedValue('<%="diskspace" & r("ip")%>');">
		</td>
		<td class="tablecell">
			<%=r("channelid") & "-" & r("channeldesc")%>
		</td>
		<td class="tablecell"><%=GetDestType(r("type"))%>&nbsp;</td>
		<td class="tablecell">
			<nobr>
			<%=GetKeyerLayerOpText(r("keyerlayerop")) & " " & r("keyerlayer")%>&nbsp;
			</nobr>
		</td>
		<td class="tablecell" align=right>&nbsp;<%=r("kb_free")%></td>
		<td class="tablecell" align=right>&nbsp;<%=r("kb_total")%></td>
		<td class="tablecell" align=center><%=iif(len(r("tablename")) > 0, "<a style=""color: black;"" href=""devicefiles.asp?devicetable=" & r("tablename") & " "" target=""content"">View</a>", "")%>&nbsp;</td>

		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<input type=submit name="<%="delete_dest" & r("ip")%>" value=Delete onclick="return confirm_on_click('DELETE Miranda ' + '<%=r("ip")%>' + '?  All other changes will be lost.');">
			&nbsp;</nobr>
		</td>
	</tr>

	<%

	ids = ids & r("ip") & ","

i = i + 1
r.movenext
loop

%>
	<tr><td class="tablefooter" colspan=10>&nbsp;</td></tr>
	<tr>
		<td colspan=3>&nbsp;</td>
		<td align=right colspan=3>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=ids value="<%=ids%>">
</form>
<%

call CloseDBs()

%>
</body>
</html>