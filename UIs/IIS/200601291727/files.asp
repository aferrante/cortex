<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>
<body>
<%
call getdbvars()
Call ConnectToDBs()
errormsg = ""

'DELETING - do not update other file extensions while deleting.
deletepos = instr(1, request.form, "delete_ext")

if deletepos > 0 then

	deletepos = deletepos + 10 'move to the end of the variable name
	deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the file extension

	strsql = "delete from exchange where criterion = '" & deletepos & "'"
	'response.write strsql
	call executeSQL(strsql, 1)

'ADD A FILE EXTENSION
elseif instr(1, request.form, "add_ext") > 0 then

	set r = executeSQL("select * from Exchange where criterion = '" & request.form("extNEW") & "'", 0)

	if not r.eof then
		errormsg = "<h3><font color=red>ERROR: File Extension " & request.form("extNEW") & " already exists in the database.</font></h3><br>"

	else

		set m = executeSQL("select max(mod) as maxmod from Exchange", 0)

		newmod = 1
		if not m.eof then
			if isnull(m("maxmod")) = False then
				newmod = newmod + cint(m("maxmod"))
			end if
		end if

		strsql = "insert into Exchange (criterion, flag, mod) values ('" & request.form("extNEW") & "', '" & fixstring(request.form("flagNEW")) & "', " & newmod & ")"

		'response.write strsql
		call executeSQL(strsql, 1)
	end if


'UPDATE ALL FILE EXTENSIONS
elseif request.form("update") = "Update" then

	extarray = split(request.form("extensionids"), ",")

		for i=0 to ubound(extarray) -1
			curext = extarray(i)

			strsql = "update exchange set flag = '" & fixstring(request.form("flag" & curext)) & "' where criterion = '" & curext & "'"

			'response.write strsql & "<BR><BR>"

			call executeSQL(strsql, 1)
		next

end if

'Determine Sort By

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "criterion"))


set r = executeSQL("select * from Exchange where len(criterion) <=3 order by " & sortby, 0)

%>

<form method=post action=<%=request.servervariables("SCRIPT_NAME")%> name=form onreset="resetStyles();">
<input type=hidden name=sortby value="<%=sortby%>">

<table border=0 cellspacing=0 cellpadding=2>
	<thead>
		<tr valign=top>
			<td colspan=2><h2><nobr><%=PageTitleIndent%>File Extensions</nobr></h2><%=errormsg%></td>
			<td align=right>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				</nobr>
			</td>
		</tr>

		<tr class="tableheader">
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("criterion", sortby)%>" title="Sort By File Extension" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">File Extension</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("flag", sortby)%>" title="Sort By Description" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Description</a></th>
			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			
		</tr>
	</thead>

	<tbody>


	<!--Begin Add New File Extension-->

	<tr class="tablerowAddNew" valign=center>
		<td class="tablecell" align=center><input id="New" type=text name=extNEW size=5 value="<%=iif(len(errormsg) > 0, request.form("extNEW"), "")%>" onchange="changedValue('extNEW');"></td>
		<td class="tablecell"><input id="New" type=text name=flagNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("flagNEW"), "")%>" onchange="changedValue('flagNEW');"></td>
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_ext" value="Add New" onclick="return verify_new_ext();"></td>
	</tr>

	<!--End Add New File Extension-->

	<%

extensionids = ""
i = 0

do while not r.eof
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell" align=center><%=r("criterion")%></td>
		<td class="tablecell"><input type=text name="<%="flag" & r("criterion")%>" size=30 value="<%=r("flag")%>" onchange="changedValue('<%="flag" & r("criterion")%>');"></td>
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<input type=submit name="<%="delete_ext" & r("criterion")%>" value=Delete onclick="return confirm_on_click('DELETE File Extension ' + '<%=r("criterion")%>' + '?  All other changes will be lost.');">
			&nbsp;</nobr>
		</td>
	</tr>
	<%

	extensionids = extensionids & r("criterion") & ","

i = i + 1
r.movenext
loop

%>
	<tr><td class="tablefooter" colspan=3>&nbsp;</td></tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td align=right>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=extensionids value="<%=extensionids%>">
<form>
<%

call CloseDBs()

%>
</body>
</html>