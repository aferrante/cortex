drop database Direct;

create database Direct;

use Direct;

drop database DirectBackup;

create database DirectBackup;

use DirectBackup;

create table Channels (ID int NOT NULL, flags int NOT NULL, description varchar(64));

create table Destinations (IP varchar(16) NOT NULL, keys varchar(256) NOT NULL, criteria varchar(256) NOT NULL, diskspace tinyint NOT NULL, extensions varchar(64) NOT NULL, type tinyint NOT NULL, description varchar(32), kb_free bigint, kb_total bigint, channelid int, keyerlayer smallint, keyerlayerop varchar(3));

create table Events (id varchar(12) NOT NULL, channel int NOT NULL, data varchar(64) NOT NULL, on_air_time int NOT NULL, status int NOT NULL, duration int NOT NULL, clip_id varchar(32), clip_title varchar(64), files varchar(1024), ip varchar(16));

create table Exchange (criterion varchar(32) NOT NULL, flag varchar(256) NOT NULL, mod int NOT NULL);

insert into Exchange (criterion, flag, mod) values ('clone', 'MAIN', 0);

insert into Exchange (criterion, flag, mod) values ('oxa', 'Miranda animation file', 1);

insert into Exchange (criterion, flag, mod) values ('oxe', 'Miranda 8-channel audio file', 2);

insert into Exchange (criterion, flag, mod) values ('oxi', 'Miranda Intuition animation file', 3);

insert into Exchange (criterion, flag, mod) values ('oxt', 'Miranda still image file', 4);

insert into Exchange (criterion, flag, mod) values ('oxw', 'Miranda stereo audio file', 5);

insert into Exchange (criterion, flag, mod) values ('ttf', 'True Type Font', 6);

insert into Exchange (criterion, flag, mod) values ('wav', 'Wave audio file', 7);

insert into Exchange (criterion, flag, mod) values ('tem', 'Miranda Intuition template file', 8);

insert into Exchange (criterion, flag, mod) values ('status_live', 'Live event status', 0);

insert into Exchange (criterion, flag, mod) values ('status_error', 'Error status', 0);

create table Menu (id tinyint NOT NULL, label varchar(64), status char(4), indent tinyint NOT NULL, icon varchar(256), subitems tinyint NOT NULL, target varchar(64), link varchar(256), description varchar(64), fulldescription varchar(255));

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (0, '', NULL, 0, 'images/vds_logo.gif', 0, 'content', 'splash.asp', 'Video Design Software', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (1, 'Cortex', 'GRN', 0, 'images/e_icon.gif', 2, 'content', 'main.asp', 'Cortex main broker module', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (2, 'Modules', NULL, 1, NULL, 0, 'content', 'modules.asp', 'Installed modules', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (3, 'User Admin', NULL, 1, NULL, 0, 'content', 'users.asp', 'Cortex user administration', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (4, 'Omnibus', 'GRN', 0, 'images/e_icon.gif', 2, 'content', 'omni.asp', 'Omnibus Colossus interface', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (5, 'Channels', NULL, 1, NULL, 0, 'content', 'channels.asp', 'Colossus channel information', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (6, 'Settings', NULL, 1, NULL, 0, 'content', 'adaptor.asp', 'Colossus Adaptor connection settings', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (7, 'Miranda', 'GRN', 0, NULL, 0, 'content', 'miranda.asp', 'Miranda device interface', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (8, 'Metadata', 'GRN', 0, NULL, 0, 'content', 'metadata.asp', 'SQL database interface', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (9, 'DiReCT', 'GRN', 0, 'images/e_icon.gif', 3, 'content', 'direct.asp', 'DiReCT process controller', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (10, 'File Types', NULL, 1, 'images/files.gif', 0, 'content', 'files.asp', 'Registered file types', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (11, 'Messages', NULL, 1, 'images/msg.gif', 0, 'content', 'msg.asp', 'System messages and errors', '');

insert into Menu (id, label, status, indent, icon, subitems, target, link, description, fulldescription) values (12, 'Help', NULL, 1, 'images/help.gif', 0, 'content', 'help.asp', 'How to use DiReCT', '');

create table Messages (message varchar(512) NOT NULL, sender varchar(64), flags int NOT NULL, systime int NOT NULL, id int NOT NULL, DeletedBy varchar(64), DeletedOn int);

create table Metadata (filename varchar(256) NOT NULL, filepath varchar(256), linked_file varchar(256), description varchar(256), operator varchar(64), type int, duration int, valid_from int, expires_after int, ingest_date int, sys_last_used int, sys_times_used int, sys_file_flags int);

create table Metadata_Col_Sort(col_name varchar(50), sortorder int, display_name varchar(100), display_on_report bit, operator varchar(50), edit bit, alt_col_name varchar(50), suppress bit, datevalue bit, optionlist varchar(100), allownewentry bit);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('filename', 1, 'File Name', 1, 'sys', 0, NULL, 0, 0, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('filepath', NULL, 'File Path', 0, 'sys', 0, NULL, 0, 0, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('linked_file', NULL, 'Linked File', 0, 'sys', 1, NULL, 0, 0, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('description', 2, 'Description', 0, 'sys', 1, NULL, 0, 0, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('operator', 3, 'Operator', 1, 'sys', 0, NULL, 0, 0, NULL, 0);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('type', 7, 'Type', 1, 'sys', 1, 'mod', 0, 0, '(cast(exchange.criterion as varchar) + '' - '' + cast(exchange.flag as varchar)) as type', 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('duration', 4, 'Duration', 0, 'sys', 1, NULL, 0, 0, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('valid_from', 8, 'Valid From', 1, 'sys', 1, NULL, 0, 1, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('expires_after', NULL, 'Expires After', 0, 'sys', 1, NULL, 0, 1, NULL, 1);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('ingest_date', 6, 'Ingest Date', 1, 'sys', 0, NULL, 0, 1, NULL, 0);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('sys_last_used', NULL, 'Last Used', 0, 'sys', 0, NULL, 0, 1, NULL, 0);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('sys_times_used', NULL, 'Times Used', 0, 'sys', 0, NULL, 0, 0, NULL, 0);

insert into Metadata_Col_Sort (col_name, sortorder, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry)
values ('sys_file_flags', NULL, 'File Flags', 0, 'sys', 0, NULL, 1, 0, NULL, 0);

create table Settings (ip varchar(15) NOT NULL, name varchar(64) NOT NULL, xml bit, enable bit);

insert into Settings (ip, name, xml, enable) values ('', 'Colossus Adaptor 1', 0, 1);

create table Users (username varchar(50), pwd varchar(64), admin bit, displayname varchar(50), default_module int);

insert into Users (username, pwd, admin, displayname, default_module) values ('sys', '+j%3`A%lJ|%@', 1, 'System', 3);