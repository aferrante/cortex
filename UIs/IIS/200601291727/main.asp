<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
call getdbvars()
Call ConnectToDBs()


set r = executeSQL("select * from Menu where id >= 1 and id <=3  order by id", 0)

%>
<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
<table align=center border=0 cellspacing=0 cellpadding=2>

<% if not r.eof then %>
		<tr valign=top>
			<td colspan=2><h2><nobr><%=PageTitleIndent%><%=r("label")%></nobr></h2>
			<p><%=r("fulldescription")%></p><br></td>
		</tr>
		<tr>
			<th class="tableheadercell">Available Interfaces</th>
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">Description</th>
		</tr>
	<%

	i = -1

	r.movenext

	do while not r.eof
	
		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		%>
		<tr class="<%=curclass%>" valign=center>
			<td class="tablecell"><a style="color: black; text-decoration: none;" href="<%=r("link")%>" title="<%="Link to " & r("label")%>"><%=r("description")%></a></td>
			<td class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr><%=r("fulldescription")%></td>
		</tr>
		<%
	i = i + 1
	r.movenext
	loop
end if
	%>
	<tr><td class="tablefooter" colspan=2>&nbsp;</td></tr>
</table>
</form>
</body>
</html>