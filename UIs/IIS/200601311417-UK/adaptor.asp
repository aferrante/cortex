<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>
<body>
<%
call getdbvars()
Call ConnectToDBs()
errormsg = ""

'DELETING - do not update other settings while deleting.
deletepos = instr(1, request.form, "delete_settings")

if deletepos > 0 then

	deletepos = deletepos + 15 'move to the end of the variable name
	deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the ip

	strsql = "delete from Settings where ip = '" & deletepos & "'"
	'response.write strsql
	call executeSQL(strsql, 1)

'ADD A SETTING
elseif instr(1, request.form, "add_setting") > 0 then

	set r = executeSQL("select * from Settings where ip = '" & request.form("ipNEW") & "'", 0)

	if not r.eof then
		errormsg = errormsg & "Settings for IP Address  " & request.form("ipNEW") & " already exists in the database.<br>"
	end if
	
	if len(request.form("ipNEW")) < 1 or validate(request.form("ipNEW"), "IP") = False then 
		errormsg = errormsg & "IP Address is not a valid format.<br>"
	end if
	
	if len(request.form("nameNEW")) < 1 then 
		errormsg = errormsg & "Name is a required field.<br>"
	end if 
	
	if len(errormsg) < 1 then 

		strsql = "insert into Settings (name, ip, xml, enable) values ('" & fixstring(request.form("nameNEW")) & "', '" & request.form("ipNEW") & "', " & iif(request.form("xmlNEW") = "on", 1, 0) & ", " & iif(request.form("enableNEW") = "on", 1, 0) & ")"

		'response.write strsql
		call executeSQL(strsql, 1)
	end if


'UPDATE ALL FILE EXTENSIONS
elseif request.form("update") = "Update" then

	idarray = split(request.form("ids"), ",")

		for i=0 to ubound(idarray) -1
			
			if len(request.form("name" & idarray(i))) < 1 then
					errormsg = errormsg & idarray & " - Name is a required field.<BR>"
			end if
			
		next
		
		if len(errormsg) < 1 then
		
			for i=0 to ubound(idarray) -1

				strsql = "update Settings set name = '" & fixstring(request.form("name" & idarray(i))) & "', xml = " & iif(request.form("xml" & idarray(i)) = "on", 1, 0) & ", enable = " & iif(request.form("enable" & idarray(i)) = "on", 1, 0) & " where ip = '" & idarray(i) & "'"

				'response.write strsql & "<BR><BR>"

				call executeSQL(strsql, 1)
			next
		end if
end if

'Determine Sort By

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "name"))


set r = executeSQL("select * from Settings order by " & sortby, 0)

%>

<form method=post action=<%=request.servervariables("SCRIPT_NAME")%> name=form onreset="resetStyles();">
<input type=hidden name=sortby value="<%=sortby%>">

<table border=0 cellspacing=0 cellpadding=2>
	<thead>
		<tr valign=top>
			<td colspan=2><h2><nobr><%=PageTitleIndent%>Settings</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
			<td align=right colspan=3>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				</nobr>
			</td>
		</tr>

		<tr class="tableheader">
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("name", sortby)%>" title="Sort By Name" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Name</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("ip", sortby)%>" title="Sort By IP Address" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">IP Address</a></th>

			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("xml", sortby)%>" title="Sort By XML" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">XML</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("enable", sortby)%>" title="Sort By Enable" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Enable</a></th>
			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			
		</tr>
	</thead>

	<tbody>


	<!--Begin Add New Setting-->

	<tr class="tablerowAddNew" valign=center>

		<td class="tablecell" align=center><input id="New" type=text name=nameNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("nameNEW"), "")%>" onchange="changedValue('nameNEW');"></td>

		<td class="tablecell"><input id="New" type=text name=ipNEW size=15 value="<%=iif(len(errormsg) > 0, request.form("ipNEW"), "")%>" onchange="changedValue('ipNEW');"></td>

		<td class="tablecell" align=center><input id="New" type=checkbox name=xmlNEW <%=iif(request.form("xmlNEW") = "on", "CHECKED", "")%>" onchange="changedValue('xmlNEW');"></td>
		
		<td class="tablecell" align=center><input id="New" type=checkbox name=enableNEW <%=iif(request.form("enableNEW") = "on", "CHECKED", "")%>" onchange="changedValue('enableNEW');"></td>
		
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_setting" value="Add New"  onclick="return confirm_on_click('add a new setting?  All other changes will be lost.');"></td>
	</tr>

	<!--End Add New Setting-->

	<%

ids = ""
i = 0

do while not r.eof
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>

		<td class="tablecell" align=center><input type=text name="<%="name" & r("ip")%>" size=30 value="<%=iif(len(errormsg) > 0, request.form("name" & r("ip")), r("name"))%>" onchange="changedValue('<%="name" & r("ip")%>');"></td>

		<td class="tablecell"><%=r("ip")%></td>

		<td class="tablecell" align=center><input type=checkbox name="<%="xml" & r("ip")%>" <%=iif((len(errormsg) > 0 and request.form("xml" & r("ip")) = "on") or r("xml") <> 0, "CHECKED", "")%> onchange="changedValue('<%="xml" & r("ip")%>');"></td>

		<td class="tablecell" align=center><input type=checkbox name="<%="enable" & r("ip")%>" <%=iif((len(errormsg) > 0 and request.form("enable" & r("ip")) = "on") or r("enable") <> 0, "CHECKED", "")%> onchange="changedValue('<%="enable" & r("ip")%>');"></td>

		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<input type=submit name="<%="delete_settings" & r("ip")%>" value=Delete onclick="return confirm_on_click('DELETE Settings for ' + '<%=r("ip")%>' + '?  All other changes will be lost.');">
			&nbsp;</nobr>
		</td>
	</tr>
	<%

	ids = ids & r("ip") & ","

i = i + 1
r.movenext
loop

%>
	<tr><td class="tablefooter" colspan=5>&nbsp;</td></tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td align=right colspan=3>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=ids value="<%=ids%>">
<form>
<%

call CloseDBs()

%>
</body>
</html>