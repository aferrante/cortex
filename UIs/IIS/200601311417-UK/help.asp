<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="help.css"-->

</head>

<body>
<%
call getdbvars()
%>

<a name=Top><h2><nobr><%=PageTitleIndent%>Help</nobr></h2></a>

<table border=0 cellpadding=2 cellspacing=0>
	<tr>
		<td colspan=3><a class=MainTopic href="#Cortex">Cortex</a></td>		
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Modules">Modules</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#User Admin">User Admin</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Omnibus">Omnibus</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Channels">Channels</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Settings">Settings</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Miranda">Miranda</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Metadata">Metadata</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#DiReCT">DiReCT</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#File Types">File Types</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Messages">Messages</a></td>
	</tr>	
	<tr>
		<td colspan=3><a class=MainTopic href="#Miscellaneous">Miscellaneous</a></td>
	</tr>	
	<tr>
		<td colspan=3><a class=MainTopic href="#Contacts">Contacts</a></td>
	</tr>	
</table>

<table width=100%>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Cortex">Cortex</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Modules">Modules</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="User Admin">User Admin</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Omnibus">Omnibus</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Channels">Channels</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Settings">Settings</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Miranda">Miranda</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Metadata">Metadata</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="DiReCT">DiReCT</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="File Types">File Types</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Messages">Messages</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Miscellaneous">Miscellaneous</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Contacts">Contacts</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
</table>

</body>
</html>