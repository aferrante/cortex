<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
call getdbvars()
Call ConnectToDBs()

pagenum = cint(iif(len(request.form("pagenum")) > 0, request.form("pagenum"), iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), 1)))

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), "messages.systime")

if request.form("refresh") = "Refresh" then
	pagenum = 1
	sortby = "messages.systime"
end if

'DELETING - do not update other file extensions while deleting.
deletepos = instr(1, request.form, "delete_msg")

if deletepos > 0 then

	deletepos = deletepos + 10 'move to the end of the variable name
	deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the file extension

	strsql = "update messages set flags = -1, deletedby = '" & fixstring(session("username")) & "', deletedon = '" & udate(Now()) & "' where id = '" & deletepos & "'"

	'response.write strsql
	db.execute(strsql)

end if

strsql = "select top " & pagenum * numperpage & " messages.id, messages.systime, messages.sender, messages.message, messages.flags, (select count(messages.id) as totalrecords from messages where flags <> -1) as totalrecords from messages where flags <> -1 order by " & sortby

'response.write strsql

set r = db.execute(strsql)

if not r.eof then

	dim filearray

	if not r.eof then
		totalrecords = r("totalrecords")
		filearray = r.getrows()
	else
		totalrecords = 0
		filearray = nullarray
	end if

	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=sortby value="<%=sortby%>">
	<input type=hidden name=pagenum value="<%=pagenum%>">
	<input id=datahaschanged type=hidden name=datahaschanged value="">

	<table border=0 cellspacing=0 cellpadding=2 width=100%>
		<thead>
			<tr valign=top>
				<td colspan=4><h2><nobr><%=PageTitleIndent%>Messages</nobr></h2></td>
			</tr>
			<tr>
				<td colspan=4><%=PrintPageNumbers(totalrecords, "&sortby=" & sortby, pagenum)%></td>
			</tr>
			<tr class="tableheader">
				<th class="tableheadercell">
					<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&sortby=<%=determinesortby("messages.systime", sortby)%>" title="Sort By Timestamp">Timestamp</a>
				</th>
				<th class="tableheadercell">
					<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&sortby=<%=determinesortby("messages.sender", sortby)%>" title="Sort By Source">Source</a>
				</th>
				<th class="tableheadercell">
					<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&sortby=<%=determinesortby("messages.message", sortby)%>" title="Sort By Message">Message</a>
				</th>
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');"></th>
			</tr>
		</thead>

		<tbody>
	<%

	pagestart = ((pagenum - 1) * numperpage)
	pageend = iif(ubound(filearray, 2) > (pagenum * numperpage), (pagenum * numperpage), ubound(filearray, 2))

	for i=pagestart to pageend

		if filearray(4,i) <> 0 then
			curstylecolor = "red"
		else
			curstylecolor = "black"
		end if

		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		%>
		<tr class="<%=curclass%>" style="color: <%=curstylecolor%>;" valign=top>
			<td class="tablecell"><nobr><%=unUdate(filearray(1,i))%></nobr></td>
			<td class="tablecell" align=left><%=filearray(2,i)%></td>
			<td class="tablecell" align=left><%=filearray(3,i)%></td>
			<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">
				<input type=submit name="<%="delete_msg" & filearray(0,i)%>" value=Delete onclick="return confirm_on_click('DELETE this message?');">
			</td>
		</tr>
		<%

	next

	%>
		<tr><td class="tablefooter" colspan=4>&nbsp;</td></tr>
	</table>
	</form>
	<%
else
	response.write "<h2><nobr>" & PageTitleIndent & "Messages</nobr></h2>"
	response.write "<center><h3>No Messages Exist</h3></center>"
end if
db.close
%>
</body>
</html>