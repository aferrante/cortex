<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
call getdbvars()
Call ConnectToDBs()


devicetable = iif(len(request.querystring("devicetable")) > 0, request.querystring("devicetable"), request.form("devicetable"))

refreshdata = iif(len(request.querystring("refreshdata")) > 0, request.querystring("refreshdata"), iif(len(request.form("refreshdata")) > 0, request.form("refreshdata"), 0))


'DELETING FILE
deletepos = instr(1, request.form, "delete_file")

if deletepos > 0 then

	deletepos = deletepos + 11 'move to the end of the variable name
	deletefileinfo = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the filename
	
	deletearray = split(URLDecode(deletefileinfo), ",")
	
	strsql = "insert into Queue (filename, [action], ip, partition, [timestamp], username) values ('" & fixstring(deletearray(0)) & "', 2, '" & mid(devicetable, 8) & "', '" & deletearray(1) & "', " & udate(Now()) & ", '" & session("username") & "')"
	
	call executeSQL(strsql, 1)

	refreshdata = 1
end if

'check to make sure there isn't a refresh in the queue, if so, display message.
	
testsql = "select * from Queue where ip = '" & mid(devicetable, 8) & "' and username = '" & session("username") & "' and [action] = 8"

set r = executeSQL(testsql, 0)

if not r.eof then 
	msg = "A file listing for this device has been requested.  The system is too busy to the complete the request at this time.  Refresh the page at a later time."
end if

if refreshdata = 1 then

	'REQUEST a refresh and wait
	
		strsql = "insert into Queue (filename, [action], ip, [timestamp], username) values (NULL, 8, '" & mid(devicetable, 8) & "', " & udate(Now()) & ", '" & session("username") & "')"

		'response.write strsql
		call executeSQL(strsql, 1)

	sleep = 1

	for i = 1 to SecondsToWait

		set r = executeSQL(testsql, 0)

		if r.eof then
			msg = ""
			exit for
		else
			msg = "A file listing for this device has been requested.  The system is too busy to the complete the request at this time.  Refresh the page at a later time."
		end if

		sql = "WAITFOR DELAY '00:00:" & right(clng(sleep),2) & "'" 
		call executeSQL(sql, 1)

	next
	
	refreshdata = 0
end if

pagenum = cint(iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), iif(len(request.form("pagenum")) > 0, request.form("pagenum"), 1)))

if request.form("applyfilter") = "Apply Filter" or deletepos > 0 then
	pagenum = 1
end if

devicetable = iif(len(request.querystring("devicetable")) > 0, request.querystring("devicetable"), request.form("devicetable"))

filterby = iif(len(request.querystring("filterby")) > 0, request.querystring("filterby"), iif(len(request.form("filterby")) > 0, request.form("filterby"), ""))

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "devicetable.filename"))


whereclause = " WHERE 1=1 "

if len(filterby) > 0 then
	whereclause = whereclause & " and devicetable.partition = '" & filterby & "' "
end if

strsql = "select top " & pagenum * numperpage & " devicetable.filename, devicetable.sys_last_used, devicetable.sys_times_used, devicetable.expires_after, devicetable.transfer_date, metadata.filename as metadataexists, (select count(devicetable.filename) as totalrecords from [" & devicetable & "] as devicetable " & whereclause & ") as totalrecords, devicetable.partition from [" & devicetable & "] as devicetable left join MetaData on devicetable.filename = MetaData.filename" & whereclause & "order by " & sortby


set r = executeSQL(strsql, 0)

dim filearray

if not r.eof then
	totalrecords = r("totalrecords")
	filearray = r.getrows()
else
	totalrecords = 0
	filearray = nullarray
	pageend = -1
end if

devicename = getdevicename(right(devicetable, len(devicetable) - 7))
%>
<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
<input id=datahaschanged type=hidden name=datahaschanged value="">
<input type=hidden name=refreshdata value="<%=refreshdata%>">
<input type=hidden name=pagenum value="<%=pagenum%>">
<input type=hidden name=sortby value="<%=sortby%>">
<input type=hidden name=devicetable value="<%=devicetable%>">

<table border=0 cellspacing=0 cellpadding=2 width=100%>
	<thead>
		<tr valign=top>
			<td colspan=6><h2><nobr><%=PageTitleIndent%>Files on <%=right(devicetable, len(devicetable) - 7) & iif(len(devicename) > 0, " - " & devicename, "")%>&nbsp;&nbsp;<a style="color: black; font-weight: bold; font-size: 12pt;" href="metadata.asp?devicetable=<%=mid(devicetable, 8)%>&defaultfilter=3">Transfer Files To This Device</a></h2><nobr> </td>
		</tr>
		<% if len(msg) > 0 then %>
				<tr valign=top>
					<td colspan=6><font color=red><b><%=msg%></b></font></td>
				</tr>
		<% end if%>
		<tr>
			<td colspan=6><%=PrintPageNumbers(totalrecords, "&filterby=" & filterby & "&devicetable=" & devicetable & "&sortby=" & sortby, pagenum)%></td>
		</tr>
		<tr class="tableheader">
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.filename", sortby)%>" title="Sort By File Name">File Name</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.partition", sortby)%>" title="Sort By Default Partition">Default Partition</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.sys_last_used", sortby)%>" title="Sort By Last Used">Last Used</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.sys_times_used", sortby)%>" title="Sort By Number of Uses">Number of Uses</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.expires_after", sortby)%>" title="Sort By Expiration Date">Expiration Date</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&filterby=<%=filterby%>&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.transfer_date", sortby)%>" title="Sort By Transfer Date">Transfer Date</a>
			</th>
			<th class="tableheadercell">Details</th>			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
	
<!--Generate Filter Form-->
	<tr class="tablerowSetAll" valign=center>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center>
			<select name=filterby size=1 smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();"> 
				<option value="">All</option>
				<%=OptionListPartitions(filterby)%>
			</select>
		</td>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center>&nbsp;</td>
		<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name=applyfilter value="Apply Filter"></td>
	</tr>
<%

pagestart = ((pagenum - 1) * numperpage)

if pageend <> -1 then 
	pageend = iif(ubound(filearray, 2) > (pagenum * numperpage), (pagenum * numperpage), ubound(filearray, 2))

	for i=pagestart to pageend


		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		%>
		<tr class="<%=curclass%>" valign=center>
			<td class="tablecell"><%=filearray(0,i)%></td>
			<td class="tablecell"><%=filearray(7,i)%>&nbsp;</td>
			<td class="tablecell" align=right><nobr>&nbsp;<%=unUdate(filearray(1,i))%></nobr></td>
			<td class="tablecell" align=center><nobr>&nbsp;<%=filearray(2,i)%></nobr></td>
			<td class="tablecell" align=right><nobr>&nbsp;<%=unUdate(filearray(3,i))%></nobr></td>
			<td class="tablecell" align=right><nobr>&nbsp;<%=unUdate(filearray(4,i))%></nobr></td>
			<td class="tablecell" align=center><%=iif(len(filearray(5,i)) > 0, "<a style=""color: black;"" href=""metadata.asp?filename=" & filearray(0,i) & " "">View</a>", "&nbsp;")%></td>
			<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<input type=submit name="<%="delete_file" & filearray(0,i) & "," & filearray(7,i)%>" value="Delete File" onclick="return confirm_on_click('DELETE the file ' + '<%=filearray(0,i)%>' + ' from this device?');">
			&nbsp;</nobr></td>

		</tr>
		<%

	next

	%>
		<tr><td class="tablefooter" colspan=8>&nbsp;</td></tr>
	</table>
	</form>
	<%
else
	%>
		<tr><td class="tablefooter" colspan=8>&nbsp;</td></tr>
	</table>
	<center><h3>No Files Found</h3></center>
	</form>
	<%
end if
call CloseDBs()
%>
</body>
</html>