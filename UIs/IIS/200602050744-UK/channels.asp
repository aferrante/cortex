<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->
</head>
<body>
<%
call getdbvars()
Call ConnectToDBs()

errormsg = ""

'REFRESH - clicking refresh does nothing other than requery the database.

'DELETING - do not update other channels while deleting.
deletepos = instr(1, request.form, "deletechannel")

if deletepos > 0 then

	deletepos = deletepos + 13 'move to the end of the variable name
	deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the channel number

	strsql = "delete from channels where id = " & deletepos
	'response.write strsql
	call executeSQL(strsql, 1)

'ADD A CHANNEL
elseif instr(1, request.form, "addchannel") > 0 then

	set r = executeSQL("select * from Channels where id = " & request.form("idNEW"), 0)

	if not r.eof then
		errormsg = "<h3><font color=red>ERROR: Omnibus Channel Number " & request.form("idNEW") & " already exists in the database.</font></h3><br>"


	else
		strsql = "insert into channels (id, flags, description) values (" & request.form("idNEW") & ", " & clng( "&H" & (iif(request.form("activeNEW") = "on", 1, 0) + cint(request.form("videostandardNEW")))) & ", '" & fixstring(request.form("descriptionNEW")) & "')"

		'response.write strsql
		call executeSQL(strsql, 1)
	end if

'SET ALL CHANNELS
elseif instr(1, request.form, "setallchannels") > 0 then

	channelarray = split(request.form("channelids"), ",")

	for i=0 to ubound(channelarray) -1
		curchannel = channelarray(i)

		strsql = "update channels set flags = " & clng( "&H" & (iif(request.form("activeALL") = "on", 1, 0) + cint(request.form("videostandard" & curchannel)))) & " where id = " & curchannel

		'response.write strsql & "<BR><BR>"

		call executeSQL(strsql, 1)
	next

'UPDATE ALL CHANNELS
elseif request.form("update") = "Update" then

	channelarray = split(request.form("channelids"), ",")

		for i=0 to ubound(channelarray) -1
			curchannel = channelarray(i)

			strsql = "update channels set flags = " & clng( "&H" & (iif(request.form("active" & curchannel) = "on", 1, 0) + cint(request.form("videostandard" & curchannel)))) & ", description = '" & fixstring(request.form("description" & curchannel)) & "' where id = " & curchannel

			'response.write strsql & "<BR><BR>"

			call executeSQL(strsql, 1)
		next

end if


'Determine Sort Order
sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "id"))


set r = executeSQL("select * from Channels order by " & iif(sortby = "channel.flags", right(sortby, 1), sortby), 0)

%>

<form name=form method=post action=<%=request.servervariables("SCRIPT_NAME")%> onreset="resetStyles();">
<input type=hidden name=sortby value="<%=sortby%>">
<input id=datahaschanged type=hidden name=datahaschanged value="">

<table border=0 cellspacing=0 cellpadding=2>
	<thead>
		<tr valign=top>
			<td colspan=3><h2><nobr><%=PageTitleIndent%>Omnibus Channel Properties</nobr></h2><%=errormsg%></td>
			<td align=right colspan=2>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				</nobr>
			</td>
		</tr>

		<tr class="tableheader">
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("channels.id", sortby)%>" title="Sort By Omnibus Channel Number" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Omnibus<br>Channel<br>Number</a></th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("channels.flags", sortby)%>" title="Sort By Asset Management Active" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Asset<br>Management<br>Active</a></th>
			
			<th class="tableheadercell">Video Standard</th>
			
			<th class="tableheadercell"><a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?sortby=<%=determinesortby("channels.description", sortby)%>" title="Sort By Description" onclick="return confirm_on_click('sort the table?  All changes will be lost.');">Description</a></th>
			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
		</tr>
	</thead>

	<tbody>

	<!--Begin Set All Channels-->

		<tr class="tablerowSetAll" valign=center>
			<td class="tablecell" align=center><b>All</b></td>
			<td class="tablecell" align=center><input type=checkbox name="activeALL"></td>
			<td class="tablecell" align=center>&nbsp;</td>
			<td class="tablecell" align=center>Use these setting for all channels.</td>
			<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="setallchannels" value="Set All" onclick="return confirm_on_click('use these settings for all channels?  All other changes will be lost.');"></td>
		</tr>

	<!--End Set All Channels-->


	<!--Begin Add New Channel-->

	<tr class="tablerowAddNew" valign=center>
		<td class="tablecell" align=center><input id="New" type=text name=idNEW size=5 value="<%=iif(len(errormsg) > 0, request.form("idNEW"), "")%>" onchange="changedValue('New');"></td>
		<td class="tablecell" align=center><input type=checkbox name="activeNEW" <%=iif(len(errormsg) > 0 and request.form("activeNEW") = "on", "CHECKED", "")%> onchange="changedValue('activeNEW');"></td>
		<td class="tablecell">
			<select name="videostandardNEW" size=1 onblur="changedValue('videostandardNEW');" smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();">
				<%=OptionListVideoStandards(iif(len(errormsg) > 0, request.form("videostandardNEW")+ 0, 0), 0)%>
			</select>
		</td>
		<td class="tablecell"><input type=text name="descriptionNEW" size=30 value="<%=iif(len(errormsg) > 0, request.form("descriptionNEW"), "")%>" onchange="changedValue('descriptionNEW');"></td>
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="addchannel" value="Add New" onclick="return verify_new_channel();"></td>
	</tr>

	<!--End Add New Channel-->

	<%

channelids = ""
i = 0

do while not r.eof
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	channelstatus = cint(r("flags")) mod 2
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell" align=center><%=r("id")%></td>
		<td class="tablecell" align=center><input type=checkbox name="<%="active" & r("id")%>" <%=iif(channelstatus = 1, "CHECKED", "")%> onchange="changedValue('<%="active" & r("id")%>');"></td>
		<td class="tablecell">
			<select name="<%="videostandard" & r("id")%>" size=1 onblur="changedValue('<%="videostandard" & r("id")%>');" smk_keystrokes="" smk_lastpresstime="" onkeypress="SMK_KeyPress();">
				<%=OptionListVideoStandards(r("flags"), 1)%>
			</select>
		</td>
		<td class="tablecell"><input type=text name="<%="description" & r("id")%>" size=30 value="<%=r("description")%>"  onchange="changedValue('<%="description" & r("id")%>');"></td>
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<% if channelstatus = 0 then %>
				<input type=submit name="<%="deletechannel" & r("id")%>" value=Delete onclick="return confirm_on_click('DELETE Omnibus Channel Number ' + <%=r("id")%> + '?  All other changes will be lost.');">
			<% end if %>
			&nbsp;</nobr>
		</td>
	</tr>
	<%

	channelids = channelids & r("id") & ","

i = i + 1
r.movenext
loop

%>
	<tr><td class="tablefooter" colspan=5>&nbsp;</td></tr>
	<tr>
		<td colspan=3>&nbsp;</td>
		<td align=right colspan=2>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=channelids value="<%=channelids%>">
</form>
<%

call CloseDBs()

%>
</body>
</html>