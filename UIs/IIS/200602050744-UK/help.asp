<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="help.css"-->

</head>

<body>
<%
call getdbvars()
%>

<a name=Top><h2><nobr><%=PageTitleIndent%>Help</nobr></h2></a>

<table border=0 cellpadding=2 cellspacing=0>
	<tr>
		<td colspan=3><a class=MainTopic href="#Cortex">Cortex</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Modules">Modules</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#User Admin">User Admin</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Omnibus">Omnibus</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Channels">Channels</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Settings">Settings</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Miranda">Miranda</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Metadata">Metadata</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#DiReCT">DiReCT</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#File Types">File Types</a></td>
	</tr>
	<tr>
		<td><nobr>&nbsp;&nbsp;</nobr></td>
		<td colspan=2><a class=SubTopic1 href="#Messages">Messages</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Miscellaneous">Miscellaneous</a></td>
	</tr>
	<tr>
		<td colspan=3><a class=MainTopic href="#Contacts">Contacts</a></td>
	</tr>
</table>

<table width=100%>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Cortex">Cortex</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>
				The Cortex Main page provides links and descriptions of the associated modules.
			</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Modules">Modules</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>
				The Modules page provides links and descriptions of all of the modules installed in the application.  Each user can choose their default module by clicking that module's "Set As Default" button.  This will be the module that is displayed after the user successfully logs into the application.  For new users, the default module will be the Modules page.
			</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="User Admin">User Admin</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>
				The User Admin page allows the user to:
				<ul>
					<li><b>Log Out: </b> To log out click the "LOG OUT" button.  The user will then be redirected to the Login Page.  The user will also be logged out automatically when there is no activity in the application for over 20 minutes.</li>

					<li><b>Manage Current Account: </b> Click the "My Account" button to manage the account that is currently logged into the application.  From here the user can change the account's display name and password.</li>

					<li><b>Manage All Accounts: </b> If the logged in user is an administrator of this application, then the "All Accounts" button will be displayed.  Click on this button to:
					<ul>
						<li><b>Add a New User: </b> Fill out the first row in the table and click the "Add Account" button.  The username is required to be unique for this application.</li>
						<li><b>Update Display Names and Administrative Rights: </b>  Make the desired changes to the information in the table and click the "Update" button.  Once a username is in the system it cannot be changed.</li>
						<li><b>Reset Account Password: </b> Enter the account's new password in to the the associated Password textbox and click the "Set" button to the right.</li>
						<li><b>Delete An Account: </b> Click the account's "Delete Account" button to delete an account.  The user will not be able to delete the account which is currently logged in.</li>
					</ul>
					</li>
				</ul>

				<p>Changes made to data in the table will be in a <font color=blue>blue</font> font.  Once the changes have been committed to the database the font will be black.</p>

			</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Omnibus">Omnibus</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>The Omnibus Main page provides links and descriptions of the associated modules.</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Channels">Channels</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>The Omnibus Channel Properties page allows the user to:
				<ul>
					<li><b>Set All Channels: </b> Check the box in the first row of the table (denoted with a blue background) and click the "Set All" button to set all channels to active.  Uncheck the box and click the "Set All" button to set all channels to inactive. If the user has not updated changes made to data in the table, then those changes will be lost by clicking this button.</li>

					<li><b>Add a New Channel: </b> Data will be entered in the second row of the table (denoted with a green background), then the user will click the "Add New" button in the rightmost column to add the channel to the database.    If the user has not updated changes made to data for other channels, then those changes will be lost by clicking this button. Channel numbers have to be unique and a numerical value.</li>

					<li><b>Modifying Existing Channel Properties: </b> The user can modify the Active Status, Video Standard, and Description.  Click the "Update" button to commit the changes to the database.</li>

					<li><b>Deleting a Channel: </b> Click the "Delete" button to delete a channel.  The delete option is only available for channels that are not active.  To delete an active channel, update the channel's active property to inactive, then delete.</li>
				</ul>

				<p>Table column headings that are underlined, allow the user to sort the table by that column.  Click the same column name again to sort the table in a descending order. If the user has not updated changes made to data in the table, then those changes will be lost by resorting the data.</p>

				<p>Changes made to data in the table will be in a <font color=blue>blue</font> font.  Once the changes have been committed to the database the font will be black.</p>
				
				<p>Switching to a Backup Channel, set primary channel to inactive, set the Backup channel to active, In Mirana Devices change the channel number on the effected devices to the back up channel number and update.</p>
				
			</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Settings">Settings</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Miranda">Miranda</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Metadata">Metadata</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="DiReCT">DiReCT</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="File Types">File Types</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Messages">Messages</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Miscellaneous">Miscellaneous</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
	<tr>
		<td><a class=MainTopic name="Contacts">Contacts</a></td>
		<td align=right><nobr><a class=Top href="#Top">Return To Top</a></nobr></td>
	</tr>
	<tr>
		<td colspan=2>
			<blockquote>Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help Help </blockquote>
		</td>
	</tr>
	<tr>
		<td colspan=2><hr width=100%></td>
	</tr>
</table>

</body>
</html>