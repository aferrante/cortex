<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
set db = CreateObject("ADODB.Connection")
call getdbvars()
db.open session("dsn"), session("dbusername"), session("dbpassword")
errormsg = ""

				

if (request.form("ManageCols") = "Manage Fields" or request.form("mode") = "ManageCols") and request.form("return") <> "Return To Metadata" then

	'ADD A COLUMN NAME
	if instr(1, request.form, "add_col") > 0 then
	
		if len(request.form("sortorderNEW")) > 0 and isnumeric(request.form("sortorderNEW")) = False then
			errormsg = errormsg & "- Column Order is not a numeric value.<br>"
		end if
		
		
		if len(request.form("colnameNEW")) < 1 then
			errormsg = errormsg & "- Column Name is a required field.<br>"
		else
		
			if validate(request.form("colnameNEW"), "DBColName") = False then
				errormsg = errormsg & "- Column Name is invalid.<br>"
			else
			
				colnameNEW = replace(request.form("colnameNEW"), " ", "_")
				
				strsql = "Select COLUMN_NAME From Information_Schema.Columns Where Table_Name = 'Metadata' and Column_Name = '" & colnameNEW & "'"
				
				'response.write strsql

				set r = db.execute(strsql)
				if not r.eof then
					errormsg = errormsg & "- This Column Name already exists in the Metadata table.<br>"
				end if
			end if
		end if

		if len(request.form("displaynameNEW")) < 1 then
			errormsg = errormsg & "- Display Name is a required field.<br>"
		end if
	
		if len(errormsg) < 1 then 

			strsql = "Alter table Metadata Add " & colnameNEW & " varchar(256)"

			'response.write strsql & "<BR><BR>"
			db.execute(strsql)

			strsql = "insert into Metadata_Col_Sort (sortorder, col_name, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry) values (" & iif(len(request.form("sortorderNEW")) > 0, request.form("sortorderNEW"), "NULL") & ", '" & colnameNEW & "', '" & fixstring(request.form("displaynameNEW")) & "', " & iif(request.form("displayNEW") = "on", 1, 0) & ", '" & session("username") & "', 1, '', 0, 0, '', 1)"
			
			'response.write strsql
			db.execute(strsql)

		end if
	end if

	'DELETE A COLUMN NAME
	if instr(1, request.form, "delete_col") > 0 then
	
		deletepos = instr(1, request.form, "delete_col") + 10 'move to the end of the variable name
		deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the column name
	
		db.execute("delete from Metadata_Col_Sort where col_name = '" & deletepos & "'")
		db.execute("alter table Metadata drop column " & deletepos)


	end if
	
	'UPDATE ALL COLUMNS
	if request.form("update") = "Update" then
	
		idarray = split(request.form("ids"), ",")
	
		for i=0 to ubound(idarray) -1
		
			if len(request.form("sortorder-" & idarray(i))) > 0 and isnumeric(request.form("sortorder-" & idarray(i))) = False then
				errormsg = errormsg & "Line " & i + 1 & " - Column Order is not a numeric value.<br>"
			end if

			if request.form("display-" & idarray(i)) <> "on" and idarray(i) = "filename" then
				errormsg = errormsg & "Line " & i + 1 & " - File Name is required to be displayed on the report.<br>"
			end if
				
			if len(request.form("display_name-" & idarray(i))) < 1 then
				errormsg = errormsg & "Line " & i + 1 & " - Display Name is a required field.<br>"
			end if
		
		next 
		
		if len(errormsg) < 1 then 
			for i=0 to ubound(idarray) -1	
				
				strsql = "update Metadata_Col_Sort set sortorder = " & iif(len(request.form("sortorder-" & idarray(i))) > 0, request.form("sortorder-" & idarray(i)), "NULL") & ", display_name = '" & fixstring(request.form("display_name-" & idarray(i))) & "', display_on_report = " & iif(request.form("display-" & idarray(i)) = "on" or idarray(i) = "filename", 1, 0) & " where col_name = '" & idarray(i) & "'"
	
				'response.write strsql & "<BR><BR>"
				db.execute(strsql)
			next
		end if
	end if

	
	'DISPLAY FORM
	
	
	strsql = "select Metadata_Col_Sort.sortorder, Metadata_Info.Column_Name, Metadata_Col_Sort.display_name, Metadata_Col_Sort.display_on_report, Metadata_Col_Sort.Operator from (Select COLUMN_NAME From Information_Schema.Columns Where Table_Name = 'Metadata') as Metadata_Info left join Metadata_Col_Sort on Metadata_Info.Column_Name = Metadata_Col_Sort.col_name order by sortorder, col_name"
	
	'response.write strsql
	set r = db.execute(strsql)
	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=mode value="ManageCols">
	<table border=0 cellspacing=0 cellpadding=2 width=100%>
	
		<thead>
		<tr valign=top>
			<td colspan=2><h2><nobr><%=PageTitleIndent%>Metadata Columns</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
			<td align=right colspan=3>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				<input type=submit name="return" value="Return To Metadata">
				</nobr>				
				
			</td>
		</tr>

			<tr class="tableheader">
				<th class="tableheadercell">Column Order</th>
				<th class="tableheadercell">Display On Report</th>	
				<th class="tableheadercell">Column Name</th>
				<th class="tableheadercell">Display Name</th>				
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			</tr>
		</thead>

	<tbody>

		<!--Begin Add New Column-->

		<tr class="tablerowAddNew" valign=center>
			<td class="tablecell" align=center>
				<input type=text name=sortorderNew size=2 value="<%=iif(len(errormsg) > 0, request.form("sortorderNew"), "")%>">
			</td>
			<td class="tablecell" align=center>
				<input type=checkbox name=displayNEW <%=iif(len(errormsg) > 0 and request.form("displayNEW") = "on", "CHECKED", "")%>>
			</td>	
			<td class="tablecell">
				<input type=text name=colnameNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("colnameNEW"), "")%>">
			</td>
			<td class="tablecell">
				<input type=text name=displaynameNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("displaynameNEW"), "")%>">
			</td>		
			<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_col" value="Add New"  onclick="return confirm_on_click('add a new column name?  All other changes will be lost.');"></td>
		</tr>

		<!--End Add New Column-->
	
	<%
	ids = ""
	i = 0
	do while not r.eof 
		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		if len(errormsg) > 0 then
			sortorder = request.form("sortorder-" & r("column_name"))
			display_name = request.form("display_name-" & r("column_name"))
			display = iif(request.form("display-" & r("column_name")) = "on", 1, 0)
		else
			sortorder = r("sortorder")
			display_name = r("display_name")
			display = r("display_on_report")
		end if
		%>
		<tr class="<%=curclass%>" valign=center>
			<td class="tablecell" align=center>
				<input type=text name="<%="sortorder-" & r("column_name")%>" size=2 value="<%=sortorder%>">
			</td>
			<td class="tablecell" align=center>
				<input type=checkbox name="<%="display-" & r("column_name")%>" <%=iif(display <> 0, "CHECKED", "")%>>
			</td>	
			<td class="tablecell">
				<%=r("column_name")%>
			</td>
			<td class="tablecell">
				<input type=text name="<%="display_name-" & r("column_name")%>" size=30 value="<%=display_name%>">
			</td>	
			<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(r("operator") & "" <> "sys", "<input type=submit name=""delete_col" & r("column_name") & """ value=Delete  onclick=""return confirm_on_click('delete this column and all associated data?  All other changes will be lost.');"">", "&nbsp;")%></td>
		</tr>
		<%
		ids = ids & r("column_name") & ","
		i = i + 1
		r.movenext
	loop

%>
	<tr><td class="tablefooter" colspan=5>&nbsp;</td></tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td align=right colspan=3>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			<input type=submit name="return" value="Return To Metadata">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=ids value="<%=ids%>">

</form>
<%

elseif (request.form("mode") = "ViewDetails" or request.querystring("mode") = "ViewDetails" or request.form("ViewDetails") = "Create New Record") and request.form("return") <> "Return To Metadata" then

	'Save setting from Metadata main page
	sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), request.form("sortby"))

	pagenum = iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), request.form("pagenum"))

	filtervars = iif(len(request.querystring("filtervars")) > 0, request.querystring("filtervars"), request.form("filtervars"))

	defaultfilter = iif(len(request.querystring("defaultfilter")) > 0, request.querystring("defaultfilter"), request.form("defaultfilter"))

	'Determine vars for this detail page	
	filename = iif(len(request.querystring("filename")) > 0, request.querystring("filename"), request.form("filename"))
	
	createoredit = iif(len(request.form("createoredit")) > 0, request.form("createoredit"), iif(len(filename) > 0, "edit", "create"))
	
	set f = db.execute("Select COLUMN_NAME, data_type, Metadata_Col_Sort.display_name, NULL as datavalue, Metadata_Col_Sort.edit, Metadata_Col_Sort.alt_col_name, Metadata_Col_Sort.suppress, Metadata_Col_Sort.datevalue, Metadata_Col_Sort.optionlist, Metadata_Col_Sort.allownewentry From Information_Schema.Columns as MetadataCols left join Metadata_Col_Sort on MetaDataCols.Column_Name  = Metadata_Col_Sort.col_name Where Table_Name = 'Metadata'")
		
	if not f.eof then
		fieldarray = f.getrows()
	end if
	
	if request.form("delete") = "Delete" and len(filename) > 0 then
	
		strsql = "delete from MetaData where filename = '" & fixstring(filename) & "'"
		
		db.execute(strsql)
		
	end if
	
		
	
	if request.form("update") = "Update" or request.form("update") = "Create" then
		errormsg = ""
		'Check for errors

		for i=0 to ubound(fieldarray, 2)

			 if fieldarray(1,i) = "int" and fieldarray(4, i) <> 0 then 'editable int
			 
			 	if fieldarray(0,i) = "duration" then
			 	
			 		if len(request.form(fieldarray(0,i))) > 0 and validate(request.form(fieldarray(0,i)), "Duration") = False then
			 			errormsg = errormsg & fieldarray(2,i) & " is not a valid format (HH:MM:SS.FF).<br>"
			 		end if
			 	
			 	else
			 	
					if len(request.form(fieldarray(0,i))) > 0 and isnumeric(request.form(fieldarray(0,i))) = False and fieldarray(7,i) = 0 then 'this is a number field
						errormsg = errormsg & fieldarray(2,i) & " is not a valid number.<br>"
					end if

					if len(request.form(fieldarray(0,i))) > 0 and fieldarray(7,i) <> 0 and (isdate(request.form(fieldarray(0,i))) = False or validate(request.form(fieldarray(0,i)), datepattern) = False) then 'this is a date field
						errormsg = errormsg & fieldarray(2,i) & " is not a valid date.<br>"
					end if

					if fieldarray(0,i) = "type" then
						if request.form(fieldarray(0,i)) = 0 then
							errormsg = errormsg & fieldarray(2,i) & " cannot be Unknown.<br>"
						end if
					end if
			 	
			 	end if
			 	
			end if
			
			if (createoredit = "create") and (fieldarray(4, i) = 0) and (fieldarray(9, i) <> 0) then 'new record that allows new entry but does not allow to edit is a required field.
				if len(request.form(fieldarray(0,i))) < 1 then
					errormsg = errormsg & fieldarray(2,i) & " is a required field.<br>"
				end if
			end if
		next
		
		if len(errormsg) < 1 then 
			if request.form("update") = "Update" then
			
				sqlclause = ""
				
				for i=0 to ubound(fieldarray, 2)
					if fieldarray(4, i) <> 0 then 'editable	
					
						if fieldarray(0,i) = "duration" then
							if len(request.form(fieldarray(0,i))) > 0 then 
								sqlclause = sqlclause & fieldarray(0,i) & " = " & HMSFtoMS(request.form(fieldarray(0,i)), framerate) & ", "
							else
								sqlclause = sqlclause & fieldarray(0,i) & " = NULL, "
							end if
						else
					
							if fieldarray(1,i) = "int" and fieldarray(7,i) = 0 then 'number
								sqlclause = sqlclause & fieldarray(0,i) & " = " & iif(len(request.form(fieldarray(0,i))) > 0, request.form(fieldarray(0,i)), "NULL") & ", "
							elseif fieldarray(1,i) = "int" and fieldarray(7,i) <> 0 then 'date

								sqlclause = sqlclause & fieldarray(0,i) & " = " & iif(len(request.form(fieldarray(0,i))) > 0, udate(request.form(fieldarray(0,i))), "NULL") & ", "
							else 'string
								sqlclause = sqlclause & fieldarray(0,i) & " = '" & fixstring(request.form(fieldarray(0,i))) & "', "
							end if			
						end if
				 	end if	
				 next	
				 	sqlclause = left(sqlclause, len(sqlclause) - 2)
				
			
				strsql = "update metadata set " & sqlclause & " where filename = '" & filename & "'"
				'response.write strsql
				db.execute(strsql)
			else
			
				sqlclausefields = ""
				sqlclausevalues = ""

				for i=0 to ubound(fieldarray, 2)
					if fieldarray(9, i) <> 0 then 'editable		

						sqlclausefields = sqlclausefields & fieldarray(0,i) & ", "

						if fieldarray(0,i) = "duration" then
							if len(request.form(fieldarray(0,i))) > 0 then 
								sqlclausevalues = sqlclausevalues & HMSFtoMS(request.form(fieldarray(0,i)), framerate) & ", "
							else
								sqlclausevalues = sqlclausevalues & "NULL, "
							end if
						else

							if fieldarray(1,i) = "int" and fieldarray(7,i) = 0 then 'number
								sqlclausevalues = sqlclausevalues & iif(len(request.form(fieldarray(0,i))) > 0, request.form(fieldarray(0,i)), "NULL") & ", "
							elseif fieldarray(1,i) = "int" and fieldarray(7,i) <> 0 then 'date
								sqlclausevalues = sqlclausevalues & iif(len(request.form(fieldarray(0,i))) > 0, udate(request.form(fieldarray(0,i))), "NULL") & ", "
							else 'string
								sqlclausevalues = sqlclausevalues & "'" & fixstring(request.form(fieldarray(0,i))) & "', "
							end if		
						end if
					else
						if (fieldarray(0,i) = "operator") then
							sqlclausefields = sqlclausefields & fieldarray(0,i) & ", "
							sqlclausevalues = sqlclausevalues & "'" & Session("username") & "', "
						end if
					end if	
				 next	

				sqlclausefields = left(sqlclausefields, len(sqlclausefields) - 2)
				sqlclausevalues = left(sqlclausevalues, len(sqlclausevalues) - 2)

				strsql = "insert into metadata (" & sqlclausefields & ") values (" & sqlclausevalues & ")"
	
				db.execute(strsql)
				createoredit = "edit"
			
				
			end if
		end if
	end if
	
	set r = db.execute("select * from metadata left join exchange on metadata.type = exchange.mod where filename = '" & filename & "'")
	
	if not r.eof then
		if (not r.eof) and (len(errormsg) < 1) and (createoredit <> "create") then

			for i=0 to ubound(fieldarray, 2)
				 if len(fieldarray(5,i)) > 0 then
					fieldarray(3, i) = r(fieldarray(5,i))
				 else
					fieldarray(3, i) =r(fieldarray(0,i))
				 end if
			next
		else
			for i=0 to ubound(fieldarray, 2)
				if ((createoredit = "edit")  and (fieldarray(4, i) = 0)) or ((createoredit = "create") and (fieldarray(9, i) = 0)) and (not r.eof) then 
					if len(fieldarray(5,i)) > 0 then  'alternative field name
						fieldarray(3, i) = r(fieldarray(5,i))
					else
						fieldarray(3, i) = r(fieldarray(0,i))
					end if
				else
					fieldarray(3, i) = request.form(fieldarray(0,i))
				end if

				if (createoredit = "create") and (fieldarray(0,i) = "operator") then
					fieldarray(3,i) = Session("username")
				end if
			next
		end if
	else
		'record was deleted
		createoredit = "deleted"
	end if
		%>
		<h2><nobr><%=PageTitleIndent%><%=iif(createoredit = "edit", "Complete Metadata Information for " & filename, iif(createoredit = "deleted", "Record Deleted", "Create Metadata Record"))%></nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%>

	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=createoredit value="<%=createoredit%>">
	<input type=hidden name=sortby value="<%=sortby%>">
	<input type=hidden name=pagenum value="<%=pagenum%>">
	<input type=hidden name=filtervars value="<%=filtervars%>">
	<input type=hidden name=mode value="ViewDetails">
	<input type=hidden name=defaultfilter value="<%=defaultfilter%>">
	
	<% if createoredit = "edit" then %>
		<input type=hidden name=filename value="<%=filename%>">
	<% end if 
	
	if not r.eof then 
		%>
		<table border=1 cellspacing=0 cellpadding=2 align=center>
		<%

		j = 0
		for i=0 to ubound(fieldarray, 2)
			curclass = iif(j mod 2 = 0, "tablerow1", "tablerow2")

			if fieldarray(6, i) = 0 then 'show fields that are not suppressed
				%>
				<tr class="<%=curclass%>" valign=center>
					<td><b><%=fieldarray(2,i)%>: </b></td>
				<%

				if ((createoredit = "edit") and (fieldarray(4, i) <> 0)) or ((createoredit = "create") and (fieldarray(9, i) <> 0)) then 'can be edited or created?

					'type should be a drop down boxes on exchange
					if len(fieldarray(8,i)) > 0 then 'special case for type dropdown box
						%>
						<td>
							<select name="<%=fieldarray(0,i)%>" size=1>
								<%=OptionListFileExtensions(fieldarray(3,i) + 0)%>
							</select>
						</td>
						<%
					elseif fieldarray(0,i) = "duration" and len(errormsg) < 1 then

						fieldvalue = MStoHMSF(fieldarray(3,i), framerate)
						%>
						<td><input type=text size=50 name="<%=fieldarray(0,i)%>" value="<%=fieldvalue%>"></td>
						<%
					else
						if fieldarray(7,i) <> 0 and len(errormsg) < 1 then
							fieldvalue = unUDate(fieldarray(3,i))
						else
							fieldvalue = fieldarray(3,i)
						end if 
						%>
						<td><input type=text size=50 name="<%=fieldarray(0,i)%>" value="<%=fieldvalue%>"></td>
						<%
					end if
				else
					response.write "<td>"

					if fieldarray(7,i) <> 0 then
						response.write unUDate(fieldarray(3,i))
					else
						response.write fieldarray(3,i)
					end if
					response.write "&nbsp;</td>"

				end if

				response.write "</tr>"
				j = j + 1
			end if

		next

		%>
		</table>
		<br>
		<center><nobr>
		<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
		<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
		<input type=submit name=update value="<%=iif(createoredit = "edit", "Update", "Create")%>" onclick="return confirm_on_click('<%=iif(createoredit = "edit", "update", "create")%> this record?');">
		<input type=submit name=delete value="Delete" onclick="return confirm_on_click('delete this record?');">
		</nobr>
		</center>
	<% end if %> 
		<br><br>
		<center><input align=center type=submit name="return" value="Return To Metadata"></center>
		
		</form>
	<%
else

	'find report column names
	
	set f = db.execute("Select COLUMN_NAME, data_type, Metadata_Col_Sort.display_name, Metadata_Col_Sort.alt_col_name, Metadata_Col_Sort.suppress, Metadata_Col_Sort.datevalue, Metadata_Col_Sort.optionlist, NULL as filterval1, NULL as filterval2 From Information_Schema.Columns as MetadataCols left join Metadata_Col_Sort on MetaDataCols.Column_Name  = Metadata_Col_Sort.col_name Where Table_Name = 'Metadata' and Metadata_Col_Sort.display_on_report <> 0 order by metadata_col_sort.sortorder, metadata_col_sort.col_name")
			
	if not f.eof then
		fieldarray = f.getrows()
	end if

	'Build the Select and From Clause
	selectclause = ""
	fromclause = ""
	
	filenamepos = 0
					
	if len(request.form("removefilter")) > 0 or len(request.form("defaultfilterbutton")) > 0 then
		filtervars = ""
	else
		if len(request.querystring("filtervars")) > 0 then
			filtervars = request.querystring("filtervars")
		else
			filtervars = request.form("filtervars")
		end if
	end if	
	
	for i=0 to ubound(fieldarray, 2)
	
		if len(fieldarray(3,i)) > 0 then 'alternate field name from another table
			selectclause = selectclause & fieldarray(6, i) & ", "
			fromclause = "left join (select * from exchange where mod <> 0) as exchange on metadata.type = exchange." & fieldarray(3,i)
		else
			selectclause = selectclause & fieldarray(0, i) & ", "	
		end if
		
		if fieldarray(0,i) = "filename" then  'locate position of filename column for use in the View Details Link
			filenamepos = i
		end if
		
		'Get Filter Values if available
		if request.form("filterby") ="Apply Filter" then
		
			if fieldarray(1, i) = "int" and fieldarray(0,i) <> "type" then
					
				fieldarray(7, i) = request.form("filter-" & fieldarray(0,i) & "1")	
				fieldarray(8, i) = request.form("filter-" & fieldarray(0,i) & "2")
				
			else
				fieldarray(7, i) = request.form("filter-" & fieldarray(0,i))
			end if
		else
		
			if fieldarray(1, i) = "int" and fieldarray(0,i) <> "type" then
		
				curfieldname = "filter-" & fieldarray(0,i) & "1="
				location = instr(1, filtervars, curfieldname)	

				if location > 0 then
					location = location + len(curfieldname)
					fieldarray(7, i) = mid(filtervars, location, instr(location, filtervars, ",") - location)
				end if
				
				curfieldname = "filter-" & fieldarray(0,i) & "2="
				location = instr(1, filtervars, curfieldname)	

				if location > 0 then
					location = location + len(curfieldname)
					fieldarray(8, i) = mid(filtervars, location, instr(location, filtervars, ",") - location)
				end if


			else

				curfieldname = "filter-" & fieldarray(0,i) & "="
				location = instr(1, filtervars, curfieldname)	
	
				if location > 0 then
					location = location + len(curfieldname)
					fieldarray(7, i) = mid(filtervars, location, instr(location, filtervars, ",") - location)
				end if
				
			end if
			
		end if
	next

	'Determine how many records to return
	if len(request.form("removefilter")) > 0 or len(request.form("defaultfilterbutton")) > 0 then
		pagenum = 1
	else
		pagenum = cint(iif(len(request.form("pagenum")) > 0, request.form("pagenum"), iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), 1)))
	end if
	
	'Build the Where Clause
	whereclause = " WHERE 1=1 "
	filtervars = ""
	
	for i=0 to ubound(fieldarray, 2)

		curfilterfield = fieldarray(0, i)	

		if fieldarray(1, i) = "int" and fieldarray(0,i) <> "type" then

			curfiltervalue1 = fieldarray(7, i)	
			curfiltervalue2 = fieldarray(8, i)

			'convert if date
			if fieldarray(5,i) <> 0 then
				if len(curfiltervalue1) > 0 and (isdate(curfiltervalue1) and validate(curfiltervalue1, datepattern) = True) then
					curfiltervalue1 = udate(curfiltervalue1)
				else
					if len(curfiltervalue1) > 0 then
						errormsg = "Error in filter criteria."
					end if
					
					curfiltervalue1 = ""
				end if
				if len(curfiltervalue2) > 0 and (isdate(curfiltervalue2) and validate(curfiltervalue2, datepattern) = True) then
					curfiltervalue2 = udate(curfiltervalue2)					
				else
					if len(curfiltervalue2) > 0 then
						errormsg = "Error in filter criteria."
					end if
					curfiltervalue2 = ""
				end if
			end if

			'convert if duration
			if fieldarray(0,i) = "duration" then
				if validate(curfiltervalue1, "Duration") then 
					curfiltervalue1 = HMSFtoMS(curfiltervalue1, framerate)
				else
					if len(curfiltervalue1) > 0 then
						errormsg = "Error in filter criteria."
					end if
					curfiltervalue1 = ""
				end if
				if validate(curfiltervalue2, "Duration") then
					curfiltervalue2 = HMSFtoMS(curfiltervalue2, framerate)
				else
					if len(curfiltervalue2) > 0 then
						errormsg = "Error in filter criteria."
					end if
					curfiltervalue2 = ""
				end if
			end if

			'determine if range				
			if len(curfiltervalue1) > 0 and len(curfiltervalue2) > 0 then 'values between
				whereclause = whereclause & " and (" & curfilterfield & " >= " & curfiltervalue1 & " and " & curfilterfield & " <= " & curfiltervalue2 & ")"
			else
				if len(curfiltervalue1) > 0 then 'values greater than
					whereclause = whereclause & " and (" & curfilterfield & " >= " & curfiltervalue1 & ")"
				elseif len(curfiltervalue2) > 0 then 'values less than
					'if fieldarray(0,i) = "duration" then
						whereclause = whereclause & " and (" & curfilterfield & " <= " & curfiltervalue2 & " or " & curfilterfield & " IS NULL)"						
					'else
					'	whereclause = whereclause & " and (" & curfilterfield & " <= " & curfiltervalue2 & ")"
					'end if
				end if 'not in the filter
			end if

			filtervars = filtervars & "filter-" & curfilterfield & "1" & "=" & fieldarray(7, i) & ","
			filtervars = filtervars & "filter-" & curfilterfield & "2" & "=" & fieldarray(8, i) & ","

		else
			curfiltervalue = fieldarray(7, i)	

			'if type
			if fieldarray(0,i) = "type" and len(curfiltervalue) > 0 then
				if curfiltervalue = 0 then
					whereclause = whereclause & " and (" & fieldarray(0, i) & " IS NULL or " & fieldarray(0,i) & " = 0)"
				else
					whereclause = whereclause & " and (" & fieldarray(0, i) & " = " & curfiltervalue & ")"
				end if

				filtervars = filtervars & "filter-" & fieldarray(0, i) & "=" & fieldarray(7, i) & ","
			else				
				if len(curfiltervalue) > 0 then
					whereclause = whereclause & " and (" & curfilterfield & " like '%" & curfiltervalue & "%')"
					filtervars = filtervars & "filter-" & curfilterfield & "=" & fieldarray(7, i) & ","
				end if
			end if


		end if
	next

	
	if len(request.form("defaultfilter")) > 0 or len(request.querystring("defaultfilter")) > 0 then
		defaultfilter = 1
	else
		defaultfilter = 0
	end if
	
	if (len(request.form("defaultfilterbutton")) > 0 or defaultfilter = 1) and len(request.form("removefilter")) < 1 then
		whereclause = whereclause & " and (operator = 'sys') "
		defaultfilter = 1
	else
		defaultfilter = 0
	end if	
	
	'Determine Sort Clause
	if len(request.form("removefilter")) > 0 or len(request.form("defaultfilterbutton")) > 0 then
		sortby = "filename"
	else
		sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), iif(len(request.form("sortby")) > 0, request.form("sortby"), "filename"))
	end if
	
	'Build Query
	strsql = "select top " & pagenum * numperpage & " " & selectclause & " (select count(metadata.filename) as totalrecords from metadata " & whereclause & ") as totalrecords from metadata " & fromclause &  whereclause & " order by " & sortby

	'response.write strsql
	set r = db.execute(strsql)

	dim filearray

	if not r.eof then
		totalrecords = r("totalrecords")
		filearray = r.getrows()	
	else
		totalrecords = 0	
		filearray = nullarray
	end if

	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=sortby value="<%=sortby%>">
	<input type=hidden name=filtervars value="<%=filtervars%>">
	<input type=hidden name=pagenum value="<%=pagenum%>">
	<input type=hidden name=defaultfilter value="<%=defaultfilter%>">
	
	<table border=0 cellspacing=0 cellpadding=2 width=100%>
		<thead>
			<tr valign=top>
				<td><h2><nobr><%=PageTitleIndent%>Metadata</nobr></h2>
				</td>
				<td colspan="<%=ubound(fieldarray, 2) + 1 %>" align=left>
					<input type=submit name="ManageCols" value="Manage Fields">
					<input type=submit name=ViewDetails value="Create New Record">
					<input type=submit name=removefilter value="Remove Filter">
					<input type=submit name=defaultfilterbutton value="Show Unassigned Files">
					
				</td>
			</tr>
			<tr>
				<td colspan=6><%=PrintPageNumbers(totalrecords, "&sortby=" & sortby & "&defaultfilter=" & defaultfilter & "&filtervars=" & filtervars, pagenum)%><%=iif(len(errormsg) > 0, "<font color=red><nobr>&nbsp;&nbsp;" & errormsg & "</nobr></font>", "")%></td>
			</tr>
			<tr class="tableheader">
			
			<%
			' Generate Header
			for i=0 to ubound(fieldarray, 2) 
				%>
				<th class="tableheadercell">
					<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&defaultfilter=<%=defaultfilter%>&filtervars=<%=filtervars%>&sortby=<%=determinesortby(fieldarray(0, i), sortby)%>" title="Sort By<%=fieldarray(2, i)%>"><%=fieldarray(2, i)%></a>
				</th>
				<%
			next			
			%>
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">Details</th>
			</tr>
		</thead>

		<tbody>
		<%
		
		'Generate Filter Form
		response.write "<tr class=""tablerowSetAll"" valign=center>"
		
		for i=0 to ubound(fieldarray, 2)
		
			if fieldarray(0,i) = "type" then
				'if len(fieldarray(7, i)) > 0 then
					val = (fieldarray(7, i))
				'else
				'	val = 0
				'end if
				%>
				<td class="tablecell" align=center>
					<select name="filter-<%=fieldarray(0, i)%>" size=1>
						<option value=></option>
						<%=OptionListFileExtensions(val)%>
					</select>
				</td>
				<%
			else
				if fieldarray(1,i) = "int" then
					%>
					<td class="tablecell" align=center>
						<nobr>
						<input type=text name="filter-<%=fieldarray(0, i)%>1" size=5 value="<%=fieldarray(7, i)%>"> - <input type=text name="filter-<%=fieldarray(0, i)%>2" size=5 value="<%=fieldarray(8, i)%>">
						</nobr>
					</td>
					<%
				else
					%>
					<td class="tablecell" align=center>
						<input type=text name="filter-<%=fieldarray(0, i)%>" size=10 value="<%=fieldarray(7, i)%>">
					</td>
					<%
				end if
			end if
		next			
		%>
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name=filterby value="Apply Filter"></th>
		</tr>
		<%

	'Generate Report Data		
	pagestart = ((pagenum - 1) * numperpage)
	pageend = iif(ubound(filearray, 2) > (pagenum * numperpage), (pagenum * numperpage), ubound(filearray, 2))

	if pageend <> 0 then 

		for i=pagestart to pageend 'loop through data rows
				curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
				%>
				<tr class="<%=curclass%>" valign=center>
				<%
			for c=0 to ubound(filearray, 1) - 1 'loop through field names

				if fieldarray(0,c) = "duration" then
					fieldvalue = MStoHMSF(filearray(c, i), framerate)
					if len(fieldvalue) < 1 then
						fieldvalue = "undefined"
					end if
					alignpos = "right"
				else
					if fieldarray(5, c) <> 0 then 'if date, convert and align right
						fieldvalue = unudate(filearray(c, i))
						fieldvalue = "<nobr>"  & iif(len(trim(fieldvalue)) > 0, fieldvalue, "&nbsp;")  & "</nobr>"
						alignpos = "right"
					elseif fieldarray(1, c) = "int" and fieldarray(5, c) = 0 and len(fieldarray(6, c)) < 1 then 'if number center
						fieldvalue = filearray(c,i)
						alignpos = "right"
					else
						fieldvalue = filearray(c, i)
						alignpos = "left"
					end if
				end if

				fieldvalue = iif(len(trim(fieldvalue)) > 0, fieldvalue, "&nbsp;")

				response.write "<td class=""tablecell"" align=""" & alignpos & """>" & fieldvalue & "</td>" & vblf

			next
			%>
				<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(len(filearray(filenamepos,i)) > 0, "<a style=""color: black;"" href=""metadata.asp?mode=ViewDetails&filename=" & filearray(filenamepos,i) & "&sortby=" & sortby & "&pagenum=" & pagenum & "&defaultfilter=" & defaultfilter & "&filtervars=" & filtervars & " ""target=""content"">View</a>", "&nbsp;")%></td>
			</tr>
			<%
		next	
	end if
	%>
		<tr><td class="tablefooter" colspan=<%=ubound(fieldarray, 2) + 2%>>&nbsp;</td></tr>
		</tbody>
	</table>
	
	<% 
	if pageend = 0 then
		response.write "<center><h3>No Matches Found</h3></center>"
	end if
	%>
	</form>
	<%

end if

db.close
%>
</body>
</html>