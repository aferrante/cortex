Main Menu

[Database Table/Field Name]
{Display Name on UI}

---------------------------
INDEX.HTM
---------------------------
12/20/05
Set up a table in the database and change the menu to an asp. Send Kenji the SQL statements to create and update his db. - DONE

1/8/06
Add jsfunctions to index.asp?

---------------------------
Channels - DONE
---------------------------
12/20/05 - DONE
Channel Properties:
	- Report: Display all [Channels] in the db.  
		- [ID]{} is static, 
		- [Flags]{} bit mask - decompile the value to display the following.  Stored as the decimal value of hex.  (ex.  Active PAL will be 0x21 or dec(33) = 2x16=32 + 1 = 33 and deactivated NDF is 0x10 or dec(16) = 1x16 = 16 + 0 = 16)
			- 0 - The channel is not available/deactivated.  This is default.
			- 1 = Active (y/n)
			- What is your video standard	
				3 Video Standards
					- 0x00 = NTSC Drop Frame (default)
					- 0x10 = NTSC No Drop Frame/NDF
					- 0x20 = PAL
		- [Description]{} can change
		
12/20/05		
	- Add FIXSTRING for description - DONE
	
	- JS Confirm on buttons - DONE
	
	- Change borders on table to TR and TD get rid of <Table border=1> - DONE
	
	- Add the first row to the table ID = ALL, Active (this will activate or deactivate all channels in the table - JS Confirm), Same for Video Standard. Desc = "Use these setting for all channels.  Click the "Update" button for the changes to take place". Make this row a different background color.  - CHANGE: Just Active no Video Standard. - DONE
	
	*** Fanciness for later = JS to check all boxes with confirm, then clear the All checkbox."
	
	- Deleting a Channel.  A delete checkbox will only appear when the Channel ID is deactivated.  On Click - JS Confirm/Warning message. - DONE

	- Adding a Channel - Only add one channel at a time. - DONE

	- On changes of any dropdown or text box, change the color of the Channel ID text. And display a message at the top ("Please click the update button for the changes to take effect.")
	
---------------------------
MetaData - DONE
---------------------------
Information about files to be moved.  

	- User can Insert or Delete fields into [MetaData] and set datatype (prefix the fieldname with "user_(entered field data - replace spaces with "_")".  Cannot Alter field.  On Delete, if data, delete anyway.  Display confirmation on action.   Add one field at a time.  Button at top "Add MetaData Field" - bring to new screen Check to make sure field name does not already exist. - Changed the way that works by adding a new table - DONE
	
	- Add another table - to decide which fields are shown in the report, include sort order.  Provide form to select the fields and change the sort order. Build the report query using this table to get the select clause fields in the correct order then the array will be correct. - DONE
	
	- Search/Filter and Sort Data - query table for field names so they can filter on anything. Checkbox for sys added records (see default report below). - DONE
 
 	- Paging - set numofrec variable, select top(pagenum * numofrec), getrows -> array, start at ((pagenum -1 *numofrec)+1) - DONE
	
	- Report (table view) - Show Summary (few fields) with link to drill down into detail of record (nice form).- DONE
	- Show where it exists now, using the [Device_*] tables on [filename] - SKIP FOR NOW
	
	- This will be the default report: Show the files the system added.  WHERE [Operator] = "sys", on update the session variable username will be saved in [Operator]  Add Button at top for this custom view, ORDER BY ingest_date.  "Show Unassigned Files" - DONE
	
	- Create NEW button will bring the user to a blank details page.
	
	- Delete in the details page. - DONE
	
Questions
	- Adding a column - what datatypes to provide? - DONE
	- filtering - by whatever fields will display on the report. - DONE
	- add button for sys report (default view) - DONE
	
	when viewing the details of a metadata record, and then returning to the report, should I save the filter and sorting criteria, and put the user back on the same page they were on before they viewed the details? - yes - DONE
	
	filtering - DONE
		type should be a drop down - DONE
		% on all text - DONE
		- date and numbers (not field type) display two boxes 1 and 2, if 2 is blank then >=... - DONE
		- number fields (i.e. duration)  - DONE
	error check date in the filter. - DONE	
---------------------------
Exchange - DONE
---------------------------	
	- File extensions records will always have len([criterion]) <=3 - DONE
	
	- User should be able to add file extensions. [mod] will be the max count + 1 for file extension records. - DONE
	
	- On error of file ext the add new values are cleared out.. fix. - DONE
	
	- Report: Display [criterion], [flag] for file extension records. - DONE
---------------------------
Messages
---------------------------
	- Users can delete, 
		- store [ID] as hidden for deleting. - DONE
		- On Delete [Flag] = -1, Update [DeletedBy] (session var - username) and [DeletedOn] (unixtime).  - DONE
		- Display buttons for deleting. - DONE
		- Confirm delete.  - DONE
	- On delete, return the user to the same page with the same sort order. - DONE
	- Order by [systime] - DONE
	- allow user to change to Desc. (sort order) - DONE
	- Display on Report - DONE
		- [systime] change to actual date
		- [message]
		- [sender]
	- Same report for two different types of messages
	 1 is error, 0 is message - color code = error RED, message - black
	- select where flag <> -1 - DONE
---------------------------
Destinations - DONE
---------------------------
	- [IP] Changeable - DONE
	- [description] Changeable - DONE
	- [diskspace] Changeable with min (5) and max (99) Error checking. - DONE
	- [type] just display  - DONE
		- for new entries prepopulate the keyer layer display fields. - DONE
		- Values
			- 1 = Imagestore
			- 2 = Intuition
			- else blank
	- [criteria]/[channelid]/[keyerlayer] - not changeable just display in two columns - DONE
		- {Channel ID} - [ChannelId](Build the [criteria] field on update with information) the digits between "==" and "," join with [Channels] to display [description] - DONE
		- {Keyer Layer} 
			- [criteria] - <=, >=, = (in the db this will be "==")
			- [criteria] digits from the right to the first non-digit. - error checking numeric and greater than 0

	- [extensions] - do not display (when adding, insert the following: if type == 2, ext = "V:tem", if type == 1, ext = "V:oxt,oxa|A:oxw,oxe", else NULL - DONE
	- [kb_free] just display - DONE
	- [kb_total] just display - DONE
	- User can Delete, Insert, Update
		Look at the join {Channel ID} with [Channels] on ID.  If [Flags] <> 0, display error message to User about Deactiving the Channel first. Yes set [Flags] to 0. Then 
		- On Delete, Delete the associated [Device*] table.  - DONE
		- On Insert Do Nothing with tables. - DONE
		- On Update of IP, Alter table name to [Device_NewIP]. - DONE
	- Link to Files on this device.  Use [Device_*].  Only display link if table exists. Bring user to new page.
	
	If description is blank - (Channel Description & " " & Type) - Javascript - DONE

		- display all fields from [Device_*].. cannot be changed.  - DONE
			- [filename] {File Name}  - DONE
			- [sys_last_used] unixtime {Last Used}  - DONE
			- [sys_times_used] {Number of Uses}  - DONE
			- [expires_after] unixtime {Expiration Date}  - DONE
			- [transfer_date] unixtime {Transfer Date}  - DONE
			- Link To MetaData if exists.  - DONE
			- Paging, Sorting, Filter (by all fields?) - DONE
	- FUNCTION - UNIX DATE - DONE
			
QUESTIONS - 
- The user never enters kb free or total? - Correct
- what is the default for the keyer layer operator? Provide blank option? - Provide blank option but error check that something is chosen
- does the channelid need to have a value? - yes


- How do you want sorting to work?  One column at a time?  Just a link as the column name? - DONE

Sort by one column at a time, the column name will be a link (asc or desc).. add alt text to link to say "sort by col_name desc".  NO Filtering now. - DONE

---------------------------
Security - DONE
---------------------------		
	- prompt for username and pwd - DONE
	- auth off of db - DONE
	- save username in session variable - DONE
	- session will never timeout - Not an issue.
	- if session var is blank redirect to login page - DONE
	- someone can add/modify users - DONE
	- determine what users can do or not do. - Future
	- set focus to username - DONE
	- change password for all users - DONE
	- add user and set default password - DONE
		- also give them admin rights - DONE
	- edit a user - DONE
		change display name - DONE
		reset password - DONE
		change admin rights - DONE
	- delete user - DONE
	- if login fails, message "please contact your administrator." - DONE
---------------------------
Connect to Db - DONE
---------------------------	
Change to vars and make a sub instead of splitting the function value. - DONE



---------------------------
Main Menu
---------------------------	
- The iframe should have a target name that matches the target of the menu items a href tag.


<table class="t"><tr><td><img src="img/clear.gif"></td><td><div><IMG SRC="img/c_grp.gif" class="grp"><a href="process.htm" target="content">Processes</A></div><div style={display:block}>
    <table class="t"><tr><td><img src="img/clear.gif"></td><td><IMG SRC="img/item.gif"><a href="brandinghelp.htm" target="content">Branding</a></td></tr></table>
    
<iframe src="default.htm" frameborder=0 name="content" width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe>    


---------------------------
Modules
---------------------------	
This will set the redirect for the splash.asp page, store the preference in the database, per user.

Muffin : I think i want to have a detail page for the data base interface item, from the cortex modules page
	Muffin : where you can change the DSN variables
	Muffin : in the direct.cdf file
Muffin : .csf

---------------------------
Misc
---------------------------	

on all form objects, onchange (change class: font-color:  blue;)

Unix Time function - when it's an exact day the time does not display and it should. - DONE

Display date and time as 24 hour clock - DONE

SQL Statements to create db from a text file
First Admin user sql statement (password should already be encrypted)