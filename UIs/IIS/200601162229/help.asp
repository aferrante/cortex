<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Help</title>
<style type=text/css>
	#.Largefont {font-family:Tahoma; color:#595959; font-size:12pt; text-decoration:none; font-weight:bold;}
	#.Medfont  {font-family:Tahoma; color:#595959; font-size:9pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; line-height:12pt;}
	#.Smfont  {font-family:Tahoma; color:#595959; font-size:7pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	#.Link  {font-family:Tahoma; color:#595959; font-size:8pt; letter-spacing:1pt; font-weight:bold; line-height:12pt;}
</style>

</head>

<body marginheight="0" marginwidth="0" bottommargin="0" rightmargin="0" topmargin="0" leftmargin="0">

<table width=100% height=100%>
<tr><td align="center" valign="middle">
<%
dim pwin
pwin = "password"

function benc( ins )

	dim dlen
	dlen = len(ins)
	const alpha = ")x-{fJ|M2mqwO( /+tS:.<*WGoF&%`7Uc$]=Hhj;D^_0}5b3NQ[l1LsAX8d>4g\y"
	const padch = "@"
	const padch2 = "~"
	dim out, n
	out = ""

	for n = 1 to dlen step 3
		dim triple, quad

		'Create one long from this 3 bytes.
		triple = &H10000 * asc(Mid(ins, n, 1)) + _
		  &H100 * retch(Mid(ins, n + 1, 1)) + retch(Mid(ins, n + 2, 1))

		'Oct splits the long To 8 groups with 3 bits
		triple = Oct(triple)

		'Add leading zeros
		triple = String(8 - Len(triple), "0") & triple

		'Convert To base64
		quad = Mid(alpha, CLng("&o" & Mid(triple, 1, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 3, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 5, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 7, 2)) + 1, 1)

		'Add the part To OutPut string
		out = out + quad

	next
	
	select case dlen mod 3
		case 0: '0 bit final - add padchs for encrypt
		  out = padch + Left(out, Len(out) - 2)  + padch2
		case 1: '8 bit final - split padchs for encrypt
		  out = padch2 +Left(out, Len(out) - 2)  + padch
		case 2: '16 bit final - prepend padchs for encrypt
		  out = padch + Left(out, Len(out) - 1) 
	end select
	benc = StrReverse(out)
end function

function retch( inch )
	if inch = "" then 
		retch = 0 
	else 
		retch = asc(inch)
	end if
end function

response.write(benc(pwin))
%>
<P>
This is the page for HELP!
</td></tr>
</table>

</body>
</html>
