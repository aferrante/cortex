<!DOCTYPE HTML PUBLIC; "-//W3C//DTD HTML 4.01 Transitional//EN">

<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->

<title>CORTEX powered by VDS</title>

<style type=text/css>
	.navLinks {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:link{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:visited{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:active{font-family:Tahoma; color:#ffffff; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#998811;}
	.navLinks A:hover {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.noBG {BACKGROUND-COLOR: transparent;}
	.mouseOver {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:link{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:visited{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:active{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:hover{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.grad {height:100%; width:100%; filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr=#fffff8,EndColorStr=#d8b030)}
</style>

<script language="javascript">

	//toggles the nav bar
	function hide()
	{
		vis.style.display='none';
		ex.style.display='block';
		getData('test.htm');
	}

	function show()
	{
		vis.style.display='block';
		ex.style.display='none';
	}

	function movr(e,f)
	{
		e.className="mouseOver";
		f.className="mouseOver";
	}

	function mout(e,f)
	{
		e.className="navLinks";
		f.className="navLinks";
	}

	function doFrame(l)
	{
	// sets a cookie so that refresh brings you back to the last viewed iframe page
		document.cookie = "cortexiframecontent=" + l; // let it expire with browser
	}


	function getPage()
	{
		// set the iframe from cookie
		var start = document.cookie.indexOf("cortexiframecontent=");
		var len = start + "cortexiframecontent=".length;
		if( ( (!start) && (document.cookie.substring(0, "cortexiframecontent=".length) != "cortexiframecontent=" ) ) || (start == -1) )
		{
			frm.innerHTML = "<iframe src='splash.htm' frameborder=0 name='content' width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe>";
		}
		else
		{
			var end = document.cookie.indexOf(";", len);
			if(end == -1) end = document.cookie.length;
			frm.innerHTML = "<iframe src='"+ document.cookie.substring(len,end) +"' frameborder=0 name='content' width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe>";
		}
	}

	function tree(idx)
	{
		// sets visibility for immediate tree subitems
		var par = eval("x" + idx);
		var lim = Number(par.num) + Number(idx);

		var x=idx+1;
		while (x<=lim)
		{
			var row = eval("x" + x);
			if(par.className=="exp") row.style.display='block';  // only do if it is to be expanded
			if(row.num>0)
			{
				var num = tree(x);
				lim += Number(num);
				x += Number(num);
			}
			x++;
		}
		// return the number of rows contained
		return (Number(lim)-Number(idx));
	}

	function clk(idx)
	{
		var img = eval("img" + idx);
		var par = eval("x" + idx);
		if(img.className=="con")
		{
			var s=img.src;
			var i=s.indexOf("e_");
			if((i==0)||((i>0)&&(s.charAt(i-1)=='/'))) img.src=s.substring(0,i)+"c_"+s.substring(i+2,s.length);


			var x=idx+1;
			var lim = Number(par.num) + Number(idx);
			while (x<=lim)
			{
				var row = eval("x" + x);
				row.style.display='block';
				if(row.num>0)
				{
					var num = tree(x);
					lim += Number(num);
					x += Number(num);
				}
				x++;
			}

			img.className="exp";
			par.className="exp";
		}
		else
		if(img.className=="exp")
		{
			var s=img.src;
			var i=s.indexOf("c_");
			if((i==0)||((i>0)&&(s.charAt(i-1)=='/'))) img.src=s.substring(0,i)+"e_"+s.substring(i+2,s.length);

			var x=idx+1;
			var lim = Number(par.num) + Number(idx);
			while (x<=lim)
			{
				var row = eval("x" + x);
				lim += Number(row.num);
				row.style.display='none';

				x++;
			}

			img.className="con";
			par.className="con";
		}
	}

	function getData(sURL)
	{
		var e = document.getElementById("hfrm");
		e.src = sURL +"?"+ Math.random();

		setTimeout('var e = document.getElementById("hfrm"); \
		if (e!=null) alert(e.contentWindow.document.childNodes(0).childNodes(1).childNodes(0).nodeValue); \
		else alert("NULL")', 50);
	}

</script>
</head>

<%
set db = CreateObject("ADODB.Connection")
call getdbvars()
db.open session("dsn"), session("dbusername"), session("dbpassword")

set r = db.execute("select * from menu order by id")

dim show
show=1
dim count
count=0

%>

<body marginheight="0" marginwidth="0" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="getPage()">
<div style={display:none} id="ex"><a onclick="show()" class="toggle"><img src="images/showsm.gif" style={position:absolute;z-index:10;cursor:pointer;} ></a></div>
<table border="0" cellspacing="0" cellpadding="0" width=100% height=100%>
<tr><td width=1 valign="top">
	<table id="vis" border="0" cellspacing="0" cellpadding="0" height=100%><tr><td align=left valign=top>
	<table border="0" cellspacing="0" cellpadding="0" height=100%>
		<tr>
			<td align="right" valign="top" height=2><a onclick="hide()" class="toggle"><img border="0" src="images/hidesm.gif" style={cursor:pointer;}></a></td>
		</tr>

	<%
	do while not r.eof
	%>
		<tr height=30 id="x<%=r("id")%>" <% if show < 1 then
			%> style={display:none} <%

				if r("id") >= count then
				show = 1
				end if
			end if
			if r("subitems") > 0 then
				if InStr(r("icon"), "/e_") > 0 then
				%>  class="con"<%
					if show > 0 then
						show=0
						count = r("id") + r("subitems")
					else
						count = count + r("subitems")
					end if
				else %> class="exp" <%
				end if
			end if
			%> num=<%=r("subitems") %>>
			<%
			if r("id") <> 0 then
				if isnull(r("status")) or Len(Trim(r("status"))) < 1 then
					%><td id="s<%=r("id")%>" class="navLinks" onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)">&nbsp;</td><%
				else
					%><td valign="middle" id="s<%=r("id")%>" class="navLinks" onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)">&nbsp;<img align="absmiddle" border="0" src="<%="images/" & Trim(r("status")) & ".gif"%>">&nbsp;</td><%
				end if
				%>
			<td id="i<%=r("id")%>" class="navLinks" valign="middle" nowrap onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)"><%=IndentMe(r("indent"))%>
				<%
				if isnull(r("icon")) or Len(Trim(r("icon"))) < 1 then
					%><img id="img<%=r("id")%>" align="absmiddle" border="0" src="images/nullicon.gif" onclick="clk(<%=r("id")%>)" <%
					if r("subitems") > 0 then
						%> class="exp"<%
					end if %>><%
				else %><img id="img<%=r("id")%>" align="absmiddle" src="<%=Trim(r("icon"))%>"  onclick="clk(<%=r("id")%>)" <%
					if r("subitems") > 0 then
						if InStr(r("icon"), "/e_") > 0 then
						%> class="con" <% else %> class="exp"<%
						end if
					end if %> ><%
				end if
			%>&nbsp;<a id="l<%=r("id")%>" target="<%=r("target")%>" href="<%=r("link")%>" onclick="doFrame('<%=r("link")%>')">&nbsp;<%=r("label")%>&nbsp;</a></td>
			<%
			else
			'put the logo in '
				%><td>
				<%
				if not isnull(r("icon")) then %><a target="<%=r("target")%>" href="<%=r("link")%>"><img border=0 src="<%=Trim(r("icon"))%>" ></a><%
				end if
				%>
			</td>
		</tr>
		<tr><td>
		<span class="grad">
		<table width=180 border="0" cellspacing="0" cellpadding="0" height=10%>
			<tr height=40><td>&nbsp;</td>
				<%
				'had to create a new table so the gradient color would fill in from below the logo.'
			end if
			%>
			</tr>
	<%
	r.movenext
	loop
	%>
			<tr height=100%><td>&nbsp;</td></tr>
		</table>
		</span></td>
	</tr>
	</table>
	</td>
	<td bgcolor=#999999><img width=5 border="0" src="images/nullicon.gif"></td><!-- the border between right and left, control width with width of transparent icon -->
	<td bgcolor=#cccccc><img width=3 border="0" src="images/nullicon.gif"></td><!-- the border between right and left, made fancier with a second drop shadow, control width with width of transparent icon -->
	</tr></table>
	</td>
	<td>
		<div id="frm">
			<!-- iframe src="splash.htm" frameborder=0 name="content" width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe -->
			<!-- if this div is empty, the default is blank.  when the onload javascript is hit, it fills this part with the iframe and "content" target -->
		</div>
	</td>
</tr>
</table>
<iframe src="q.htm" style={display:none} id="hfrm" width=100%></iframe>
<%
db.close
%>

</body>
</html>
