<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<!--#INCLUDE FILE="contentpage.css"-->
</head>
<body>
<%

set db = CreateObject("ADODB.Connection")

cArray = split(GetConnectStr(), ",")
db.open cArray(0), cArray(1), cArray(2)

if len(request.form("channelids")) > 0 then

	channelarray = split(request.form("channelids"), ",")

	for i=0 to ubound(channelarray) -1
		curchannel = channelarray(i)
		strsql = "update channels set flags = " & clng( "&H" & (iif(request.form("active" & curchannel) = "on", 1, 0) + cint(request.form("videostandard" & curchannel)))) & ", description = '" & request.form("description" & curchannel) & "' where id = " & curchannel

		'response.write strsql & "<BR><BR>"

		db.execute(strsql)
	next

end if



set r = db.execute("select * from Channels")

%>

<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>

<table border=1 cellspacing=0 cellpadding=2>
	<thead>
		<tr class="tabletitle">
			<td class="tabletitle" colspan=3><h2><nobr><%=PageTitleIndent%>Channel Properties</nobr></h2></td>
			<td class="tabletitle" align=right><nobr><input type=reset value="Reset">&nbsp;<input type=submit name=update value="Update"></nobr></td>
		</tr>

		<tr class="tableheader">
			<th>Omnibus<br>Channel<br>Number</th>
			<th>Asset<br>Management<br>Active</th>
			<th>Video Standard</th>
			<th>Description</th>
		</tr>
	</thead>
<%
channelids = ""

i = 0
do while not r.eof
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>">
		<td align=center><%=r("id")%></td>
		<td align=center><input type=checkbox name="<%="active" & r("id")%>" <%=iif(cint(r("flags")) mod 2 = 1, "CHECKED", "")%>></td>
		<td>
			<select name="<%="videostandard" & r("id")%>" size=1>
				<%=OptionListVideoStandards(r("flags"))%>
			</select>
		</td>
		<td><input type=text name="<%="description" & r("id")%>" size=30 value="<%=r("description")%>"></td>
	</tr>
	<%

	channelids = channelids & r("id") & ","

i = i + 1
r.movenext
loop

%>
	<tr class="tabletitle">
		<td class="tabletitle" colspan=3>&nbsp;</td>
		<td class="tabletitle" align=right><nobr><input type=reset value="Reset">&nbsp;<input type=submit name=update value="Update"></nobr></td>
	</tr>
</table>
<input type=hidden name=channelids value="<%=channelids%>">
<form>
<%

db.close

%>
</body>
</html>