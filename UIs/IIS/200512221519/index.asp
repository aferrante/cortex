<!DOCTYPE HTML PUBLIC; "-//W3C//DTD HTML 4.01 Transitional//EN">

<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->

<title>CORTEX powered by VDS</title>

<style type=text/css>
	.navLinks {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:link{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:visited{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	.navLinks A:active{font-family:Tahoma; color:#ffffff; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#998811;}
	.navLinks A:hover {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.noBG {BACKGROUND-COLOR: transparent;}
	.mouseOver {font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:link{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:visited{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:active{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.mouseOver A:hover{font-family:Tahoma; color:#000000; font-size:11pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; background-color:#ffffff;}
	.grad {height:100%; width:100%; filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr=#fffff8,EndColorStr=#d8b030)}
</style>

<script language="javascript">

	//toggles the nav bar
	function hide()
	{
		vis.style.display='none';
		ex.style.display='block';
	}

	function show()
	{
		vis.style.display='block';
		ex.style.display='none';
	}

	function movr(e,f)
	{
		e.className="mouseOver";
		f.className="mouseOver";
	}

	function mout(e,f)
	{
		e.className="navLinks";
		f.className="navLinks";
	}

	function doFrame(l)
	{
	// sets a cookie so that refresh brings you back to the last viewed iframe page
		document.cookie = "cortexiframecontent=" + l; // let it expire with browser
	}


	function getPage()
	{
		// set the iframe from cookie
		var start = document.cookie.indexOf("cortexiframecontent=");
		var len = start + "cortexiframecontent=".length;
		if( ( (!start) && (document.cookie.substring(0, "cortexiframecontent=".length) != "cortexiframecontent=" ) ) || (start == -1) )
		{
			frm.innerHTML = "<iframe src='splash.htm' frameborder=0 name='content' width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe>";
		}
		else
		{
			var end = document.cookie.indexOf(";", len);
			if(end == -1) end = document.cookie.length;
			frm.innerHTML = "<iframe src='"+ document.cookie.substring(len,end) +"' frameborder=0 name='content' width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe>";
		}
	}
</script>
</head>

<%
set db = CreateObject("ADODB.Connection")

cArray = split(GetConnectStr(), ",")
db.open cArray(0), cArray(1), cArray(2)

set r = db.execute("select * from menu order by id")

%>

<body marginheight="0" marginwidth="0" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="getPage()">
<div style={display:none} id="ex"><a onclick="show()" class="toggle"><img src="images/showsm.gif" style={position:absolute;z-index:10;cursor:pointer;} ></a></div>
<table border="0" cellspacing="0" cellpadding="0" width=100% height=100%>
<tr><td width=1 valign="top">
	<table id="vis" border="0" cellspacing="0" cellpadding="0" height=100%><tr><td align=left valign=top>
	<table border="0" cellspacing="0" cellpadding="0" height=100%>
		<tr>
			<td align="right" valign="top" height=2><a onclick="hide()" class="toggle"><img border="0" src="images/hidesm.gif" style={cursor:pointer;}></a></td>
		</tr>

	<%
	do while not r.eof
	%>
		<tr height=30>
			<%
			if r("id") <> 0 then
				if isnull(r("status")) then
					%><td id="s<%=r("id")%>" class="navLinks" onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)">&nbsp;</td><%
				else
					%><td valign="middle" id="s<%=r("id")%>" class="navLinks" onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)">&nbsp;<img align="absmiddle" border="0" src="<%="images/" & Trim(r("status")) & ".gif"%>">&nbsp;</td><%
				end if
				%>
			<td id="i<%=r("id")%>" class="navLinks" valign="middle" nowrap onmouseover="movr(i<%=r("id")%>,s<%=r("id")%>)" onmouseout="mout(i<%=r("id")%>,s<%=r("id")%>)"><%=IndentMe(r("indent"))%>
				<%
				if isnull(r("icon")) then %><img align="absmiddle" border="0" src="images/nullicon.gif" ><%
				else %><img align="absmiddle" src="<%=Trim(r("icon"))%>" ><%
				end if 
			%>&nbsp;<a id="l<%=r("id")%>" target="<%=r("target")%>" href="<%=r("link")%>" onclick="doFrame('<%=r("link")%>')">&nbsp;<%=r("label")%>&nbsp;</a></td>
			<%
			else
			'put the logo in
				%><td>
				<%
				if not isnull(r("icon")) then %><a target="<%=r("target")%>" href="<%=r("link")%>"><img border=0 src="<%=Trim(r("icon"))%>" ></a><%
				end if
				%>			
			</td>
		</tr>
		<tr><td>	
		<span class="grad">
		<table width=180 border="0" cellspacing="0" cellpadding="0" height=10%>
			<tr height=40><td>&nbsp;</td>
				<% 
				'had to create a new table so the gradient color would fill in from below the logo.
			end if
			%>
			</tr>
	<%
	r.movenext
	loop
	%>
			<tr height=100%><td>&nbsp;</td></tr>
		</table>
		</span></td>
	</tr>
	</table>
	</td>
	<td bgcolor=#999999><img width=5 border="0" src="images/nullicon.gif"></td><!-- the border between right and left, control width with width of transparent icon -->
	<td bgcolor=#cccccc><img width=3 border="0" src="images/nullicon.gif"></td><!-- the border between right and left, made fancier with a second drop shadow, control width with width of transparent icon -->
	</tr></table>
	</td>
	<td>
		<div id="frm">
			<!-- iframe src="splash.htm" frameborder=0 name="content" width=100% height=100% hspace=0 vspace=0 marginheight=0 marginwidth=0></iframe -->
			<!-- if this div is empty, the default is blank.  when the onload javascript is hit, it fills this part with the iframe and "content" target -->
		</div>
	</td>
</tr>
</table>

<%
db.close
%>

</body>
</html>
