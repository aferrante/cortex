<%

'--------------------------------------------
'	Constants
'--------------------------------------------

dbpwdfile = "c:\cortex\direct.csf" 'Location of the file that contains the database password.


'--------------------------------------------
'	Functions
'--------------------------------------------

Function GetConnectStr()
	'Reads in the database password from a text file.
	'chr(13) Carriage return code

	dim fs, f, pos, connectstr, filevar, val

	connectstr = ""

	Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(dbpwdfile, 1)
	filecontents = (f.ReadAll)

	'Find DSN
	filevar = "PrimaryDSN"
	pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
	connectstr = connectstr & mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)

	'Find Username
	filevar = "PrimaryDBUser"
	pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
	val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
	connectstr = connectstr & "," & val

	'Find Password
	filevar = "PrimaryDBPassword"
	pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
	val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
	connectstr = connectstr & "," & val

	f.Close
	Set f=Nothing
	Set fs=Nothing

	GetConnectStr = connectstr

end function



function iif(cond, val1, val2)

	if cond then
		iif = val1
	else
		iif = val2
	end if

end function


function OptionListVideoStandards(Highlight)

	dim optionlist, cHighlight

	cHighlight = cint(hex(Highlight)/10) * 10

	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(cHighlight = 0, "SELECTED", "") & ">NTSC Drop Frame (29.97 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=10 " & iif(cHighlight = 10, "SELECTED", "") & ">NTSC No Drop Frame/NDF (30 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=20 " & iif(cHighlight = 20, "SELECTED", "") & ">PAL (25 fps)</option>" & vbcrlf

	OptionListVideoStandards = optionlist

end function

function PageTitleIndent()

	PageTitleIndent= IndentMe(5)

end function

function IndentMe(indentnum)

	dim i, tmp

	tmp = ""
	for i=1 to indentnum
		tmp = tmp & "&nbsp;&nbsp;&nbsp;&nbsp;"
	next

	IndentMe = tmp
end function

%>