<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
set db = CreateObject("ADODB.Connection")
call getdbvars()
db.open session("dsn"), session("dbusername"), session("dbpassword")
errormsg = ""

if request.form("mode") = "ManageCols" and request.form("return") <> "Return To Metadata" then

	'ADD A COLUMN NAME
	if instr(1, request.form, "add_col") > 0 then
	
		if len(request.form("sortorderNEW")) > 0 and isnumeric(request.form("sortorderNEW")) = False then
			errormsg = errormsg & "- Column Order is not a numeric value.<br>"
		end if
		
		
		if len(request.form("colnameNEW")) < 1 then
			errormsg = errormsg & "- Column Name is a required field.<br>"
		else
		
			if validate(request.form("colnameNEW"), "DBColName") = False then
				errormsg = errormsg & "- Column Name is invalid.<br>"
			else
			
				colnameNEW = replace(request.form("colnameNEW"), " ", "_")
				
				strsql = "Select COLUMN_NAME From Information_Schema.Columns Where Table_Name = 'Metadata' and Column_Name = '" & colnameNEW & "'"
				
				'response.write strsql

				set r = db.execute(strsql)
				if not r.eof then
					errormsg = errormsg & "- This Column Name already exists in the Metadata table.<br>"
				end if
			end if
		end if

		if len(request.form("displaynameNEW")) < 1 then
			errormsg = errormsg & "- Display Name is a required field.<br>"
		end if
	
		if len(errormsg) < 1 then 

			strsql = "Alter table Metadata Add " & colnameNEW & " varchar(256)"

			'response.write strsql & "<BR><BR>"
			db.execute(strsql)

			strsql = "insert into Metadata_Col_Sort (sortorder, col_name, display_name, display_on_report, operator, edit, alt_col_name, suppress, datevalue, optionlist, allownewentry) values (" & iif(len(request.form("sortorderNEW")) > 0, request.form("sortorderNEW"), "NULL") & ", '" & colnameNEW & "', '" & fixstring(request.form("displaynameNEW")) & "', " & iif(request.form("displayNEW") = "on", 1, 0) & ", '" & session("username") & "', 1, '', 0, 0, '', 1)"
			
			'response.write strsql
			db.execute(strsql)

		end if
	end if

	'DELETE A COLUMN NAME
	if instr(1, request.form, "delete_col") > 0 then
	
		deletepos = instr(1, request.form, "delete_col") + 10 'move to the end of the variable name
		deletepos = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the column name
	
		db.execute("delete from Metadata_Col_Sort where col_name = '" & deletepos & "'")
		db.execute("alter table Metadata drop column " & deletepos)


	end if
	
	'UPDATE ALL COLUMNS
	if request.form("update") = "Update" then
	
		idarray = split(request.form("ids"), ",")
	
		for i=0 to ubound(idarray) -1
		
			if len(request.form("sortorder-" & idarray(i))) > 0 and isnumeric(request.form("sortorder-" & idarray(i))) = False then
				errormsg = errormsg & "Line " & i + 1 & " - Column Order is not a numeric value.<br>"
			end if
				
			if len(request.form("display_name-" & idarray(i))) < 1 then
				errormsg = errormsg & "Line " & i + 1 & " - Display Name is a required field.<br>"
			end if
			
		next 
		
		if len(errormsg) < 1 then 
			for i=0 to ubound(idarray) -1	
				
				strsql = "update Metadata_Col_Sort set sortorder = " & iif(len(request.form("sortorder-" & idarray(i))) > 0, request.form("sortorder-" & idarray(i)), "NULL") & ", display_name = '" & fixstring(request.form("display_name-" & idarray(i))) & "', display_on_report = " & iif(request.form("display-" & idarray(i)) = "on", 1, 0) & " where col_name = '" & idarray(i) & "'"
	
				'response.write strsql & "<BR><BR>"
				db.execute(strsql)
			next
		end if
	end if

	
	'DISPLAY FORM
	
	
	strsql = "select Metadata_Col_Sort.sortorder, Metadata_Info.Column_Name, Metadata_Col_Sort.display_name, Metadata_Col_Sort.display_on_report, Metadata_Col_Sort.Operator from (Select COLUMN_NAME From Information_Schema.Columns Where Table_Name = 'Metadata') as Metadata_Info left join Metadata_Col_Sort on Metadata_Info.Column_Name = Metadata_Col_Sort.col_name order by sortorder, col_name"
	
	'response.write strsql
	set r = db.execute(strsql)
	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=mode value="ManageCols">
	<table border=0 cellspacing=0 cellpadding=2 width=100%>
	
		<thead>
		<tr valign=top>
			<td colspan=2><h2><nobr><%=PageTitleIndent%>Metadata Columns</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
			<td align=right colspan=3>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				<input type=submit name="return" value="Return To Metadata">
				</nobr>				
				
			</td>
		</tr>

			<tr class="tableheader">
				<th class="tableheadercell">Column Order</th>
				<th class="tableheadercell">Column Name</th>
				<th class="tableheadercell">Display Name</th>
				<th class="tableheadercell">Display On Report</th>									
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			</tr>
		</thead>

	<tbody>

		<!--Begin Add New Column-->

		<tr class="tablerowAddNew" valign=center>
			<td class="tablecell" align=center>
				<input type=text name=sortorderNew size=2 value="<%=iif(len(errormsg) > 0, request.form("sortorderNew"), "")%>">
			</td>
			<td class="tablecell">
				<input type=text name=colnameNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("colnameNEW"), "")%>">
			</td>
			<td class="tablecell">
				<input type=text name=displaynameNEW size=30 value="<%=iif(len(errormsg) > 0, request.form("displaynameNEW"), "")%>">
			</td>	
			<td class="tablecell" align=center>
				<input type=checkbox name=displayNEW <%=iif(len(errormsg) > 0 and request.form("displayNEW") = "on", "CHECKED", "")%>>
			</td>		
			<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_col" value="Add New"  onclick="return confirm_on_click('add a new column name?  All other changes will be lost.');"></td>
		</tr>

		<!--End Add New Column-->
	
	<%
	ids = ""
	i = 0
	do while not r.eof 
		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		if len(errormsg) > 0 then
			sortorder = request.form("sortorder-" & r("column_name"))
			display_name = request.form("display_name-" & r("column_name"))
			display = iif(request.form("display-" & r("column_name")) = "on", 1, 0)
		else
			sortorder = r("sortorder")
			display_name = r("display_name")
			display = r("display_on_report")
		end if
		%>
		<tr class="<%=curclass%>" valign=center>
			<td class="tablecell" align=center>
				<input type=text name="<%="sortorder-" & r("column_name")%>" size=2 value="<%=sortorder%>">
			</td>
			<td class="tablecell">
				<%=r("column_name")%>
			</td>
			<td class="tablecell">
				<input type=text name="<%="display_name-" & r("column_name")%>" size=30 value="<%=display_name%>">
			</td>
			<td class="tablecell" align=center>
				<input type=checkbox name="<%="display-" & r("column_name")%>" <%=iif(display <> 0, "CHECKED", "")%>>
			</td>		
			<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(r("operator") & "" <> "sys", "<input type=submit name=""delete_col" & r("column_name") & """ value=Delete  onclick=""return confirm_on_click('delete this column and all associated data?  All other changes will be lost.');"">", "&nbsp;")%></td>
		</tr>
		<%
		ids = ids & r("column_name") & ","
		i = i + 1
		r.movenext
	loop

%>
	<tr><td class="tablefooter" colspan=5>&nbsp;</td></tr>
	<tr>
		<td colspan=2>&nbsp;</td>
		<td align=right colspan=3>
			<nobr>
			<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
			<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
			<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
			<input type=submit name="return" value="Return To Metadata">
			</nobr>
		</td>
	</tr>
</tbody>
</table>
<input type=hidden name=ids value="<%=ids%>">

</form>
<%


elseif (request.form("mode") = "ViewDetails" or request.querystring("mode") = "ViewDetails") and request.form("return") <> "Return To Metadata" then

	filename = iif(len(request.querystring("filename")) > 0, request.querystring("filename"), request.form("filename"))
	
	set f = db.execute("Select COLUMN_NAME, data_type, Metadata_Col_Sort.display_name, NULL as datavalue, Metadata_Col_Sort.edit, Metadata_Col_Sort.alt_col_name, Metadata_Col_Sort.suppress, Metadata_Col_Sort.datevalue, Metadata_Col_Sort.optionlist, Metadata_Col_Sort.allownewentry From Information_Schema.Columns as MetadataCols left join Metadata_Col_Sort on MetaDataCols.Column_Name  = Metadata_Col_Sort.col_name Where Table_Name = 'Metadata'")
		
	if not f.eof then
		fieldarray = f.getrows()
	end if
	
	if request.form("update") = "Update" or request.form("update") = "Create" then
		errormsg = ""
		'Check for errors

		for i=0 to ubound(fieldarray, 2)

			 if fieldarray(1,i) = "int" and fieldarray(4, i) <> 0 then 'editable int
			 			response.write "IN<BR>"
			 	if len(request.form(fieldarray(0,i))) > 0 and isnumeric(request.form(fieldarray(0,i))) = False and fieldarray(7,i) = 0 then 'this is a number field
			 		errormsg = errormsg & fieldarray(2,i) & " is not a valid number.<br>"
			 	end if
			 	
			 	if len(request.form(fieldarray(0,i))) > 0 and isdate(request.form(fieldarray(0,i))) = False and fieldarray(7,i) <> 0 then 'this is a date field
					errormsg = errormsg & fieldarray(2,i) & " is not a valid date.<br>"
			 	end if
			 	
			 	if fieldarray(0,i) = "type" then
			 		if request.form(fieldarray(0,i)) = 0 then
			 			errormsg = errormsg & fieldarray(2,i) & " cannot be Unknown.<br>"
			 		end if
			 	end if
			 	
			end if
		next
		
		if len(errormsg) < 1 then 
			if request.form("update") = "Update" then
			
				sqlclause = ""
				
				for i=0 to ubound(fieldarray, 2)
					if fieldarray(4, i) <> 0 then 'editable						
				 		if fieldarray(1,i) = "int" and fieldarray(7,i) = 0 then 'number
				 			sqlclause = sqlclause & fieldarray(0,i) & " = " & iif(len(request.form(fieldarray(0,i))) > 0, request.form(fieldarray(0,i)), NULL) & ", "
				 		elseif fieldarray(1,i) = "int" and fieldarray(7,i) <> 0 then 'date
				 			sqlclause = sqlclause & fieldarray(0,i) & " = " & iif(len(request.form(fieldarray(0,i))) > 0, UDate(request.form(fieldarray(0,i))), NULL) & ", "
				 		else 'string
							sqlclause = sqlclause & fieldarray(0,i) & " = '" & fixstring(request.form(fieldarray(0,i))) & "', "
				 		end if					 		
				 	end if	
				 next	
				 	sqlclause = left(sqlclause, len(sqlclause) - 2)
				
			
				strsql = "update metadata set " & sqlclause & " where filename = '" & filename & "'"
				response.write strsql
				db.execute(strsql)
			else
			
				sqlclausefields = ""
				sqlclausevalue = ""

				for i=0 to ubound(fieldarray, 2)
					if fieldarray(4, i) <> 0 then 'editable		

						sqlclausefields = sqlclausefields & fieldarray(0,i) & ", "

						if fieldarray(1,i) = "int" and fieldarray(7,i) = 0 then 'number
							sqlclausevalue = sqlclausevalue & iif(len(request.form(fieldarray(0,i))) > 0, request.form(fieldarray(0,i)), NULL) & ", "
						elseif fieldarray(1,i) = "int" and fieldarray(7,i) <> 0 then 'date
							sqlclausevalue = sqlclausevalue & fieldarray(0,i) & " = " & iif(len(request.form(fieldarray(0,i))) > 0, UDate(request.form(fieldarray(0,i))), NULL) & ", "
						else 'string
							sqlclausevalue = sqlclausevalue & fieldarray(0,i) & " = '" & fixstring(request.form(fieldarray(0,i))) & "', "
						end if					 		
					end if	
				 next	

				sqlclausefields = left(sqlclausefields, len(sqlclausefields) - 2)
				sqlclausevalue = left(sqlclausevalue, len(sqlclausevalue) - 2)

				strsql = "insert into metadate (" & sqlclausefields & ") values (" & sqlclausevalues & ")"
				response.write strsql
				'db.execute(strsql)
				
			end if
		end if
	end if
	
	set r = db.execute("select * from metadata left join exchange on metadata.type = exchange.mod where filename = '" & filename & "'")
	
	
	if not r.eof and len(errormsg) < 1 then
	
		for i=0 to ubound(fieldarray, 2)
			 if len(fieldarray(5,i)) > 0 then
			 	fieldarray(3, i) = r(fieldarray(5,i))
			 else
			 	fieldarray(3, i) =r(fieldarray(0,i))
			 end if
		next
	else
		for i=0 to ubound(fieldarray, 2)
			if ((len(filename) > 0  and fieldarray(4, i) = 0) or (len(filename) <1 and fieldarray(9, i) = 0)) and not r.eof then 
				if len(fieldarray(5,i)) > 0 then
					fieldarray(3, i) = r(fieldarray(5,i))
				else
					fieldarray(3, i) =r(fieldarray(0,i))
				end if
			else
				fieldarray(3, i) = request.form(fieldarray(0,i))
			end if
		next
	end if

	%>
	<h2><nobr><%=PageTitleIndent%><%=iif(len(filename) > 0, "Complete Metadata Information for " & filename, "Create Metadata Record")%></nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%>

	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=mode value="ViewDetails">
	<% if len(filename) > 0 then %>
		<input type=hidden name=filename value="<%=filename%>">
	<% end if %>
	
	<table border=1 cellspacing=0 cellpadding=2 align=center>
	<%
	
	j = 0
	for i=0 to ubound(fieldarray, 2)
		curclass = iif(j mod 2 = 0, "tablerow1", "tablerow2")
	
		if fieldarray(6, i) = 0 then 'show fields that are not suppressed
			%>
			<tr class="<%=curclass%>" valign=center>
				<td><b><%=fieldarray(2,i)%>: </b></td>
			<%

			if (len(filename) > 0 and fieldarray(4, i) <> 0) or (len(filename) < 1 and fieldarray(9, i) <> 0) then 'can be edited?
				'type should be a drop down boxes on exchange
				if len(fieldarray(8,i)) > 0 then 'special case for type dropdown box
					%>
					<td>
						<select name="<%=fieldarray(0,i)%>" size=1>
							<%=OptionListFileExtensions(fieldarray(3,i) + 0)%>
						</select>
					</td>
					<%
				else
					if fieldarray(7,i) <> 0 and len(errormsg) < 1 then
						fieldvalue = unUDate(fieldarray(3,i))
					else
						fieldvalue = fieldarray(3,i)
					end if 
					%>
					<td><input type=text size=50 name="<%=iif(fieldarray(0,i) = "filename", "filenameNEW", fieldarray(0,i))%>" value="<%=fieldvalue%>"></td>
					<%
				end if
			else
				response.write "<td>"
				
				if fieldarray(7,i) <> 0 then
					response.write unUDate(fieldarray(3,i))
				else
					response.write fieldarray(3,i)
				end if
				response.write "&nbsp;</td>"
				
			end if

			response.write "</tr>"
			j = j + 1
		end if
		
	next
	
	%>
	</table>
	<br>
	<center><nobr>
	<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
	<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
	<input type=submit name=update value="<%=iif(len(filename) > 0, "Update", "Create")%>" onclick="return confirm_on_click('<%=iif(len(filename) > 0, "update", "create")%> this record?');">
	<input type=submit name="return" value="Return To Metadata">
	</nobr>
	</center>
	</form>
	<%
else

	pagenum = cint(iif(len(request.form("pagenum")) > 0, request.form("pagenum"), iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), 1)))

	devicetable = iif(len(request.querystring("devicetable")) > 0, request.querystring("devicetable"), request.form("devicetable"))

	whereclause = " WHERE 1=1 "

	whereclause = whereclause & " and operator = 'sys' "

	strsql = "select top " & pagenum * numperpage & " *, (select count(metadata.filename) as totalrecords from metadata " & whereclause & ") as totalrecords from metadata " & whereclause & "order by metadata.filename"

	'response.write strsql

	set r = db.execute(strsql)

	dim filearray

	if not r.eof then
		totalrecords = r("totalrecords")
		filearray = r.getrows()	
	else
		totalrecords = 0	
		filearray = nullarray
	end if

	%>
	<table border=0 cellspacing=0 cellpadding=2 width=100%>
		<thead>
			<tr valign=top>
				<td colspan=3><h2><nobr><%=PageTitleIndent%>Metadata</nobr></h2></td>
				<td>
					<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
					<input type=hidden name=mode value="ManageCols">
					<input type=submit value="Manage Fields">
					</form>
					<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
					<input type=hidden name=mode value="ViewDetails">
					<input type=submit value="Create New Record">
					</form>
				</td>
			</tr>
			<tr>
				<td colspan=6><%=PrintPageNumbers(totalrecords, "", pagenum)%></td>
			</tr>
			<tr class="tableheader">
				<th class="tableheadercell">File Name</th>
				<th class="tableheadercell">Last Used</th>
				<th class="tableheadercell">Number of Uses</th>
				<th class="tableheadercell">Expiration Date</th>
				<th class="tableheadercell">Transfer Date</th>			
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">Details</th>
			</tr>
		</thead>

		<tbody>
	<%

	pagestart = ((pagenum - 1) * numperpage)
	pageend = iif(ubound(filearray, 2) > (pagenum * numperpage), (pagenum * numperpage), ubound(filearray, 2))

	for i=pagestart to pageend


		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		%>
		<tr class="<%=curclass%>" valign=center>
			<td class="tablecell"><%=filearray(0,i)%></td>
			<td class="tablecell" align=right><%=(filearray(1,i))%></td>
			<td class="tablecell" align=center>&nbsp;<%=filearray(2,i)%></td>
			<td class="tablecell" align=right>&nbsp;<%=(filearray(3,i))%></td>
			<td class="tablecell" align=right>&nbsp;<%=(filearray(4,i))%></td>
			<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(len(filearray(5,i)) > 0, "<a style=""color: black;"" href=""metadata.asp?mode=ViewDetails&filename=" & filearray(0,i) & " ""target=""content"">View</a>", "&nbsp;")%></td>
		</tr>
		<%

	next

	%>
		<tr><td class="tablefooter" colspan=7>&nbsp;</td></tr>
	</table>
	<%

end if

db.close
%>
</body>
</html>