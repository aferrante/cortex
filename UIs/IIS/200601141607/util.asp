<%

'--------------------------------------------
'	Constants
'--------------------------------------------

dbpwdfile = "c:\cortex\direct.csf" 'Location of the file that contains the database password.
numperpage = 50

'--------------------------------------------
'	Variables
'--------------------------------------------
public p_channelarray
public nullarray(1,0)

nullarray(0,0) = NULL
nullarray(1,0) = NULL

'--------------------------------------------
'	Functions
'--------------------------------------------

function validuser()
	if len(session("username")) < 1 then
		response.redirect "users.asp"
	end if
end function


Sub getdbvars()
	'Reads in the database password from a text file.
	'chr(13) Carriage return code
	
	if len(session("dsn")) < 1 then 
		dim fs, f, pos, connectstr, filevar, val

		Set fs=Server.CreateObject("Scripting.FileSystemObject")
		Set f=fs.OpenTextFile(dbpwdfile, 1)
		filecontents = (f.ReadAll)

		'Find DSN
		filevar = "PrimaryDSN"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		'connectstr = connectstr & mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		session("dsn") = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)

		'Find Username
		filevar = "PrimaryDBUser"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		'connectstr = connectstr & "," & val
		session("dbusername") = val

		'Find Password
		filevar = "PrimaryDBPassword"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		'connectstr = connectstr & "," & val
		session("dbpassword") = val

		f.Close
		Set f=Nothing
		Set fs=Nothing
	end if
	
end sub




function iif(cond, val1, val2)

	if cond then
		iif = val1
	else
		iif = val2
	end if

end function


function OptionListVideoStandards(Highlight, fromDB)

	dim optionlist, cHighlight

	if fromDB = 1 then
		cHighlight = cint(hex(Highlight)/10) * 10
	else
		cHighlight = Highlight
	end if
	
	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(cHighlight = 0 or len(cHighlight) < 1, "SELECTED", "") & ">NTSC Drop Frame (29.97 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=10 " & iif(cHighlight = 10, "SELECTED", "") & ">NTSC No Drop Frame/NDF (30 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=20 " & iif(cHighlight = 20, "SELECTED", "") & ">PAL (25 fps)</option>" & vbcrlf

	OptionListVideoStandards = optionlist

end function


function LoadOptionListChannels()

	dim db, r
	set db = CreateObject("ADODB.Connection")
	call getdbvars()
	db.open session("dsn"), session("dbusername"), session("dbpassword")

	set r = db.execute("select id, description from Channels order by id")
	
	if not r.eof then
		p_channelarray = r.getrows()
	else
		p_channelarray = nullarray
	end if
	
	db.close

end function




function OptionListChannels(Highlight)

	dim i, optionlist
	
	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0 or len(Highlight) < 1, "SELECTED", "") & "></option>" & vblf
	
	if not isnull(p_channelarray(0,0)) then 
		for i = 0 to ubound(p_channelarray, 2)
			optionlist = optionlist & "<option value=" & p_channelarray(0,i) & " " & iif(highlight = p_channelarray(0,i), "SELECTED", "") & ">" & p_channelarray(0,i) & "-" & p_channelarray(1,i) & "</option>" & vblf
		next
	end if

	OptionListChannels = optionlist

end function



function OptionListFileExtensions(Highlight)

	dim db, r, optionlist
	
	set db = CreateObject("ADODB.Connection")
	call getdbvars()
	db.open session("dsn"), session("dbusername"), session("dbpassword")
	
	set r = db.execute("select criterion, mod, flag from exchange where mod > 0")
	
	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0 or len(Highlight) < 1, "SELECTED", "") & ">Unknown</option>" & vblf
	
	do while not r.eof 
		optionlist = optionlist & "<option value=""" & r("mod") & """ " & iif(highlight = r("mod"), "SELECTED", "") & ">" & r("criterion") & " - " & r("flag") & "</option>" & vblf
	r.movenext
	loop

	OptionListFileExtensions = optionlist

end function




Function OptionListKeyerLayerOp(Highlight)

	optionlist = ""
	
	optionlist = optionlist & "<option value="""" " & iif(Highlight = "", "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=""<="" " & iif(Highlight = "<=", "SELECTED", "") & ">Less Than Or Equal To</option>" & vbcrlf
	optionlist = optionlist & "<option value="">="" " & iif(Highlight = ">=", "SELECTED", "") & ">Greater Than Or Equal To</option>" & vbcrlf
	optionlist = optionlist & "<option value=""=="" " & iif(Highlight = "==", "SELECTED", "") & ">Equal To</option>" & vbcrlf

	OptionListKeyerLayerOp = optionlist

end function

Function GetKeyerLayerOpText(Highlight)

	if Highlight = "<=" then
		GetKeyerLayerOpText = "Less Than Or Equal To"
	elseif Highlight = ">=" then
		GetKeyerLayerOpText = "Greater Than Or Equal To"
	elseif Highlight = "==" then
		GetKeyerLayerOpText = "Equal To"
	else
		GetKeyerLayerOpText = ""
	end if

end function

Function OptionListDestTypes(Highlight)

	optionlist = ""
	
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0, "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=1 " & iif(Highlight = 1, "SELECTED", "") & ">Imagestore</option>" & vbcrlf
	optionlist = optionlist & "<option value=2 " & iif(Highlight = 2, "SELECTED", "") & ">Intuition</option>" & vbcrlf

	OptionListDestTypes = optionlist

end function


Function GetDestType(curtype)

	if len(curtype) < 1 then 
		GetDestType = ""
	else
		if curtype = 1 then
			GetDestType = "Imagestore"
		elseif curtype = 2 then
			GetDestType = "Intuition"
		else
			GetDestType = ""
		end if
	end if
end function



function PageTitleIndent()

	PageTitleIndent= IndentMe(5)

end function

function IndentMe(indentnum)

	dim i, tmp

	tmp = ""
	for i=1 to indentnum
		tmp = tmp & "&nbsp;&nbsp;&nbsp;&nbsp;"
	next

	IndentMe = tmp
end function

function fixstring(curstring)

	if len(curstring) > 0 then
		fixstring = replace(curstring, "'", "''")
	else
		fixstring = ""
	end if
end function


function Validate(curstr, curtype)

	dim re
	set re = new regexp
	
	if curtype = "IP" then 
		re.Pattern = "^([0-9]{1,3}\.){3}([0-9]{1,3}){1}$"
	elseif curtype = "DBColName" then
		re.Pattern = "^([0-9]|[a-z]|[_]|\s)+$"
	end if
	
	re.IgnoreCase = true
	re.Global = true
	Validate = re.test(curstr)

end function



function unUDate(intTimeStamp)
	if len(intTimeStamp) > 0 then 
		unUDate = DateAdd("s", intTimeStamp, "01/01/1970 00:00:00") & iif(intTimeStamp mod 86400 = 0, " 12:00:00 AM", "")
	else
		unUDate = ""
	end if
end function


function UDate(curDate)

	if len(curDate) > 0 then 
		UDate = DateDiff("s", "01/01/1970 00:00:00", curDate)
	else
		UDate = ""
	end if
end function



function PrintPageNumbers(numofpages, pagevars, curpage)
	dim i, tmp
	
	if numofpages > 0 then
		numofpages = (numofpages/numperpage) + iif(numofpages mod numperpage <> 0, 1, 0)
		tmp = "Page "
		for i=1 to numofpages
			tmp = tmp & "<a style=""color: black;"" href=""" & request.servervariables("SCRIPT_NAME") & "?pagenum=" & i & pagevars & """>" & iif(i = curpage, "<b>" & i & "</b>", i) & "</a>&nbsp;"
		next
	else
		tmp = ""
	end if
	
	PrintPageNumbers = tmp
end function


function OptionListDataTypes(Highlight)

	optionlist = ""
	
	optionlist = optionlist & "<option value="""" " & iif(Highlight = "", "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=""int"" " & iif(Highlight = "int", "SELECTED", "") & ">Integer</option>" & vbcrlf
	optionlist = optionlist & "<option value="""" " & iif(Highlight = "varchar(256)", "SELECTED", "") & ">Text</option>" & vbcrlf

	OptionListDataTypes = optionlist


end function


function getdevicename(curipaddress)
	dim db, r
	set db = CreateObject("ADODB.Connection")
	call getdbvars()
	db.open session("dsn"), session("dbusername"), session("dbpassword")

	set r = db.execute("select description from Destinations where ip = '" & curipaddress & "'")
	
	if not r.eof then
		getdevicename = r("description")
	else
		getdevicename = ""
	end if
	
	db.close

end function


function determinesortby(curcol, cursortby)

	if (instr(1, cstr(cursortby), " desc") > 0) or (cstr(curcol) <> cstr(cursortby)) then
		determinesortby = curcol
	else
		determinesortby = curcol & " desc"
	end if
		
end function

dim pwin
pwin = "password"

function benc( ins )

	dim dlen
	dlen = len(ins)
	const alpha = ")x-{fJ|M2mqwO( /+tS:.<*WGoF&%`7Uc$]=Hhj;D^_0}5b3NQ[l1LsAX8d>4g\y"
	const padch = "@"
	const padch2 = "~"
	dim out, n
	out = ""

	for n = 1 to dlen step 3
		dim triple, quad

		'Create one long from this 3 bytes.
		triple = &H10000 * asc(Mid(ins, n, 1)) + _
		  &H100 * retch(Mid(ins, n + 1, 1)) + retch(Mid(ins, n + 2, 1))

		'Oct splits the long To 8 groups with 3 bits
		triple = Oct(triple)

		'Add leading zeros
		triple = String(8 - Len(triple), "0") & triple

		'Convert To base64
		quad = Mid(alpha, CLng("&o" & Mid(triple, 1, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 3, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 5, 2)) + 1, 1) + _
		  Mid(alpha, CLng("&o" & Mid(triple, 7, 2)) + 1, 1)

		'Add the part To OutPut string
		out = out + quad

	next
	
	select case dlen mod 3
		case 0: '0 bit final - add padchs for encrypt
		  out = padch + Left(out, Len(out) - 2)  + padch2
		case 1: '8 bit final - split padchs for encrypt
		  out = padch2 +Left(out, Len(out) - 2)  + padch
		case 2: '16 bit final - prepend padchs for encrypt
		  out = padch + Left(out, Len(out) - 1) 
	end select
	benc = StrReverse(out)
end function

function retch( inch )
	if inch = "" then 
		retch = 0 
	else 
		retch = asc(inch)
	end if
end function
%>