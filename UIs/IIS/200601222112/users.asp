<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
Call ConnectToDBs()

if request.form("login") = "LOGIN" then


	set r = executeSQL("select username, admin, displayname from users where username = '" & fixstring(request.form("username")) & "' and pwd = '" & benc(request.form("password")) & "'", 0)
	
	if not r.eof then
		session("username") = r("username")
		session("admin") = iif(r("admin") <> 0, 1, 0)
		session("displayname") = r("displayname")
	else
		errormsg = "Login failed.  Please contact your administrator.<br>"
	end if
	
end if


if request.form("logout") = "LOG OUT" then

		session("username") = ""
		session("admin") = ""
		session("displayname") = ""
		
		%>
		<script language="JavaScript">
		 parent.location.replace("default.asp");
		</script>
		<%
end if

if len(request.form("acctadmin")) > 0 then
	call validuser()
	errormsg = ""
	
	if len(request.form("update")) > 0 then 
		
		if len(request.form("displayname")) < 1 then 
			errormsg = errormsg & "Display Name is a required field.<br>"
		end if
				
		if len(request.form("newpwd")) > 0 or len(request.form("confirmpwd")) > 0 or len(request.form("currentpwd")) > 0 then 
		
			if len(request.form("currentpwd")) > 0 then
				set r = executeSQL("select username, admin from users where username = '" & fixstring(session("username")) & "' and pwd = '" & benc(request.form("currentpwd")) & "'", 0)

				if r.eof then
					errormsg = errormsg & "Current Password is incorrect.<br>"
				end if
			end if

			if len(request.form("newpwd")) < pwdlenmin or len(request.form("newpwd")) > pwdlenmax then 
				errormsg = errormsg & "New Password must be between " & pwdlenmin & " and " & pwdlenmax & " characters.<br>"
			end if
		
			if request.form("newpwd") <> request.form("confirmpwd") then
				errormsg = errormsg & "Confirm Password did not match.<br>"
			end if
		end if
		
		if len(errormsg) < 1 then 
		
			if len(request.form("newpwd")) > 0 then
				setclause = ", pwd = '" & benc(request.form("newpwd")) & "'"
			else
				setclause = ""
			end if
			
			call executeSQL("update users set displayname = '" & fixstring(request.form("displayname")) & "'" & setclause & " where username = '" & session("username") & "'", 1)
		
		end if
	
	end if
	
	response.write "<center><h2>Account Administration</h2></center>"
	response.write "<font color=red>" & errormsg & "</font>"
	set r =  executeSQL("select username, displayname from users where username = '" & fixstring(session("username")) & "'", 0)
	
	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=acctadmin value="acctadmin">
	<table align=center>
		<tr>
			<td><b>Display Name: </b></td>
			<td><input type=text name=displayname size=30 value="<%=r("displayname")%>"></td>
		</tr>
		<tr>
			<td><b>Current Password: </b></td>
			<td><input type=password name=currentpwd size=30 value=""></td>
		</tr>
		<tr>
			<td><b>New Password: </b></td>
			<td><input type=password name=newpwd size=30 value=""></td>
		</tr>
		<tr>
			<td><b>Confirm New Password: </b></td>
			<td><input type=password name=confirmpwd size=30 value=""></td>
		</tr>	
		<tr>
			<td></td>
			<td><input type=submit name=update value="Update My Account"> &nbsp;<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>">Return</a></td>
		</tr>
	</table>
	</form>
	<%

elseif len(request.form("acctadminall")) > 0 and len(request.form("return")) < 1 then
	call validuser()

	deletepos = instr(1, request.form, "delete_acct")
	resetpos = instr(1, request.form, "reset_acct")
	errormsg = ""
	
	if deletepos > 0 then  

		deletepos = deletepos + 11 'move to the end of the variable name
		deleteuser = mid(request.form, deletepos, instr(deletepos, request.form, "=") - deletepos) 'find the username
		
		strsql ="delete from users where username = '" & trim(deleteuser) & "'"
		
		call executeSQL(strsql, 1)
	
	elseif resetpos > 0 then
	
		resetpos = resetpos + 10 'move to the end of the variable name
		resetuser = mid(request.form, resetpos, instr(resetpos, request.form, "=") - resetpos) 'find the username
			
		if len(request.form("pwd-" & resetuser)) < pwdlenmin or len(request.form("pwd-" & resetuser)) > pwdlenmax then 
			errormsg = errormsg & "The new password for " & resetuser & " must be between " & pwdlenmin & " and " & pwdlenmax & " characters.<br>"
		end if
		
		if len(errormsg) < 1 then
			strsql = "update users set pwd = '" & benc(request.form("pwd-" & resetuser)) & "' where username = '" & resetuser & "'"
			
			call executeSQL(strsql, 1)
			response.write "<h3>The password for " & resetuser & " has been set.</h3>"
		end if
		
	elseif len(request.form("add_user")) > 0 then

		if len(request.form("displaynameNEW")) < 1 then 
			errormsg = errormsg & "Display Name is a required field.<br>"
		end if

		if len(request.form("usernameNEW")) < 1 then 
			errormsg = errormsg & "Username is a required field.<br>"
		else
			set r = executeSQL("select username from users where username = '" & request.form("usernameNEW") & "'", 0)
			
			if not r.eof then
				errormsg = errormsg & "This username already exists.<br>"
			end if
			
			if request.form("usernameNEW") = "sys" then
				errormsg = errormsg & "Username cannot be ""sys"".<br>"
			end if
		end if

		if len(request.form("pwdNEW")) < pwdlenmin or len(request.form("pwdNEW")) > pwdlenmax then 
			errormsg = errormsg & "New Password must be between " & pwdlenmin & " and " & pwdlenmax & " characters.<br>"
		end if
		
		if len(errormsg) < 1 then
		
			strsql = "insert into users (username, displayname, admin, pwd) values ('" & request.form("usernameNEW") & "', '" & fixstring(request.form("displaynameNEW")) & "', " & iif(request.form("adminNEW") = "on", 1, 0) & ", '" & benc(request.form("pwdNEW")) & "')"
			
			call executeSQL(strsql, 1)
		
		end if

	elseif len(request.form("update")) > 0 then

		idarray = split(request.form("ids"), ",")
		
		for i=0 to ubound(idarray) -1
			
			if len(request.form("displayname-" & idarray(i))) < 1 then 
				errormsg = errormsg & "Line " & i + 1 & " - Display Name is a required field.<br>"
			end if
		next
		
		if len(errormsg) < 1 then
			for i=0 to ubound(idarray) -1
				strsql = "update users set displayname = '" & fixstring(request.form("displayname-" & idarray(i))) & "', admin = " & iif(request.form("admin-" & idarray(i)) = "on", 1, 0) & " where username = '" & idarray(i) & "'"
				
				call executeSQL(strsql, 1)
				
				if session("username") = idarray(i) then
					session("admin") = iif(request.form("admin-" & idarray(i)) = "on", 1, 0)
					session("displayname") = request.form("displayname-" & idarray(i))
				end if
			next
		end if
		
	end if
	
	if session("admin") <> 1 then
		response.redirect request.servervariables("SCRIPT_NAME")
	end if
		
	strsql = "select username, admin, displayname from users order by displayname"
	
	set r = executeSQL(strsql, 0)
	
	%>
	<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
	<input type=hidden name=acctadminall value=acctadminall>
	
	<table border=0 cellspacing=0 cellpadding=2 align=center>
		<tr valign=top>
			<td colspan=3><h2><nobr><%=PageTitleIndent%>Account Administration - All</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
			<td colspan=2 align=right>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				<input type=submit name="return" value="Return">
				</nobr>
			</td>
		</tr>
		<tr class="tableheader">
				<th class="tableheadercell">Display Name</th>
				<th class="tableheadercell">Username</th>
				<th class="tableheadercell">Admin Rights</th>
				<th class="tableheadercell">Password</th>
				<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
	
		
		<!--Begin Add New User Account-->
		
		<tr class="tablerowAddNew" valign=center>
			
			<td class="tablecell" align=center><input type=text name=displaynameNEW size=20 value="<%=iif(len(errormsg) > 0, request.form("displaynameNEW"), "")%>"></td>
			
			<td class="tablecell" align=center><input type=text name=usernameNEW size=10 value="<%=iif(len(errormsg) > 0, request.form("usernameNEW"), "")%>"></td>
			
			<td class="tablecell" align=center><input type=checkbox name=adminNEW <%=iif(len(errormsg) > 0 and request.form("adminNEW") = "on", "CHECKED", "")%>></td>
			
			<td class="tablecell" align=left><input type=password name=pwdNEW size=10 value="<%=iif(len(errormsg) > 0, request.form("pwdNEW"), "")%>"></td>
			
			<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><input type=submit name="add_user" value="Add Account" onclick="return confirm_on_click('add a new account?  All other changes will be lost.')"></td>
		</tr>
		
		<!--End Add New User Account-->
	<%
	
	ids = ""	
	i = 0
	
	do while not r.eof
		curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
		
		if len(errormsg) > 0 then
			if request.form("admin-" & r("username")) = "on" then 
				adminrights = 1
			else
				adminrights = 0
			end if
		else
			if r("admin") = 0 then
				adminrights = 0
			else
				adminrights = 1 
			end if
		end if
		
		%>
		<tr class="<%=curclass%>" valign=center>
			
			<td class="tablecell" align=center><input type=text name="<%="displayname-" & r("username")%>" size=20 value="<%=iif(len(errormsg) > 0, request.form("displayname-" & r("username")), r("displayname"))%>"></td>
			
			<td class="tablecell" align=center><%=r("username")%></td>
			
			<td class="tablecell" align=center>
			<% if session("username") <> r("username") then%>
				<input type=checkbox name="<%="admin-" & r("username")%>" <%=iif(adminrights = 1, "CHECKED", "")%>>
			<% else %>
					Yes<input type=hidden name="<%="admin-" & r("username")%>" value="on">
			<% end if %>
			</td>			
			
			<td class="tablecell"><nobr><input type=password name="<%="pwd-" & r("username")%>" size=10 value="">&nbsp;<input type=submit name="<%="reset_acct" & r("username")%>" value="Set" onclick="return confirm_on_click('reset password for ' + '<%=r("username")%>' + '?  All other changes will be lost.')"></nobr></td>			
			
			<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<% if session("username") <> r("username") then%>
				<input type=submit name="<%="delete_acct" & r("username")%>" value="Delete Account" onclick="return confirm_on_click('delete the ' + '<%=r("username")%>' + ' account?  All other changes will be lost.')">
			<% end if %>
			</td>
		</tr>
		<%
		i = i + 1 
		ids = ids & r("username") & ","
		r.movenext
	loop

	%>
	<tr><td class="tablefooter" colspan=5>&nbsp;</td></tr>
		<tr>
			<td colspan=3>&nbsp;</td>
			<td align=right colspan=2>
				<nobr>
				<input type=submit name=refresh value="Refresh" onclick="return confirm_on_click('refresh?');">
				<input type=reset value="Reset" onclick="return confirm_on_click('reset?');">&nbsp;
				<input type=submit name=update value="Update" onclick="return confirm_on_click('update?');">
				<input type=submit name="return" value="Return">
				</nobr>
			</td>
	</tr>
	<input type=hidden name=ids value="<%=ids%>">
	</form>
	<%
else
	response.write "<center><font color=red>" & errormsg & "</font></center>"
	
	%><form name=userlogin method=post action=<%=request.servervariables("SCRIPT_NAME")%>><%
	
	if len(session("username")) > 0 then
		%>
		<br><br><br>
		<center>
			<h3><%=session("displayname")%> is currently logged in.</h3><br>
			<input type=submit name=logout value="LOG OUT">
			<br><br>
			<input type=submit name=acctadmin value="My Account">
			<% if session("admin") = 1 then %>
				<br><br>
				<input type=submit name=acctadminall value="All Accounts">
			<% end if %>

		</center>
		<%
	else
		%>	
		<br><br><br>
		<center>
			<h3>Please Login</h3>
		</center>

		<table align=center>
			<tr>
				<td><b>Username: </b></td>
				<td><input type=text name=username size=20 value="<%=request.form("username")%>"></td>
			</tr>
			<tr>
				<td><b>Password: </b></td>
				<td><input type=password name=password size=20 value=""></td>
			</tr>
			<tr>
				<td></td>
				<td><input type=submit name=login value="LOGIN"></td>
			</tr>

		</table>
		<script language="JavaScript">
			document.userlogin.username.focus();
		</script>

	<%
	end if

	response.write "</form>"
end if

call CloseDBs()
%>



</body>
</html>