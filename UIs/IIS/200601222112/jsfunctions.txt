<script type="text/javascript">

/* The SMK_KeyPress function (SMK = Select Match Keystrokes) provides keystroke matching
   for SELECT controls. The SELECT element declaration should direct the onkeypress event
   to this function, and declare two expandos: smk_keystrokes and smk_lastpresstime.
*/
function SMK_KeyPress() {
    var sndr = window.event.srcElement;
    var key = window.event.keyCode;
    var char = String.fromCharCode(key);
    var sysdate = new Date();
    // if the last key press was more than 2 seconds ago, reset the keystrokes
    if(sndr.smk_lastpresstime=="" || sysdate.getTime()-sndr.smk_lastpresstime>2000) {
        sndr.smk_keystrokes = "";
    }
    sndr.smk_lastpresstime = sysdate.getTime();
    // set up a regular expression for comparing with list box entries
    var re = new RegExp("^" + sndr.smk_keystrokes + char, "i");        // "i" -> ignoreCase
    // check each list box item for a match
    for(var i=0; i<sndr.options.length; i++) {
        if(re.test(sndr.options[i].text)) {
            sndr.options[i].selected=true;
            sndr.smk_keystrokes += char;
            break;
        }
    }
    // sink the keypress, ie don't pass it on to Windows or anything else
    window.event.returnValue = false;
}

function confirm_on_click(eventmsg) {	
	return confirm("Are you sure you want to " + eventmsg);
}

function verify_new_channel() {	
	var curfield = document.getElementById('New');
	
	if ((isNaN(curfield.value)) || (curfield.value.length < 1)) {
		alert('The Omnibus Channel Number is not a numeric value.');
		returnval = ((!isNaN(curfield.value)) && (curfield.value.length > 0));
	}
	else
	{
		returnval = confirm_on_click('add a new channel?  All other changes will be lost.');
	};
	
	return returnval;
}

function verify_new_ext() {	
	var curfield = document.getElementById('New');
	
	if ((curfield.value.length < 1) || (curfield.value.length > 3)) {
		alert('The file extension is not a valid length');
		returnval = !((curfield.value.length < 1) || (curfield.value.length > 3)) ;
	}
	else
	{
		returnval = confirm_on_click('add a new file extension?  All other changes will be lost.');
	};
	
	return returnval;
}


function SetDestDefaults() {
	var typeNEW = document.getElementById('typeNEW');
	var keyerlayeropNEW = document.getElementById('keyerlayeropNEW');	
	var keyerlayerNEW = document.getElementById('keyerlayerNEW');
	
	

	if (typeNEW.value.length > 0) {
	
		if (typeNEW.selectedIndex == 1) {  //Imagestore
			keyerlayeropNEW.selectedIndex = 1;  //value is <=
		}
		else 
		{
			if (typeNEW.selectedIndex == 2) {  //Intuition
				keyerlayeropNEW.selectedIndex = 2; //value is >=
			};
		};
	
		
		if (typeNEW.selectedIndex == 1) {  //Imagestore
			keyerlayerNEW.value = 2;
			}
		else 
		{
			if (typeNEW.selectedIndex == 2) { //Intuition
				keyerlayerNEW.value = 3;	
			};
		};
		
	};
			
}

function Right(str, n)
{
	if (n <= 0)     // Invalid bound, return blank string
	   return "";
	else if (n > String(str).length)   // Invalid bound, return
	   return str;                     // entire string
	else { // Valid bound, return appropriate substring
	   var iLen = String(str).length;
	   return String(str).substring(iLen, iLen - n);
	}
}

function BuildDestDesc() {

	var descNEW = document.getElementById('descNEW');
	var typeNEW = document.getElementById('typeNEW');
	var channelidNEW = document.getElementById('channelidNEW');

	if (descNEW.value.length < 1) {
		if ((typeNEW.value > 0) && (channelidNEW.value > 0)) {
			var channeltext = Right(channelidNEW.options[channelidNEW.options.selectedIndex].text, (channelidNEW.options[channelidNEW.options.selectedIndex].text.length - channelidNEW.options[channelidNEW.options.selectedIndex].text.indexOf('-') - 1));
						
			descNEW.value = channeltext + " " + typeNEW.options[typeNEW.options.selectedIndex].text;

			
		}
	}

}  



function DestTypeFunctionCalls() {

	BuildDestDesc();
	SetDestDefaults();

}


</script>   