<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<%call validuser() %>
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
Call ConnectToDBs()

pagenum = cint(iif(len(request.form("pagenum")) > 0, request.form("pagenum"), iif(len(request.querystring("pagenum")) > 0, request.querystring("pagenum"), 1)))

devicetable = iif(len(request.querystring("devicetable")) > 0, request.querystring("devicetable"), request.form("devicetable"))

sortby = iif(len(request.querystring("sortby")) > 0, request.querystring("sortby"), "devicetable.filename")

whereclause = " WHERE 1=1 "

'whereclause = whereclause & " and devicetable.filename like '%e%' "

strsql = "select top " & pagenum * numperpage & " devicetable.filename, devicetable.sys_last_used, devicetable.sys_times_used, devicetable.expires_after, devicetable.transfer_date, metadata.filename as metadataexists, (select count(devicetable.filename) as totalrecords from [" & devicetable & "] as devicetable " & whereclause & ") as totalrecords from [" & devicetable & "] as devicetable left join MetaData on devicetable.filename = MetaData.filename" & whereclause & "order by " & sortby

'response.write strsql

set r = executeSQL(strsql, 0)

dim filearray

if not r.eof then
	totalrecords = r("totalrecords")
	filearray = r.getrows()	
else
	totalrecords = 0	
	filearray = nullarray
end if

devicename = getdevicename(right(devicetable, len(devicetable) - 7))
%>
<table border=0 cellspacing=0 cellpadding=2 width=100%>
	<thead>
		<tr valign=top>
			<td colspan=6><h2><nobr><%=PageTitleIndent%>Files on <%=right(devicetable, len(devicetable) - 7) & iif(len(devicename) > 0, " - " & devicename, "")%></nobr></h2></td>
		</tr>
		<tr>
			<td colspan=6><%=PrintPageNumbers(totalrecords, "&devicetable=" & devicetable & "&sortby=" & sortby, pagenum)%></td>
		</tr>
		<tr class="tableheader">
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.filename", sortby)%>" title="Sort By File Name">File Name</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.sys_last_used", sortby)%>" title="Sort By Last Used">Last Used</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.sys_times_used", sortby)%>" title="Sort By Number of Uses">Number of Uses</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.expires_after", sortby)%>" title="Sort By Expiration Date">Expiration Date</a>
			</th>
			<th class="tableheadercell">
				<a style="color: black;" href="<%=request.servervariables("SCRIPT_NAME")%>?pagenum=1&devicetable=<%=devicetable%>&sortby=<%=determinesortby("devicetable.transfer_date", sortby)%>" title="Sort By Transfer Date">Transfer Date</a>
			</th>			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">Details</th>
		</tr>
	</thead>
	
	<tbody>
<%

pagestart = ((pagenum - 1) * numperpage)
pageend = iif(ubound(filearray, 2) > (pagenum * numperpage), (pagenum * numperpage), ubound(filearray, 2))

for i=pagestart to pageend

	
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell"><%=filearray(0,i)%></td>
		<td class="tablecell" align=right><%=unUdate(filearray(1,i))%></td>
		<td class="tablecell" align=center><%=filearray(2,i)%></td>
		<td class="tablecell" align=right>&nbsp;<%=unUdate(filearray(3,i))%></td>
		<td class="tablecell" align=right>&nbsp;<%=unUdate(filearray(4,i))%></td>
		<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(len(filearray(5,i)) > 0, "<a style=""color: black;"" href=""metadata.asp?filename=" & filearray(0,i) & " "">View</a>", "&nbsp;")%></td>
	</tr>
	<%

next

%>
	<tr><td class="tablefooter" colspan=7>&nbsp;</td></tr>
</table>
<%

call CloseDBs()
%>
</body>
</html>