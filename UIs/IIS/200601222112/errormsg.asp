<%response.buffer = true%>
<html>
<body>
<%

'error messages
select case request.querystring("errorcode")
	case 0
		errormsg = "Everything is fine."
	case 1
		errormsg = "ERROR CODE 1 - CSF File cannot be found."
	case 2
		errormsg = "ERROR CODE 2 - Primary database information cannot be found in the CSF file.  User does not want to use Backup database."	
	case 3
		errormsg = "ERROR CODE 3 - There is no DSN information available for the primary or the backup database."
	case 4
		errormsg = "ERROR CODE 4 - A connection could not be established with the primary database.  User does not want to use Backup database."
	case 5
		errormsg = "ERROR CODE 5 - A connection could not be established with the primary database.  There is no DSN information available for the backup database."
	case 6
		errormsg = "ERROR CODE 6 - A connection to the backup database has failed and the user did not want to just use the primary database."
	case 7
		errormsg = "ERROR CODE 7 - A connection could not be made to the primary database nor the backup database."
	case 8
		errormsg = "ERROR CODE 8 - The SQL statement could not be executed using the primary database."
	case 9
		errormsg = "ERROR CODE 9 - The SQL statement could not be executed using the backup database."
	case 10 
		errormsg = "ERROR CODE 10 - A connection could not be establised with the backup database.  The use of a backup database has been required in the CSF file."
	case 11
		errormsg = "ERROR CODE 11 - A connection could not be established with the primary database."
	case else
		errormsg = "Error unknown."
end select

response.write "<center><h2>" & errormsg & " - " & request.querystring("desc") & "</h2></center>"

%>
</body>
</html>