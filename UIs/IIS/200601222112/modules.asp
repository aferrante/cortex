<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
Call ConnectToDBs()

pos = instr(1, request.form, "setasdefault")

if pos > 0 then  

	pos = pos + 12 'move to the end of the variable name
	pos = mid(request.form, pos, instr(pos, request.form, "=") - pos) 'find the menu id

	strsql = "update Users set default_module = " & pos & " where username = '" & session("username") & "'"
	'response.write strsql
	call executeSQL(strsql, 1)

end if

set r = executeSQL("select * from Menu left join (select * from Users where Users.username = '" & session("username") & "') as Users on menu.id = Users.default_module where id > 0 order by id", 0)

%>
<form method=post action=<%=request.servervariables("SCRIPT_NAME")%>>
<table align=center border=0 cellspacing=0 cellpadding=2>

	<tr valign=top>
		<td colspan=4><h2><nobr><%=PageTitleIndent%>Modules</nobr></h2><%=iif(len(errormsg) > 0, "<font color=red>ERROR:<br>" & errormsg & "</font></br>", "")%></td>
	</tr>
	<tr>
		<th class="tableheadercell">Installed Modules</th>
		<th class="tableheadercell">Available Interfaces</th>
		<th class="tableheadercell">Default Interface</th>
		<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">&nbsp;</th>
	</tr>
<%

i = -1

do while not r.eof
	
	if r("indent") = 0 then
		i = i + 1
	end if
	
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell" <%=iif(r("indent") <> 0, "style=""border-top: none;""", "")%>><%=iif(r("indent")=0, r("label"), "&nbsp;")%></td>
		<td class="tablecell"><a style="color: black; text-decoration: none;" href="<%=r("id")%>"><%=r("description")%></a></td>
		<td class="tablecell" align=center><%=iif(r("default_module") = r("id"), "Selected", "&nbsp;")%></td>
		<td align=center class="tablecell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><nobr>&nbsp;
			<input type=submit name="<%="setasdefault" & r("id")%>" value="Set As Default"  onclick="return confirm_on_click('update your default module?');">
			&nbsp;</nobr>
		</td>
	</tr>
	<%
	
r.movenext
loop
%>
<tr><td class="tablefooter" colspan=4>&nbsp;</td></tr>
</table>
</form>
</body>
</html>
