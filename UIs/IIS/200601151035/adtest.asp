<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Omnibus Colossus Adaptor Tester</title>
<style type=text/css>
	#.Largefont {font-family:Tahoma; color:#595959; font-size:12pt; text-decoration:none; font-weight:bold;}
	#.Medfont  {font-family:Tahoma; color:#595959; font-size:9pt; text-decoration:none; letter-spacing:1pt; font-weight:bold; line-height:12pt;}
	#.Smfont  {font-family:Tahoma; color:#595959; font-size:7pt; text-decoration:none; letter-spacing:1pt; font-weight:bold;}
	#.Link  {font-family:Tahoma; color:#595959; font-size:8pt; letter-spacing:1pt; font-weight:bold; line-height:12pt;}
</style>




</head>

<body marginheight="0" marginwidth="0" bottommargin="0" rightmargin="0" topmargin="0" leftmargin="0">

<table width=100% height=100%>
<tr><td align="center" valign="middle">
Omnibus Colossus Adaptor Tester
<P>

<%
dim dur
dur = 6333

dim framerate
framerate = 29.97 'NTSC drop frame

function MStoHMSF(milliseconds, basis)

	dim duration, part
	duration = ""
	part = formatnumber(int(milliseconds/3600000), 0, -1, 0, 0) 
	if len(part)<2 then
		duration = duration + "0"
	end if
	duration = duration + part + ":"
	
	part = formatnumber(int(milliseconds/60000) mod 60, 0, -1, 0, 0)
	if len(part)<2 then
		duration = duration + "0"
	end if
	duration = duration + part + ":"
	part = formatnumber((int(milliseconds/1000) mod 60)mod 60, 0, -1, 0, 0)
	if len(part)<2 then
		duration = duration + "0"
	end if
	duration = duration + part + "."

	select case int(basis) 'pal=25, ntsc = 30, ndf = 29
	case 25: 'PAL	(25 fps)
		part = formatnumber(int((milliseconds mod 1000)/40),0, -1, 0, 0)  '(int)(1000.0/25.0) = 40
	case 30:' NTSC no drop frame (30 fps)
		part = formatnumber(int((milliseconds mod 1000)/33),0, -1, 0, 0)  '(int)(1000.0/30.0)  = (int)(33.333333) = 33
	case else: 'default to NTSC drop frame   (29.97 fps)
		part = formatnumber(int((milliseconds mod 1000)/33),0, -1, 0, 0)  '(int)(1000.0/29.97) = (int)(33.366700) = 33

	end select
	if len(part)<2 then
		duration = duration + "0"
	end if
	duration = duration + part
	
	MStoHMSF = duration
end function


function HMSFtoMS(dur, basis)
	
	'12:45:78.90
	dim ms, hours, minutes, seconds, frames
	
	hours = 	cint(mid(dur, 1, 2))
	minutes = 	cint(mid(dur, 4, 2))
	seconds = 	cint(mid(dur, 7, 2))
	frames = 	cint(mid(dur, 10, 2))
	
	ms = int(basis + 0.1)
	if hours>23 or hours<0  or minutes>59 or minutes<0 or seconds>59 or seconds<0 or seconds>59 or frames<0 or frames >= ms then 
		ms = -1
	else
		ms = (hours*3600000+minutes*60000+seconds*1000+(int)(((frames)*1000.0)/basis)) ' parens for rounding 
  	end if
	HMSFtoMS = ms
end function


response.write("formatted milliseconds to HMSF: " + MStoHMSF(dur, framerate))

%><p><%

response.write("formatted HMSF to milliseconds: " & HMSFtoMS("01:40:15.15", framerate) )

curdur = "01:40:15.15"

response.write "<BR>" & mid(curdur, 1, 2) & "<BR>"
response.write mid(curdur, 4, 2) & "<BR>"
response.write mid(curdur, 7, 2) & "<BR>"
response.write mid(curdur, 10, 2) & "<BR>"
%>
</td></tr>
</table>

</body>
</html>
