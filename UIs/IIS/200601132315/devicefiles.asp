<%Response.Buffer=true%>
<html>
<head>
<!--#INCLUDE FILE="util.asp"-->
<!--#INCLUDE FILE="jsfunctions.txt"-->
<!--#INCLUDE FILE="contentpage.css"-->

</head>

<body>
<%
set db = CreateObject("ADODB.Connection")
call getdbvars()
db.open session("dsn"), session("dbusername"), session("dbpassword")


strsql = "select devicetable.filename, devicetable.sys_last_used, devicetable.sys_times_used, devicetable.expires_after, devicetable.transfer_date, metadata.filename as metadataexists from [" & request.querystring("devicetable") & "] as devicetable left join MetaData on devicetable.filename = MetaData.filename order by devicetable.filename"

set r = db.execute(strsql)
%>
<table border=0 cellspacing=0 cellpadding=2>
	<thead>
		<tr valign=top>
			<td colspan=3><h2><nobr><%=PageTitleIndent%>Device Files</nobr></h2></td>
		</tr>

		<tr class="tableheader">
			<th class="tableheadercell">File Name</th>
			<th class="tableheadercell">Last Used</th>
			<th class="tableheadercell">Number of Uses</th>
			<th class="tableheadercell">Expiration Date</th>
			<th class="tableheadercell">Transfer Date</th>			
			<th class="tableheadercell" style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;">Details</th>
		</tr>
	</thead>
	
	<tbody>
<%

i = 0

do while not r.eof

	
	curclass = iif(i mod 2 = 0, "tablerow1", "tablerow2")
	%>
	<tr class="<%=curclass%>" valign=center>
		<td class="tablecell"><%=r("filename")%></td>
		<td class="tablecell" align=right><%=r("sys_last_used")%></td>
		<td class="tablecell" align=center><%=r("sys_times_used")%></td>
		<td class="tablecell" align=right><%=r("expires_after")%></td>
		<td class="tablecell" align=right><%=r("transfer_date")%></td>
		<td class="tablecell" align=center style="border-right-style:solid; border-right-width: 1px;border-right-color:#000000;"><%=iif(len(r("metadataexists")) > 0, "<a style=""color: black;"" href=""metadata.asp?filename=" & r("filename") & " ""target=_blank>View</a>", "&nbsp;")%></td>
	</tr>
	<%
	i = i + 1
r.movenext
loop

%>
	<tr><td class="tablefooter" colspan=7>&nbsp;</td></tr>
</table>
<%

db.close
%>
</body>
</html>