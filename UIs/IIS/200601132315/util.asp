<%

'--------------------------------------------
'	Constants
'--------------------------------------------

dbpwdfile = "c:\cortex\direct.csf" 'Location of the file that contains the database password.


'--------------------------------------------
'	Variables
'--------------------------------------------
public p_channelarray
public nullarray(1,0)

nullarray(0,0) = NULL
nullarray(1,0) = NULL

'--------------------------------------------
'	Functions
'--------------------------------------------

Sub getdbvars()
	'Reads in the database password from a text file.
	'chr(13) Carriage return code
	
	if len(session("dsn")) < 1 then 
		dim fs, f, pos, connectstr, filevar, val

		Set fs=Server.CreateObject("Scripting.FileSystemObject")
		Set f=fs.OpenTextFile(dbpwdfile, 1)
		filecontents = (f.ReadAll)

		'Find DSN
		filevar = "PrimaryDSN"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		'connectstr = connectstr & mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		session("dsn") = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)

		'Find Username
		filevar = "PrimaryDBUser"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		'connectstr = connectstr & "," & val
		session("dbusername") = val

		'Find Password
		filevar = "PrimaryDBPassword"
		pos = instr(1, filecontents, filevar & "=") + len(filevar) + 1
		val = mid(filecontents, pos, instr(pos, filecontents, chr(13)) - pos)
		'connectstr = connectstr & "," & val
		session("dbpassword") = val

		f.Close
		Set f=Nothing
		Set fs=Nothing
	end if
	
end sub




function iif(cond, val1, val2)

	if cond then
		iif = val1
	else
		iif = val2
	end if

end function


function OptionListVideoStandards(Highlight, fromDB)

	dim optionlist, cHighlight

	if fromDB = 1 then
		cHighlight = cint(hex(Highlight)/10) * 10
	else
		cHighlight = Highlight
	end if
	
	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(cHighlight = 0 or len(cHighlight) < 1, "SELECTED", "") & ">NTSC Drop Frame (29.97 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=10 " & iif(cHighlight = 10, "SELECTED", "") & ">NTSC No Drop Frame/NDF (30 fps)</option>" & vbcrlf
	optionlist = optionlist & "<option value=20 " & iif(cHighlight = 20, "SELECTED", "") & ">PAL (25 fps)</option>" & vbcrlf

	OptionListVideoStandards = optionlist

end function


function LoadOptionListChannels()

	dim db, r
	set db = CreateObject("ADODB.Connection")
	call getdbvars()
	db.open session("dsn"), session("dbusername"), session("dbpassword")

	set r = db.execute("select id, description from Channels order by id")
	
	if not r.eof then
		p_channelarray = r.getrows()
	else
		p_channelarray = nullarray
	end if
	
	db.close

end function




function OptionListChannels(Highlight)

	dim i, optionlist
	
	optionlist = ""
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0 or len(Highlight) < 1, "SELECTED", "") & "></option>" & vblf
	
	if not isnull(p_channelarray(0,0)) then 
		for i = 0 to ubound(p_channelarray, 2)
			optionlist = optionlist & "<option value=" & p_channelarray(0,i) & " " & iif(highlight = p_channelarray(0,i), "SELECTED", "") & ">" & p_channelarray(0,i) & "-" & p_channelarray(1,i) & "</option>" & vblf
		next
	end if

	OptionListChannels = optionlist

end function


Function OptionListKeyerLayerOp(Highlight)

	optionlist = ""
	
	optionlist = optionlist & "<option value="""" " & iif(Highlight = "", "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=""<="" " & iif(Highlight = "<=", "SELECTED", "") & "><=</option>" & vbcrlf
	optionlist = optionlist & "<option value="">="" " & iif(Highlight = ">=", "SELECTED", "") & ">>=</option>" & vbcrlf
	optionlist = optionlist & "<option value=""=="" " & iif(Highlight = "==", "SELECTED", "") & ">=</option>" & vbcrlf

	OptionListKeyerLayerOp = optionlist

end function



Function OptionListDestTypes(Highlight)

	optionlist = ""
	
	optionlist = optionlist & "<option value=0 " & iif(Highlight = 0, "SELECTED", "") & "></option>" & vbcrlf
	optionlist = optionlist & "<option value=1 " & iif(Highlight = 1, "SELECTED", "") & ">Imagestore</option>" & vbcrlf
	optionlist = optionlist & "<option value=2 " & iif(Highlight = 2, "SELECTED", "") & ">Intuition</option>" & vbcrlf

	OptionListDestTypes = optionlist

end function


Function GetDestType(curtype)

	if len(curtype) < 1 then 
		GetDestType = ""
	else
		if curtype = 1 then
			GetDestType = "Imagestore"
		elseif curtype = 2 then
			GetDestType = "Intuition"
		else
			GetDestType = ""
		end if
	end if
end function



function PageTitleIndent()

	PageTitleIndent= IndentMe(5)

end function

function IndentMe(indentnum)

	dim i, tmp

	tmp = ""
	for i=1 to indentnum
		tmp = tmp & "&nbsp;&nbsp;&nbsp;&nbsp;"
	next

	IndentMe = tmp
end function

function fixstring(curstring)

	if len(curstring) > 0 then
		fixstring = replace(curstring, "'", "''")
	else
		fixstring = ""
	end if
end function


function Validate(curstr, curtype)

	dim re
	set re = new regexp
	
	if curtype = "IP" then 
		re.Pattern = "^([0-9]{1,3}\.){3}([0-9]{1,3}){1}$"
	end if
	
	re.IgnoreCase = true
	re.Global = true
	Validate = re.test(curstr)

end function

%>